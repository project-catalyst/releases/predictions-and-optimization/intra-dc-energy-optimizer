package ro.tuc.dsrl.geyser.execution.actionexecutors;


import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.geyser.execution.start.ActionExecution;

public class ChargeTesActionExecutor extends ActionExecutor {

//	@Override
//	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
//		System.out.println("execute charge tes");
//		double multiplierToPower = (HOUR_IN_MILLIS) / (action.getEnd().getTime() - action.getStart().getTime());
//		// power amount
//		double chargePowerAmount = action.getAmount() * multiplierToPower;
//
//		//we no longer put the factor for TES, because it is taken on account ONLY on DISPLAY
//		//results.setTesCharge(results.getTesCharge() + DCResourcesConstants.TES_CHARGE_RATE.value() * chargePowerAmount);
//		results.setTesCharge(results.getTesCharge() + chargePowerAmount);
//
//		return results;
//	}

	@Override
	public DatacenterStateDTO execute(OptimizationActionDTO action, DatacenterStateDTO results) {
		LOGGER.info("Execute : " + action);
		double multiplierToPower = (HOUR_IN_MILLIS) / (action.getEndTime() - action.getStartTime());
		// power amount
		double chargePowerAmount = action.getAmount() * multiplierToPower;

		//we no longer put the factor for TES, because it is taken on account ONLY on DISPLAY
		//results.setTesCharge(results.getTesCharge() + DCResourcesConstants.TES_CHARGE_RATE.value() * chargePowerAmount);

		//results.setTesCharge(results.getTesCharge() + chargePowerAmount);
		double tesCurrent = results.getTesCurrent();
		results.setTesCurrent(tesCurrent + chargePowerAmount/ ActionExecution.FIVE_MIN_SLOTS_PER_HOUR);

		return results;
	}


}
