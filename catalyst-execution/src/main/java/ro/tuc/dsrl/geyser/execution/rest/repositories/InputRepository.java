package ro.tuc.dsrl.geyser.execution.rest.repositories;

import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.geyser.execution.remoteconnection.CatalystDBApiURL;
import ro.tuc.dsrl.geyser.execution.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.geyser.execution.utility.ExecutionPropertiesLoader;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class InputRepository {
	private static final String DC_NAME = ExecutionPropertiesLoader.DATACENTER_NAME;
	public static DatacenterStateDTO getCurrentDCInput(LocalDateTime current) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("date", DateUtils.dateTimeToMillis(current));
		urlVariables.put("dataCenterName", DC_NAME);
		return RestAPIClient.getObject(CatalystDBApiURL.INPUT_CURRENT, DatacenterStateDTO.class, urlVariables);

	}

}
