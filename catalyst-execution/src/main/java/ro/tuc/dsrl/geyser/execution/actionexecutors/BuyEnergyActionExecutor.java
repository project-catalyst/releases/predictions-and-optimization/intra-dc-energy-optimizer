//package ro.tuc.dsrl.geyser.execution.actionexecutors;
//
//import ro.tuc.dsrl.catalyst.model.dto.BaseActionDTO;
//import ro.tuc.dsrl.catalyst.model.dto.TotalExecutionResultsDTO;
//import ro.tuc.dsrl.geyser.datamodel.api.db.entities.OptAction;
//import ro.tuc.dsrl.geyser.datamodel.api.db.entities.TotalExecutionResults;
//
//public class BuyEnergyActionExecutor extends ActionExecutor {
//
////	@Override
////	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
////		System.out.println("execute buy energy");
////
////		double multiplierToPower = (HOUR_IN_MILLIS)/(action.getEnd().getTime() - action.getStart().getTime());
////
////		//get the correct amount, according to time slice
////		double buyAmount = action.getAmount()*multiplierToPower;
////
////		results.setPowerFromSmartGrid(results.getPowerFromSmartGrid() + buyAmount);
////
////		return results;
////	}
//
//	@Override
//	public TotalExecutionResultsDTO execute(BaseActionDTO action, TotalExecutionResultsDTO results) {
//		System.out.println("execute buy energy");
//
//		double multiplierToPower = (HOUR_IN_MILLIS)/(action.getEnd() - action.getStart());
//
//		//get the correct amount, according to time slice
//		double buyAmount = action.getValue()*multiplierToPower;
//
//		results.setPowerFromSmartGrid(results.getPowerFromSmartGrid() + buyAmount);
//
//		return results;
//	}
//
//}
