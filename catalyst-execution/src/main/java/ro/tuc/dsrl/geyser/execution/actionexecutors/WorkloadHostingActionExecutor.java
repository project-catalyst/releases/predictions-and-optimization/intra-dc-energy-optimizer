package ro.tuc.dsrl.geyser.execution.actionexecutors;

import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;

public class WorkloadHostingActionExecutor extends ActionExecutor {
//    @Override
//    public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
//        return null;
//    }

    @Override
    public DatacenterStateDTO execute(OptimizationActionDTO action, DatacenterStateDTO results) {
        LOGGER.info("Execute : " + action);

        results.setHostWorkload(action.getAmount());
        return results;
    }
}
