package ro.tuc.dsrl.geyser.execution.rest.repositories;

import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.geyser.execution.remoteconnection.CatalystDBApiURL;
import ro.tuc.dsrl.geyser.execution.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.geyser.execution.utility.ExecutionPropertiesLoader;

import java.util.HashMap;
import java.util.Map;

public class ExecutionRepository {
	private static final String DC_NAME = ExecutionPropertiesLoader.DATACENTER_NAME;

	public static DatacenterStateDTO getLatestMonitoring(DateTime current, double confidence) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("date", current.getMillis());
		urlVariables.put("dataCenterName", DC_NAME);

		return RestAPIClient.getObject(CatalystDBApiURL.EXECUTION_LATEST, DatacenterStateDTO.class, urlVariables);

	}

	public static void saveTotalExecutionResults(DatacenterStateDTO results) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("dataCenterName", DC_NAME);
		RestAPIClient.postObject(CatalystDBApiURL.EXECUTION, results, Void.class, urlVariables);
	}
}
