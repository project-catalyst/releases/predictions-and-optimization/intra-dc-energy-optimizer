package ro.tuc.dsrl.geyser.execution.actionexecutors;

import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.geyser.execution.start.ActionExecution;

public class DischargeBatteryActionExecutor extends ActionExecutor {

//	@Override
//	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
//		System.out.println("execute discharge battery");
//		double multiplierToPower = (HOUR_IN_MILLIS) / (action.getEnd().getTime() - action.getStart().getTime());
//		// power amount
//		double dischargePowerAmount = action.getAmount() * multiplierToPower;
//		System.out.println("execute discharge battery "+ dischargePowerAmount);
//		results.setEsdDischarge(
//				results.getEsdDischarge() + dischargePowerAmount);
//
//		return results;
//	}

	@Override
	public DatacenterStateDTO execute(OptimizationActionDTO action, DatacenterStateDTO results) {
		LOGGER.info("Execute : " + action);
		double multiplierToPower = (HOUR_IN_MILLIS) / (action.getEndTime() - action.getStartTime());
		// power amount
		double dischargePowerAmount = action.getAmount() * multiplierToPower;
//		results.setEsdDischarge(
//				results.getEsdDischarge() + dischargePowerAmount);
//
		double esdCurrent = results.getEsdCurrent();
		results.setEsdCurrent(esdCurrent - dischargePowerAmount/ ActionExecution.FIVE_MIN_SLOTS_PER_HOUR);
		return results;
	}

}
