package ro.tuc.dsrl.geyser.execution.rest.repositories;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.geyser.execution.remoteconnection.CatalystDBApiURL;
import ro.tuc.dsrl.geyser.execution.remoteconnection.RestAPIClient;

import java.util.*;

public class ActionRepository {

	public static List<OptimizationActionDTO> getCurrentOptimizationActions(Date current, double confidenceLevel, String dataCenterName) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("currentDate", current.getTime());
		urlVariables.put("confidenceLevel", confidenceLevel);
		urlVariables.put("dataCenterName", dataCenterName);

		List<OptimizationActionDTO> toReturn = new ArrayList<>(Arrays.asList(
				RestAPIClient.getObject(CatalystDBApiURL.OPT_PLAN_CURRENT_ACTIONS, OptimizationActionDTO[].class, urlVariables)));


		return toReturn;
	}
}
