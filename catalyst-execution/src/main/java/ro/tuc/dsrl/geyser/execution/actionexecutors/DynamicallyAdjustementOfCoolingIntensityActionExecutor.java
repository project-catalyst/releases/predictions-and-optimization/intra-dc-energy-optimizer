package ro.tuc.dsrl.geyser.execution.actionexecutors;

import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;

public class DynamicallyAdjustementOfCoolingIntensityActionExecutor extends ActionExecutor {

//	@Override
//	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
//		System.out.println("execute dynamically adj");
//		double multiplierToPower = (HOUR_IN_MILLIS)/(action.getEnd().getTime() - action.getStart().getTime());
//		results.setFacilityPowerConsumption(action.getAmount()*multiplierToPower);
//
//		return results;
//	}

	@Override
	public DatacenterStateDTO execute(OptimizationActionDTO action, DatacenterStateDTO results) {
		LOGGER.info("Execute : " + action);
		double multiplierToPower = (HOUR_IN_MILLIS)/(action.getEndTime() - action.getStartTime());
		results.setFacilityPowerConsumption(action.getAmount()*multiplierToPower);

		return results;
	}


}
