package ro.tuc.dsrl.geyser.execution.actionexecutors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.geyser.datamodel.actions.*;
import ro.tuc.dsrl.geyser.execution.start.ActionExecution;

import java.util.HashMap;
import java.util.Map;

public abstract class ActionExecutor {

	protected static final Logger LOGGER = LoggerFactory.getLogger(ActionExecutor.class);

	protected static final int TIME_SLICE_PERIOD = 300000;
	protected static final int HOUR_IN_MILLIS = 3600000;

	public static Map<String, ActionExecutor> getExecutors() {
		Map<String, ActionExecutor> executors = new HashMap<String, ActionExecutor>();
		executors.put(ChargeBattery.ACTION_TYPE, new ChargeBatteryActionExecutor());
		executors.put(DischargeBattery.ACTION_TYPE, new DischargeBatteryActionExecutor());
		executors.put(ShiftDelayTolerantWorkload.ACTION_TYPE, new ShiftDelayTolerableActionExecutor());
		//executors.put(BuyEnergy.ACTION_TYPE, new BuyEnergyActionExecutor());
		//executors.put(SellEnergy.ACTION_TYPE, new SellEnergyActionExecutor());
		executors.put(ChargeTes.ACTION_TYPE, new ChargeTesActionExecutor());
		executors.put(DischargeTes.ACTION_TYPE, new DischargeTesActionExecutor());
		//executors.put(RunDieselGeneratorsForPeriodicMaintenance.ACTION_TYPE, new RunDieselGeneratorActionExecutor());
		executors.put(DynamicAdjustmentOfCoolingIntensity.ACTION_TYPE,
				new DynamicallyAdjustementOfCoolingIntensityActionExecutor());
//		executors.put(DynamicallyUsingNonElectricalCooling.ACTION_TYPE,
//				new DynamicallyUsingNonElectricCoolingActionExecutor());
		executors.put(WorkloadRelocation.ACTION_TYPE, new WorkloadRealocationActionExecutor());
		//executors.put(ThermalSelfHeat.ACTION_TYPE, new SelfThermalHeatActionExecutor());
		executors.put(WorkloadHosting.ACTION_TYPE, new WorkloadHostingActionExecutor());
		return executors;
	}
//
//	public abstract TotalExecutionResults execute(OptAction action, TotalExecutionResults results);

	public abstract DatacenterStateDTO execute(OptimizationActionDTO action, DatacenterStateDTO results);
}
