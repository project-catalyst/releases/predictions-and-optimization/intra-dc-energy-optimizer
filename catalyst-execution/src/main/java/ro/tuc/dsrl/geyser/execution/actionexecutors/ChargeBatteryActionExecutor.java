package ro.tuc.dsrl.geyser.execution.actionexecutors;

import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.geyser.execution.start.ActionExecution;

public class ChargeBatteryActionExecutor extends ActionExecutor {

//	@Override
//	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
//		System.out.println("execute charge battery");
//		double multiplierToPower = (HOUR_IN_MILLIS)/(action.getEnd().getTime() - action.getStart().getTime());
//		//power amount
//		double chargePowerAmount = action.getAmount()*multiplierToPower;
//
//		results.setEsdCharge(results.getEsdCharge() + chargePowerAmount);
//
//		return results;
//
//	}

	@Override
	public DatacenterStateDTO execute(OptimizationActionDTO action, DatacenterStateDTO results) {
		LOGGER.info("Execute : " + action);
		double multiplierToPower = (HOUR_IN_MILLIS)/(action.getEndTime() - action.getStartTime());
		//power amount
		double chargePowerAmount = action.getAmount()*multiplierToPower;

		//results.setEsdCharge(results.getEsdCharge() + chargePowerAmount);

		double esdCurrent = results.getEsdCurrent();
		results.setEsdCurrent(esdCurrent + chargePowerAmount / ActionExecution.FIVE_MIN_SLOTS_PER_HOUR);
		return results;
	}

}
