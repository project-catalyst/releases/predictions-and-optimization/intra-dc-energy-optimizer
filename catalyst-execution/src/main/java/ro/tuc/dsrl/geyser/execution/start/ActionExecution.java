package ro.tuc.dsrl.geyser.execution.start;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.geyser.execution.actionexecutors.ActionExecutor;
import ro.tuc.dsrl.geyser.execution.rest.repositories.ActionRepository;
import ro.tuc.dsrl.geyser.execution.rest.repositories.ExecutionRepository;
import ro.tuc.dsrl.geyser.execution.rest.repositories.InputRepository;
import ro.tuc.dsrl.geyser.execution.utility.ExecutionPropertiesLoader;

import java.util.List;
import java.util.Map;

public class ActionExecution {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActionExecution.class);
	public static final int FIVE_MIN_SLOTS_PER_HOUR = 12;

	// public static void main(String[] args) {
	// execute(Scenarios.REN.time().plusHours(1), 0.0);
	//
	// }

	public static void executeBetween(DateTime start,DateTime end, double minUPSConstraint, double confidenceLevel){
		
		for(DateTime current= start; current.isBefore(end); current = current.plusMinutes(5) ){
			execute(current, minUPSConstraint, confidenceLevel);
		}
		
	}

	public static void execute(DateTime current, double minUPSConstraint, double confidenceLevel) {
		// get actions from repository for date and confidence (all actions, DA,
		// ID, RT)

		String dataCenterName = ExecutionPropertiesLoader.DATACENTER_NAME;

		List<OptimizationActionDTO> currentActions = ActionRepository.getCurrentOptimizationActions(current.toDate(),
				confidenceLevel, dataCenterName);

		Map<String, ActionExecutor> executors = ActionExecutor.getExecutors();

		DatacenterStateDTO lastResults = getCurrentExecutionResults(current, confidenceLevel);

		// execute each action by its type
		for (OptimizationActionDTO action : currentActions) {

			String actionType = action.getType();

			ActionExecutor executor = executors.get(actionType);
			if (executor != null) {
				lastResults = executor.execute(action, lastResults);
			} else {
				LOGGER.info("No executor implemented for " + actionType);
			}
		}

		// TODO: check on GitHub if this part is really commented, and ask Claudia if it should be commented
		/*----------------------
		ADDITION: charges or discharges if possible to bring the consumption/production closer
		------------------------*/
		// TODO: the minUPSConstraint parameter is not passed :) look into and
		// modify
		// computeAdditionalBatteryAction(lastResults, minUPSConstraint);
		/*------------------
		DONE ADDITION
		--------------------*/

		// TODO: save new results to the database
		// save results into the database for this date
		lastResults.setItPowerConsumptionDt(lastResults.getItPowerConsumptionDt() + lastResults.getRealocEnergy());
		saveNewExecutionResults(lastResults);
	}

	private static DatacenterStateDTO getCurrentExecutionResults(DateTime current, double confidenceLevel) {

		// TODO: redirect TotalExecution and TotalPower get to the new DB Api
		DateTime lastFive = current.minusMinutes(5);

		DatacenterStateDTO results = ExecutionRepository.getLatestMonitoring(lastFive, confidenceLevel);
		DatacenterStateDTO inputDC = InputRepository.getCurrentDCInput(DateUtils.millisToUTCLocalDateTime(current.getMillis()));
	//	computeCurrentStorage(results);
		results.setItPowerConsumptionRt(inputDC.getItPowerConsumptionRt());
		results.setItPowerConsumptionDt(0.0);
//		results.setGreenGeneration(inputDC.getGreenGeneration());
//		results.setIncomingRealloc(inputDC.getIncomingRealloc());
		
//		results.setEsdCharge(0.0);
//		results.setEsdDischarge(0.0);
//		results.setTesCharge(0.0);
//		results.setTesDischarge(0.0);
		results.setPowerFromSmartGrid(0.0);
		results.setPowerToSmartGrid(0.0);
//		results.setBrownGeneration(0.0);
		results.setFacilityPowerConsumption(0.0);
		results.setRealocEnergy(0.0);
		results.setRecordTime(current.getMillis());

		return results;
	}

//	private static void computeCurrentStorage(TotalExecutionResultsDTO results) {
//		double lastAmountESD = results.getEsdCurrent();
//		// double chargeAmountESD = results.getEsdCharge() /
//		// DCResourcesConstants.BATTERY_CHARGE_RATE.value();
//		// double dischargeAmountESD = results.getEsdDischarge() /
//		// DCResourcesConstants.BATTERY_DISCHARGE_RATE.value();
//		double chargeAmountESD = results.getEsdCharge();
//		double dischargeAmountESD = results.getEsdDischarge();
//		chargeAmountESD = chargeAmountESD / FIVE_MIN_SLOTS_PER_HOUR;
//		dischargeAmountESD = dischargeAmountESD / FIVE_MIN_SLOTS_PER_HOUR;
//
//		double lastAmountTES = results.getTesCurrent();
//		// double chargeAmountTES = results.getTesCharge() /
//		// DCResourcesConstants.TES_CHARGE_RATE.value();
//		// double dischargeAmountTES = results.getTesDischarge() /
//		// DCResourcesConstants.TES_DISCHARGE_RATE.value();
//		// no longer divide with loss rate, as entry in db is no longer
//		// multiplied with loss rate
//		double chargeAmountTES = results.getTesCharge();
//		double dischargeAmountTES = results.getTesDischarge();
//		chargeAmountTES = chargeAmountTES / FIVE_MIN_SLOTS_PER_HOUR;
//		dischargeAmountTES = dischargeAmountTES / FIVE_MIN_SLOTS_PER_HOUR;
//
//		double currentAmountESD = lastAmountESD + chargeAmountESD - dischargeAmountESD;
//		double currentAmountTES = lastAmountTES + chargeAmountTES - dischargeAmountTES;
//
//		results.setEsdCurrent(currentAmountESD);
//		results.setTesCurrent(currentAmountTES);
//	}

//	private static void computeAdditionalBatteryAction(TotalExecutionResults results, double minUPSConstraint) {
//		// after executing everything, we should check if we are allowed to
//		// use battery to bring the consumption closer to production
//		double maxUPSConstraint = DCResourcesConstants.BATTERY_MAX.value();
//		double energyBudget = results.getItPowerConsumptionRt() + results.getItPowerConsumptionDt()
//				+ (results.getFacilityPowerConsumption()
//						- DCResourcesConstants.TES_CHARGE_RATE.value() * results.getTesCharge()
//						+ DCResourcesConstants.TES_DISCHARGE_RATE.value() * results.getTesDischarge())
//				+ DCResourcesConstants.BATTERY_CHARGE_RATE.value() * results.getEsdCharge()
//				+ DCResourcesConstants.TES_CHARGE_RATE.value() * results.getTesCharge() + results.getPowerToSmartGrid()
//				- results.getGreenGeneration()
//				- DCResourcesConstants.BATTERY_DISCHARGE_RATE.value() * results.getEsdDischarge()
//				- DCResourcesConstants.TES_DISCHARGE_RATE.value() * results.getTesDischarge()
//				- results.getPowerFromSmartGrid();
//		energyBudget /= FIVE_MIN_SLOTS_PER_HOUR; // bring to energy
//		double levelUPS = results.getEsdCurrent() - results.getEsdDischarge() / FIVE_MIN_SLOTS_PER_HOUR
//				+ results.getEsdCharge() / FIVE_MIN_SLOTS_PER_HOUR;
//
//		// DEBT => discharge if possible
//		if (energyBudget > 0 && levelUPS > minUPSConstraint) {
//			// double extraUps = (levelUPS - minUPSConstraint) /
//			// EnergyMeasurementUnit.KW5MIN.value();
//			// no longer multiplying with 12, to remain energy instead of power
//			double extraUps = levelUPS - minUPSConstraint;
//			extraUps *= DCResourcesConstants.BATTERY_DISCHARGE_RATE.value();
//
//			results.setEsdDischarge(results.getEsdDischarge() + Math.min(extraUps, energyBudget)
//					* FIVE_MIN_SLOTS_PER_HOUR / DCResourcesConstants.BATTERY_DISCHARGE_RATE.value());
//		}
//		// power consumption < contracted energy
//		// PROFIT => charge if possible
//		else if (energyBudget < 0 && levelUPS < maxUPSConstraint) {
//			// 1. try to re-charge batteries
//			// double freeUps = (maxUPSConstraint - levelUPS) /
//			// EnergyMeasurementUnit.KW5MIN.value();
//			// same as above
//			double freeUps = maxUPSConstraint - levelUPS;
//			freeUps *= DCResourcesConstants.BATTERY_CHARGE_RATE.value();
//
//			results.setEsdCharge(results.getEsdCharge() + Math.min(freeUps, -energyBudget) * FIVE_MIN_SLOTS_PER_HOUR
//					/ DCResourcesConstants.BATTERY_CHARGE_RATE.value());
//		}
//
//		double finalValue = results.getEsdCharge() - results.getEsdDischarge();
//
//		if (finalValue < 0) {
//			results.setEsdCharge(0.0);
//			results.setEsdDischarge(Math.abs(finalValue));
//		} else {
//			results.setEsdCharge(Math.abs(finalValue));
//			results.setEsdDischarge(0.0);
//		}
//
//	}

	private static void saveNewExecutionResults(DatacenterStateDTO results) {
		results.setId(null);
		ExecutionRepository.saveTotalExecutionResults(results);
	}

}
