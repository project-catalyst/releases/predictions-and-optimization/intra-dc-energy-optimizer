//package ro.tuc.dsrl.geyser.execution.actionexecutors;
//
//import ro.tuc.dsrl.catalyst.model.dto.BaseActionDTO;
//import ro.tuc.dsrl.catalyst.model.dto.TotalExecutionResultsDTO;
//import ro.tuc.dsrl.geyser.datamodel.api.db.entities.OptAction;
//import ro.tuc.dsrl.geyser.datamodel.api.db.entities.TotalExecutionResults;
//
//public class SellEnergyActionExecutor extends ActionExecutor{
//
////	@Override
////	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
////		System.out.println("execute sell energy");
////
////		double multiplierToPower = (HOUR_IN_MILLIS)/(action.getEnd().getTime() - action.getStart().getTime());
////
////		//get the correct amount, according to time slice
////		double sellAmount = action.getAmount() * multiplierToPower;
////
////		results.setPowerToSmartGrid(results.getPowerToSmartGrid()+sellAmount);
////
////		return results;
////	}
//
//	@Override
//	public TotalExecutionResultsDTO execute(BaseActionDTO action, TotalExecutionResultsDTO results) {
//		System.out.println("execute sell energy");
//
//		double multiplierToPower = (HOUR_IN_MILLIS)/(action.getEnd() - action.getStart());
//
//		//get the correct amount, according to time slice
//		double sellAmount = action.getValue() * multiplierToPower;
//
//		results.setPowerToSmartGrid(results.getPowerToSmartGrid()+sellAmount);
//
//		return results;
//	}
//
//
//}
