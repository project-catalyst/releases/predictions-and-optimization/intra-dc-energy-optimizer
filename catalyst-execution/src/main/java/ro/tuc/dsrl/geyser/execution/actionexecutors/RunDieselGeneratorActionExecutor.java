//package ro.tuc.dsrl.geyser.execution.actionexecutors;
//
//import ro.tuc.dsrl.catalyst.model.dto.BaseActionDTO;
//import ro.tuc.dsrl.catalyst.model.dto.TotalExecutionResultsDTO;
//import ro.tuc.dsrl.geyser.datamodel.api.db.entities.OptAction;
//import ro.tuc.dsrl.geyser.datamodel.api.db.entities.TotalExecutionResults;
//
//public class RunDieselGeneratorActionExecutor extends ActionExecutor {
//
//	@Override
//	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
//		System.out.println("execute run diesel");
//
//		results.setBrownGeneration(action.getAmount());
//		return results;
//	}
//
//	@Override
//	public TotalExecutionResultsDTO execute(BaseActionDTO action, TotalExecutionResultsDTO results) {
//		System.out.println("execute run diesel");
//
//
//		return results;
//	}
//
//
//}
