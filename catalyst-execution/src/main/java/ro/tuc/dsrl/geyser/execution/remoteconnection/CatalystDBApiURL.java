package ro.tuc.dsrl.geyser.execution.remoteconnection;

import ro.tuc.dsrl.geyser.execution.utility.ExecutionPropertiesLoader;

public enum CatalystDBApiURL implements APIURL {
    OPT_PLAN_CURRENT_ACTIONS("/action/active/{currentDate}/{confidenceLevel}"),
    EXECUTION_LATEST("/monitored-value/current/{date}/{dataCenterName}"),
    EXECUTION("/monitored-value/insert-snapshot/{dataCenterName}"),
    INPUT_CURRENT("/monitored-value/pre-opt/current/{date}/{dataCenterName}");

    private final String catalystDbApiHost = ExecutionPropertiesLoader.CATALYST_DB_API_HOST;
    private String value;

    CatalystDBApiURL(String value) {
        this.value = value;
    }


    @Override
    public String value() {
        return catalystDbApiHost + value;
    }
}
