package ro.tuc.dsrl.geyser.execution.actionexecutors;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.geyser.execution.rest.repositories.InputRepository;

import java.time.LocalDateTime;

public class WorkloadRealocationActionExecutor extends ActionExecutor {

//	@Override
//	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
////		Date current = results.getRecordTime();
////		TotalPowerREST inputDC = InputRepository.getCurrentDCInput(new DateTime(current));
////
////		double reallocEnergy = results.getRealocEnergy();
////		reallocEnergy += inputDC.getItPowerConsumptionDt()*action.getReallocationPercentage();
////		results.setRealocEnergy(reallocEnergy);
//
//		return results;
//	}

    @Override
    public DatacenterStateDTO execute(OptimizationActionDTO action, DatacenterStateDTO results) {

        LOGGER.info("Execute : " + action);

        Long currentMillis = results.getRecordTime();
        LocalDateTime currentDateTime = DateUtils.millisToUTCLocalDateTime(currentMillis);
        DatacenterStateDTO inputDC = InputRepository.getCurrentDCInput(currentDateTime);

        double reallocEnergy = results.getRealocEnergy();
        reallocEnergy += inputDC.getItPowerConsumptionDt() * action.getMovePercentage();
        results.setRealocEnergy(reallocEnergy);
        //System.out.println("Realoc: " + reallocEnergy);
        return results;

    }

}
