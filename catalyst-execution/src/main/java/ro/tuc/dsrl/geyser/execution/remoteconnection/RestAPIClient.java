package ro.tuc.dsrl.geyser.execution.remoteconnection;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * 
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: rest-client
 * @Since: Dec 17, 2014
 * @Description:
 *
 */
public final class RestAPIClient {

	private RestAPIClient() {
	}

	public static <T> T postObject(APIURL url, Object o, Class<T> clazz) {
		RestTemplate rt = new RestTemplate();
		rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		rt.getMessageConverters().add(new StringHttpMessageConverter());
		return rt.postForObject(url.value(), o, clazz);
	}

	public static <T> T postObject(APIURL url, Object o, Class<T> clazz, Map<String, ?> urlVariables) {
		RestTemplate rt = new RestTemplate();
		rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		rt.getMessageConverters().add(new StringHttpMessageConverter());
		return rt.postForObject(url.value(), o, clazz, urlVariables);
	}

	public static <T> T getObject(APIURL url, Class<T> responseType, Map<String, ?> urlVariables) {
		RestTemplate rt = new RestTemplate();
		rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		rt.getMessageConverters().add(new StringHttpMessageConverter());
		return rt.getForObject(url.value(), responseType, urlVariables);
	}

	public static <T> T getObject(APIURL url, Class<T> responseType) {
		RestTemplate rt = new RestTemplate();
		rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		rt.getMessageConverters().add(new StringHttpMessageConverter());
		return rt.getForObject(url.value(), responseType);
	}

}
