package ro.tuc.dsrl.geyser.execution.actionexecutors;


import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.geyser.execution.rest.repositories.InputRepository;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class ShiftDelayTolerableActionExecutor extends ActionExecutor {


//	@Override
//	public TotalExecutionResults execute(OptAction action, TotalExecutionResults results) {
//		System.out.println("execute sdtw");
//		Date current = results.getRecordTime();
//		DateTime from = new DateTime(action.getSdtwFrom());
//		from = from.plus(current.getTime()-action.getStart().getTime());
//
//		TotalPowerREST inputDC = InputRepository.getCurrentDCInput(from);
//		//TODO: possible bug. shouldn't the DT be added to the old value? and not SET
//		double previous = results.getItPowerConsumptionDt();
//		results.setItPowerConsumptionDt(previous + inputDC.getItPowerConsumptionDt()*action.getSdtwPercentage());
//
//		return results;
//	}

    @Override
    public DatacenterStateDTO execute(OptimizationActionDTO action, DatacenterStateDTO results) {
        LOGGER.info("Execute : " + action);

//			Date current = results.getRecordTime();
//			DateTime from = new DateTime(action.getSdtwFrom());
//			from = from.plus(current.getTime() - action.getStart().getTime());
//
        Long currentTimeMillis = results.getRecordTime();
        LocalDateTime from = DateUtils.millisToUTCLocalDateTime(action.getMoveFromDate());
        from = from.plus(currentTimeMillis - action.getStartTime(), ChronoUnit.MILLIS);

        DatacenterStateDTO inputDC = InputRepository.getCurrentDCInput(from);

        System.out.println(action.getMovePercentage());
        // previous is the accumulated value of the current sdtw
        double previous = results.getItPowerConsumptionDt();
        results.setItPowerConsumptionDt(previous + (inputDC.getItPowerConsumptionDt() - results.getRealocEnergy()) * action.getMovePercentage());
        //System.out.println("SDT : " + results.getItPowerConsumptionDt());

        return results;
    }

}
