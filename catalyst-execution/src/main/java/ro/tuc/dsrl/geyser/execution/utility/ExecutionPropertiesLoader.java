package ro.tuc.dsrl.geyser.execution.utility;

import org.apache.log4j.Logger;

/**
 * 
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: rest-client
 * @Since: Dec 17, 2014
 * @Description:
 *
 */
public final class ExecutionPropertiesLoader {
	private static final Logger LOGGER = Logger.getLogger(ExecutionPropertiesLoader.class);

	public static String CATALYST_DB_API_HOST;

	public static String DATACENTER_NAME;

}
