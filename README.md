# Intra DC Energy Optimizer

The optimization techniques are based on discrete modelling of the DCs as concrete systems and involving
nonlinear system dynamics that affect the quality of the energy flexibility action plan. 