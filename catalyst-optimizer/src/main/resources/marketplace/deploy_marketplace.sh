#!/usr/bin/env bash

#########################################
# Get the source code for information broker component #
#########################################

# Information Broker
git_project="catalyst-information-broker"
if [ -d "${git_project}" ]; then
  echo "Source code for ${git_project} already exists. Pulling.."
  cwd=$(pwd)
  cd ${git_project}
  git pull
  cd ${cwd}
else
  echo "Source code for ${git_project} does not exist. Cloning.."
  git clone http://dsrl.fairuse.org:4000/catalyst/${git_project}.git
fi


#########################################
#      Deploy db container and information broker instance#
#########################################

# If sudo is not available (e.g. Windows or su-based distros), replace accordingly.
cd ${git_project}/config/docker
sudo docker-compose build && \
sudo docker-compose down && \
sudo docker-compose rm -f && \
sudo docker-compose up -d
echo "Done."
