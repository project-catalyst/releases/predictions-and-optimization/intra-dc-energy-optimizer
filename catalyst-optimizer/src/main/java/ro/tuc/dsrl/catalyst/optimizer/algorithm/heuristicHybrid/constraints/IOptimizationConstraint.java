package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.constraints;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.Solution;

public interface IOptimizationConstraint<T extends Solution> {
    void verify(T solution);
}
