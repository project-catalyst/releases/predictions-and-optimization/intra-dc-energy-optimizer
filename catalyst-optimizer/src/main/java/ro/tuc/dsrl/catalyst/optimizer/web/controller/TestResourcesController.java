package ro.tuc.dsrl.catalyst.optimizer.web.controller;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.optimizer.market.DefaultDBMarketHandler;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.*;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTimeframes;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTypes;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketAPIHandlerInterface;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.ActiveSessionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.CorrelatedActionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.MarketActionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.MarketCorrelationDTO;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@RestController
@RequestMapping(value = "/test")
public class TestResourcesController {

    private final static Logger LOGGER = LoggerFactory.getLogger(TestResourcesController.class);

    @Value("${CATALYST_DB_API_HOST}")
    private String name;

    private static String NAME_STATIC;

    @Value("${CATALYST_DB_API_HOST}")
    public void setNameStatic(String name) {
        TestResourcesController.NAME_STATIC = name;
    }

    @GetMapping("/properties")
    public List<String> getProperties() {
        return Arrays.asList(this.name, NAME_STATIC);
    }


    @GetMapping("/logging")
    public String testLogging() {
        LOGGER.trace("this is TRACE");
        LOGGER.info("this is INFO");
        LOGGER.debug("this is DEBUG");
        LOGGER.error("this is ERROR");
        LOGGER.warn("this is WARNING");

        return "Checkout the logs";
    }


    @RequestMapping(value = "/test/{time}", method = RequestMethod.GET)
    public DCResources dcResources(@PathVariable("time") long time) {
        Date date = new Date(time);
        return new DCResources(date);
    }


    @RequestMapping(value = "/market-actions-dto", method = RequestMethod.POST)
    public List<MarketActionDTO> marketActionsEndpoint(@RequestBody List<MarketActionDTO> marketActionsDTOS) {
        List<MarketActionDTO> ma = marketActionsDTOS;
        return ma;
    }

    @RequestMapping(value = "/correlated-actions-dto", method = RequestMethod.POST)
    public List<CorrelatedActionDTO> correlatedActions(@RequestBody List<CorrelatedActionDTO> correlatedActionsDTOS) {
        List<CorrelatedActionDTO> ma = correlatedActionsDTOS;
        return ma;
    }

    @RequestMapping(value = "/active-sessions-dto", method = RequestMethod.POST)
    public List<ActiveSessionDTO> activeSessions(@RequestBody List<ActiveSessionDTO> activeSessionDTOS) {
        List<ActiveSessionDTO> ma = activeSessionDTOS;
        return ma;
    }

    @RequestMapping(value = "/market-correlations-dto", method = RequestMethod.POST)
    public List<MarketCorrelationDTO> marketCorrelations(@RequestBody List<MarketCorrelationDTO> marketCorrelationDTOS) {
        List<MarketCorrelationDTO> ma = marketCorrelationDTOS;
        return ma;
    }


    public List<ClearingPriceDTO> clearingPrice(@RequestBody List<ClearingPriceDTO> clearingPriceDTOS) {
        List<ClearingPriceDTO> ma = clearingPriceDTOS;
        return ma;
    }

    @RequestMapping(value = "/reference-price-dto", method = RequestMethod.POST)
    public List<ReferencePriceDTO> referencePrice(@RequestBody List<ReferencePriceDTO> referencePriceDTOS) {
        List<ReferencePriceDTO> ma = referencePriceDTOS;
        return ma;
    }

    @RequestMapping(value = "/db-get-clearing/{market_type}/{market_timeframe}", method = RequestMethod.GET)
    public List<Double> getClearing(@PathVariable("market_type") String type, @PathVariable("market_timeframe") String timeframe) {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        System.out.println(type);
        System.out.println(timeframe);
        System.out.println(timeframe.toUpperCase());
        System.out.println(type.toUpperCase());
        MarketTypes marketType = MarketTypes.valueOf(type.toUpperCase());
        System.out.println(type.toUpperCase());
        MarketTimeframes marketTimeframe = MarketTimeframes.valueOf(timeframe.toUpperCase());
        System.out.println(type.toUpperCase());
        return handler.getClearingPrices(marketType, marketTimeframe, DateTime.now());
    }

    @RequestMapping(value = "/db-get-reference/{market_type}/{market_timeframe}", method = RequestMethod.GET)
    public List<Double> getReference(@PathVariable("market_type") String type, @PathVariable("market_timeframe") String timeframe) {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        MarketTypes marketType = MarketTypes.valueOf(type.toUpperCase());
        MarketTimeframes marketTimeframe = MarketTimeframes.valueOf(timeframe.toUpperCase());
        return handler.getRefrencePrices(marketType, marketTimeframe, DateTime.now());
    }

    @RequestMapping(value = "/db-get-active-sessions/{market_type}/{market_timeframe}", method = RequestMethod.GET)
    public List<ActiveSessionDTO> getActiveSessions(@PathVariable("market_type") String type, @PathVariable("market_timeframe") String timeframe) {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        MarketTypes marketType = MarketTypes.valueOf(type.toUpperCase());
        MarketTimeframes marketTimeframe = MarketTimeframes.valueOf(timeframe.toUpperCase());
        return handler.getActiveSessions(marketType, marketTimeframe, DateTime.now());
    }

    @RequestMapping(value = "/db-get-market-actions/{market_type}/{market_timeframe}", method = RequestMethod.GET)
    public List<MarketActionDTO> getMarketActions(@PathVariable("market_type") String type, @PathVariable("market_timeframe") String timeframe) {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        MarketTypes marketType = MarketTypes.valueOf(type.toUpperCase());
        MarketTimeframes marketTimeframe = MarketTimeframes.valueOf(timeframe.toUpperCase());
        return handler.getMarketActions(marketType, marketTimeframe, DateTime.now());
    }

    @RequestMapping(value = "/register-buy-actions/{market_type}/{market_timeframe}", method = RequestMethod.POST)
    public List<MarketActionDTO> buyActions(@PathVariable("market_type") String type, @PathVariable("market_timeframe") String timeframe, @RequestBody List<MarketActionDTO> marketActionsDTOS) {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        MarketTypes marketType = MarketTypes.valueOf(type.toUpperCase());
        MarketTimeframes marketTimeframe = MarketTimeframes.valueOf(timeframe.toUpperCase());
        return handler.registerActions(marketActionsDTOS, marketType, marketTimeframe);
    }

    @RequestMapping(value = "/register-sell-actions/{market_type}/{market_timeframe}", method = RequestMethod.POST)
    public List<MarketActionDTO> sellActions(@PathVariable("market_type") String type, @PathVariable("market_timeframe") String timeframe, @RequestBody List<MarketActionDTO> marketActionsDTOS) {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        MarketTypes marketType = MarketTypes.valueOf(type.toUpperCase());
        MarketTimeframes marketTimeframe = MarketTimeframes.valueOf(timeframe.toUpperCase());
        return handler.registerActions(marketActionsDTOS, marketType, marketTimeframe);
    }

    @RequestMapping(value = "/register-correlated-actions/{market_type}/{market_timeframe}", method = RequestMethod.POST)
    public MarketCorrelationDTO registerCorrelation(@PathVariable("market_type") String type, @PathVariable("market_timeframe") String timeframe, @RequestBody MarketCorrelationDTO marketCorrelationsDTOS) {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        MarketTypes marketType = MarketTypes.valueOf(type.toUpperCase());
        MarketTimeframes marketTimeframe = MarketTimeframes.valueOf(timeframe.toUpperCase());
        return handler.registerCorrelatedActions(marketCorrelationsDTOS, marketTimeframe);
    }

    @RequestMapping(value = "/get-reference-prices-previous-day-from-marketplace-connector", method = RequestMethod.GET)
    public List<Double> getReferencePricesPreviousDayFromMarketplaceConnector() {
        MarketAPIHandlerInterface mpcmHandlerInterface = MarketAPIHandlerInterface.getInstance(
                MarketAPIHandlerInterface.MarketAPIType.MPCM);

        return mpcmHandlerInterface.getRefrencePrices(MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD, DateTime.now());
    }

    @RequestMapping(value = "/get-clearing-prices-previous-day-from-marketplace-connector", method = RequestMethod.GET)
    public List<Double> getClearingPricesPreviousDayFromMarketplaceConnector() {
        MarketAPIHandlerInterface mpcmHandlerInterface = MarketAPIHandlerInterface.getInstance(
                MarketAPIHandlerInterface.MarketAPIType.MPCM);

        return mpcmHandlerInterface.getClearingPrices(MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD, DateTime.now());
    }

    @RequestMapping(value = "/get-market-sessions-from-marketplace-connector", method = RequestMethod.GET)
    public List<ActiveSessionDTO> getMarketSessionsFromMarketplaceConnector() {
        MarketAPIHandlerInterface mpcmHandlerInterface = MarketAPIHandlerInterface.getInstance(
                MarketAPIHandlerInterface.MarketAPIType.MPCM);

        return mpcmHandlerInterface.getActiveSessions(MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD, DateTime.now());
    }

    @RequestMapping(value = "/get-market-actions-from-marketplace-connector", method = RequestMethod.GET)
    public List<MarketActionDTO> getMarketActionsFromMarketplaceConnector() {
        MarketAPIHandlerInterface mpcmHandlerInterface = MarketAPIHandlerInterface.getInstance(
                MarketAPIHandlerInterface.MarketAPIType.MPCM);

        return mpcmHandlerInterface.getMarketActions(MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD, DateTime.now());
    }

    @RequestMapping(value = "/register-buy-market-actions-from-marketplace-connector", method = RequestMethod.GET)
    public List<MarketActionDTO> registerBuyActionsToMarketplaceConnector() {
        MarketAPIHandlerInterface mpcmHandlerInterface = MarketAPIHandlerInterface.getInstance(
                MarketAPIHandlerInterface.MarketAPIType.MPCM);

        List<MarketActionDTO> marketActionsToRegister = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String date = "2020-01-20T16:00:00Z";

        MarketActionDTO marketAction = new MarketActionDTO();
        marketAction.setDate(DateTime.parse(date, formatter));
        marketAction.setActionStartTime(DateTime.parse(date, formatter));
        marketAction.setActionEndTime(DateTime.parse(date, formatter));
        marketAction.setValue(10.000);
        marketAction.setPrice(10.000);
        marketAction.setUom("uom");
        marketAction.setActionType("bid");

        MarketActionDTO marketAction2 = new MarketActionDTO();
        marketAction2.setDate(DateTime.parse(date, formatter));
        marketAction2.setActionStartTime(DateTime.parse(date, formatter));
        marketAction2.setActionEndTime(DateTime.parse(date, formatter));
        marketAction2.setValue(10.000);
        marketAction2.setPrice(10.000);
        marketAction2.setUom("uom");
        marketAction2.setActionType("offer");

        marketActionsToRegister.add(marketAction);
        marketActionsToRegister.add(marketAction2);

        return mpcmHandlerInterface.registerActions(marketActionsToRegister, MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD);
    }


    @RequestMapping(value = "/register-sell-market-actions-from-marketplace-connector", method = RequestMethod.GET)
    public List<MarketActionDTO> registerSellActionsToMarketplaceConnector() {
        MarketAPIHandlerInterface mpcmHandlerInterface = MarketAPIHandlerInterface.getInstance(
                MarketAPIHandlerInterface.MarketAPIType.MPCM);

        List<MarketActionDTO> marketActionsToRegister = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String date = "2020-01-20T16:00:00Z";

        MarketActionDTO marketAction = new MarketActionDTO();
        marketAction.setDate(DateTime.parse(date, formatter));
        marketAction.setActionStartTime(DateTime.parse(date, formatter));
        marketAction.setActionEndTime(DateTime.parse(date, formatter));
        marketAction.setValue(10.000);
        marketAction.setPrice(10.000);
        marketAction.setUom("Kwh");
        marketAction.setActionType("bid");

        MarketActionDTO marketAction2 = new MarketActionDTO();
        marketAction2.setDate(DateTime.parse(date, formatter));
        marketAction2.setActionStartTime(DateTime.parse(date, formatter));
        marketAction2.setActionEndTime(DateTime.parse(date, formatter));
        marketAction2.setValue(10.000);
        marketAction2.setPrice(10.000);
        marketAction2.setUom("Kwh");
        marketAction2.setActionType("offer");

        marketActionsToRegister.add(marketAction);
        marketActionsToRegister.add(marketAction2);

        return mpcmHandlerInterface.registerActions(marketActionsToRegister, MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD);
    }

    @RequestMapping(value = "/register-market-correlations-from-marketplace-connector", method = RequestMethod.GET)
    public MarketCorrelationDTO registerMarketCorrelationsToMarketplaceConnector() {
        MarketAPIHandlerInterface mpcmHandlerInterface = MarketAPIHandlerInterface.getInstance(
                MarketAPIHandlerInterface.MarketAPIType.MPCM);

        MarketCorrelationDTO marketCorrelationDTO = new MarketCorrelationDTO();
        marketCorrelationDTO.setConstraintType("ALL OR NOTHING");

        CorrelatedActionDTO correlatedAction1 = new CorrelatedActionDTO();
        correlatedAction1.setActionId(10);
        correlatedAction1.setMarketplaceForm("electric_energy");

        CorrelatedActionDTO correlatedAction2 = new CorrelatedActionDTO();
        correlatedAction2.setActionId(20);
        correlatedAction2.setMarketplaceForm("thermal");

        List<CorrelatedActionDTO> marketActions = new ArrayList<>();
        marketActions.add(correlatedAction1);
        marketActions.add(correlatedAction2);

        marketCorrelationDTO.setMarketActions(marketActions);

        return mpcmHandlerInterface.registerCorrelatedActions(marketCorrelationDTO, MarketTimeframes.DAYAHEAD);
    }

    @RequestMapping(value = "/defaultdb/intraday/clearing_prices", method = RequestMethod.GET)
    public Map<String, List<Double>> TestDefaultDbIntraDayClearinPrices() {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        List<Double> electric_prices = handler.getClearingPrices(MarketTypes.ELECTRIC, MarketTimeframes.INTRADAY, DateTime.now());
        List<Double> thermal_prices = handler.getClearingPrices(MarketTypes.ELECTRIC, MarketTimeframes.INTRADAY, DateTime.now());
        List<Double> it_load_prices = handler.getClearingPrices(MarketTypes.ELECTRIC, MarketTimeframes.INTRADAY, DateTime.now());
        Map<String, List<Double>> result = new HashMap<>();
        result.put("electric", electric_prices);
        result.put("thermal", thermal_prices);
        result.put("it_load", it_load_prices);
        return result;

    }

    @RequestMapping(value = "/defaultdb/dayahead/clearing_prices", method = RequestMethod.GET)
    public Map<String, List<Double>> TestDefaultDbDayAheadDayClearinPrices() {
        DefaultDBMarketHandler handler = new DefaultDBMarketHandler();
        List<Double> electric_prices = handler.getClearingPrices(MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD, DateTime.now());
        List<Double> thermal_prices = handler.getClearingPrices(MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD, DateTime.now());
        List<Double> it_load_prices = handler.getClearingPrices(MarketTypes.ELECTRIC, MarketTimeframes.DAYAHEAD, DateTime.now());
        Map<String, List<Double>> result = new HashMap<>();
        result.put("electric", electric_prices);
        result.put("thermal", thermal_prices);
        result.put("it_load", it_load_prices);
        return result;

    }

}
