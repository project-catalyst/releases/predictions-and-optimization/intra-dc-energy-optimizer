package ro.tuc.dsrl.catalyst.optimizer.resources;

public class GeneratorResources {
	private double maximumCapacity;
	private double currentEnergyProduction;

	/**
	 * @return the maximumCapacity
	 */
	public double getMaximumCapacity() {
		return maximumCapacity;
	}

	/**
	 * @param maximumCapacity the maximumCapacity to set
	 */
	public void setMaximumCapacity(double maximumCapacity) {
		this.maximumCapacity = maximumCapacity;
	}

	/**
	 * @return the currentEnergyProduction
	 */
	public double getCurrentEnergyProduction() {
		return currentEnergyProduction;
	}

	/**
	 * @param currentEnergyProduction the currentEnergyProduction to set
	 */
	public void setCurrentEnergyProduction(double currentEnergyProduction) {
		this.currentEnergyProduction = currentEnergyProduction;
	}

}
