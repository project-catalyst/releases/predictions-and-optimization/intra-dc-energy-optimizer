package ro.tuc.dsrl.catalyst.optimizer.remoteconnection;

import ro.tuc.dsrl.catalyst.optimizer.utility.PropertiesLoader;

public enum CatalystDBApiURL implements APIURL {
    OPTIMIZATION_PLANS("optimization-plan/opt-container"),
    FORECAST_DAY_DELAY_TOLERABLE_WORKLOAD("energy-consumption/dayahead/dt/{datacenter-name}/{startdate}"),
    FORECAST_DAY_REALTIME_WORKLOAD("energy-consumption/dayahead/rt/{datacenter-name}/{startdate}"),
    FORECAST_INTRADAY_REALTIME_WORKLOAD_RT("energy-consumption/intraday/rt/{datacenter-name}/{startdate}"),
    FORECAST_INTRADAY_REALTIME_WORKLOAD_DT("energy-consumption/intraday/dt/{datacenter-name}/{startdate}"),
    INTRADAY_REFERENCE_PRICE("market/reference-prices/intraday/{datacenter-name}/{startdate}"),
    DAYAHEAD_REFERENCE_PRICE("market/reference-prices/dayahead/{datacenter-name}/{startdate}"),
    HISTORICAL_DR_PRICE("/dr-incentives/dayahead/{datacenter-name}/{startdate}"),
    HISTORICAL_IT_LOAD_PRICE("market/reference-prices/workload/latest/{granularity}/{datacenter-name}/{startdate}"),
    HISTORICAL_ENERGY_PRICE("market/reference-prices/energy/latest/{granularity}/{datacenter-name}/{startdate}"),
    HISTORICAL_THERMAL_PRICE("market/reference-prices/thermal/latest/{granularity}/{datacenter-name}/{startdate}"),
    DAYAHEAD_HISTORICAL_DR_INCENTIVE("market/reference-prices/dr-incentives/latest/dayahead/{datacenter-name}/{startdate}"),
    BASELINE_INTRADAY("monitored-measurement/baseline/intraday/{datacenter-name}/{startdate}"),
    BASELINE_DAYAHEAD("monitored-measurement/baseline/dayahead/{datacenter-name}/{startdate}"),
    DR_SIGNAL_INTRADAY("monitored-measurement/dr-signal/intraday/{datacenter-name}/{startdate}"),
    DR_SIGNAL_DAYAHEAD("monitored-measurement/dr-signal/dayahead/{datacenter-name}/{startdate}"),
    RENEWABLE_DAYAHEAD("monitored-measurement/renewable/dayahead/{datacenter-name}/{startdate}"),
    RENEWABLE_INTRADAY("monitored-measurement/renewable/intraday/{datacenter-name}/{startdate}"),
    SERVER_ROOM_ALL("resources/server-room/{dc-name}/{time}"),
    COOLING_SYSTEM_ALL("resources/cooling-system/{dc-name}/{time}"),
    DATACENTER("resources/datacenter/{dc-name}/{time}"),
    BATTERY_ALL("resources/battery/{dc-name}/{time}"),
    TES_ALL("resources/tes/{dc-name}/{time}"),
    PREDICTED_VALUES_MULTIPLE_ENERGY_TYPES("predicted-value/insert_predicted_values_multiple_energy_types/"),
    FLEXIBILITY_STRATEGY("/strategies/{time}"),
    SETUP_ESTIMATED_PROFILES("/energy-consumption/setup/estimated-profiles/{startdate}"),
    MARKET_DATA_DTO_INSERT("/data-init/save-marketplace-values/"),
    METRIC("/metric"),
    DEVICE_ID("/device/label/{device-name}"),
    MONITORED("/electrical/history/monitored-values/consumption/{granularity}/{dataCenterID}/entireDC/{startdate}"),
    DC_ADAPTED("/monitored-measurement/optimized/dc/{granularity}/{datacenter-name}/{startdate}"),
    SERVER_ADAPTED("/monitored-measurement/optimized/server/{granularity}/{datacenter-name}/{startdate}"),
    BASELINE("/monitored-measurement/baseline/{granularity}/{datacenter-name}/{startdate}"),
    THERMAL_BASELINE("/monitored-measurement/baseline/thermal/{granularity}/{datacenter-name}/{startdate}"),
    THERMAL_ADAPTED("/monitored-measurement/optimized/thermal/{granularity}/{datacenter-name}/{startdate}"),
    CO2_ADAPTED("/monitored-measurement/optimized/co2/{granularity}/{datacenter-name}/{startdate}"),
    CO2_BASELINE("/monitored-measurement/baseline/co2/{granularity}/{datacenter-name}/{startdate}"),
    COOLING_ADAPTED("/monitored-measurement/optimized/cooling/{granularity}/{datacenter-name}/{startdate}"),
    MARKET("/market/reference-prices/{granularity}/{datacenter-name}/{startdate}");

    private final String catalystDbApiHost = PropertiesLoader.CATALYST_DB_API_HOST;
    private String value;

    CatalystDBApiURL(String value) {
        this.value = value;
    }


    @Override
    public String value() {
        return catalystDbApiHost + value;
    }
}
