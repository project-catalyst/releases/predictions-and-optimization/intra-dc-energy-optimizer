package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.fitness;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.Solution;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.EnergyEfficiencySolution;

import java.util.Arrays;

public class FitnessFunctionGenetic implements IFitnessFunction {
    private OptimizationDataCatalyst data;

    public FitnessFunctionGenetic(OptimizationDataCatalyst data) {
        this.data = data;
    }

    public double compute(Solution solution2) {
        EnergyEfficiencySolution solution = (EnergyEfficiencySolution) solution2;
        double sum;

        double[] price = new double[4];
        Arrays.fill(price, 0);
        for (int i = 0; i < data.getSize(); ++i) {

            price[0] += solution.getEDC()[i] * data.getEnergyPrice()[i];
            price[1] += solution.gettDc()[i] * data.getHeatPrice()[i];
            price[2] += Math.pow(solution.getEDC()[i] - data.getDrSignal()[i], 2) * data.getDemandPrice()[i];
            price[3] += solution.getItRelocateActions()[i] * data.getDelayTolerableEnergy()[i] * data.getLoadPrice()[i];
        }

        sum = data.getWE()[0] * price[0] - data.getWT()[0] * price[1] + data.getWF()[0] * price[2]
                - (data.getRelocateActive()[0] + data.getWL()[0]) * price[3];
        solution.setEnergyCost(price[0]);
        solution.setThermalProfit(price[1]);
        solution.setFlexibilityPenalty(price[2]);
        solution.setLoadProfit(price[3]);
        solution.setFitnessValue(sum);
        return sum;
    }

}

