package ro.tuc.dsrl.catalyst.optimizer.utility;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Parser {

    private static List<String> getRecordFromLine(String line) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(",");
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }

        return values;
    }

    public static boolean isNumeric(String nr) {
        try {
            double d = Double.parseDouble(nr);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }

        return true;
    }

    public static Map<String, List<Double>> parse(String path) throws IOException {
        Map<String, List<Double>> extraParams = new HashMap<>();

        List<List<String>> records = new ArrayList<>();

        try (Scanner scanner = new Scanner(new File(path))) {
            while (scanner.hasNextLine()) {
                records.add(getRecordFromLine(scanner.nextLine()));
            }
        }

        int n = records.size();
        int m = records.get(1).size();

        List<List<Double>> matrix = new ArrayList<>();
        boolean matrixRead = false;

        System.out.println(n);

        for (int j = 0; j < m; j++) {

            List<Double> list = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                if (j < records.get(i).size() - 1 && !isNumeric(records.get(i).get(j)) && isNumeric((records.get(i).get(j + 1)))) {
                    extraParams.put(records.get(i).get(j), Collections.singletonList(Double.parseDouble(records.get(i).get(j + 1))));
                } else if (j < records.get(i).size() && isNumeric(records.get(i).get(j) )) {
                    list.add(Double.parseDouble(records.get(i).get(j)));
                }
            }

            if(matrixRead || records.get(0).get(j).equals("Y"))
            {
                matrixRead = true;
                matrix.add(list);
                System.out.println(list);
                System.out.println(list.size());
                continue;
            }

            if (!isNumeric(records.get(0).get(j)) && extraParams.get(records.get(0).get(j)) == null)
                extraParams.put(records.get(0).get(j), list);

        }

        extraParams.put("Y", convertMatrix(matrix));
        return extraParams;
    }

    private static List<Double> convertMatrix(List<List<Double>> matrix) {
        List<Double> list = new ArrayList<>();

        for(int i = 0; i < matrix.get(0).size(); i++)
            for(int j = 0; j < matrix.size(); j++)
                list.add(matrix.get(j).get(i));

        System.out.println(list.size());
        return list;
    }

    public static Map<String, List<Double>> specialParse(String path, Double percentage) throws IOException {
        Map<String, List<Double>> extraParams = parse(path);

        /*List<Double> energy = (List<Double>)extraParams.get("E_Total");
        extraParams.put("E_R", energy.stream().map(x -> percentage * x).collect(Collectors.toList()));
        extraParams.put("E_D", energy.stream().map(x -> (1 - percentage) * x).collect(Collectors.toList()));
        */

        return extraParams;
    }
}
