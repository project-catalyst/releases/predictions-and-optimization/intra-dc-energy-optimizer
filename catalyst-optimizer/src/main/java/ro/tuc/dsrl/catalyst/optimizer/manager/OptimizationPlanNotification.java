package ro.tuc.dsrl.catalyst.optimizer.manager;

import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileMultipleEnergyTypesDTO;
import ro.tuc.dsrl.catalyst.model.dto.MarketDataDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MetricsDTO;
import ro.tuc.dsrl.catalyst.model.enums.Timeframe;
import ro.tuc.dsrl.catalyst.optimizer.planner.ActionPlan;
import ro.tuc.dsrl.catalyst.optimizer.planner.DayaheadPlanner;
import ro.tuc.dsrl.catalyst.optimizer.planner.IntradayPlanner;
import ro.tuc.dsrl.catalyst.optimizer.planner.Planner;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystMetricsApiURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;
import ro.tuc.dsrl.catalyst.optimizer.mapper.ActionMapper;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystDBApiURL;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptimizationPlanNotification {

	private OptimizationPlanNotification() {
	}

	public static void notifyDayAheadPlans(ActionPlan dayAheadActionPlan, DateTime startDate) {
		List<ActionPlan> dayAheadActionPlans = new ArrayList<>();
		dayAheadActionPlans.add(dayAheadActionPlan);

		DateTime endTime = startDate.plus(DayaheadPlanner.DURATION);
		endTime = endTime.minusMinutes(1);
		OptimizationPlansContainer container = ActionMapper.getOptimizationPlanContainer(dayAheadActionPlans,
				startDate, endTime, Timeframe.DAY_AHEAD.getName(), Planner.DATACENTER_NAME);
		RestAPIClient.postObject(CatalystDBApiURL.OPTIMIZATION_PLANS, container, Void.class);
	}

	public static void notifyIntraDayPlans(ActionPlan intraDayActionPlan, DateTime startDate) {
		List<ActionPlan> plans = new ArrayList<ActionPlan>();
		plans.add(intraDayActionPlan);
		OptimizationPlansContainer container = ActionMapper.getOptimizationPlanContainer(plans, startDate,
				startDate.plus(IntradayPlanner.DURATION), Timeframe.INTRA_DAY.getName(),  Planner.DATACENTER_NAME);
		RestAPIClient.postObject(CatalystDBApiURL.OPTIMIZATION_PLANS, container, Void.class);
	}

	public static void notifyOptimizedProfile(EnergyProfileMultipleEnergyTypesDTO optimizedProfiles){
		RestAPIClient.postObject(CatalystDBApiURL.PREDICTED_VALUES_MULTIPLE_ENERGY_TYPES, optimizedProfiles, Void.class);
	}

	public static void notifyMarketData(MarketDataDTO marketDataDTO){
		RestAPIClient.postObject(CatalystDBApiURL.MARKET_DATA_DTO_INSERT, marketDataDTO, Void.class);
	}


}
