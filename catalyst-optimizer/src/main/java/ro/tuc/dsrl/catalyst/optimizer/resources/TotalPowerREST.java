//package ro.tuc.dsrl.catalyst.optimizer.resources;
//
//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
//import com.fasterxml.jackson.databind.annotation.JsonSerialize;
//import ro.tuc.dsrl.catalyst.datamodel.api.serializers.CustomDateSerializer;
//
//import java.io.Serializable;
//import java.util.Date;
//
//public class TotalPowerREST implements Serializable {
//	private static final long serialVersionUID = 1L;
//	private Integer id;
//	@JsonDeserialize(using = ro.tuc.dsrl.catalyst.datamodel.api.serializers.CustomDateDeserializer.class)
//	@JsonSerialize(using = CustomDateSerializer.class)
//	private Date recordTime;
//	private Double greenGeneration;
//	private Double brownGeneration;
//	private Double facilityPowerConsumption;
//	private Double officesPowerConsumption;
//	private Double itPowerConsumptionRt;
//	private Double itPowerConsumptionDt;
//	private Double powerToSmartGrid;
//	private Double powerFromSmartGrid;
//
//	public TotalPowerREST() {
//	}
//
//	public TotalPowerREST(Integer id) {
//		this.id = id;
//	}
//
//	public TotalPowerREST(Integer id, Date recordTime) {
//		this.id = id;
//		this.recordTime = recordTime;
//	}
//
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	public Date getRecordTime() {
//		return recordTime;
//	}
//
//	public void setRecordTime(Date recordTime) {
//		this.recordTime = recordTime;
//	}
//
//	public Double getGreenGeneration() {
//		return greenGeneration;
//	}
//
//	public void setGreenGeneration(Double greenGeneration) {
//		this.greenGeneration = greenGeneration;
//	}
//
//	public Double getBrownGeneration() {
//		return brownGeneration;
//	}
//
//	public void setBrownGeneration(Double brownGeneration) {
//		this.brownGeneration = brownGeneration;
//	}
//
//	public Double getFacilityPowerConsumption() {
//		return facilityPowerConsumption;
//	}
//
//	public void setFacilityPowerConsumption(Double facilityPowerConsumption) {
//		this.facilityPowerConsumption = facilityPowerConsumption;
//	}
//
//	public Double getOfficesPowerConsumption() {
//		return officesPowerConsumption;
//	}
//
//	public void setOfficesPowerConsumption(Double officesPowerConsumption) {
//		this.officesPowerConsumption = officesPowerConsumption;
//	}
//
//	public Double getItPowerConsumptionRt() {
//		return itPowerConsumptionRt;
//	}
//
//	public void setItPowerConsumptionRt(Double itPowerConsumptionRt) {
//		this.itPowerConsumptionRt = itPowerConsumptionRt;
//	}
//
//	public Double getItPowerConsumptionDt() {
//		return itPowerConsumptionDt;
//	}
//
//	public void setItPowerConsumptionDt(Double itPowerConsumptionDt) {
//		this.itPowerConsumptionDt = itPowerConsumptionDt;
//	}
//
//	public Double getPowerToSmartGrid() {
//		return powerToSmartGrid;
//	}
//
//	public void setPowerToSmartGrid(Double powerToSmartGrid) {
//		this.powerToSmartGrid = powerToSmartGrid;
//	}
//
//	public Double getPowerFromSmartGrid() {
//		return powerFromSmartGrid;
//	}
//
//	public void setPowerFromSmartGrid(Double powerFromSmartGrid) {
//		this.powerFromSmartGrid = powerFromSmartGrid;
//	}
//
//}
