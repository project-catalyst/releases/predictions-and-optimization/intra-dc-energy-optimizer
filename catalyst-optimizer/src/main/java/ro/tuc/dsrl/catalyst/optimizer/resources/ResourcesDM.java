package ro.tuc.dsrl.catalyst.optimizer.resources;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;
import ro.tuc.dsrl.catalyst.optimizer.planner.Planner;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystDBApiURL;

import java.util.*;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 9, 2015
 * @Description:
 *
 */
public class ResourcesDM implements ResourcesInterface {


	public List<Battery> getAllBatteries(Date date) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("time", date.getTime());
		urlVariables.put("dc-name", Planner.DATACENTER_NAME);
		return Arrays.asList(RestAPIClient.getObject(CatalystDBApiURL.BATTERY_ALL, Battery[].class, urlVariables));
	}

	public List<ThermalEnergyStorage> getAllThermalStorages(Date date) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("time", date.getTime());
		urlVariables.put("dc-name", Planner.DATACENTER_NAME);
		return Arrays
				.asList(RestAPIClient.getObject(CatalystDBApiURL.TES_ALL, ThermalEnergyStorage[].class,urlVariables));
	}

	public List<ServerRoom> getAllServerRooms(Date date) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("time", date.getTime());
		urlVariables.put("dc-name", Planner.DATACENTER_NAME);
		return Arrays
				.asList(RestAPIClient.getObject(CatalystDBApiURL.SERVER_ROOM_ALL, ServerRoom[].class,urlVariables));
	}

	public List<CoolingSystem> getAllCoolingSystems(Date date) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("time", date.getTime());
		urlVariables.put("dc-name", Planner.DATACENTER_NAME);
		return Arrays
				.asList(RestAPIClient.getObject(CatalystDBApiURL.COOLING_SYSTEM_ALL, CoolingSystem[].class,urlVariables));
	}


	public DataCentre getDatacentre(Date date) {
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("time", date.getTime());
		urlVariables.put("dc-name", Planner.DATACENTER_NAME);
	   return RestAPIClient.getObject(CatalystDBApiURL.DATACENTER, DataCentre.class, urlVariables);

	}


	public Double getDTWMonitoredConsumtion(Interval arrivalTime, Interval recordTime, double divide) {
		return 0.0;
	}



//	@Override
//	public TotalPowerREST getCurrentState(Date date) {
//		Map<String, Object> urlVariables = new HashMap<String, Object>();
//		urlVariables.put("date", date.getTime());
//		return RestAPIClient.getObject(DataModelAPIURL.TOTAL_POWER, TotalPowerREST.class, urlVariables);
//	}
//
//	@Override
//	public void deleteRedundantActions(DateTime startTime, double d) {
//		Map<String, Object> urlVariables = new HashMap<String, Object>();
//		urlVariables.put("date", startTime.getMillis());
//		urlVariables.put("confidence", d);
//		RestAPIClient.getObject(DataModelAPIURL.DELETE_REDUNDANT_ACTIONS, Void.class, urlVariables);
//
//	}


}
