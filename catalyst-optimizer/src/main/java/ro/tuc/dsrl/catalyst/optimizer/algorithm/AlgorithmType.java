package ro.tuc.dsrl.catalyst.optimizer.algorithm;

public enum AlgorithmType {
	LINGO, GENETIC, FILE, GREEDY, HYBRID
}

