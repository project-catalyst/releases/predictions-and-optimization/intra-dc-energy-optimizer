//package ro.tuc.dsrl.catalyst.optimizer.remoteconnection;
//
//import ro.tuc.dsrl.catalyst.optimizer.utility.ConfigurationProperties;
//import ro.tuc.dsrl.catalyst.optimizer.utility.ExecutionPropertiesLoader;
//
///**
// * @Author: Technical University of Cluj-Napoca, Romania
// *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
// * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
// * @Module: intra-dc-energy-optimizer-v2
// * @Since: Feb 24, 2015
// * @Description:
// *
// */
//public enum EsmaAPIURL implements  APIURL{
//	OFFER_URL(ExecutionPropertiesLoader.getProperty(ConfigurationProperties.OFFER_URL)),
//	BID_URL(ExecutionPropertiesLoader.getProperty(ConfigurationProperties.BID_URL));
//
//	private String value;
//	private String esmaHost = ExecutionPropertiesLoader.getProperty(ConfigurationProperties.ESMA_API_HOST);
//
//	EsmaAPIURL(String url) {
//		this.value = url;
//	}
//
//	@Override
//	public String value() {
//		return esmaHost + value;
//	}
//}
