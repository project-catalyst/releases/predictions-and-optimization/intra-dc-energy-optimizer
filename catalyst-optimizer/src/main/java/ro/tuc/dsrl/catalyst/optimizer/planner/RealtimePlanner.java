package ro.tuc.dsrl.catalyst.optimizer.planner;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;
import org.joda.time.Period;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationActionAdapter;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.geyser.datamodel.actions.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 16, 2015
 * @Description:
 *
 */
public class RealtimePlanner extends Planner {
	private static final int PREDICTION_PERIOD_LENGTH = 5;
	public static final Period DURATION = new Period().withMinutes(PREDICTION_PERIOD_LENGTH);
	public static final String TIMEFRAME = "real_time";

	public RealtimePlanner(ActionPlan actionPlan, DCResources resources) {
		super(actionPlan, resources);
	}

	public ActionPlan execute(DateTime currentTime) {
		ActionPlan realtimeActionPlan = new ActionPlan();
		List<EnergyEfficiencyOptimizationAction> actionList = new ArrayList<EnergyEfficiencyOptimizationAction>();
		DateTime endDate = getDateTime(currentTime);
		ResourceConstraints constraint = getEndConstraint(endDate);
		double energyBudget = computeBudget(currentTime, resources, actionPlan);
		double levelUPS = resources.getBatteryResource().getActualLoadedCapacity();

		// simulate battery actions
		for (DateTime date = currentTime; date.isBefore(endDate); date = date.plus(DURATION)) {
			List<EnergyEfficiencyOptimizationAction> actions = actionPlan.getCurrentActions(currentTime);
			for (EnergyEfficiencyOptimizationAction action : actions) {
				long actionDuration = action.getEndTime().getTime() - action.getStartTime().getTime();
				long scale = actionDuration / DURATION.toStandardDuration().getMillis();
				// long scale = 12
				// scale will bring kWh & kW30min to kW5min
				if (action instanceof ChargeBattery) {
					levelUPS += ((ChargeBattery) action).getAmountOfEnergy() / scale;
				}
				if (action instanceof DischargeBattery) {
					levelUPS -= ((DischargeBattery) action).getAmountOfEnergy() / scale;
				}
			}
		}

		// power consumption > contracted energy
		// DEBT
		double minUpsConstraunt = constraint.getUpsLevel();
		double maxUpsConstraint = resources.getBatteryResource().getMaximumCapacity();

		if (energyBudget > 0 && levelUPS > minUpsConstraunt) {
			// double extraUps = (levelUPS - minUpsConstraunt) /
			// EnergyMeasurementUnit.KW5MIN.value()
			// no longer multiplying with 12, to remain energy in kW5min instead
			// of power
			double extraUps = levelUPS - minUpsConstraunt;
			if (extraUps <= energyBudget) {
				actionList.addAll(OptimizationActionAdapter.createBatteryActions(currentTime,
						currentTime.plus(DURATION), 0, extraUps));
				energyBudget -= extraUps;
			} else {
				actionList.addAll(OptimizationActionAdapter.createBatteryActions(currentTime,
						currentTime.plus(DURATION), 0, energyBudget));
				energyBudget = 0;
			}
		}
		// power consumption < contracted energy
		// PROFIT
		else if (energyBudget < 0 && levelUPS < maxUpsConstraint) {
			// 1. try to re-charge batteries
			// double freeUps = (maxUpsConstraint - levelUPS) /
			// EnergyMeasurementUnit.KW5MIN.value()
			// same as above
			double freeUps = (maxUpsConstraint - levelUPS);
			double chargeUPS = 0;
			if (freeUps < -energyBudget) {
				chargeUPS = freeUps;
				energyBudget += freeUps;
			} else {
				chargeUPS = -energyBudget;
				energyBudget = 0;
			}
			actionList.addAll(OptimizationActionAdapter.createBatteryActions(currentTime, currentTime.plus(DURATION),
					chargeUPS, 0));
		}
		realtimeActionPlan.addEntry(new Interval(currentTime, currentTime.plus(DURATION)), actionList);

		// ExcelIO.writeActionsToCSVFile(realtimeActionPlan.getActionPool(),
		// "realTime"+currentTime.getMillis() +".csv")
		return realtimeActionPlan;
	}

	/**
	 * @param currentTime
	 * @param resources
	 * @param activeActionPlan
	 */
	private static double computeBudget(DateTime currentTime, DCResources resources, ActionPlan activeActionPlan) {
		double budgetValue = 0.0;
		List<EnergyEfficiencyOptimizationAction> actions = activeActionPlan.getCurrentActions(currentTime);

		// compute contracted energy
		for (EnergyEfficiencyOptimizationAction action : actions) {
			if (action instanceof BuyEnergy) {
				budgetValue -= ((BuyEnergy) action).getAmountOfEnergy();
			}
			if (action instanceof SellEnergy) {
				budgetValue += ((SellEnergy) action).getAmountOfEnergy();
			}
			if (action instanceof ChargeBattery) {
				budgetValue += resources.getBatteryResource().getChargeLossRate() * action.getAmountOfEnergy();
			}
			if (action instanceof DischargeBattery) {
				budgetValue -= resources.getBatteryResource().getDischargeLossRate() * action.getAmountOfEnergy();
			}
			if (action instanceof ChargeTes) {
				budgetValue += resources.getTesResource().getChargeLossRate() * action.getAmountOfEnergy();
			}
			if (action instanceof DischargeTes) {
				budgetValue -= resources.getTesResource().getDischargeLossRate() * action.getAmountOfEnergy();
			}
		}
		// add renewable energy
		budgetValue -= resources.getWindResources().getCurrentEnergyProduction();

		// energy consumption
		budgetValue += resources.getServerResources().getCurrentItEnergyConsumptionKW();
		budgetValue += resources.getCoolingResources().getCurrentEnergyConsumption();
		return budgetValue;
	}

	/**
	 * @param currentTime
	 * @return
	 */
	private static DateTime getDateTime(DateTime currentTime) {
		int minutes = currentTime.getMinuteOfHour();
		DateTime date = new DateTime(currentTime);
		if (minutes < DateTimeConstants.MINUTES_PER_HOUR / 2) {
			date = date.plusMinutes(DateTimeConstants.MINUTES_PER_HOUR / 2 - minutes);
		} else {
			date = date.plusMinutes(DateTimeConstants.MINUTES_PER_HOUR - minutes);
		}
		return date;
	}

}
