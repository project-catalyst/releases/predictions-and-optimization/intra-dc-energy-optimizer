package ro.tuc.dsrl.catalyst.optimizer.resources;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 16, 2015
 * @Description:
 *
 */
public class WindResources {
	private double currentEnergyProduction;

	/**
	 * @return the currentEnergyProduction
	 */
	public double getCurrentEnergyProduction() {
		return currentEnergyProduction;
	}

	/**
	 * @param currentEnergyProduction the currentEnergyProduction to set
	 */
	public void setCurrentEnergyProduction(double currentEnergyProduction) {
		this.currentEnergyProduction = currentEnergyProduction;
	}

}
