package ro.tuc.dsrl.catalyst.optimizer.mapper;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import ro.tuc.dsrl.geyser.datamodel.actions.*;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizerPlan;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;
import ro.tuc.dsrl.catalyst.optimizer.planner.ActionPlan;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class ActionMapper {

	private ActionMapper() {
	}

	public static OptimizationPlansContainer getOptimizationPlanContainer(List<ActionPlan> dayAheadActionPlans,
																		  DateTime startDate, DateTime endDate, String timeframe,
																		  String dataCenterName) {
		OptimizationPlansContainer container = new OptimizationPlansContainer();
		for (ActionPlan plan : dayAheadActionPlans) {
			container.addOptimizationDecisions(getOptimizationDecisions(plan));
		}
		container.setStartDate(startDate.toDate());
		container.setEndDate(endDate.toDate());
		container.setTimeframe(timeframe);
		container.setDataCenterName(dataCenterName);
		return container;
	}

	public static OptimizationPlansContainer getOptimizationPlanContainer(List<ActionPlan> dayAheadActionPlans, OptimizationDataCatalyst data,
																		  DateTime startDate, DateTime endDate, String timeframe) {
		OptimizationPlansContainer container = new OptimizationPlansContainer();

		for (ActionPlan plan : dayAheadActionPlans) {
			container.addOptimizationDecisions(getOptimizationDecisions(plan));
		}

		container.setStartDate(startDate.toDate());
		container.setEndDate(endDate.toDate());
		container.setTimeframe(timeframe);
		return container;
	}

	public static OptimizerPlan getOptimizationDecisions(ActionPlan actionPlan) {
		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		for (Interval ti : actionPlan.getActionPool().keySet()) {
			actions.addAll(actionPlan.getActionPool().get(ti));
		}
		OptimizerPlan optPlan = new OptimizerPlan(actionPlan.getId(), actions, actionPlan.getConfidenceLevel());

		optPlan.setWE(actionPlan.getWE());
		optPlan.setWL(actionPlan.getWL());
		optPlan.setWF(actionPlan.getWF());
		optPlan.setWT(actionPlan.getWT());
		optPlan.setWren(actionPlan.getWren());
		optPlan.setReallocActive(actionPlan.isReallocActive());

		return optPlan;
	}

	/*public static List<Bid> getBidsFromBuyActions(List<MarketplaceAction> buyActions, DateTime currentTime) {
		List<Bid> bids = new ArrayList<Bid>();
		for (MarketplaceAction action : buyActions) {
			if (action instanceof BuyEnergy)
				bids.add(getBidFromBuyEnergyAction((BuyEnergy)action, currentTime));
			if (action instanceof BuyHeat)
				bids.add(getBidFromBuyHeatAction((BuyHeat)action, currentTime));

		}
		return bids;
	}*/

	/*public static List<Offer> getOffersFromSellActions(List<MarketplaceAction> sellActions, DateTime currentTime) {
		List<Offer> offers = new ArrayList<Offer>();
		for (MarketplaceAction action : sellActions) {
			if(action instanceof SellEnergy)
				offers.add(getOfferFromSellEnergyAction((SellEnergy) action, currentTime));
			if(action instanceof SellHeat)
				offers.add(getOfferFromSellHeatAction((SellHeat) action, currentTime));
			if (action instanceof SellFlexibility)
				offers.add(getOfferFromSellFlexibilityAction((SellFlexibility) action, currentTime));
		}
		return offers;
	}*/

	/**
	 * @param action
	 * @param currentTime
	 * @return
	 */
	/*private static Offer getOfferFromSellEnergyAction(SellEnergy action, DateTime currentTime) {
		Offer offer = new Offer();
		Marketaction marketAction = getMarketAction(action, currentTime);
		offer.setMarketActionid(marketAction);
		return offer;
	}*/

	/**
	 * @param action
	 * @param currentTime
	 * @return
	 */
	/*private static Offer getOfferFromSellHeatAction(SellHeat action, DateTime currentTime) {
		Offer offer = new Offer();
		Marketaction marketAction = getMarketAction(action, currentTime);
		offer.setMarketActionid(marketAction);
		return offer;
	}*/

	/**
	 * @param action
	 * @param currentTime
	 * @return
	 */
	/*private static Offer getOfferFromSellFlexibilityAction(SellFlexibility action, DateTime currentTime) {
		Offer offer = new Offer();
		Marketaction marketAction = getMarketAction(action, currentTime);
		offer.setMarketActionid(marketAction);
		return offer;
	}*/

	/**
	 * @param action
	 * @param currentTime
	 * @return
	 */
	/*private static Bid getBidFromBuyEnergyAction(BuyEnergy action, DateTime currentTime) {
		Bid bid = new Bid();
		Marketaction marketAction = getMarketAction(action, currentTime);
		bid.setMarketActionid(marketAction);
		return bid;
	}*/

	/**
	 * @param action
	 * @param currentTime
	 * @return
	 */
	/*private static Bid getBidFromBuyHeatAction(BuyHeat action, DateTime currentTime) {
		Bid bid = new Bid();
		Marketaction marketAction = getMarketAction(action, currentTime);
		bid.setMarketActionid(marketAction);
		return bid;
	}*/

	/**
	 * @param action
	 * @return
	 */
	/*private static Marketaction getMarketAction(MarketplaceAction action, DateTime currentTime) {
		Marketaction marketAction = new Marketaction();
		marketAction.setActionStartTime(action.getStartTime());
		marketAction.setActionEndTime(action.getEndTime());
		marketAction.setDate(currentTime.toDate());
		marketAction.setValue((int)action.getAmountOfEnergy());
		// TODO add the price & energy
		return marketAction;
	}*/

}
