package ro.tuc.dsrl.catalyst.optimizer.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.optimizer.web.controller.IndexController;

public class ExcelIO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelIO.class);
	private static boolean correctData = true;

	private static LingoConfigData lingoConfigData;

	private ExcelIO() {
	}
//
//	public static Map<Integer, ExcelData> getExcelData(String file, int size) {
//		InputStream input = ExcelIO.class.getResourceAsStream(file);
//
//		return readExcel(input, size);
//	}
//
//	@SuppressWarnings("resource")
//	private static Map<Integer, ExcelData> readExcel(InputStream input, int size) {
//		Map<Integer, ExcelData> excelValues = new HashMap<Integer, ExcelData>();
//		try {
//
//			// Create Workbook instance holding reference to .xls file
//			HSSFWorkbook workbook = new HSSFWorkbook(input);
//
//			// Get first/desired sheet from the workbook
//			HSSFSheet sheet = workbook.getSheetAt(0);
//
//			// Iterate through each rows one by one
//			Iterator<Row> rowIterator = sheet.iterator();
//			int iterator = 0;
//
//			while (rowIterator.hasNext()) {
//				Row row = rowIterator.next();
//				// For each row, iterate through all the columns
//				Iterator<Cell> cellIterator = row.cellIterator();
//				double contraactEnergy = nextCell(cellIterator);
//				double marketEnergy = nextCell(cellIterator);
//				double greenEnergy = nextCell(cellIterator);
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				double realtimeEnergy = nextCell(cellIterator);
//				double delayEnergy = nextCell(cellIterator);
//				double delayDeadline = nextCell(cellIterator);
//				double energyPrice = nextCell(cellIterator);
//				double transactioned = nextCell(cellIterator);
//				double batteryLevel = nextCell(cellIterator);
//				double tesLevel = nextCell(cellIterator);
//
//				double itCool = nextCell(cellIterator);
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				double estimatedDe = nextCell(cellIterator);
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				double cool = nextCell(cellIterator);
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				if (cellIterator.hasNext()) {
//					cellIterator.next();
//				}
//				double disESD = nextCell(cellIterator);
//				double chargeESD = nextCell(cellIterator);
//				double disTES = nextCell(cellIterator);
//				double chargeTES = nextCell(cellIterator);
//
//				if (correctData) {
//					excelValues.put(iterator,
//							new ExcelData(realtimeEnergy, delayEnergy, contraactEnergy, greenEnergy, energyPrice,
//									marketEnergy, delayDeadline, transactioned, tesLevel, disTES, chargeTES,
//									batteryLevel, disESD, chargeESD, estimatedDe, itCool, cool));
//					iterator++;
//
//				}
//				correctData = true;
//				if (size == 0) {
//					break;
//				}
//				size--;
//			}
//			if (!rowIterator.hasNext()) {
//				return excelValues;
//			}
//
//			double maxT = nextRow(rowIterator);
//			double maxTR = nextRow(rowIterator);
//			double f = nextRow(rowIterator);
//			double batteryDamage = nextRow(rowIterator);
//			double transactionFee = nextRow(rowIterator);
//			double eS = nextRow(rowIterator);
//			double fTES = nextRow(rowIterator);
//			double coP = nextRow(rowIterator);
//			double tesMAX = nextRow(rowIterator);
//			double tesCOST = nextRow(rowIterator);
//			double tesR = nextRow(rowIterator);
//			double tesD = nextRow(rowIterator);
//			double esdR = nextRow(rowIterator);
//			double esdD = nextRow(rowIterator);
//			double maxDisTES = nextRow(rowIterator);
//			double maxEsdDIS = nextRow(rowIterator);
//			double dod = nextRow(rowIterator);
//			double maxIT = nextRow(rowIterator);
//			double edPercentage = nextRow(rowIterator);
//			double peakCharge = nextRow(rowIterator);
//			double egrActive = nextRow(rowIterator);
//			double contractActive = nextRow(rowIterator);
//			double etrActive = nextRow(rowIterator);
//			double eUsage = nextRow(rowIterator);
//			double dieselActiv = nextRow(rowIterator);
//			double dieselCap = nextRow(rowIterator);
//			double operational = nextRow(rowIterator);
//			double tesINIT = nextRow(rowIterator);
//			double esdINIT = nextRow(rowIterator);
//			double esdFIN = nextRow(rowIterator);
//			double tesFIN = nextRow(rowIterator);
//			if (!correctData) {
//				System.out.println("Config Data reading went wrong");
//			}
//
//			LingoConfigData configData = new LingoConfigData();
//			configData.setTimePeriod(maxT);
//			configData.setTransactionEnergyLimit(maxTR);
//			configData.setFactor(f);
//			configData.setBatteryDamage(batteryDamage);
//			configData.setTransactionFee(transactionFee);
//			configData.setBatteryMaxCapacity(eS);
//			configData.setFactorTes(fTES);
//			configData.setCop(coP);
//			configData.setTesMaxCapacity(tesMAX);
//			configData.setTesDamage(tesCOST);
//			configData.setTesChargeLossRate(tesR);
//			configData.setTesDischargeLossRate(tesD);
//			configData.setBatteryChargeLossRate(esdR);
//			configData.setBatteryDischargeLossRate(esdD);
//			configData.setTesMaxDischargeRate(maxDisTES);
//			configData.setBatteryMaxDischargeRate(maxEsdDIS);
//			configData.setDod(dod);
//			configData.setItMaxConsumption(maxIT);
//			configData.setEdPercentage(edPercentage);
//			configData.setPeakCharge(peakCharge);
//			configData.setDieselMaxCapacity(dieselCap);
//			configData.setContractEnergyActive(contractActive);
//			configData.setGreenEnergyActive(egrActive);
//			configData.setTransactionEnergyActive(etrActive);
//			configData.setMarketplaceGreenEnergyActive(eUsage);
//			configData.setDieselGeneratorsActive(dieselActiv);
//			configData.setBatteryInitialValue(esdINIT);
//			configData.setTesInitialValue(tesINIT);
//			configData.setOperationalOptimizationActiv(operational);
//			configData.setBatteryFinalConstraint(esdFIN);
//			configData.setTesFinalConstraint(tesFIN);
//
//			lingoConfigData = configData;
//
//		} catch (Exception e) {
//			LOGGER.error("", e);
//		}
//		return excelValues;
//
//	}
//
//	public static LingoConfigData getConfigData() {
//		return lingoConfigData;
//	}
//
//	private static double nextRow(Iterator<Row> rowIterator) {
//		if (rowIterator.hasNext()) {
//			Row row = rowIterator.next();
//
//			// For each row, iterate through all the columns
//			Iterator<Cell> cellIterator = row.cellIterator();
//			if (cellIterator.hasNext()) {
//				cellIterator.next();
//			}
//			return nextCell(cellIterator);
//		}
//		correctData = false;
//		return -1;
//	}
//
//	private static double nextCell(Iterator<Cell> cellIterator) {
//		if (cellIterator.hasNext()) {
//			Cell cell1 = cellIterator.next();
//			// Check the cell type and format accordingly
//			if (cell1.getCellType() == Cell.CELL_TYPE_NUMERIC) {
//				return (double) cell1.getNumericCellValue();
//			}
//		}
//		correctData = false;
//		return -1;
//	}
//
//	public static void writeDataToCSVFile(OptimizationData data, String fileName) {
//		PrintWriter writer;
//		try {
//			writer = new PrintWriter(fileName, "UTF-8");
//			writer.println("Econtract, Emarket, Egr, E-Total, E_R, E_D,TD,Pgr, IncomingW, Etr,"
//					+ "Es, Tes, D-ESD, R_ESD, D_TES, R_TES, E_de, ITCooling, Final Cooling");
//
//			for (int i = 0; i < data.getSize(); i++) {
//				String toPrint = "";
//				toPrint += data.getContractedEnergy()[i] + ",";
//				toPrint += data.getMarketEnergy()[i] + ",";
//				toPrint += data.getRenewableEnergy()[i] + ",";
//				toPrint += data.getRealtimeEnergy()[i] + data.getDelayTolerableEnergy()[i] + ",";
//				toPrint += data.getRealtimeEnergy()[i] + ",";
//				toPrint += data.getDelayTolerableEnergy()[i] + ",";
//				toPrint += data.getDelayDeadline()[i] + ",";
//				toPrint += data.getEnergyPrice()[i] + ",";
//				toPrint += data.getIncomingReallocatedWorkload()[i]+",";
//
//				// ==== output ====
//				toPrint += data.getTransactionEnergy()[i] + ",";
//				toPrint += data.getBatteryLevel()[i] + ",";
//				toPrint += data.getTesLevel()[i] + ",";
//				toPrint += data.getDischargeBattery()[i] + "," + data.getChargeBattery()[i] + ",";
//				toPrint += data.getDischargeTes()[i] + "," + data.getChargeTes()[i] + ",";
//				toPrint += data.getEstimatedDelayExecution()[i] + ",";
//				toPrint += data.getItCooling()[i] + ",";
//				toPrint += data.getFinalCooling()[i] + ",";
//
//				writer.println(toPrint);
//			}
//
//			String toPrint = "";
//			writer.println("MAX_T, " + data.getTimePeriod()[0] + ",");
//			writer.println("MAX_TR, " + data.getTransactionEnergyLimit()[0] + ",");
//			writer.println("f," + data.getFactor()[0] + ",");
//			writer.println("battery_damage," + data.getBatteryDamage()[0] + ",");
//			writer.println("transaction_fee," + data.getTransactionFee()[0] + ",");
//			writer.println("E_S," + data.getBatteryMaxCapacity()[0] + ",");
//			writer.println("f_TES," + data.getFactorTES()[0] + ",");
//			writer.println("COP," + data.getCop()[0] + ",");
//			writer.println("TES_MAX," + data.getTesMaxCapacity()[0] + ",");
//			writer.println("TES_COST," + data.getTesDamage()[0] + ",");
//			writer.println("TES_R," + data.getTesChargeLossRate()[0] + ",");
//			writer.println("TES_D," + data.getTesDischargeLossRate()[0] + ",");
//			writer.println("ESD_R," + data.getBatteryChargeLossRate()[0] + ",");
//			writer.println("ESD_D," + data.getBatteryDischargeLossRate()[0] + ",");
//			writer.println("MAX_DIS_TES," + data.getTesMaxDischargeRate()[0] + ",");
//			writer.println("MAX_DIS," + data.getBatteryMaxDischargeRate()[0] + ",");
//			writer.println("Dod," + data.getDod()[0] + ",");
//			writer.println("MAX_IT," + data.getItMaxConsumption()[0] + ",");
//			writer.println("E_D_Percentage," + data.getEdPercentage()[0] + ",");
//			writer.println("Peak_Charge," + data.getPeakCharge()[0] + ",");
//			writer.println("Egr_Active," + data.getGreenEnergyActive()[0] + ",");
//			writer.println("Econtract_active," + data.getContractEnergyActive()[0] + ",");
//			writer.println("Etr_active," + data.getTransactionEnergyActive()[0] + ",");
//			writer.println("E_usage," + data.getMarketplaceGreenEnergyActive()[0] + ",");
//			writer.println("Diesel_activ," + data.getDieselGeneratorsActive()[0] + ",");
//			writer.println("Diesel_cap," + data.getDieselMaxCapacity()[0] + ",");
//			writer.println("E_operational," + data.getOperationalOptimizationActiv()[0] + ",");
//			writer.println("ESD_INIT," + data.getBatteryInitialValue()[0] + ",");
//			writer.println("TES_INIT," + data.getTesInitialValue()[0] + ",");
//			writer.println("ESD_FIN," + data.getBatteryFinalConstraint()[0] + ",");
//			writer.println("TES_FIN," + data.getTesFinalConstraint()[0] + ",");
//			writer.println(toPrint);
//
//			writer.println();
//			writer.println("Delayed Percentages:");
//			double[] y = data.getSchedulingMatrix();
//			int size = (int) Math.sqrt(y.length);
//
//			for (int i = 0; i < size; i++) {
//				String line = "";
//				for (int j = 0; j < size; j++) {
//					line += y[i * size + j] + ",";
//				}
//				writer.println(line);
//			}
//
//			writer.println();
//			writer.println("Realloc Matrix:");
//			writer.println();
//			double[] migration  = data.getMigrationSchedulingMatrix();
//			int length = migration.length;
//			 size = length/6;
//
//			for (int i = 0; i < size; i++) {
//				String line = "";
//				for (int j = 0; j < 6; j++) {
//					line += migration[i * 6 + j] + ",";
//				}
//				writer.println(line);
//			}
//
//			writer.println();
//			writer.println("Prices: ");
//			writer.println();
//			double[] prices  = data.getHostingPrices();
//			int lengthPrices = prices.length;
//			 size = lengthPrices/24;
//
//			for (int i = 0; i < size; i++) {
//				String line = "";
//				for (int j = 0; j < 24; j++) {
//					line += prices[i * 24 + j] + ",";
//				}
//				writer.println(line);
//			}
//
//			writer.close();
//		} catch (FileNotFoundException e) {
//			LOGGER.error("", e);
//		} catch (UnsupportedEncodingException e) {
//			LOGGER.error("", e);
//		}
//	}
//
//	public static void writeDataToCSVFileAS(OptimizationData data, String fileName) {
//		PrintWriter writer;
//		try {
//			writer = new PrintWriter(fileName, "UTF-8");
//			writer.println(
//					"Econtract,   E_R, E_D,TD," + "Es, Tes, D-ESD, R_ESD, D_TES, R_TES, E_de, ITCooling, Final Cooling,"
//							+ "DCFinalConsumption, DieselGen, finalDCPowerFactor");
//			for (int i = 0; i < data.getSize(); i++) {
//				String toPrint = "";
//				if (i < 48) {
//					toPrint += data.getContractedEnergy()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 8) {
//					toPrint += data.getRealtimeEnergy()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 4) {
//					toPrint += data.getDelayTolerableEnergy()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 4) {
//					toPrint += data.getDelayDeadline()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				// ==== output ====
//				if (i < 48) {
//					toPrint += data.getBatteryLevel()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 8) {
//					toPrint += data.getTesLevel()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 48) {
//					toPrint += data.getDischargeBattery()[i] + "," + data.getChargeBattery()[i] + ",";
//				} else {
//					toPrint += " , ,";
//				}
//				if (i < 8) {
//					toPrint += data.getDischargeTes()[i] + "," + data.getChargeTes()[i] + ",";
//				} else {
//					toPrint += " , ,";
//				}
//				if (i < 4) {
//					toPrint += data.getEstimatedDelayExecution()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 8) {
//					toPrint += data.getItCooling()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 8) {
//					toPrint += data.getFinalCooling()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 48) {
//					toPrint += data.getDcFinalConsumption()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 4) {
//					toPrint += data.getEnableDieselGen()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				if (i < 8) {
//					toPrint += data.getFinalDCPowerFactor()[i] + ",";
//				} else {
//					toPrint += " ,";
//				}
//				writer.println(toPrint);
//			}
//
//			String toPrint = "";
//			writer.println("MAX_T, " + data.getTimePeriod()[0] + ",");
//			// writer.println("MAX_TR, " + data.getTransactionEnergyLimit()[0] +
//			// ",")
//			writer.println("f," + data.getFactor()[0] + ",");
//			// writer.println("battery_damage," + data.getBatteryDamage()[0] +
//			// ",")
//			// writer.println("transaction_fee," + data.getTransactionFee()[0] +
//			// ",")
//			writer.println("E_S," + data.getBatteryMaxCapacity()[0] + ",");
//			writer.println("f_TES," + data.getFactorTES()[0] + ",");
//			writer.println("COP," + data.getCop()[0] + ",");
//			writer.println("TES_MAX," + data.getTesMaxCapacity()[0] + ",");
//			// writer.println("TES_COST," + data.getTesDamage()[0] + ",")
//			writer.println("TES_R," + data.getTesChargeLossRate()[0] + ",");
//			writer.println("TES_D," + data.getTesDischargeLossRate()[0] + ",");
//			writer.println("ESD_R," + data.getBatteryChargeLossRate()[0] + ",");
//			writer.println("ESD_D," + data.getBatteryDischargeLossRate()[0] + ",");
//			writer.println("MAX_DIS_TES," + data.getTesMaxDischargeRate()[0] + ",");
//			writer.println("MAX_DIS," + data.getBatteryMaxDischargeRate()[0] + ",");
//			writer.println("Dod," + data.getDod()[0] + ",");
//			writer.println("MAX_IT," + data.getItMaxConsumption()[0] + ",");
//			// writer.println("E_D_Percentage," + data.getEdPercentage()[0] +
//			// ",")
//			// writer.println("Peak_Charge," + data.getPeakCharge()[0] + ",")
//			// writer.println("Egr_Active," + data.getGreenEnergyActive()[0] +
//			// ",")
//			// writer.println("Econtract_active," +
//			// data.getContractEnergyActive()[0] + ",")
//			// writer.println("Etr_active," +
//			// data.getTransactionEnergyActive()[0] + ",")
//			// writer.println("E_usage," +
//			// data.getMarketplaceGreenEnergyActive()[0] + ",")
//			writer.println("Diesel_activ," + data.getDieselGeneratorsActive()[0] + ",");
//			writer.println("Diesel_cap," + data.getDieselMaxCapacity()[0] + ",");
//			// writer.println("E_operational," +
//			// data.getOperationalOptimizationActiv()[0] + ",")
//			writer.println("ESD_INIT," + data.getBatteryInitialValue()[0] + ",");
//			writer.println("TES_INIT," + data.getTesInitialValue()[0] + ",");
//			writer.println("ESD_FIN," + data.getBatteryFinalConstraint()[0] + ",");
//			writer.println("TES_FIN," + data.getTesFinalConstraint()[0] + ",");
//			writer.println(toPrint);
//
//			writer.println();
//			double[] y = data.getSchedulingMatrix();
//			int size = (int) Math.sqrt(y.length);
//
//			for (int i = 0; i < size; i++) {
//				String line = "";
//				for (int j = 0; j < size; j++) {
//					line += y[i * size + j] + ",";
//				}
//				writer.println(line);
//			}
//
//			writer.close();
//		} catch (FileNotFoundException e) {
//			LOGGER.error("", e);
//		} catch (UnsupportedEncodingException e) {
//			LOGGER.error("", e);
//		}
//	}
//
//	public static void writeActionsToCSVFile(Map<Interval, List<EnergyEfficiencyOptimizationAction>> data,
//			String fileName) {
//		List<EnergyEfficiencyOptimizationAction> dtwActions = new ArrayList<EnergyEfficiencyOptimizationAction>();
//
//		try(PrintWriter	writer = new PrintWriter(fileName, "UTF-8")) {
//			writer.println("from, to,start, end, percentage, value");
//			for (List<EnergyEfficiencyOptimizationAction> actions : data.values()) {
//				for (EnergyEfficiencyOptimizationAction action : actions) {
//					if (action instanceof ShiftDelayTolerantWorkload) {
//						ShiftDelayTolerantWorkload sdtw = (ShiftDelayTolerantWorkload) action;
//						writer.print(sdtw.getFromTime() + ",");
//						writer.print(sdtw.getToTime() + ",");
//						writer.print(sdtw.getStartTime() + ",");
//						writer.print(sdtw.getEndTime() + ",");
//						writer.print(sdtw.getPercentOfWorkloadDelayed() + ",");
//						writer.print(sdtw.getAmountOfEnergy() + ",");
//						writer.println();
//						dtwActions.add(sdtw);
//					}
//
//					if (action instanceof WorkloadRelocation) {
//						WorkloadRelocation sdtw = (WorkloadRelocation) action;
//						writer.print("-" + ",");
//						writer.print(sdtw.getDestinationDC()+ ",");
//						writer.print(sdtw.getStartTime() + ",");
//						writer.print(sdtw.getEndTime() + ",");
//						writer.print(sdtw.getPercentOfWorkloadReallocated() + ",");
//						writer.print(sdtw.getAmountOfEnergy() + ",");
//						writer.println();
//						dtwActions.add(sdtw);
//					}
//				}
//			}
//
//			writer.println();
//			LOGGER.info("Actions in file!");
//		} catch (Exception ex) {
//			LOGGER.error("", ex);
//		}
//		// OptimizationPlan plan = new OptimizationPlan(0, dtwActions)
//		// RestAPIClient.postObject(DataModelAPIURL.ACTIONS_DT, plan,
//		// Void.class)
//	}

	public static class LingoConfigData {
		private double timePeriod;// MAX_T // 8
		private double transactionEnergyLimit;// MAX_TR // 9
		private double factor;// f // 10
		private double batteryDamage; // battery_damage 11
		private double transactionFee; // transaction_fee 12
		private double batteryMaxCapacity;// E_S // 13
		private double factorTes;// f_TES // 14
		private double cop; // 15
		private double tesMaxCapacity;// TES_MAX // 16
		private double tesDamage; // TES_COST 17
		private double tesChargeLossRate;// TES_R // 18
		private double tesDischargeLossRate;// TES_D // 19
		private double batteryChargeLossRate; // ESD_R// 20
		private double batteryDischargeLossRate;// ESD_D // 21
		private double tesMaxDischargeRate;// MAX_DIS_TES // 22
		private double batteryMaxDischargeRate;// MAX_DIS // 23
		private double dod; // 24
		private double itMaxConsumption;// MAX_IT // 25
		private double edPercentage;// E_D_Percentage // 26
		private double peakCharge;// Peak_Charge // 27
		private double dieselMaxCapacity;// Diesel_Cap // 28
		private double contractEnergyActive;// Econtract_active // 29
		private double greenEnergyActive; // Egr_active// 30
		private double transactionEnergyActive;// Etr_active // 31
		private double marketplaceGreenEnergyActive;// E_usage // 32
		private double dieselGeneratorsActive;// Diesel_activ // 33
		private double batteryInitialValue;// ESD_INI // 34
		private double tesInitialValue;// TES_INI // 35
		private double operationalOptimizationActiv;// E_Operational // 36
		private double batteryFinalConstraint; // ESD_FIN// 50
		private double tesFinalConstraint;// TES_FIN // 51

		public LingoConfigData() {

		}

		public double getTimePeriod() {
			return timePeriod;
		}

		public void setTimePeriod(double timePeriod) {
			this.timePeriod = timePeriod;
		}

		public double getTransactionEnergyLimit() {
			return transactionEnergyLimit;
		}

		public void setTransactionEnergyLimit(double transactionEnergyLimit) {
			this.transactionEnergyLimit = transactionEnergyLimit;
		}

		public double getFactor() {
			return factor;
		}

		public void setFactor(double factor) {
			this.factor = factor;
		}

		public double getBatteryDamage() {
			return batteryDamage;
		}

		public void setBatteryDamage(double batteryDamage) {
			this.batteryDamage = batteryDamage;
		}

		public double getTransactionFee() {
			return transactionFee;
		}

		public void setTransactionFee(double transactionFee) {
			this.transactionFee = transactionFee;
		}

		public double getBatteryMaxCapacity() {
			return batteryMaxCapacity;
		}

		public void setBatteryMaxCapacity(double batteryMaxCapacity) {
			this.batteryMaxCapacity = batteryMaxCapacity;
		}

		public double getFactorTes() {
			return factorTes;
		}

		public void setFactorTes(double factorTes) {
			this.factorTes = factorTes;
		}

		public double getCop() {
			return cop;
		}

		public void setCop(double cop) {
			this.cop = cop;
		}

		public double getTesMaxCapacity() {
			return tesMaxCapacity;
		}

		public void setTesMaxCapacity(double tesMaxCapacity) {
			this.tesMaxCapacity = tesMaxCapacity;
		}

		public double getTesDamage() {
			return tesDamage;
		}

		public void setTesDamage(double tesDamage) {
			this.tesDamage = tesDamage;
		}

		public double getTesChargeLossRate() {
			return tesChargeLossRate;
		}

		public void setTesChargeLossRate(double tesChargeLossRate) {
			this.tesChargeLossRate = tesChargeLossRate;
		}

		public double getTesDischargeLossRate() {
			return tesDischargeLossRate;
		}

		public void setTesDischargeLossRate(double tesDischargeLossRate) {
			this.tesDischargeLossRate = tesDischargeLossRate;
		}

		public double getBatteryChargeLossRate() {
			return batteryChargeLossRate;
		}

		public void setBatteryChargeLossRate(double batteryChargeLossRate) {
			this.batteryChargeLossRate = batteryChargeLossRate;
		}

		public double getBatteryDischargeLossRate() {
			return batteryDischargeLossRate;
		}

		public void setBatteryDischargeLossRate(double batteryDischargeLossRate) {
			this.batteryDischargeLossRate = batteryDischargeLossRate;
		}

		public double getTesMaxDischargeRate() {
			return tesMaxDischargeRate;
		}

		public void setTesMaxDischargeRate(double tesMaxDischargeRate) {
			this.tesMaxDischargeRate = tesMaxDischargeRate;
		}

		public double getBatteryMaxDischargeRate() {
			return batteryMaxDischargeRate;
		}

		public void setBatteryMaxDischargeRate(double batteryMaxDischargeRate) {
			this.batteryMaxDischargeRate = batteryMaxDischargeRate;
		}

		public double getDod() {
			return dod;
		}

		public void setDod(double dod) {
			this.dod = dod;
		}

		public double getItMaxConsumption() {
			return itMaxConsumption;
		}

		public void setItMaxConsumption(double itMaxConsumption) {
			this.itMaxConsumption = itMaxConsumption;
		}

		public double getEdPercentage() {
			return edPercentage;
		}

		public void setEdPercentage(double edPercentage) {
			this.edPercentage = edPercentage;
		}

		public double getPeakCharge() {
			return peakCharge;
		}

		public void setPeakCharge(double peakCharge) {
			this.peakCharge = peakCharge;
		}

		public double getDieselMaxCapacity() {
			return dieselMaxCapacity;
		}

		public void setDieselMaxCapacity(double dieselMaxCapacity) {
			this.dieselMaxCapacity = dieselMaxCapacity;
		}

		public double getContractEnergyActive() {
			return contractEnergyActive;
		}

		public void setContractEnergyActive(double contractEnergyActive) {
			this.contractEnergyActive = contractEnergyActive;
		}

		public double getGreenEnergyActive() {
			return greenEnergyActive;
		}

		public void setGreenEnergyActive(double greenEnergyActive) {
			this.greenEnergyActive = greenEnergyActive;
		}

		public double getTransactionEnergyActive() {
			return transactionEnergyActive;
		}

		public void setTransactionEnergyActive(double transactionEnergyActive) {
			this.transactionEnergyActive = transactionEnergyActive;
		}

		public double getMarketplaceGreenEnergyActive() {
			return marketplaceGreenEnergyActive;
		}

		public void setMarketplaceGreenEnergyActive(double marketplaceGreenEnergyActive) {
			this.marketplaceGreenEnergyActive = marketplaceGreenEnergyActive;
		}

		public double getDieselGeneratorsActive() {
			return dieselGeneratorsActive;
		}

		public void setDieselGeneratorsActive(double dieselGeneratorsActive) {
			this.dieselGeneratorsActive = dieselGeneratorsActive;
		}

		public double getBatteryInitialValue() {
			return batteryInitialValue;
		}

		public void setBatteryInitialValue(double batteryInitialValue) {
			this.batteryInitialValue = batteryInitialValue;
		}

		public double getTesInitialValue() {
			return tesInitialValue;
		}

		public void setTesInitialValue(double tesInitialValue) {
			this.tesInitialValue = tesInitialValue;
		}

		public double getOperationalOptimizationActiv() {
			return operationalOptimizationActiv;
		}

		public void setOperationalOptimizationActiv(double operationalOptimizationActiv) {
			this.operationalOptimizationActiv = operationalOptimizationActiv;
		}

		public double getBatteryFinalConstraint() {
			return batteryFinalConstraint;
		}

		public void setBatteryFinalConstraint(double batteryFinalConstraint) {
			this.batteryFinalConstraint = batteryFinalConstraint;
		}

		public double getTesFinalConstraint() {
			return tesFinalConstraint;
		}

		public void setTesFinalConstraint(double tesFinalConstraint) {
			this.tesFinalConstraint = tesFinalConstraint;
		}

	}

	public static class ExcelData {
		private double realtimeEnergy;
		private double delayTEnergy;
		private double contractedEnergy;
		private double greenEnergy;
		private double energyPrice;
		private double marketEnergy;
		private double delayDeadline;
		private double transactionedEnergy;
		private double tesLevel;
		private double dischergeTes;
		private double chargeTes;
		private double batteryLevel;
		private double dischargeBattery;
		private double chargeBattery;
		private double estimatedDelayExecution;
		private double itCool;
		private double cool;

		public ExcelData(double rEnergy, double dEnergy, double contractEnergy, double greenEnergy, double energyPrice,
				double marketEnergy, double delayDeadline, double etr, double tES, double dischargeTEs,
				double chargeTes, double eS, double dischargeBattery, double chargeBattery, double estimatedDelay,
				double itCool, double cOOL) {
			this.realtimeEnergy = rEnergy;
			this.delayTEnergy = dEnergy;
			this.contractedEnergy = contractEnergy;
			this.greenEnergy = greenEnergy;
			this.energyPrice = energyPrice;
			this.marketEnergy = marketEnergy;
			this.delayDeadline = delayDeadline;
			this.transactionedEnergy = etr;
			this.tesLevel = tES;
			this.dischergeTes = dischargeTEs;
			this.chargeTes = chargeTes;
			this.batteryLevel = eS;
			this.dischargeBattery = dischargeBattery;
			this.chargeBattery = chargeBattery;
			this.estimatedDelayExecution = estimatedDelay;
			this.itCool = itCool;
			this.cool = cOOL;
		}

		public double getRealTimeEnergy() {
			return realtimeEnergy;
		}

		public double getDelayEnergy() {
			return delayTEnergy;
		}

		public double getEContract() {
			return contractedEnergy;
		}

		public double getGreenEnergy() {
			return greenEnergy;
		}

		public double getEtr() {
			return transactionedEnergy;
		}

		public double getTES() {
			return tesLevel;
		}

		public double getDischargeTES() {
			return dischergeTes;
		}

		public double getChargeTES() {
			return chargeTes;
		}

		public double getES() {
			return batteryLevel;
		}

		public double getDischargeBattery() {
			return dischargeBattery;
		}

		public double getChargeBattery() {
			return chargeBattery;
		}

		public double getEstimatedDelay() {
			return estimatedDelayExecution;
		}

		public double getTotalCool() {
			return itCool;
		}

		public double getCOOL() {
			return cool;
		}

		public double getPgr() {
			return energyPrice;
		}

		public double getMarketEnergy() {
			return marketEnergy;
		}

		public double getRealtimeEnergy() {
			return realtimeEnergy;
		}

		public void setRealtimeEnergy(double realtimeEnergy) {
			this.realtimeEnergy = realtimeEnergy;
		}

		public double getDelayTEnergy() {
			return delayTEnergy;
		}

		public void setDelayTEnergy(double delayTEnergy) {
			this.delayTEnergy = delayTEnergy;
		}

		public double getContractedEnergy() {
			return contractedEnergy;
		}

		public void setContractedEnergy(double contractedEnergy) {
			this.contractedEnergy = contractedEnergy;
		}

		public double getEnergyPrice() {
			return energyPrice;
		}

		public void setEnergyPrice(double energyPrice) {
			this.energyPrice = energyPrice;
		}

		public double getTransactionedEnergy() {
			return transactionedEnergy;
		}

		public void setTransactionedEnergy(double transactionedEnergy) {
			this.transactionedEnergy = transactionedEnergy;
		}

		public double getTesLevel() {
			return tesLevel;
		}

		public void setTesLevel(double tesLevel) {
			this.tesLevel = tesLevel;
		}

		public double getDischergeTes() {
			return dischergeTes;
		}

		public void setDischergeTes(double dischergeTes) {
			this.dischergeTes = dischergeTes;
		}

		public double getChargeTes() {
			return chargeTes;
		}

		public void setChargeTes(double chargeTes) {
			this.chargeTes = chargeTes;
		}

		public double getBatteryLevel() {
			return batteryLevel;
		}

		public void setBatteryLevel(double batteryLevel) {
			this.batteryLevel = batteryLevel;
		}

		public double getEstimatedDelayExecution() {
			return estimatedDelayExecution;
		}

		public void setEstimatedDelayExecution(double estimatedDelayExecution) {
			this.estimatedDelayExecution = estimatedDelayExecution;
		}

		public double getItCool() {
			return itCool;
		}

		public void setItCool(double itCool) {
			this.itCool = itCool;
		}

		public double getCool() {
			return cool;
		}

		public void setCool(double cool) {
			this.cool = cool;
		}

		public void setGreenEnergy(double greenEnergy) {
			this.greenEnergy = greenEnergy;
		}

		public void setMarketEnergy(double marketEnergy) {
			this.marketEnergy = marketEnergy;
		}

		public void setDischargeBattery(double dischargeBattery) {
			this.dischargeBattery = dischargeBattery;
		}

		public void setChargeBattery(double chargeBattery) {
			this.chargeBattery = chargeBattery;
		}

		public double getDelayDeadline() {
			return delayDeadline;
		}

		public void setDelayDeadline(double delayDeadline) {
			this.delayDeadline = delayDeadline;
		}

	}

}
