package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic;

public interface Solution extends Comparable<Solution> {
    void initialize();

    void rank();

    Solution crossover(Solution part);

    Solution mutate();

    Solution newInstance();
}
