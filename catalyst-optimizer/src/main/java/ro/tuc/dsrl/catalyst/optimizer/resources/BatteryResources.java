package ro.tuc.dsrl.catalyst.optimizer.resources;


import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;

import java.util.ArrayList;
import java.util.List;



/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Jan 30, 2015
 * @Description:
 *
 */
public class BatteryResources {

	private List<Battery> batteries;
	public static final double BATTERY_DAMAGE = 0.45;
	public static final double BATTERY_DOD = 0.4;

	public BatteryResources() {
		this.batteries = new ArrayList<>();
	}
	public double getMaximumCapacity() {
		double maximumCapacity = 0.0;
		for (Battery battery : batteries) {
			maximumCapacity += battery.getMaximumCapacity();
		}

		return maximumCapacity;
	}

	public double getFactorEsd() {
		if(batteries.size() ==0){
			return 0;
		}
		double factorEsd = 0.0;
		for (Battery battery : batteries) {
			factorEsd += battery.getEsdFactor();
		}

		return factorEsd / batteries.size();
	}

	public double getDod() {
		if(batteries.size() ==0){
			return 0;
		}
		double dod = 0.0;
		for (Battery battery : batteries) {
			dod += battery.getDod();
		}

		return dod / batteries.size();
	}


	public double getActualLoadedCapacity() {
		double actualLoadedCapacity = 0.0;
		for (Battery battery : batteries) {
			actualLoadedCapacity += battery.getActualLoadedCapacity();
		}

		return actualLoadedCapacity;
	}

	public double getDischargeLossRate() {
		if(batteries.size() ==0){
			return 0;
		}
		double dischargeLossRate = 0.0;
		for (Battery battery : batteries) {
			dischargeLossRate += battery.getDischargeLossRate();
		}
		return dischargeLossRate / (double) batteries.size();
	}

	public double getChargeLossRate() {
		if(batteries.size() ==0){
			return 0;
		}
		double chargeLossRate = 0.0;
		for (Battery battery : batteries) {
			chargeLossRate += battery.getChargeLossRate();
		}
		return chargeLossRate / (double) batteries.size();
	}

	public double getMaxDischargeRate() {
		double maxDischargeRate = 0.0;
		for (Battery battery : batteries) {
			maxDischargeRate += battery.getMaxDischargeRate();
		}
		return maxDischargeRate;
	}

	public double getMaxChargeRate() {
		double maxChargeRate = 0.0;
		for (Battery battery : batteries) {
			maxChargeRate += battery.getMaxChargeRate();
		}
		return maxChargeRate;
	}

	/**
	 * @return the batteries
	 */
	public List<Battery> getBatteries() {
		return batteries;
	}

	/**
	 * @param batteries
	 *            the batteries to set
	 */
	public void setBatteries(List<Battery> batteries) {
		this.batteries = batteries;
	}

}
