package ro.tuc.dsrl.catalyst.optimizer.planner;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import ro.tuc.dsrl.geyser.datamodel.actions.*;

import java.util.*;


/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Dec 16, 2014
 * @Description: ActionPlan stores and manages the action pool computed by the
 *               planning algorithms.
 *
 */
public class ActionPlan {
	private long id;
	private Map<Interval, List<EnergyEfficiencyOptimizationAction>> actionPool;
	private Map<Interval, List<EnergyEfficiencyOptimizationAction>> correlatedActionPool;
	// resource constraint at the start of the DateTime
	// ex: hour 4 = hour 3 + actions on [3->4]
	private Map<DateTime, ResourceConstraints> constraints;

	private double confidenceLevel;
	private double WE;
	private double WL;
	private double WF;
	private double WT;
	private double WRen;
	private boolean reallocActive;


	public ActionPlan() {
		id = 0;
		actionPool = new HashMap<Interval, List<EnergyEfficiencyOptimizationAction>>();
		correlatedActionPool = new HashMap<Interval, List<EnergyEfficiencyOptimizationAction>>();
		constraints = new HashMap<DateTime, ResourceConstraints>();
	}


	public ActionPlan(long id, Map<Interval, List<EnergyEfficiencyOptimizationAction>> actionPool,
			Map<DateTime, ResourceConstraints> constraints) {
		this.id = id;
		this.actionPool = actionPool;
		this.constraints = constraints;
		this.correlatedActionPool = new HashMap<Interval, List<EnergyEfficiencyOptimizationAction>>();
	}

	public ActionPlan(long id, Map<Interval, List<EnergyEfficiencyOptimizationAction>> actionPool,
					  Map<Interval, List<EnergyEfficiencyOptimizationAction>> correlatedActionPool,
					  Map<DateTime, ResourceConstraints> constraints) {
		this.id = id;
		this.actionPool = actionPool;
		this.constraints = constraints;
		this.correlatedActionPool = correlatedActionPool;
	}

	public void addResourceConstraint(DateTime date, ResourceConstraints constraints) {
		this.constraints.put(date, constraints);
	}

	/**
	 * @param currentDate
	 * @return A list of EnergyEfficiencyOptimizationAction for the currentDate
	 */
	public List<EnergyEfficiencyOptimizationAction> getCurrentActions(DateTime currentDate) {
		List<EnergyEfficiencyOptimizationAction> list = new ArrayList<EnergyEfficiencyOptimizationAction>();
		for (Interval time : actionPool.keySet()) {
			if ((currentDate.isAfter(time.getStart()) || currentDate.equals(time.getStart()))
					&& currentDate.isBefore(time.getEnd())) {
				list.addAll(getActions(time));
			}
		}
		return list;
	}

	/**
	 * @param currentDate
	 * @return A list of EnergyEfficiencyOptimizationAction having startDate
	 *         equal to currentDate
	 */
	public List<EnergyEfficiencyOptimizationAction> getActionsByStartDate(DateTime currentDate) {
		List<EnergyEfficiencyOptimizationAction> list = new ArrayList<EnergyEfficiencyOptimizationAction>();
		for (Interval time : actionPool.keySet()) {
			if (currentDate.equals(time.getStart())) {
				list.addAll(getActions(time));
			}
		}
		return list;
	}

	/**
	 * SDTWActions delayed from past (interval given by initialization Interval)
	 * to the time currentTime
	 * 
	 * @param currentTime
	 * @return
	 */
	public List<ShiftDelayTolerantWorkload> getSDTWActionsDelayedToCurrentTime(Interval intialization,
																			   DateTime currentTime) {
		List<ShiftDelayTolerantWorkload> delayedActions = new ArrayList<ShiftDelayTolerantWorkload>();
		for (Interval time : actionPool.keySet()) {
			if (intialization.contains(time.getStart())
					&& (intialization.contains(time.getEnd()) || intialization.getEnd().equals(time.getEnd()))) {
				for (EnergyEfficiencyOptimizationAction action : getActions(time)) {
					if (action instanceof ShiftDelayTolerantWorkload) {
						DateTime toTime = new DateTime(((ShiftDelayTolerantWorkload) action).getToTime());
						if (currentTime.equals(toTime)) {
							delayedActions.add((ShiftDelayTolerantWorkload) action);
						}
					}
				}
			}
		}
		return delayedActions;
	}

	/**
	 * returns all the SDTWactions that were created at fromTime and were
	 * already executed before currentTime
	 * 
	 * @param fromTime
	 * @param currentTime
	 * @return
	 */
	public List<ShiftDelayTolerantWorkload> getSDTWExecutedBeforeCurrentTime(DateTime fromTime, DateTime currentTime) {
		List<EnergyEfficiencyOptimizationAction> list = getActions(new Interval(fromTime, fromTime.plusHours(1)));
		List<ShiftDelayTolerantWorkload> toReturn = new ArrayList<ShiftDelayTolerantWorkload>();
		Interval interval = new Interval(fromTime, currentTime);
		for (EnergyEfficiencyOptimizationAction action : list) {
			if (action instanceof ShiftDelayTolerantWorkload) {
				ShiftDelayTolerantWorkload sdtw = (ShiftDelayTolerantWorkload) action;
				if (interval.contains(sdtw.getToTime().getTime())) {
					toReturn.add(sdtw);
				}
			}
		}

		return toReturn;
	}

	/**
	 * @param currentDate
	 * @param toRemove
	 */
	public void deleteActions(DateTime currentDate, List<EnergyEfficiencyOptimizationAction> toRemove) {
		for (Interval time : actionPool.keySet()) {
			if ((currentDate.isAfter(time.getStart()) || currentDate.equals(time.getStart()))
					&& currentDate.isBefore(time.getEnd())) {
				Iterator<EnergyEfficiencyOptimizationAction> it = toRemove.iterator();
				while (it.hasNext()) {
					actionPool.get(time).remove(it.next());
				}
			}
		}
	}

	/**
	 * Method that merges the two action pools by the following rules: 1. if an
	 * entry contains actions it is overridden 2. if there are no actions for an
	 * entry, we add them
	 */
	public void mergeActionPlans(ActionPlan actionPlan) {
		Set<Interval> keySet = actionPlan.getActionPool().keySet();
		Iterator<Interval> timeIterator = keySet.iterator();
		while (timeIterator.hasNext()) {
			Interval time = timeIterator.next();
			List<EnergyEfficiencyOptimizationAction> actionList = actionPlan.getActionPool().get(time);
			addEntry(time, actionList);
		}
		this.constraints.putAll(actionPlan.getConstraints());
	}

	/**
	 * @param date
	 * @return
	 */
	public ResourceConstraints getResourceConstraintsByDate(DateTime date) {
		return constraints.get(date);
	}

	/**
	 * place in the map at the date entry the list array and OVERWRITE the old
	 * values, if they exist
	 * */
	public void putEntry(Interval date, List<EnergyEfficiencyOptimizationAction> list) {
		actionPool.put(date, list);
	}
	public void putEntryCorrelated(Interval date, List<EnergyEfficiencyOptimizationAction> list) {
		correlatedActionPool.put(date, list);
	}

	/**
	 * place in the map at the date entry the list array by adding it after the
	 * old values, if they exist
	 * */
	public void addEntry(Interval date, List<EnergyEfficiencyOptimizationAction> list) {
		List<EnergyEfficiencyOptimizationAction> actionList = actionPool.get(date);
		if (actionList == null) {
			actionList = new ArrayList<EnergyEfficiencyOptimizationAction>();
			actionPool.put(date, actionList);
		}
		actionList.addAll(list);
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the actionPool
	 */
	public Map<Interval, List<EnergyEfficiencyOptimizationAction>> getActionPool() {
		return actionPool;
	}

	/**
	 * @param actionPool
	 *            the actionPool to set
	 */
	public void setActionPool(Map<Interval, List<EnergyEfficiencyOptimizationAction>> actionPool) {
		this.actionPool = actionPool;
	}

	/**
	 * @return the constraints
	 */
	public Map<DateTime, ResourceConstraints> getConstraints() {
		return constraints;
	}

	/**
	 * @param constraints
	 *            the constraints to set
	 */
	public void setConstraints(Map<DateTime, ResourceConstraints> constraints) {
		this.constraints = constraints;
	}

	// dynamicallyadj of cooling intensity
	public ActionPlan getDeletedActions(Interval window) {

		Map<Interval, List<EnergyEfficiencyOptimizationAction>> mapToBeDeleted = new HashMap<Interval, List<EnergyEfficiencyOptimizationAction>>();

		for (DateTime time = window.getStart(); time.isBefore(window.getEnd()); time = time.plusHours(1)) {
			Interval currentInterval = new Interval(time, time.plusHours(1));
			List<EnergyEfficiencyOptimizationAction> currentActions = getActions(currentInterval);
			List<EnergyEfficiencyOptimizationAction> currentToDelete = new ArrayList<EnergyEfficiencyOptimizationAction>();

			for (EnergyEfficiencyOptimizationAction action : currentActions) {
				if (action instanceof EnergyStorageOptimization
						|| action instanceof DynamicAdjustmentOfCoolingIntensity
						|| action instanceof DynamicallyUsingNonElectricalCooling) {
					List<EnergyEfficiencyOptimizationAction> actionList = mapToBeDeleted.get(currentInterval);
					if (actionList == null) {
						actionList = new ArrayList<EnergyEfficiencyOptimizationAction>();
						mapToBeDeleted.put(currentInterval, actionList);
					}
					actionList.add(action);
					currentToDelete.add(action);
				}
			}
			currentActions.removeAll(currentToDelete);
		}
		return new ActionPlan(0, mapToBeDeleted, new HashMap<DateTime, ResourceConstraints>());
	}

	public MarketplaceAction getCurrentMarketplaceAction(DateTime startTime) {
		Interval interval = new Interval(startTime, startTime.plusHours(1));
		List<EnergyEfficiencyOptimizationAction> actions = getActions(interval);
		for (EnergyEfficiencyOptimizationAction action : actions) {
			if (action instanceof MarketplaceAction) {
				return (MarketplaceAction) action;
			}
		}
		return null;
	}

	public List<EnergyStorageOptimization> getEnergyStorageOptimization(DateTime startTime) {
		Interval interval = new Interval(startTime, startTime.plusHours(1));
		List<EnergyStorageOptimization> energyStorageActions = new ArrayList<EnergyStorageOptimization>();
		List<EnergyEfficiencyOptimizationAction> actions = getActions(interval);
		for (EnergyEfficiencyOptimizationAction action : actions) {
			if (action instanceof EnergyStorageOptimization) {
				energyStorageActions.add((EnergyStorageOptimization) action);
			}
		}
		return energyStorageActions;
	}

	/**
	 * @return list of BuyEnergy actions creating an energy plan
	 */
	public List<MarketplaceAction> getBuyEnergyActions() {
		List<MarketplaceAction> buyActions = new ArrayList<>();
		for (Interval time : actionPool.keySet()) {
			for (EnergyEfficiencyOptimizationAction action : actionPool.get(time)) {
				if (action instanceof BuyEnergy) {
					buyActions.add((BuyEnergy) action);
				}
				if (action instanceof BuyHeat) {
					buyActions.add((BuyHeat) action);
				}
			}
		}
		return buyActions;
	}

	/**
	 * @return list of SellEnergy actions creating an energy plan
	 */
	public List<MarketplaceAction> getSellEnergyActions() {
		List<MarketplaceAction> sellActions = new ArrayList<>();
		for (Interval time : actionPool.keySet()) {
			for (EnergyEfficiencyOptimizationAction action : actionPool.get(time)) {
				if (action instanceof SellEnergy) {
					sellActions.add((SellEnergy) action);
				}if (action instanceof SellHeat) {
					sellActions.add((SellHeat) action);
				}

			}
		}
		return sellActions;
	}

	public List<EnergyEfficiencyOptimizationAction> getActions(Interval interval) {
		List<EnergyEfficiencyOptimizationAction> actions = actionPool.get(interval);
		if (actions == null) {
			actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		}
		return actions;
	}


	public Map<Interval, List<EnergyEfficiencyOptimizationAction>> getCorrelatedActionPool() {
		return correlatedActionPool;
	}

	public void setCorrelatedActionPool(Map<Interval, List<EnergyEfficiencyOptimizationAction>> correlatedActionPool) {
		this.correlatedActionPool = correlatedActionPool;
	}
	
	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	public double getWE() {
		return WE;
	}

	public void setWE(double WE) {
		this.WE = WE;
	}

	public double getWL() {
		return WL;
	}

	public void setWL(double WL) {
		this.WL = WL;
	}

	public double getWF() {
		return WF;
	}

	public void setWF(double WF) {
		this.WF = WF;
	}

	public double getWT() {
		return WT;
	}

	public void setWT(double WT) {
		this.WT = WT;
	}

	public void setWRen(double Wren) {this.WRen = Wren;}

	public double getWren() {return this.WRen;}

	public boolean isReallocActive() {
		return reallocActive;
	}

	public void setReallocActive(boolean reallocActive) {
		this.reallocActive = reallocActive;
	}
}
