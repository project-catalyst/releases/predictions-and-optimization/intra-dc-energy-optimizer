package ro.tuc.dsrl.catalyst.optimizer.utility;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.utility.RandomGenerator;

import java.math.BigDecimal;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 4, 2015
 * @Description:
 *
 */
public class MathUtility {


	private static final int EXPONENT_TWO = 2;
	private static final int EXPONENT_FOUR = 4;

	private MathUtility() {
	}



	public static double round2Decimals(double value) {
		return round(value, EXPONENT_TWO);
	}

	public static double round4Decimals(double value) {
		return round(value, EXPONENT_FOUR);
	}

	public static double round(double value, int decimals) {
		BigDecimal bd =  BigDecimal.valueOf(value).setScale(decimals, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

}
