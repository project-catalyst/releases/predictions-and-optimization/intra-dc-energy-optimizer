package ro.tuc.dsrl.catalyst.optimizer.energyefficiencymetricscalculator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import ro.tuc.dsrl.catalyst.model.dto.EnergyPointValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MetricsDTO;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.Timeframe;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.geyser.datamodel.other.MarketPrices;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class FlexibilityMetricsService {
    private static final Log LOGGER = LogFactory.getLog(FlexibilityMetricsService.class);
    private static final String TIMEFRAME_DA = Timeframe.DAY_AHEAD.getName();
    private static final String TIMEFRAME_ID = Timeframe.INTRA_DAY.getName();
    private static final String TIMEFRAME_RT = Timeframe.REAL_TIME.getName();
    private final EnergyProfileRepository energyProfileRepository;
    @Value("${dc.name}")
    private String dcName;


    public FlexibilityMetricsService() {
        this.energyProfileRepository = new EnergyProfileRepository();
    }

    public List<MetricsDTO> getDA( long millis){
        System.out.println(millis);
        LocalDateTime time = DateUtils.millisToLocalDateTime(millis);
        List<MetricsDTO> metrics = new ArrayList<>();
        MetricsDTO pue = this.computePue(time, "dayahead");
        MetricsDTO ere = this.computeEre(time, "dayahead");
        MetricsDTO reusePercent = this.computeReusePercent(time, "dayahead");
        MetricsDTO heatAdapt = this.computeHeatAdaptability(time, "dayahead");
        MetricsDTO co2 = this.computeCO2Emissions(time, "dayahead");
        MetricsDTO dcProfit = this.computeDCProfit(time, "dayahead");
        metrics.add(pue);
        metrics.add(ere);
        metrics.add(reusePercent);
        metrics.add(heatAdapt);
        metrics.add(co2);
        metrics.add(dcProfit);
        return metrics;
    }


    public MetricsDTO computePue(LocalDateTime startTime, String granularity) {
        ForecastDayAheadDTO adaptedServer = energyProfileRepository.getServerAdaptedProfile(startTime, dcName, granularity);
        ForecastDayAheadDTO adaptedCooling = energyProfileRepository.getCoolingAdaptedProfile(startTime,dcName, granularity);

        double value = computePUEValue(adaptedServer.getEnergyPointValueDTOS(), adaptedCooling.getEnergyPointValueDTOS());
        MetricsDTO metric = new MetricsDTO(DateUtils.localDateTimeToLongInMillis(startTime), dcName, MeasurementType.PUE.type(), value, TIMEFRAME_RT);
        return energyProfileRepository.insertMetric(metric);
    }

    public MetricsDTO computeEre(LocalDateTime startTime, String granularity) {
        ForecastDayAheadDTO adaptedServer = energyProfileRepository.getServerAdaptedProfile(startTime, dcName, granularity);
        ForecastDayAheadDTO adaptedCooling = energyProfileRepository.getCoolingAdaptedProfile(startTime,dcName, granularity);
        ForecastDayAheadDTO adaptedHeat = energyProfileRepository.getHeatAdaptedProfile(startTime,dcName, granularity);

        double value = computeEREValue(adaptedServer.getEnergyPointValueDTOS(), adaptedCooling.getEnergyPointValueDTOS(), adaptedHeat.getEnergyPointValueDTOS());
        MetricsDTO metric = new MetricsDTO(DateUtils.localDateTimeToLongInMillis(startTime), dcName, MeasurementType.ERE.type(), value, TIMEFRAME_ID);
        return energyProfileRepository.insertMetric(metric);
    }

    public MetricsDTO computeDCAdapt(LocalDateTime startTime, String granularity) {
        ForecastDayAheadDTO baseline = energyProfileRepository.getDCBaselineProfile(startTime, dcName, granularity);
        EnergyProfileDTO monitored = energyProfileRepository.getMonitoredConsumptionProfile(startTime, dcName, granularity);

        double kDCA = computeKdca(baseline.getEnergyPointValueDTOS(), monitored.getCurve());
        double value = 1 - computeDCAMetric(kDCA, baseline.getEnergyPointValueDTOS(), monitored.getCurve());
        MetricsDTO metric = new MetricsDTO(DateUtils.localDateTimeToLongInMillis(startTime), dcName, MeasurementType.DCAdapt.type(), value, TIMEFRAME_ID);
        return energyProfileRepository.insertMetric(metric);
    }

    public MetricsDTO computeReusePercent(LocalDateTime startTime, String granularity) {
        ForecastDayAheadDTO adaptedServer = energyProfileRepository.getServerAdaptedProfile(startTime, dcName, granularity);
        ForecastDayAheadDTO adaptedCooling = energyProfileRepository.getCoolingAdaptedProfile(startTime,dcName, granularity);
        ForecastDayAheadDTO adaptedHeat = energyProfileRepository.getHeatAdaptedProfile(startTime,dcName, granularity);

        double value = computeReusePercentValue(adaptedServer.getEnergyPointValueDTOS(), adaptedCooling.getEnergyPointValueDTOS(), adaptedHeat.getEnergyPointValueDTOS());
        MetricsDTO metric = new MetricsDTO(DateUtils.localDateTimeToLongInMillis(startTime), dcName, MeasurementType.ReusePercent.type(), value, TIMEFRAME_ID);
        return energyProfileRepository.insertMetric(metric);
    }

    public MetricsDTO computeHeatAdaptability(LocalDateTime startTime, String granularity) {
        ForecastDayAheadDTO adaptedHeat = energyProfileRepository.getHeatAdaptedProfile(startTime,dcName, granularity);
        ForecastDayAheadDTO baselineHeat = energyProfileRepository.getHeatBaselineProfile(startTime,dcName, granularity);

        double value = computeAdaptabilityValue(adaptedHeat.getEnergyPointValueDTOS(), baselineHeat.getEnergyPointValueDTOS());
        MetricsDTO metric = new MetricsDTO(DateUtils.localDateTimeToLongInMillis(startTime), dcName, MeasurementType.HeatAdaptability.type(), value, TIMEFRAME_ID);
        return energyProfileRepository.insertMetric(metric);
    }

    public MetricsDTO computeCO2Emissions(LocalDateTime startTime, String granularity) {
        ForecastDayAheadDTO adaptedCO2 = energyProfileRepository.getCO2AdaptedProfile(startTime,dcName, granularity);
        ForecastDayAheadDTO baselineCO2 = energyProfileRepository.getCO2BaselineProfile(startTime,dcName, granularity);

        double value = computeAdaptabilityValue(adaptedCO2.getEnergyPointValueDTOS(), baselineCO2.getEnergyPointValueDTOS());
        MetricsDTO metric = new MetricsDTO(DateUtils.localDateTimeToLongInMillis(startTime), dcName, MeasurementType.CO2emissions.type(), value, TIMEFRAME_ID);
        return energyProfileRepository.insertMetric(metric);
    }

    public MetricsDTO computeDCProfit(LocalDateTime startTime, String granularity) {
        ForecastDayAheadDTO baseline = energyProfileRepository.getDCBaselineProfile(startTime, dcName, granularity);
        ForecastDayAheadDTO adaptedDC = energyProfileRepository.getDCAdaptedProfile(startTime, dcName, granularity);
        MarketPrices prices = energyProfileRepository.getMarketPrices(startTime,dcName, granularity);

        double value = computeDCProfitValue(adaptedDC.getEnergyPointValueDTOS(), baseline.getEnergyPointValueDTOS(), prices.getEnergyPrices().getEnergyPointValueDTOS());
        MetricsDTO metric = new MetricsDTO(DateUtils.localDateTimeToLongInMillis(startTime), dcName, MeasurementType.DCProfit.type(), value, TIMEFRAME_ID);
        return energyProfileRepository.insertMetric(metric);
    }

    private double computeDCProfitValue(List<EnergyPointValueDTO> dcAdapt, List<EnergyPointValueDTO> dcBaseline, List<EnergyPointValueDTO> prices){
        assert (dcAdapt.size() == dcBaseline.size() && dcAdapt.size() == prices.size());
        double numerator = 0;
        double denominator = 0;
        for (int i = 0, j = 0; i < dcBaseline.size() && j < dcAdapt.size(); i++) {
            if (dcAdapt.get(j).getTimeStamp() !=  dcBaseline.get(i).getTimeStamp()) {
                LOGGER.error("The compared values are not corresponding to the same time");
            }
            else {
                numerator += (dcBaseline.get(i).getEnergyValue() - dcAdapt.get(j).getEnergyValue()) * prices.get(j).getEnergyValue();
                denominator += dcBaseline.get(i).getEnergyValue() * prices.get(j).getEnergyValue();
                ++j;
            }
        }

        denominator = denominator != 0 ? denominator : 1.0;
        return   numerator / denominator;
    }

    private double computeAdaptabilityValue(List<EnergyPointValueDTO> adapted, List<EnergyPointValueDTO> baseline){
        assert (adapted.size() == baseline.size());
        double numerator = 0;
        double denominator = 0;
        for (int i = 0; i < baseline.size(); i++) {
            if (adapted.get(i).getTimeStamp() !=  baseline.get(i).getTimeStamp()) {
                LOGGER.error("The compared values are not corresponding to the same time");
            }
            numerator += Math.abs( adapted.get(i).getEnergyValue()- baseline.get(i).getEnergyValue() );
            denominator += baseline.get(i).getEnergyValue();
        }

        denominator = denominator != 0 ? denominator : 1.0;
        return   numerator / denominator;
    }


    private double computeReusePercentValue(List<EnergyPointValueDTO> serverAdapt, List<EnergyPointValueDTO> coolingAdapt, List<EnergyPointValueDTO> heatAdapt){
        assert (serverAdapt.size() == coolingAdapt.size() && serverAdapt.size() == heatAdapt.size());
        double numerator = 0;
        double denominator = 0;
        for (int i = 0; i < serverAdapt.size(); i++) {
            if (serverAdapt.get(i).getTimeStamp() !=  coolingAdapt.get(i).getTimeStamp()) {
                LOGGER.error("The compared values are not corresponding to the same time");
            }
            numerator += heatAdapt.get(i).getEnergyValue();
            denominator += serverAdapt.get(i).getEnergyValue() + coolingAdapt.get(i).getEnergyValue();
        }

        denominator = denominator != 0 ? denominator : 1.0;
        return   numerator / denominator;
    }

    private double computeEREValue(List<EnergyPointValueDTO> serverAdapt, List<EnergyPointValueDTO> coolingAdapt, List<EnergyPointValueDTO> heatAdapt){
        assert (serverAdapt.size() == coolingAdapt.size() && serverAdapt.size() == heatAdapt.size());
        double numerator = 0;
        for (int i = 0; i < serverAdapt.size(); i++) {
            if (serverAdapt.get(i).getTimeStamp() !=  coolingAdapt.get(i).getTimeStamp()) {
                LOGGER.error("The compared values are not corresponding to the same time");
            }
            numerator += ( serverAdapt.get(i).getEnergyValue() + coolingAdapt.get(i).getEnergyValue() -heatAdapt.get(i).getEnergyValue())
                    / serverAdapt.get(i).getEnergyValue();
        }

        double denominator = !serverAdapt.isEmpty() ? serverAdapt.size() : 1.0;
        return   numerator / denominator;
    }

    private double computePUEValue(List<EnergyPointValueDTO> serverAdapt, List<EnergyPointValueDTO> coolingAdapt){
        assert serverAdapt.size() == coolingAdapt.size();
        double numerator = 0;
        for (int i = 0; i < serverAdapt.size(); i++) {
            if (serverAdapt.get(i).getTimeStamp() !=  coolingAdapt.get(i).getTimeStamp()) {
                LOGGER.error("The compared values are not corresponding to the same time");
            }
            numerator += ( serverAdapt.get(i).getEnergyValue() + coolingAdapt.get(i).getEnergyValue() )
                    / serverAdapt.get(i).getEnergyValue();
        }

        double denominator = !serverAdapt.isEmpty()? serverAdapt.size() : 1.0;
        return   numerator / denominator;
    }

    private double computeKdca(List<EnergyPointValueDTO> baseline, List<EnergySampleDTO> monitored){
        assert baseline.size() == monitored.size();
        double numerator = 0;
        double denominator = 0;
        for (int i = 0; i < baseline.size(); i++) {
            if (baseline.get(i).getTimeStamp() != DateUtils.dateTimeToMillis(monitored.get(i).getTimestamp())) {
                LOGGER.error("The compared values are not corresponding to the same time");
            }
            numerator += baseline.get(i).getEnergyValue();
            denominator += monitored.get(i).getEnergyValue();
        }
        denominator = denominator != 0 ? denominator : 1.0;
        return  numerator / denominator;
    }

    private double computeDCAMetric(double kDCA, List<EnergyPointValueDTO> baseline, List<EnergySampleDTO> monitored) {
        assert baseline.size() == monitored.size();
        double numerator = 0;
        double denominator = 0;
        for (int i = 0; i < baseline.size(); i++) {
            if (baseline.get(i).getTimeStamp() !=  DateUtils.dateTimeToMillis(monitored.get(i).getTimestamp())) {
                LOGGER.error("The compared values are not corresponding to the same time");
            }
            numerator += Math.abs(kDCA * monitored.get(i).getEnergyValue()- baseline.get(i).getEnergyValue() );
            denominator += baseline.get(i).getEnergyValue();
        }
        denominator = denominator != 0 ? denominator : 1.0;
        return  numerator / denominator;
    }
}
