package ro.tuc.dsrl.catalyst.optimizer.market.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTypes;
import ro.tuc.dsrl.catalyst.optimizer.serializers.CustomLocalDateTimeDeserializer;
import ro.tuc.dsrl.catalyst.optimizer.serializers.CustomLocalDateTimeSerializer;

import java.util.Objects;


public class MarketActionDTO {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private int id;

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime date;

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime actionStartTime;

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime actionEndTime;

    private Double value;
    private String uom;
    private Double price;
    private String actionType;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String status;

    @JsonIgnore
    private MarketTypes marketType;

    public MarketActionDTO() {
    }

    public MarketActionDTO(int id, DateTime date, DateTime actionStartTime, DateTime actionEndTime, double value, String uom, Double price, String actionType, String status) {
        this.id = id;
        this.date = date;
        this.actionStartTime = actionStartTime;
        this.actionEndTime = actionEndTime;
        this.value = value;
        this.uom = uom;
        this.price = price;
        this.actionType = actionType;
        this.status = status;
    }

   // @JsonIgnore
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public DateTime getActionStartTime() {
        return actionStartTime;
    }

    public void setActionStartTime(DateTime actionStartTime) {
        this.actionStartTime = actionStartTime;
    }

    public DateTime getActionEndTime() {
        return actionEndTime;
    }

    public void setActionEndTime(DateTime actionEndTime) {
        this.actionEndTime = actionEndTime;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

   // @JsonIgnore
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MarketTypes getMarketType() {
        return marketType;
    }

    public void setMarketType(MarketTypes marketType) {
        this.marketType = marketType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarketActionDTO that = (MarketActionDTO) o;
        return Objects.equals(actionStartTime, that.actionStartTime) &&
                Objects.equals(actionEndTime, that.actionEndTime) &&
                Objects.equals(value, that.value) &&
                Objects.equals(uom, that.uom) &&
                Objects.equals(price, that.price) &&
                Objects.equals(actionType, that.actionType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actionStartTime, actionEndTime, value, uom, price, actionType);
    }
}
