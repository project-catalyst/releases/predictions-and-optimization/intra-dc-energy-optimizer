package ro.tuc.dsrl.catalyst.optimizer.planner;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Dec 16, 2014
 * @Description: Container class that holds a checkpoint for the storage levels
 *               used by the optimization algorithms
 *
 */
public class ResourceConstraints {

	private double tesLevel;
	private double upsLevel;

	public ResourceConstraints() {
	}

	public ResourceConstraints(double tesLevel, double upsLevel) {
		this.tesLevel = tesLevel;
		this.upsLevel = upsLevel;
	}

	public double getTesLevel() {
		return tesLevel;
	}

	public void setTesLevel(double tesLevel) {
		this.tesLevel = tesLevel;
	}

	public double getUpsLevel() {
		return upsLevel;
	}

	public void setUpsLevel(double upsLevel) {
		this.upsLevel = upsLevel;
	}
}
