package ro.tuc.dsrl.catalyst.optimizer.market;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.CorrelatedActionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.MarketActionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.MarketCorrelationDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketActionType;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketConstraint;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTimeframes;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTypes;
import ro.tuc.dsrl.catalyst.optimizer.planner.DayaheadPlanner;
import ro.tuc.dsrl.catalyst.optimizer.utility.PropertiesLoader;
import ro.tuc.dsrl.geyser.datamodel.actions.*;

import java.util.*;

public class MarketManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(MarketManager.class);

    public static final String MARKET_ACTOR_NAME = PropertiesLoader.MARKET_ACTOR_NAME;

    private static MarketManager marketManagerInstance;

    private MarketAPIHandlerInterface marketHandler;


    public static MarketManager createInstance(MarketAPIHandlerInterface.MarketAPIType apiType) {
        if (marketManagerInstance == null) {
            marketManagerInstance = new MarketManager();
        }
        marketManagerInstance.marketHandler = MarketAPIHandlerInterface.getInstance(apiType);
        return marketManagerInstance;
    }

    public static MarketManager getInstance() {
        if (marketManagerInstance == null) {
            marketManagerInstance = new MarketManager();
            marketManagerInstance.marketHandler = MarketAPIHandlerInterface.getInstance(MarketAPIHandlerInterface.MarketAPIType.DEFAULT_DB);
        }
        return marketManagerInstance;
    }


    public MarketData getMarketData(DateTime startDate , MarketTimeframes timeframe) {
        MarketData data = null;
        try {
            data = fetchData(startDate, timeframe);
        }catch(Exception e){
            LOGGER.error(e.getLocalizedMessage());
            createInstance(MarketAPIHandlerInterface.MarketAPIType.DEFAULT_DB);
            data = fetchData(startDate, timeframe);
        }
        return data;
    }


    public void postMarketActions(DateTime currentDate, DateTime startDate, MarketTimeframes timeframe ,Map<Interval, List<EnergyEfficiencyOptimizationAction>> optimizationActions, Map<Interval, List<EnergyEfficiencyOptimizationAction>> correlatedActions) {

        List<MarketActionDTO> electricalEnergyActions = new ArrayList<>();
        List<MarketActionDTO> thermalEnergyActions = new ArrayList<>();
        List<MarketActionDTO> flexibilityEnergyActions = new ArrayList<>();
        Map<EnergyEfficiencyOptimizationAction, MarketActionDTO> afterRegistration = new HashMap<>();
        Map<MarketActionDTO, EnergyEfficiencyOptimizationAction> beforeRegistration = new HashMap<>();
        Map<MarketActionDTO, MarketTypes> marketTypesForCorrelatedActions = new HashMap<>();

        for(Map.Entry<Interval, List<EnergyEfficiencyOptimizationAction>> actionsInInterval: optimizationActions.entrySet()){
            Interval timeslot = actionsInInterval.getKey();
            List<EnergyEfficiencyOptimizationAction> actionsAtHour = actionsInInterval.getValue();
            for(EnergyEfficiencyOptimizationAction action: actionsAtHour){
                if(action instanceof BuyEnergy){
                    BuyEnergy marketAction = (BuyEnergy) action;
                    MarketActionDTO marketActionDTO = createMarketAction(marketAction, MarketActionType.BUY, timeslot,currentDate);
                    marketActionDTO.setMarketType(MarketTypes.ELECTRIC);
                    electricalEnergyActions.add(marketActionDTO);
                    beforeRegistration.put(marketActionDTO, action);
                    marketTypesForCorrelatedActions.put(marketActionDTO, marketActionDTO.getMarketType());
                }
                if(action instanceof SellEnergy){
                    SellEnergy marketAction = (SellEnergy) action;
                    MarketActionDTO marketActionDTO = createMarketAction(marketAction, MarketActionType.SELL, timeslot,currentDate);
                    marketActionDTO.setMarketType(MarketTypes.ELECTRIC);
                    electricalEnergyActions.add(marketActionDTO);
                    beforeRegistration.put(marketActionDTO, action);
                    marketTypesForCorrelatedActions.put(marketActionDTO, marketActionDTO.getMarketType());
                }
                if(action instanceof SellFlexibility){
                    SellFlexibility marketAction = (SellFlexibility) action;
                    MarketActionDTO marketActionDTO = createMarketAction(marketAction, MarketActionType.SELL, timeslot,currentDate);
                    marketActionDTO.setMarketType(MarketTypes.CONGESTION_MANAGEMENT);
                    flexibilityEnergyActions.add(marketActionDTO);
                    //beforeRegistration.put(marketActionDTO, action);
                }
                if(action instanceof SellHeat){
                    SellHeat marketAction = (SellHeat) action;
                    MarketActionDTO marketActionDTO = createMarketAction(marketAction, MarketActionType.SELL, timeslot,currentDate);
                    marketActionDTO.setMarketType(MarketTypes.THERMAL);
                    thermalEnergyActions.add(marketActionDTO);
                    beforeRegistration.put(marketActionDTO, action);
                    marketTypesForCorrelatedActions.put(marketActionDTO, marketActionDTO.getMarketType());
                }
            }
        }
        List<MarketActionDTO>  registerdElectricalEnergyActions = marketHandler.registerActions(electricalEnergyActions, MarketTypes.ELECTRIC, timeframe);
        List<MarketActionDTO>  registerdThermalEnergyActions = marketHandler.registerActions(thermalEnergyActions, MarketTypes.THERMAL, timeframe);
        List<MarketActionDTO>  registerdFlexibilityEnergyActions = marketHandler.registerActions(flexibilityEnergyActions, MarketTypes.CONGESTION_MANAGEMENT, timeframe);
        for(MarketActionDTO marketAction: registerdElectricalEnergyActions){
            EnergyEfficiencyOptimizationAction energyEfficiencyOptimizationAction = beforeRegistration.get(marketAction);
            if(energyEfficiencyOptimizationAction!=null) {
                MarketTypes marketType = marketTypesForCorrelatedActions.get(marketAction);
                marketAction.setMarketType(marketType);
                afterRegistration.put(energyEfficiencyOptimizationAction, marketAction);
            }
        }
        for(MarketActionDTO marketAction: registerdThermalEnergyActions){
            EnergyEfficiencyOptimizationAction energyEfficiencyOptimizationAction = beforeRegistration.get(marketAction);
            if(energyEfficiencyOptimizationAction!=null) {
                MarketTypes marketType = marketTypesForCorrelatedActions.get(marketAction);
                marketAction.setMarketType(marketType);
                afterRegistration.put(energyEfficiencyOptimizationAction, marketAction);
            }
        }
//        for(MarketActionDTO marketAction: registerdFlexibilityEnergyActions){
//            EnergyEfficiencyOptimizationAction energyEfficiencyOptimizationAction = beforeRegistration.get(marketAction);
//            if(energyEfficiencyOptimizationAction!=null) {
//                afterRegistration.put(energyEfficiencyOptimizationAction, marketAction);
//            }
//        }
        for(Map.Entry<Interval, List<EnergyEfficiencyOptimizationAction>> correlatedActionsInInterval: correlatedActions.entrySet()){
            Interval timeslot = correlatedActionsInInterval.getKey();
            List<EnergyEfficiencyOptimizationAction> correlatedActionList = correlatedActionsInInterval.getValue();
            MarketCorrelationDTO marketCorrelationDTO = new MarketCorrelationDTO();
            List<CorrelatedActionDTO> marketActions = new ArrayList<>();
            for(EnergyEfficiencyOptimizationAction action: correlatedActionList){
                CorrelatedActionDTO correlatedActionDTO = new CorrelatedActionDTO();
                if(afterRegistration.get(action)!=null) {
                    correlatedActionDTO.setActionId(afterRegistration.get(action).getId());
                    correlatedActionDTO.setMarketplaceForm(afterRegistration.get(action).getMarketType().getValue());
                    marketActions.add(correlatedActionDTO);
                }
            }
            marketCorrelationDTO.setMarketActions(marketActions);
            marketCorrelationDTO.setConstraintType(MarketConstraint.ALL.getValue());
            marketHandler.registerCorrelatedActions(marketCorrelationDTO, timeframe);
        }
   }

    private MarketActionDTO createMarketAction(MarketplaceAction action, MarketActionType type, Interval timeslot, DateTime time){
            MarketActionDTO marketActionDTO = new MarketActionDTO();
            marketActionDTO.setActionType(type.getValue());
            marketActionDTO.setActionStartTime(timeslot.getStart());
            marketActionDTO.setActionEndTime(timeslot.getEnd().minusMinutes(1));
            marketActionDTO.setDate(time);
            marketActionDTO.setPrice(action.getPrice());
            marketActionDTO.setValue(action.getAmountOfEnergy());
            marketActionDTO.setUom("Kwh");
            return marketActionDTO;
    }

    private MarketData fetchData(DateTime startDate, MarketTimeframes timeframe) {
        List<Double> energyPrice = marketHandler.getRefrencePrices(MarketTypes.ELECTRIC, timeframe, startDate);
        List<Double> workloadPrices = marketHandler.getRefrencePrices(MarketTypes.IT_LOAD, timeframe, startDate);
        List<Double> thermalPrices = marketHandler.getRefrencePrices(MarketTypes.THERMAL, timeframe, startDate);
        List<MarketActionDTO> marketActions = marketHandler.getMarketActions(MarketTypes.CONGESTION_MANAGEMENT, timeframe, startDate);
        if(marketActions.isEmpty()){
            marketActions = marketHandler.getMarketActions(MarketTypes.REACTIVE_POWER_COMPENSATION, timeframe, startDate);
        }
        if(marketActions.isEmpty()){
            marketActions = marketHandler.getMarketActions(MarketTypes.SCHEDULING,timeframe, startDate);
        }
        if(marketActions.isEmpty()){
            marketActions = marketHandler.getMarketActions(MarketTypes.SPINNING_RESERVE, timeframe, startDate);
        }
        List<Double> drSignal = new ArrayList<Double>(Collections.nCopies(timeframe.getTimeslots(), 0.0));
        List<Double> drPrice = new ArrayList<Double>(Collections.nCopies(timeframe.getTimeslots(), 0.0));

        int periodicityFactor = MarketTimeframes.DAYAHEAD.getGranularityInMinutes()/timeframe.getGranularityInMinutes();
        for(MarketActionDTO m: marketActions){
            if(MarketActionType.BUY.getValue().equals(m.getActionType())) {
                for (DateTime timeslot = m.getActionStartTime(); timeslot.isBefore(m.getActionEndTime()); timeslot = timeslot.plusMinutes(timeframe.getGranularityInMinutes())) {
                    double value = m.getValue();
                    double price = m.getPrice();

                    int index = periodicityFactor * timeslot.getHourOfDay() + timeslot.getMinuteOfHour() % timeframe.getGranularityInMinutes();
                    drSignal.set(index, value);
                    drPrice.set(index, price);
                }
            }
        }
        MarketData data = new MarketData();
        data.setEnergyPrice(energyPrice);
        data.setWorkloadPrices(workloadPrices);
        data.setThermalPrices(thermalPrices);
        data.setDrSignal(drSignal);
        data.setDrIncentives(drPrice);
        return data;
    }


}
