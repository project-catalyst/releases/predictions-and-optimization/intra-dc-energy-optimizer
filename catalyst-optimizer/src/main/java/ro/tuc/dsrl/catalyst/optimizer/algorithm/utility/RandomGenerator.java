package ro.tuc.dsrl.catalyst.optimizer.algorithm.utility;

import java.util.Date;
import java.util.Random;

public class RandomGenerator {
    private static Random random = new Random(new Date().getTime());

    private RandomGenerator() {
    }

    public static double random(double min, double max) {
        return min + (max - min) * random.nextDouble();
    }

    public static int random(int min, int max) {
        return min + Math.abs(random.nextInt() % (max - min));
    }

    public static double randomRound1Decimal(double a, double b){
        return Math.round(RandomGenerator.random(a, b)*10)/10.0;
    }
}
