package ro.tuc.dsrl.catalyst.optimizer.market.enums;

public enum MarketTypes {
    ELECTRIC("electric_energy"),
    THERMAL("thermal_energy"),
    IT_LOAD("it_load"),
    CONGESTION_MANAGEMENT("congestion_management"),
    REACTIVE_POWER_COMPENSATION("reactive_power_compensation"),
    SPINNING_RESERVE("spinning_reserve"),
    SCHEDULING("scheduling");

    private String value;

    MarketTypes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
