package ro.tuc.dsrl.catalyst.optimizer.algorithm;

import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketData;
import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictedData;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.catalyst.optimizer.utility.MathUtility;
import ro.tuc.dsrl.catalyst.optimizer.planner.ResourceConstraints;

import java.util.List;

public class OptimizationDataCatalyst {
	private static final long serialVersionUID = 1L;
	public static final double ED_PERCENTAGE = 0.3;

	private int size;

	// input
	private double[] realtimeEnergy; // E_R
	private double[] delayTolerableEnergy;// E_D
	private double[] energyPrice;// E_Price
	private double[] heatPrice; // T_Price
	private double[] loadPrice;// L_Price
	private double[] demandPrice;// D_Price
	private double[] timePeriod;// MAX_T // 24


	private double[] renewableEnergy;//REN
	//private double[] timePeriodIntraday;// MAX_T_TES
	private double[] factor;// f // factor for battery

	private double[] batteryMaxCapacity;// ESD
	private double[] factorTes;// f_TES
	private double[] copC; // COP_C
	private double[] copH; // COP_H
	private double[] tesMaxCapacity;// TES_MAX

	private double[] tesChargeLossRate;// TES_R
	private double[] tesDischargeLossRate;// TES_D
	private double[] batteryChargeLossRate; // ESD_R/
	private double[] batteryDischargeLossRate;// ESD_D
	private double[] tesMaxDischargeRate;// MAX_DIS_TES
	private double[] batteryMaxDischargeRate;// MAX_DIS
	private double[] dod; // DOD
	private double[] itMaxConsumption;// MAX_IT
	private double[] edPercentage;// E_D_Percentage
	private double[] maxHost;
	private double[] maxRealloc;

	private double[] relocateActive;// relocate workload enable

	private double[] batteryInitialValue;// ESD_INIT
	private double[] tesInitialValue;// TES_INIT
	private double[] batteryFinalConstraint; // ESD_FIN
	private double[] tesFinalConstraint;// TES_FIN
	private double[] drSignal;// drSignal
	private double[] baseline;

	private double[] WE;
	private double[] WL;
	private double[] WF;
	private double[] WT;
	private double[] WREN;

	// Output

	private double[] dischargeBattery; // D_ESD
	private double[] chargeBattery; // R_ESD
	private double[] dischargeTes; // D_TES
	private double[] chargeTes;// R_TES
	private double[] schedulingMatrix; // Y



	private double[] itCooling;// E_C
	private double[] finalCooling;// E_C
	private double[] estimatedDelayExecution; // E_DE
	private double[] batteryLevel; // EsD
	private double[] tesLevel; // Tes
	private double[] dcFinalConsumption; // E_DC
	private double[] dcThermalGeneration;//T_DC
	private double[] relocateWorkload;
	private double[] hostWorkload;
	private double[] heatBaseline;
	private double[] co2baseline;
	private double[] co2adapted;
	private double[] itLoad;
	// Optimization Objectives
	private double[] energyCost;
	private double[] thermalProfit;
	private double[] flexibilityPenalty;
	private double[] loadProfit;
	private double[] totalCost;

	private boolean success;

	public OptimizationDataCatalyst(int size) {
		this.size = size;

		this.dischargeBattery = new double[size];
		this.chargeBattery = new double[size];
		this.dischargeTes = new double[size];
		this.chargeTes = new double[size];
		this.schedulingMatrix = new double[size*size];
		this.finalCooling = new double[size];
		this.itCooling = new double[size];
		this.estimatedDelayExecution = new double[size];
		this.batteryLevel = new double[size];
		this.tesLevel = new double[size];
		this.relocateWorkload = new double[size];
		this.hostWorkload = new double[size];
		this.dcThermalGeneration = new double[size];
		this.dcFinalConsumption =  new double[size];
		this.energyCost = new double[1];
		this.thermalProfit = new double[1];
		this.flexibilityPenalty = new double[1];
		this.loadProfit = new double[1];
		this.totalCost = new double[1];
		this.heatBaseline= new double[size];
		this.co2baseline= new double[size];
		this.co2adapted= new double[size];
		this.itLoad= new double[size];
		this.renewableEnergy = new double[size];
	}

	public static OptimizationDataCatalyst getInstance(List<Double> drSignal, FlexibilityStrategyDTO strategies, PredictedData predictedData,
                                                       MarketData marketData, DCResources resources, ResourceConstraints startConstraint, ResourceConstraints endConstraint,
                                                       int timeHorizon) {
		OptimizationDataCatalyst outputData = new OptimizationDataCatalyst(timeHorizon);
		outputData.setRealtimeEnergy(listToArray(predictedData.getRealtimeWorkload()));
		outputData.setDelayTolerableEnergy(listToArray((predictedData.getDelayTolerableWorkload())));
//		outputData.setEnergyPrice(listToArray(predictedData.getEnergyPrice()));
//		outputData.setHeatPrice(listToArray(predictedData.getThermalPrices()));
//		outputData.setLoadPrice(listToArray(predictedData.getWorkloadPrices()));
//		outputData.setDemandPrice(listToArray(predictedData.getDrIncentives()));
		outputData.setEnergyPrice(listToArray(marketData.getEnergyPrice()));
		outputData.setHeatPrice(listToArray(marketData.getThermalPrices()));
		outputData.setLoadPrice(listToArray(marketData.getWorkloadPrices()));
		if((marketData.getDrIncentives()!= null)&&(marketData.getEnergyPrice()!=null)){
			for(int i = 0; i< marketData.getDrIncentives().size(); i++){
				double d = marketData.getDrIncentives().get(i);
				if(d == 0.0) marketData.getDrIncentives().set(i, marketData.getEnergyPrice().get(i));
			}
		}
		outputData.setDemandPrice(listToArray(marketData.getDrIncentives()));
		outputData.setBaseline(listToArray(predictedData.getBaseline()));
		if((drSignal!= null)&&(predictedData.getBaseline()!=null)){
			for(int i = 0; i< drSignal.size(); i++){
				double d = drSignal.get(i);
				if((d == 0.0)&&(i< predictedData.getBaseline().size())) drSignal.set(i, predictedData.getBaseline().get(i));
			}
		}
		outputData.setDrSignal(listToArray(drSignal));
		outputData.setRenewableEnergy(listToArray(predictedData.getRenewableEnergy()));

		outputData.setTimePeriod( new double[] {timeHorizon});
		outputData.setFactorTes( new double[] {resources.getTesResource().getFactor()});
		outputData.setFactor( new double[] {resources.getBatteryResource().getFactorEsd()});
		outputData.setBatteryMaxCapacity(new double[] {resources.getBatteryResource().getMaximumCapacity()});
		outputData.setTesMaxCapacity( new double[] {resources.getTesResource().getMaximumCapacity()});
		outputData.setTesChargeLossRate( new double[] {resources.getTesResource().getChargeLossRate()});
		outputData.setTesDischargeLossRate( new double[] {resources.getTesResource().getDischargeLossRate()});
		outputData.setTesMaxDischargeRate( new double[] {resources.getTesResource().getMaxDischargeRate()});
		outputData.setBatteryChargeLossRate( new double[] {resources.getBatteryResource().getChargeLossRate()});
		outputData.setBatteryDischargeLossRate(new double[] {resources.getBatteryResource().getDischargeLossRate()});
		outputData.setBatteryMaxDischargeRate( new double[] {resources.getBatteryResource().getMaxDischargeRate()});
		outputData.setItMaxConsumption( new double[] {resources.getServerResources().getItMaximumConsumptionKW()});
		outputData.setMaxHost(new double[] {resources.getServerResources().getMaxHostLoad()});
		outputData.setMaxRealloc(new double[] {resources.getServerResources().getMaxReallocLoad()});

		outputData.setDod( new double[] {resources.getBatteryResource().getDod()});
		outputData.setBatteryInitialValue( new double[] {startConstraint.getUpsLevel()});
		outputData.setBatteryFinalConstraint( new double[] {endConstraint.getUpsLevel()});
		outputData.setTesInitialValue( new double[] {startConstraint.getTesLevel()});
		outputData.setTesFinalConstraint( new double[] {endConstraint.getTesLevel()});
		outputData.setCopC( new double[] {resources.getCoolingResources().getCopC()});
		outputData.setCopH( new double[] {resources.getCoolingResources().getCopH()});

		outputData.setWE( new double[] {strategies.getWe()});
		outputData.setWF( new double[] {strategies.getWf()});
		outputData.setWL( new double[] {0.0});
		outputData.setWT( new double[] {strategies.getWt()});
		outputData.setWREN(new double[]{strategies.getREN()});

		outputData.setRelocateActive( new double[] {strategies.isReallocActive()? 1 :0});

		outputData.setEdPercentage(new double[] {resources.getServerResources().getCurrentDcEdPercentage()});

		return outputData;
	}

	private static double[] listToArray(List<? extends Number> list) {
		double[] array = new double[list.size()];
		int i = 0;
		for (Number e : list) {
			array[i++] = MathUtility.round(e.doubleValue(), 5);
		}
		return array;
	}

	public double getThermalEquivalentForElectricalEnergy(double electricalEnergy){
		double ret = electricalEnergy;
		ret = (this.getCopH()[0]-1) / this.getCopC()[0] * ret;
		return ret;
	}

	public double[] getMaxHost() {
		return maxHost;
	}

	public void setMaxHost(double[] maxHost) {
		this.maxHost = maxHost;
	}

	public double[] getMaxRealloc() {
		return maxRealloc;
	}

	public void setMaxRealloc(double[] maxRealloc) {
		this.maxRealloc = maxRealloc;
	}

	public double[] getBaseline() {
		return baseline;
	}

	public void setBaseline(double[] baseline) {
		this.baseline = baseline;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double[] getRealtimeEnergy() {
		return realtimeEnergy;
	}

	public void setRealtimeEnergy(double[] realtimeEnergy) {
		this.realtimeEnergy = realtimeEnergy;
	}

	public double[] getDelayTolerableEnergy() {
		return delayTolerableEnergy;
	}

	public void setDelayTolerableEnergy(double[] delayTolerableEnergy) {
		this.delayTolerableEnergy = delayTolerableEnergy;
	}

	public double[] getEnergyPrice() {
		return energyPrice;
	}

	public void setEnergyPrice(double[] energyPrice) {
		this.energyPrice = energyPrice;
	}

	public double[] getHeatPrice() {
		return heatPrice;
	}

	public void setHeatPrice(double[] heatPrice) {
		this.heatPrice = heatPrice;
	}

	public double[] getLoadPrice() {
		return loadPrice;
	}

	public void setLoadPrice(double[] loadPrice) {
		this.loadPrice = loadPrice;
	}

	public double[] getDemandPrice() {
		return demandPrice;
	}

	public void setDemandPrice(double[] demandPrice) {
		this.demandPrice = demandPrice;
	}

	public double[] getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(double[] timePeriod) {
		this.timePeriod = timePeriod;
	}

//	public double[] getTimePeriodIntraday() {
//		return timePeriodIntraday;
//	}
//
//	public void setTimePeriodIntraday(double[] timePeriodIntraday) {
//		this.timePeriodIntraday = timePeriodIntraday;
//	}

	public double[] getFactor() {
		return factor;
	}

	public void setFactor(double[] factor) {
		this.factor = factor;
	}

	public double[] getBatteryMaxCapacity() {
		return batteryMaxCapacity;
	}

	public void setBatteryMaxCapacity(double[] batteryMaxCapacity) {
		this.batteryMaxCapacity = batteryMaxCapacity;
	}

	public double[] getFactorTes() {
		return factorTes;
	}

	public void setFactorTes(double[] factorTes) {
		this.factorTes = factorTes;
	}

	public double[] getCopC() {
		return copC;
	}

	public void setCopC(double[] copC) {
		this.copC = copC;
	}

	public double[] getCopH() {
		return copH;
	}

	public void setCopH(double[] copH) {
		this.copH = copH;
	}

	public double[] getTesMaxCapacity() {
		return tesMaxCapacity;
	}

	public void setTesMaxCapacity(double[] tesMaxCapacity) {
		this.tesMaxCapacity = tesMaxCapacity;
	}

	public double[] getTesChargeLossRate() {
		return tesChargeLossRate;
	}

	public void setTesChargeLossRate(double[] tesChargeLossRate) {
		this.tesChargeLossRate = tesChargeLossRate;
	}

	public double[] getTesDischargeLossRate() {
		return tesDischargeLossRate;
	}

	public void setTesDischargeLossRate(double[] tesDischargeLossRate) {
		this.tesDischargeLossRate = tesDischargeLossRate;
	}

	public double[] getBatteryChargeLossRate() {
		return batteryChargeLossRate;
	}

	public void setBatteryChargeLossRate(double[] batteryChargeLossRate) {
		this.batteryChargeLossRate = batteryChargeLossRate;
	}

	public double[] getBatteryDischargeLossRate() {
		return batteryDischargeLossRate;
	}

	public void setBatteryDischargeLossRate(double[] batteryDischargeLossRate) {
		this.batteryDischargeLossRate = batteryDischargeLossRate;
	}

	public double[] getTesMaxDischargeRate() {
		return tesMaxDischargeRate;
	}

	public void setTesMaxDischargeRate(double[] tesMaxDischargeRate) {
		this.tesMaxDischargeRate = tesMaxDischargeRate;
	}

	public double[] getBatteryMaxDischargeRate() {
		return batteryMaxDischargeRate;
	}

	public void setBatteryMaxDischargeRate(double[] batteryMaxDischargeRate) {
		this.batteryMaxDischargeRate = batteryMaxDischargeRate;
	}

	public double[] getDod() {
		return dod;
	}

	public void setDod(double[] dod) {
		this.dod = dod;
	}

	public double[] getItMaxConsumption() {
		return itMaxConsumption;
	}

	public void setItMaxConsumption(double[] itMaxConsumption) {
		this.itMaxConsumption = itMaxConsumption;
	}

	public double[] getEdPercentage() {
		return edPercentage;
	}

	public void setEdPercentage(double[] edPercentage) {
		this.edPercentage = edPercentage;
	}

	public double[] getRelocateActive() {
		return relocateActive;
	}

	public void setRelocateActive(double[] relocateActive) {
		this.relocateActive = relocateActive;
	}

	public double[] getBatteryInitialValue() {
		return batteryInitialValue;
	}

	public void setBatteryInitialValue(double[] batteryInitialValue) {
		this.batteryInitialValue = batteryInitialValue;
	}

	public double[] getTesInitialValue() {
		return tesInitialValue;
	}

	public void setTesInitialValue(double[] tesInitialValue) {
		this.tesInitialValue = tesInitialValue;
	}

	public double[] getBatteryFinalConstraint() {
		return batteryFinalConstraint;
	}

	public void setBatteryFinalConstraint(double[] batteryFinalConstraint) {
		this.batteryFinalConstraint = batteryFinalConstraint;
	}

	public double[] getTesFinalConstraint() {
		return tesFinalConstraint;
	}

	public void setTesFinalConstraint(double[] tesFinalConstraint) {
		this.tesFinalConstraint = tesFinalConstraint;
	}

	public double[] getDrSignal() {
		return drSignal;
	}

	public void setDrSignal(double[] drSignal) {
		this.drSignal = drSignal;
	}

	public double[] getWE() {
		return WE;
	}

	public void setWE(double[] wE) {
		WE = wE;
	}

	public double[] getWL() {
		return WL;
	}

	public void setWL(double[] wL) {
		WL = wL;
	}

	public double[] getWF() {
		return WF;
	}

	public void setWF(double[] wF) {
		WF = wF;
	}

	public double[] getWT() {
		return WT;
	}

	public void setWT(double[] wT) {
		WT = wT;
	}

	public double[] getDischargeBattery() {
		return dischargeBattery;
	}

	public void setDischargeBattery(double[] dischargeBattery) {
		this.dischargeBattery = dischargeBattery;
	}

	public double[] getChargeBattery() {
		return chargeBattery;
	}

	public void setChargeBattery(double[] chargeBattery) {
		this.chargeBattery = chargeBattery;
	}

	public double[] getDischargeTes() {
		return dischargeTes;
	}

	public void setDischargeTes(double[] dischargeTes) {
		this.dischargeTes = dischargeTes;
	}

	public double[] getChargeTes() {
		return chargeTes;
	}

	public void setChargeTes(double[] chargeTes) {
		this.chargeTes = chargeTes;
	}

	public double[] 	getSchedulingMatrix() {
		return schedulingMatrix;
	}

	public void setSchedulingMatrix(double[] schedulingMatrix) {
		this.schedulingMatrix = schedulingMatrix;
	}

	public double[] getFinalCooling() {
		return finalCooling;
	}

	public void setFinalCooling(double[] finalCooling) {
		this.finalCooling = finalCooling;
	}

	public double[] getEstimatedDelayExecution() {
		return estimatedDelayExecution;
	}

	public void setEstimatedDelayExecution(double[] estimatedDelayExecution) {
		this.estimatedDelayExecution = estimatedDelayExecution;
	}

	public double[] getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(double[] batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public double[] getTesLevel() {
		return tesLevel;
	}

	public void setTesLevel(double[] tesLevel) {
		this.tesLevel = tesLevel;
	}

	public double[] getDcFinalConsumption() {
		return dcFinalConsumption;
	}

	public void setDcFinalConsumption(double[] dcFinalConsumption) {
		this.dcFinalConsumption = dcFinalConsumption;
	}

	public double[] getRelocateWorkload() {
		return relocateWorkload;
	}

	public void setRelocateWorkload(double[] relocateWorkload) {
		this.relocateWorkload = relocateWorkload;
	}

	public double[] getHostWorkload() {
		return hostWorkload;
	}

	public void setHostWorkload(double[] hostWorkload) {
		this.hostWorkload = hostWorkload;
	}

	public double[] getEnergyCost() {
		return energyCost;
	}

	public void setEnergyCost(double[] energyCost) {
		this.energyCost = energyCost;
	}

	public double[] getThermalProfit() {
		return thermalProfit;
	}

	public void setThermalProfit(double[] thermalProfit) {
		this.thermalProfit = thermalProfit;
	}

	public double[] getFlexibilityPenalty() {
		return flexibilityPenalty;
	}

	public void setFlexibilityPenalty(double[] flexibilityPenalty) {
		this.flexibilityPenalty = flexibilityPenalty;
	}

	public double[] getLoadProfit() {
		return loadProfit;
	}

	public void setLoadProfit(double[] loadProfit) {
		this.loadProfit = loadProfit;
	}

	public double[] getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double[] totalCost) {
		this.totalCost = totalCost;
	}
	public double[] getHeatBaseline() {
		return heatBaseline;
	}

	public void setHeatBaseline(double[] heatBaseline) {
		this.heatBaseline = heatBaseline;
	}


	public double[] getCo2baseline() {
		return co2baseline;
	}

	public void setCo2baseline(double[] co2baseline) {
		this.co2baseline = co2baseline;
	}



	public double[] getCo2adapted() {
		return co2adapted;
	}

	public void setCo2adapted(double[] co2adapted) {
		this.co2adapted = co2adapted;
	}



	public double[] getItLoad() {
		return itLoad;
	}

	public void setItLoad(double[] itLoad) {
		this.itLoad = itLoad;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
/*
	private static double[] listToArray(List<? extends Number> list) {
		double[] array = new double[list.size()];
		int i = 0;
		for (Number e : list) {
			array[i++] = MathUtility.round(e.doubleValue(), 2);
		}
		return array;
	}
*/
	public double[] getDcThermalGeneration() {
		return dcThermalGeneration;
	}

	public void setDcThermalGeneration(double[] dcThermalGeneration) {
		this.dcThermalGeneration = dcThermalGeneration;
	}
	public double[] getItCooling() {
		return itCooling;
	}

	public void setItCooling(double[] itCooling) {
		this.itCooling = itCooling;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}


	public double[] getRenewableEnergy() {
		return renewableEnergy;
	}

	public void setRenewableEnergy(double[] renewableEnergy) {
		this.renewableEnergy = renewableEnergy;
	}

	public double[] getWREN() {
		return WREN;
	}

	public void setWREN(double[] WREN) {
		this.WREN = WREN;
	}
}

