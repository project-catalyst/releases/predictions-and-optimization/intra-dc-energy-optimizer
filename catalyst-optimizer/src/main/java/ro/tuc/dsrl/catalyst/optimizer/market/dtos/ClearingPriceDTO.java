package ro.tuc.dsrl.catalyst.optimizer.market.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.optimizer.serializers.CustomLocalDateTimeDeserializer;
import ro.tuc.dsrl.catalyst.optimizer.serializers.CustomLocalDateTimeSerializer;

public class ClearingPriceDTO {

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime validityStartTime;

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime validityEndTime;

    private Double clearingPrice;

    public ClearingPriceDTO() {

    }

    public ClearingPriceDTO(DateTime validityStartTime, DateTime validityEndTime, Double clearingPrice) {
        this.validityStartTime = validityStartTime;
        this.validityEndTime = validityEndTime;
        this.clearingPrice = clearingPrice;
    }

    public DateTime getValidityStartTime() {
        return validityStartTime;
    }

    public void setValidityStartTime(DateTime validityStartTime) {
        this.validityStartTime = validityStartTime;
    }

    public DateTime getValidityEndTime() {
        return validityEndTime;
    }

    public void setValidityEndTime(DateTime validityEndTime) {
        this.validityEndTime = validityEndTime;
    }

    public Double getClearingPrice() {
        return clearingPrice;
    }

    public void setClearingPrice(Double clearingPrice) {
        this.clearingPrice = clearingPrice;
    }
}
