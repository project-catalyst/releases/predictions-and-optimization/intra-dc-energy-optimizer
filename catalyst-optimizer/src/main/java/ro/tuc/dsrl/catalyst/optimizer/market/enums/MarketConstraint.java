package ro.tuc.dsrl.catalyst.optimizer.market.enums;

public enum MarketConstraint {
    ALL("ALL OR NOTHING"),
    ONE("AT LEAST ONE");

    private String value;

    MarketConstraint(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
