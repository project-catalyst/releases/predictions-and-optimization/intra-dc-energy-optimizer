package ro.tuc.dsrl.catalyst.optimizer.utility;

import java.util.Date;
import java.util.Random;

public class RandomGenerator {
	
	private final static int bound = 10000;
	private static Random rd = new Random(new Date().getTime());
	
	public static double random(double a, double b){
		double ret = 0;
		//Random rd = new Random(new Date().getTime());
		double r = rd.nextDouble();
		ret = a + (b - a) * r;
		return ret;
	}
	
	public static int random(int a, int b){
		int ret = 0;
		int r = rd.nextInt(bound);
		ret = a + r % (b - a);
		return ret;
	}

}
