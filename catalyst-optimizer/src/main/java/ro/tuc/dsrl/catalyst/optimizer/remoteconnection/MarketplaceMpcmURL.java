package ro.tuc.dsrl.catalyst.optimizer.remoteconnection;

import ro.tuc.dsrl.catalyst.optimizer.utility.PropertiesLoader;


public enum MarketplaceMpcmURL implements APIURL {


    REFERENCE_PRICE_PREVIOUS_DAY("/referencePricesPreviousDay/{marketActorName}/{marketplaceForm}/{timeframe}/"),
    CLEARING_PRICE_PREVIOUS_DAY("/clearingPricesPreviousDay/{marketActorName}/{marketplaceForm}/{timeframe}/"),
    ACTIVE_SESSIONS("/activeSessions/{marketActorName}/{marketplaceForm}/{timeframe}/"),
    MARKET_ACTIONS("/marketActions/{marketActorName}/{marketplaceForm}/{timeframe}/"),
    MARKET_RESULTS("/marketResults/{marketplaceForm}/{timeframe}/"),
    CORRELATED_MARKET_ACTIONS("/correlatedMarketActions/{marketActorName}/{timeframe}/");


    private final String catalystMarketHost = PropertiesLoader.MARKETPLACE_MPCM;
    private String value;

    MarketplaceMpcmURL(String value) {
        this.value = value;
    }


    @Override
    public String value() {
        return catalystMarketHost + value;
    }
}
