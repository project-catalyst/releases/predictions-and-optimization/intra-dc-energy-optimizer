package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.constraints;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.EnergyEfficiencySolution;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.utility.RandomGenerator;

public class TesBatteryConstraint implements IOptimizationConstraint<EnergyEfficiencySolution> {
    @Override
    public void verify(EnergyEfficiencySolution solution) {
        int[] tesViolations = solution.getTesViolations();
        int[] esdViolations = solution.getEsdViolations();

        double[] esdActions = solution.getEsdActions();
        double[] tesActions = solution.getTesActions();

        for (int i = 0; i < solution.getSize(); ++i) {
            if (esdActions[i] * tesActions[i] >= 0) {
                continue;
            }

            // do not allow charge and discharge actions
            // or only charge, or only discharge
            // both action arrays have the same sign
            esdViolations[i] += esdViolations[i] > 0 ? 1 : 0;
            tesViolations[i] += tesViolations[i] > 0 ? 1 : 0;

            if ((tesViolations[i] == 0) && (esdViolations[i] == 0)) {
                // we have a two valid actions
                if (RandomGenerator.random(0., 1.) > .5) {
                    ++tesViolations[i];
                } else {
                    ++esdViolations[i];
                }
            }
        }

        solution.setTesViolations(tesViolations);
        solution.setEsdViolations(esdViolations);
    }
}
