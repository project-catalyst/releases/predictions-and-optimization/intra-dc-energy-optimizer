package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.constraints;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.EnergyEfficiencySolution;

public class TesConstraint implements IOptimizationConstraint<EnergyEfficiencySolution> {
    private double maxTes;
    private double maxChargeTes;
    private double maxDischargeTes;
    private double tesFin;

    public TesConstraint(double maxTes, double maxChargeTes, double maxDischargeTes, double tesFin) {
        this.maxTes = maxTes;
        this.maxChargeTes = maxChargeTes;
        this.maxDischargeTes = maxDischargeTes;
        this.tesFin = tesFin;
    }

    @Override
    public void verify(EnergyEfficiencySolution solution) {
        int[] violations = new int[solution.getSize()];
        double sum = 0;

        for (int i = 0; i < solution.getSize(); ++i) {
            violations[i] = 0;

            if (solution.getTes()[i] > this.maxTes) {
                ++violations[i];
            }

            if (solution.getTesActions()[i] + solution.getEDC()[i] < 0) {
                ++violations[i];
            }

            if ((solution.getTesActions()[i] < 0 && solution.getTesActions()[i] < this.maxDischargeTes) ||
                    (solution.getTesActions()[i] > 0 && solution.getTesActions()[i] > this.maxChargeTes)) {
                ++violations[i];
            }

            sum += solution.getTes()[i];
        }

        for (int i = 0; i < solution.getSize() && sum < this.tesFin; ++i) {
            if (solution.getTesActions()[i] < 0) {
                ++violations[i];
            }
        }

        solution.setTesViolations(violations);
    }
}
