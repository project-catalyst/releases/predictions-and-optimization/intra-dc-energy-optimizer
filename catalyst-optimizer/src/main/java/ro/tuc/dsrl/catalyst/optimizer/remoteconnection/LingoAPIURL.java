package ro.tuc.dsrl.catalyst.optimizer.remoteconnection;

import ro.tuc.dsrl.catalyst.optimizer.utility.PropertiesLoader;

public enum LingoAPIURL implements  APIURL {
	LINGO("/lingo/process"),
	LINGO_FOR_AS("/lingo/processAS");

	private String value;
	private String lingoHost = PropertiesLoader.LINGO_API_HOST;

	LingoAPIURL(String url) {
		this.value = url;
	}

	@Override
	public String value() {
		return lingoHost + value;
	}
}
