package ro.tuc.dsrl.catalyst.optimizer.algorithm;

public enum LingoStatusCodes {
	ERROR_NO_FEASIBLE_SOLUTION(81),
	ERROR_SOLUTION_NOT_AVAILABLE(193),
	STATUS_OK(0);

	private int value;

	private LingoStatusCodes(int url) {
		this.value = url;
	}

	public int value() {
		return value;
	}
}
