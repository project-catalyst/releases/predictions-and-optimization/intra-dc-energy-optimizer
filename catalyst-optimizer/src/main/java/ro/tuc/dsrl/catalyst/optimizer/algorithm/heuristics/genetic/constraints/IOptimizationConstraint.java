package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.constraints;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.Solution;

public interface IOptimizationConstraint<T extends Solution> {
    void verify(T solution);
}
