package ro.tuc.dsrl.catalyst.optimizer.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.enums.Scenario;
import ro.tuc.dsrl.catalyst.optimizer.manager.OptimizerTimerTask;

import java.time.LocalDateTime;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@RestController
public class IndexController {


	private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);
	private static boolean STARTED = false;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "Optimizer index";
	}

	@RequestMapping(value = "/start-scenario", method = RequestMethod.GET)
	public boolean startScenarioSimulation() {
		int OPT_RUNNING_PERIOD = 2000;
		DateTimeZone.setDefault(DateTimeZone.UTC);
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		if (STARTED) {
			return false;
		}
		Runnable run = () -> {
            LOGGER.info("Optimizer Started....");

            LocalDateTime scenarioTime = Scenario.SCENARIO_SBP.getTime().minusDays(1);
            DateTime currentTime = new DateTime(scenarioTime.getYear(), scenarioTime.getMonthValue(),
                    scenarioTime.getDayOfMonth(), 19, 50);

           OptimizerTimerTask instance = OptimizerTimerTask.getInstance(currentTime);

            ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(1);
            ScheduledFuture<?> f = stpe.scheduleWithFixedDelay(instance, 0, OPT_RUNNING_PERIOD,
                    TimeUnit.MILLISECONDS);
            try {
                f.get();
            } catch (Exception ex) {
                LOGGER.error("", ex);
            }
        };
		new Thread(run).start();
		STARTED = true;
		return true;
	}

	@RequestMapping(value = "/start-pilot", method = RequestMethod.GET)
	public boolean startPilotSimulation() {
		int OPT_RUNNING_PERIOD = 5*60*1000; //5 minutes in miliseconds
		DateTimeZone.setDefault(DateTimeZone.UTC);
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		if (STARTED) {
			return false;
		}
		Runnable run = () -> {
			LOGGER.info("Optimizer Pilot Started....");
			DateTime systemTime = new DateTime();
			DateTime startTime = new DateTime(systemTime.getYear(),
					systemTime.getMonthOfYear(),
					systemTime.getDayOfMonth(), 18, 50); //TODO: change hour : must be before 20:00
																// TODO: keep in mind that the hour is in UTC
			startTime= startTime.withSecondOfMinute(10);

			long initialDelay= startTime.getMillis() -
					systemTime.getMillis();
			LOGGER.info("Start time "+startTime+" System time "+ systemTime);
			OptimizerTimerTask instance = OptimizerTimerTask.getInstance(startTime);
			ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(1);
			ScheduledFuture<?> f = stpe.scheduleAtFixedRate(instance, initialDelay, OPT_RUNNING_PERIOD,
			     TimeUnit.MILLISECONDS);
			try {
				f.get();
			} catch (Exception ex) {
				LOGGER.error("", ex);
			}
		};
		new Thread(run).start();
		STARTED = true;
		return true;
	}

//	@RequestMapping(value = "/strategies", method = RequestMethod.POST)
//	public String setStrategies(@RequestBody FlexibilityStrategies strategies) {
//		OptimizerTimerTask instance = OptimizerTimerTask.getInstance(null);
//		return instance.setStrategies(strategies);
//	}

	@RequestMapping(value = "/select-dayahead-plan", method = RequestMethod.POST)
	public boolean selectDayAheadPlan(@RequestBody Integer id) {
		OptimizerTimerTask instance = OptimizerTimerTask.getInstance(null);
		return instance.setSelectedDayAheadPlanId(id);
	}

	@RequestMapping(value = "/select-intraday-plan", method = RequestMethod.POST)
	public boolean selectIntradayPlan(@RequestBody Integer id) {
		OptimizerTimerTask instance = OptimizerTimerTask.getInstance(null);
		return instance.setSelectedIntraDayPlanId(id);
	}

}
