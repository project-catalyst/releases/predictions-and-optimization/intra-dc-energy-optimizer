package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.fitness;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.EnergyEfficiencySolution;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.MetaheuristicAlgorithm;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.Solution;

import java.util.Arrays;

public class FitnessFunctionGenetic implements IFitnessFunction {
    private OptimizationDataCatalyst data;

    public FitnessFunctionGenetic(OptimizationDataCatalyst data) {
        this.data = data;
    }

    private int countVariations(double[] x, double limit){
        int ret = 0;
        for(int i =0; i< x.length-1; i++){
            if(Math.abs(x[i]-x[i+1]) > limit) ret++;
        }
        return ret;
    }

    private double estimateVariations(double[] x, double loweLimit, double upperLimit){
        double ret = 0;
        for(int i =0; i< x.length-1; i++){
            if(Math.abs(x[i]-x[i+1]) < loweLimit) ret += Math.pow(x[i]-x[i+1],8);
            else
            if(Math.abs(x[i]-x[i+1]) < upperLimit) ret += Math.pow(x[i]-x[i+1],2);
            else ret += Math.pow(x[i]-x[i+1],2);
        }
        return ret;
    }

    private double[] computePrice(EnergyEfficiencySolution solution){
        double[] price = new double[6];
        Arrays.fill(price, 0);
        for (int i = 0; i < data.getSize(); ++i) {
            price[0] += solution.getEDC()[i] * data.getEnergyPrice()[i];
            price[1] += solution.gettDc()[i] * data.getHeatPrice()[i];
            price[2] += Math.pow(solution.getEDC()[i] - data.getDrSignal()[i], 2) * 1000 * data.getDemandPrice()[i]
                    + Math.pow(solution.getEDC()[i] - data.getBaseline()[i],2) * (1.0 / Math.max(0.01, Math.abs(data.getBaseline()[i]-data.getDrSignal()[i])));
            double workloadMoved = 0;
            if(solution.getItRelocateActions()[i]<0) workloadMoved = solution.getItRelocateActions()[i] * data.getDelayTolerableEnergy()[i];
            else workloadMoved = solution.getItRelocateActions()[i] * data.getMaxHost()[0];
            price[3] += workloadMoved * data.getLoadPrice()[i];
            //price[4] +=  Math.pow(solution.getEDC()[i] - (data.getRenewableEnergy()[i] + solution.getDeltaRen()), 2) * data.getEnergyPrice()[i];
            price[4] +=  solution.getEDC()[i] * solution.getRenCost()[i];
            //price[4] += Math.pow(solution.getEDC()[i] - data.getBaseline()[i], 2);
        }
        price[5] = estimateVariations(solution.getEDC(), data.getMaxHost()[0], data.getTesMaxDischargeRate()[0] + data.getBatteryMaxDischargeRate()[0]);
        return price;
    }

    public double computeSum(double[] price){
        double sum =   data.getWE()[0] * price[0] - data.getWT()[0] * price[1] + data.getWF()[0] * price[2]
                - (data.getRelocateActive()[0] + data.getWL()[0]) * price[3]
                + data.getWREN()[0] * price[4];
        return sum;
    }
    public double compute(Solution solution2) {
        EnergyEfficiencySolution solution = (EnergyEfficiencySolution) solution2;
        double sum;
        solution.resetTESandESD();
        double[] price = computePrice(solution);
        sum = computeSum(price);

        double minimum = sum;
        int tesIndex = 0;
        int esdIndex = 0;
        int batteryDurationMax = 0;
        int tesIterations = data.getSize();
        int esdIterations = data.getSize();
//        if(!MetaheuristicAlgorithm.ENABLE_TES) tesIterations = 4;
//        if(!MetaheuristicAlgorithm.ENABLE_ESD) esdIterations = 3;
//        for (int t1 = 0; t1 < tesIterations-3; ++t1){
//            for(int t2 = t1; t2< esdIterations-2; ++t2) {
//                solution.resetTESandESD();
//                solution.dischargeTESatHour(t1);
//                solution.dischargeESDatHour(t2);
//                solution.recomputeActions();
//                price = computePrice(solution);
//                sum = data.getWE()[0] * price[0] - data.getWT()[0] * price[1] + data.getWF()[0] * price[2]
//                        - (data.getRelocateActive()[0] + data.getWL()[0]) * price[3];
//                if(minimum > sum){
//                    minimum = sum;
//                    tesIndex = t1;
//                    esdIndex = t2;
//                }
//            }
//        }
        if((!MetaheuristicAlgorithm.ENABLE_TES)&&(MetaheuristicAlgorithm.ENABLE_ESD)){
                tesIndex = 0;
                for (int batteryDuration = 1; batteryDuration < solution.getBatteryDuration(); batteryDuration++) {
                    for (int t2 = 0; t2 < esdIterations - 2 * batteryDuration; ++t2) {
                        solution.resetTESandESD();
                        solution.dischargeTESatHour(tesIndex);
                        solution.dischargeESDatHour(t2, batteryDuration);
                        solution.recomputeActions();
                        price = computePrice(solution);
                        sum = computeSum(price);
                        if (minimum > sum) {
                            minimum = sum;
                            esdIndex = t2;
                            batteryDurationMax =  batteryDuration;
                            //System.out.println("Optimum battery duration for discharge "+batteryDurationMax);
                        }
                    }
            }
            }

        else if((MetaheuristicAlgorithm.ENABLE_TES)&&(!MetaheuristicAlgorithm.ENABLE_ESD)){
            esdIndex = 0;
            for (int t1 = 0; t1 < tesIterations - 5; ++t1) {
                    solution.resetTESandESD();
                    solution.dischargeTESatHour(t1);
                    solution.dischargeESDatHour(esdIndex, 0);
                    solution.recomputeActions();
                    price = computePrice(solution);
                    sum = computeSum(price);
                    if (minimum > sum) {
                        minimum = sum;
                        tesIndex = t1;
                        batteryDurationMax = 0;
                        //System.out.println("Optimum battery duration for discharge "+batteryDurationMax);
                    }
                }
        }
        else if((MetaheuristicAlgorithm.ENABLE_TES)&&(MetaheuristicAlgorithm.ENABLE_ESD)) {
            for (int t1 = 0; t1 < tesIterations - 5; ++t1) {
                for (int batteryDuration = 1; batteryDuration < solution.getBatteryDuration(); batteryDuration++) {
                    for (int t2 = t1; t2 < esdIterations - 2 * batteryDuration; ++t2) {
                        solution.resetTESandESD();
                        solution.dischargeTESatHour(t1);
                        solution.dischargeESDatHour(t2, batteryDuration);
                        solution.recomputeActions();
                        price = computePrice(solution);
                        sum = computeSum(price);
                        if (minimum > sum) {
                            minimum = sum;
                            tesIndex = t1;
                            esdIndex = t2;
                            batteryDurationMax =  batteryDuration;
                            //System.out.println("Optimum battery duration for discharge "+batteryDurationMax);
                        }
                    }
                }
            }
        }
        solution.resetTESandESD();
        solution.dischargeTESatHour(tesIndex);
        solution.dischargeESDatHour(esdIndex, batteryDurationMax);
        solution.recomputeActions();
        price = computePrice(solution);
        sum = computeSum(price);
        solution.setEnergyCost(price[0]);
        solution.setThermalProfit(price[1]);
        solution.setFlexibilityPenalty(price[2]);
        solution.setLoadProfit(price[3]);
        solution.setDiffRen(price[4]);
        solution.setFitnessValue(sum);
        return sum;
    }

}

