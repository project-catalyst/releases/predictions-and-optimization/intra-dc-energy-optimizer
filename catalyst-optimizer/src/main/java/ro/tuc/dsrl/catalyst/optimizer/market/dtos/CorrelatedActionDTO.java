package ro.tuc.dsrl.catalyst.optimizer.market.dtos;



public class CorrelatedActionDTO {
    private int actionId;
    private String marketplaceForm;

    public CorrelatedActionDTO() {
    }

    public CorrelatedActionDTO(int actionId, String marketplaceForm) {
        this.actionId = actionId;
        this.marketplaceForm = marketplaceForm;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public String getMarketplaceForm() {
        return marketplaceForm;
    }

    public void setMarketplaceForm(String marketplaceForm) {
        this.marketplaceForm = marketplaceForm;
    }
}
