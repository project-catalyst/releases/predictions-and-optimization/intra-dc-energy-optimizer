package ro.tuc.dsrl.catalyst.optimizer.algorithm;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.OptimizationProblem;

public class GeneticAlgorithm implements OptimizationAlgorithm {
    private OptimizationDataCatalyst data;
    private OptimizationProblem optimizationProblem;

    public GeneticAlgorithm(OptimizationDataCatalyst data) {
        this.data = data;

        optimizationProblem = new OptimizationProblem();
    }

    @Override
    public OptimizationDataCatalyst getOptimizationData() {
        return data;
    }

    @Override
    public void compute(ServiceType serviceType) {
        this.data = this.optimizationProblem.computeOptimalSolution(this.data);
    }
}
