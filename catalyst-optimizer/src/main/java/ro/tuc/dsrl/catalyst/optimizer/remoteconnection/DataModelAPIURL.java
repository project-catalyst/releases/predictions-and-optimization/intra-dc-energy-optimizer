//package ro.tuc.dsrl.catalyst.optimizer.remoteconnection;
//
//import ro.tuc.dsrl.catalyst.optimizer.utility.ConfigurationProperties;
//import ro.tuc.dsrl.catalyst.optimizer.utility.ExecutionPropertiesLoader;
//
///**
// * @Author: Technical University of Cluj-Napoca, Romania
// *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
// * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
// * @Module: intra-dc-energy-optimizer
// * @Since: Feb 2, 2015
// * @Description:
// *
// */
//public enum DataModelAPIURL implements APIURL {
//	BATTERY_ALL("/db/batteries/{date}"),
//	THERMAL_STORAGE_ALL("/db/thermal-storages/{date}"),
//	OPTIMIZATION_PLANS("/optimization-plan/opt-container"),
//	PREDICTIONS("/predictions/container"),
//	SAVE_ACTIONS("/optimization-plan/actions"),
//	TOTAL_POWER("/db/total-power/{date}"),
//	SAVE_AS_SIGNAL("/db/as-signal/{startdate},{enddate}"),
//	SAVE_AS_RESULT("/db/as-result/{startdate}"),
//	DC_PRICES("/db/dc-prices/{startdate},{enddate}"),
//	INCOMING_REALLOCATED_WORKLOAD("/db/incoming-reallocated-workload/{startdate},{enddate},{sample}"),
//	DELETE_REDUNDANT_ACTIONS("/optimization-plan/delete-redundant-actions/{date}/{confidence}"),
//
//    // NEW ENDPOINTS FROM CATALYST DB API
//	OPTIMIZATION_PLANS_NEW("/optimization/optimization-plan/opt-container");
//
//	private final String resourceHost = ExecutionPropertiesLoader.getProperty(ConfigurationProperties.DM_API_HOST);
//
//	private String value;
//	DataModelAPIURL(String url) {
//		this.value = url;
//	}
//
//
//	@Override
//	public String value() {
//		return resourceHost + value;
//	}
//}
