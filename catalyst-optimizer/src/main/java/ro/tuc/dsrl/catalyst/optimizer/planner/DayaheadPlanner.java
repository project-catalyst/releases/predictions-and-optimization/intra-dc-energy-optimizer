package ro.tuc.dsrl.catalyst.optimizer.planner;

import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileMultipleEnergyTypesDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySamplesForMeasurementDTO;
import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.model.dto.MarketDataDTO;
import ro.tuc.dsrl.catalyst.model.enums.AggregationGranularity;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.*;
import ro.tuc.dsrl.catalyst.optimizer.manager.OptimizationPlanNotification;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketData;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketManager;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTimeframes;
import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictedData;
import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictionHandler;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystMetricsApiURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelValuesIO;

import java.util.*;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 * Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 4, 2015
 * @Description:
 */
public class DayaheadPlanner extends Planner {
    private static final Logger LOGGER = LoggerFactory.getLogger(DayaheadPlanner.class);

    private static final double PREDICTION_CONFIDENCE_LEVEL = 0.0;
    public static final int PREDICTION_PERIOD_LENGTH = 24;
    private static final int SAMPLING_VALUE = 1;
    private static final int LESS_MAX_IT = 500000;
    private static final int OPTIMIZER_DELTA = 20; //timpul necesar rularii optimizatorului in minute
    private static final int DAYAHEAD_HOURS = 8; //perioada in ore pentru dayahead

    public static final int TIME_HORIZON = PREDICTION_PERIOD_LENGTH / SAMPLING_VALUE;
    public static final Period DURATION = new Period().withHours(PREDICTION_PERIOD_LENGTH);
    public static final Period SAMPLING = new Period().withHours(SAMPLING_VALUE);
    //public static final String TIMEFRAME = "day_ahead";

    private List<List<Double>> delayTolerablePercentageList;
    private List<List<Double>> predictedRT;

    private static MarketManager marketManager;

    public DayaheadPlanner(ActionPlan actionPlan, DCResources resources, MarketManager marketManager) {
        super(actionPlan, resources);
        this.delayTolerablePercentageList = new ArrayList<List<Double>>();
        this.predictedRT = new ArrayList<List<Double>>();
        this.marketManager = marketManager;
    }

    public List<ActionPlan> execute(FlexibilityStrategyDTO strategies, DateTime startTime, DateTime currentTime) {
        ResourceConstraints startConstraint = getStartConstraint(startTime);
        ResourceConstraints endConstraint = getEndConstraint(startTime.plus(DURATION));
        int index = 0;

        MarketData marketData = marketManager.getMarketData(startTime, MarketTimeframes.DAYAHEAD);
        PredictedData predictedData = computePredictedData(startTime, currentTime, PREDICTION_CONFIDENCE_LEVEL, index);

        OptimizationAlgorithm optimizationAlgorithm = OptimizationAlgorithmFactory.create(marketData.getDrSignal(),
                                                    strategies, predictedData, marketData, resources,
                                                    startConstraint, endConstraint, TIME_HORIZON, AlgorithmType.HYBRID);
        optimizationAlgorithm.compute(ServiceType.FLEXIBILITY);
        OptimizationDataCatalyst optimData = optimizationAlgorithm.getOptimizationData();

        ExcelValuesIO.writeDataToCSVFile(optimData, "day-ahead-post" + startTime.dayOfYear()+"-"+
                startTime.getMonthOfYear()+"-"+ startTime.getDayOfMonth()+"-"+
                startTime.getHourOfDay() + "-" + index + ".csv");
        if (!optimData.isSuccess()) {
            LOGGER.error("Error in optimization data, day-ahead action plan skiped for confidence level " + PREDICTION_CONFIDENCE_LEVEL);
        }
        ActionPlan actionPlan = OptimizationActionAdapter.extract(optimData, resources, startTime, SAMPLING, DURATION);
        ExcelValuesIO.writePlan(actionPlan, "day-ahead-post-plan" + new Date().getTime() + "-" + index + ".csv");
        actionPlan.setConfidenceLevel(PREDICTION_CONFIDENCE_LEVEL);
        actionPlan.setId(index);

        actionPlan.setWE(optimData.getWE()[0]);
        actionPlan.setWL(optimData.getWL()[0]);
        actionPlan.setWF(optimData.getWF()[0]);
        actionPlan.setWT(optimData.getWT()[0]);
        actionPlan.setWRen(optimData.getWREN()[0]);
        actionPlan.setReallocActive(optimData.getRelocateActive()[0] != 0);

        List<ActionPlan> plans = new ArrayList<ActionPlan>();
        plans.add(actionPlan);
        saveOptimizedProfilesToDB(optimData, startTime);
        return plans;
    }


    private void saveOptimizedProfilesToDB(OptimizationDataCatalyst data, DateTime startTime) {

        //==== ADAPTED PROFILES ====
        Double[] optimizedDCArray = ArrayUtils.toObject(data.getDcFinalConsumption());
        List<Double> optimizedDCProfile = Arrays.asList(optimizedDCArray);

        Double[] optimizedCoolingArray = ArrayUtils.toObject(data.getFinalCooling());
        List<Double> optimizedCoolingProfile = Arrays.asList(optimizedCoolingArray);

        Double[] optimizedServerArray = ArrayUtils.toObject(data.getItLoad());
        List<Double> optimizedServerProfile = Arrays.asList(optimizedServerArray);

        Double[] optimizedThermalArray = ArrayUtils.toObject(data.getDcThermalGeneration());
        List<Double> optimizedThermalProfile = Arrays.asList(optimizedThermalArray);

        Double[] optimizedCO2Array = ArrayUtils.toObject(data.getCo2adapted());
        List<Double> optimizedCO2Profile = Arrays.asList(optimizedCO2Array);

        //==== BASELINE PROFILES ====

        Double[] baselineThermalArray = ArrayUtils.toObject(data.getHeatBaseline());
        List<Double> baselineThermalProfile = Arrays.asList(baselineThermalArray);

        Double[] baselineCO2Array = ArrayUtils.toObject(data.getCo2baseline());
        List<Double> baselineCO2Profile = Arrays.asList(baselineCO2Array);

        EnergySamplesForMeasurementDTO optimizedServerSamples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_SERVER, optimizedServerProfile);
        EnergySamplesForMeasurementDTO optimizedThermalSamples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_THERMAL, optimizedThermalProfile);
        EnergySamplesForMeasurementDTO optimizedCO2Samples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_CO2, optimizedCO2Profile);
        EnergySamplesForMeasurementDTO optimizedDCSamples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_DC, optimizedDCProfile);
        EnergySamplesForMeasurementDTO optimizedCoolingSamples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_COOLING, optimizedCoolingProfile);
        EnergySamplesForMeasurementDTO baselineCO2Samples = new EnergySamplesForMeasurementDTO(MeasurementType.DATA_CENTER_HEAT_BASELINE, baselineThermalProfile);
        EnergySamplesForMeasurementDTO baselineDCSamples = new EnergySamplesForMeasurementDTO(MeasurementType.DATA_CENTER_CO2_BASELINE, baselineCO2Profile);

        List<EnergySamplesForMeasurementDTO> optimizedSamplesForThermalAndEnergy = new ArrayList<>();
        optimizedSamplesForThermalAndEnergy.add(optimizedServerSamples);
        optimizedSamplesForThermalAndEnergy.add(optimizedThermalSamples);
        optimizedSamplesForThermalAndEnergy.add(optimizedCO2Samples);
        optimizedSamplesForThermalAndEnergy.add(optimizedDCSamples);
        optimizedSamplesForThermalAndEnergy.add(optimizedCoolingSamples);
        optimizedSamplesForThermalAndEnergy.add(baselineCO2Samples);
        optimizedSamplesForThermalAndEnergy.add(baselineDCSamples);

        EnergyProfileMultipleEnergyTypesDTO energyProfile = new EnergyProfileMultipleEnergyTypesDTO(DATACENTER_NAME,
                AggregationGranularity.HOUR, PredictionGranularity.DAYAHEAD, PredictionType.OPTIMIZED_PROFILE,
                ro.tuc.dsrl.catalyst.model.enums.AlgorithmType.OPTIMIZATION, startTime.getMillis(), optimizedSamplesForThermalAndEnergy);

        OptimizationPlanNotification.notifyOptimizedProfile(energyProfile);

        //========MarketPlace Data=============
        MarketDataDTO marketDataDTO = new MarketDataDTO();
        List<Double> drSignal = new ArrayList<>();
        if(data.getDrSignal()!=null) {
            for (int i = 0; i < data.getDrSignal().length; i++) {
                drSignal.add(data.getDrSignal()[i]);
            }
            marketDataDTO.setDrSignal(drSignal);
        }
        marketDataDTO.setRecordTime(DateUtils.millisToLocalDateTime(startTime.getMillis()));
        OptimizationPlanNotification.notifyMarketData(marketDataDTO);


    }


    /**
     * @param startTime
     * @param confLevel
     * @return
     */
    private PredictedData computePredictedData(DateTime startTime, DateTime currentTime, double confLevel, int index) {
        PredictedData predictedData = PredictionHandler.getDayAheadPredictedData(startTime, confLevel, currentTime);
        List<Double> dtp = getDelayTolerablePercentage(predictedData);
        delayTolerablePercentageList.add(index, dtp);
        predictedRT.add(index, predictedData.getRealtimeWorkload());
        return predictedData;
    }

    /**
     * @param predictedData
     * @return
     */
    private List<Double> getDelayTolerablePercentage(PredictedData predictedData) {
        List<Double> dtp = new ArrayList<Double>();
        for (int i = 0; i < predictedData.getRealtimeWorkload().size(); i++) {
            dtp.add(predictedData.getDelayTolerableWorkload().get(i) / predictedData.getRealtimeWorkload().get(i));
        }
        return dtp;
    }

//	private void setLocalRealocDTWorkloadPercentage(double[] migrationSchedulingMatrix) {
//		final int reallocMatrixSlots = DayaheadPlanner.TIME_HORIZON / IntradayPlanner.PREDICTION_PERIOD_LENGTH;
//		localRealocDTWorkloadPercentage = new ArrayList<Double>();
//
//		for (int i = 0; i < DayaheadPlanner.TIME_HORIZON; i++) {
//			localRealocDTWorkloadPercentage.add(migrationSchedulingMatrix[i/reallocMatrixSlots]);
//		}
//	}
//	
//	public List<Double> getLocalRealocDTWorkloadPercentage() {
//		return localRealocDTWorkloadPercentage;
//	}

    /**
     * @param index
     * @return
     */
    public List<Double> getDelayTolerablePercentage(int index) {
        return delayTolerablePercentageList.get(index);
    }

    public List<Double> getPredictedRT(int index) {
        return predictedRT.get(index);
    }
}
