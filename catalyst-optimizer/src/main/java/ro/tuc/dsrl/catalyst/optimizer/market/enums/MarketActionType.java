package ro.tuc.dsrl.catalyst.optimizer.market.enums;

public enum MarketActionType {
    SELL("offer"),
    BUY("bid");

    private String value;

    MarketActionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
