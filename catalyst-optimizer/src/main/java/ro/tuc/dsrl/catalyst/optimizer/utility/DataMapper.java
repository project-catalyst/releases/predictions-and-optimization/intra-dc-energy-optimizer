package ro.tuc.dsrl.catalyst.optimizer.utility;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;

import java.util.List;
import java.util.Map;

public class DataMapper {

    public static OptimizationDataCatalyst map(Map<String, List<Double>> params)
    {
        OptimizationDataCatalyst optimizationDataCatalyst = new OptimizationDataCatalyst(params.get("MAX_T").get(0).intValue());

        optimizationDataCatalyst.setRealtimeEnergy(arrayFromList(params.get("Real Time Workload")));
        optimizationDataCatalyst.setDelayTolerableEnergy(arrayFromList(params.get("Delay Tolerant Workload")));
        optimizationDataCatalyst.setEnergyPrice(arrayFromList(params.get("Electrical Energy Price") != null ? params.get("Electrical Energy Price") : params.get("E_Price") != null ? params.get("E_Price") : params.get("Energy Price")));
        optimizationDataCatalyst.setLoadPrice(arrayFromList(params.get("L_Price")));
        optimizationDataCatalyst.setItCooling(arrayFromList(params.get("Cooling System")));
        optimizationDataCatalyst.setHostWorkload(arrayFromList(params.get("Hosted Workload from partner DC")));

        //HeatDemand ?
        optimizationDataCatalyst.setHeatBaseline(arrayFromList(params.get("Heat Demand")));

        optimizationDataCatalyst.setHeatPrice(arrayFromList(params.get("T_Price")));
        optimizationDataCatalyst.setDemandPrice(arrayFromList(params.get("D_Price")));
        optimizationDataCatalyst.setDrSignal(arrayFromList(params.get("DR_signal")));

        optimizationDataCatalyst.setTimePeriod(arrayFromList(params.get("MAX_T")));

        optimizationDataCatalyst.setFactor(arrayFromList(params.get("f_ESD")));
        optimizationDataCatalyst.setBatteryMaxCapacity(arrayFromList(params.get("ESD_MAX")));
        optimizationDataCatalyst.setFactorTes(arrayFromList(params.get("f_TES")));
        optimizationDataCatalyst.setTesMaxCapacity(arrayFromList(params.get("TES_MAX")));
        optimizationDataCatalyst.setTesChargeLossRate(arrayFromList(params.get("TES_R")));
        optimizationDataCatalyst.setTesDischargeLossRate(arrayFromList(params.get("TES_D")));
        optimizationDataCatalyst.setBatteryChargeLossRate(arrayFromList(params.get("ESD_R")));
        optimizationDataCatalyst.setBatteryDischargeLossRate(arrayFromList(params.get("ESD_D")));
        optimizationDataCatalyst.setTesMaxDischargeRate(arrayFromList(params.get("MAX_DIS_TES")));
        optimizationDataCatalyst.setBatteryMaxDischargeRate(arrayFromList(params.get("MAX_DIS_ESD")));
        optimizationDataCatalyst.setDod(arrayFromList(params.get("DoD")));
        optimizationDataCatalyst.setItMaxConsumption(arrayFromList(params.get("MAX_IT")));
        optimizationDataCatalyst.setCopC(arrayFromList(params.get("COP_C")));
        optimizationDataCatalyst.setCopH(arrayFromList(params.get("COP_H")));
        optimizationDataCatalyst.setMaxHost(arrayFromList(params.get("MAX_Host")));
        optimizationDataCatalyst.setMaxRealloc(arrayFromList(params.get("MAX_Relocate")));
        optimizationDataCatalyst.setRelocateActive(arrayFromList(params.get("relocateEnable")));

        optimizationDataCatalyst.setTesInitialValue(arrayFromList(params.get("TES_INIT")));
        optimizationDataCatalyst.setBatteryInitialValue(arrayFromList(params.get("ESD_INIT")));
        optimizationDataCatalyst.setBatteryFinalConstraint(arrayFromList(params.get("ESD_FIN")));
        optimizationDataCatalyst.setTesFinalConstraint(arrayFromList(params.get("TES_FIN")));

        optimizationDataCatalyst.setWE(arrayFromList(params.get("WE")));
        optimizationDataCatalyst.setWF(arrayFromList(params.get("WF")));
        optimizationDataCatalyst.setWT(arrayFromList(params.get("WT")));
        optimizationDataCatalyst.setWL(arrayFromList(params.get("WL")));


        optimizationDataCatalyst.setDcThermalGeneration(arrayFromList(params.get("DC Thermal generation without optimization")));

        optimizationDataCatalyst.setBaseline(arrayFromList(params.get("InitialDC")));


        return optimizationDataCatalyst;
    }

    private static double[] arrayFromList(List<Double> list) {
        double[] answer = new double[list.size()];
        int k=0;

        for (Double d: list) {
            answer[k] = d;
            k++;
        }

        System.out.println(list);
        return answer;
    }
}
