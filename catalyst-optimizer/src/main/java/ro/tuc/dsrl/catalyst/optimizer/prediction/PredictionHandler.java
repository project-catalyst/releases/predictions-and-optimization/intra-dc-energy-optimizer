package ro.tuc.dsrl.catalyst.optimizer.prediction;

import org.joda.time.DateTime;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class PredictionHandler {

	//private static PredictionInterface instance = new PredictionCatalyst();

	private static PredictionInterface instance = new PredictionNewEBB();

	private PredictionHandler() {
	}

	public static PredictedData getDayAheadPredictedData(DateTime startDate, double confidenceLevel,
			DateTime currentDate) {
		return instance.getDayAheadPredictedData(startDate, confidenceLevel, currentDate);
	}

	public static PredictedData getIntraDayPredictedData(DateTime startDate, double confidenceLevel,
			DateTime currentDate) {
		return instance.getIntraDayPredictedData(startDate, confidenceLevel, currentDate);
	}

	/**
	 * @param instance the instance to set
	 */
	public static void setInstance(PredictionInterface instance) {
		PredictionHandler.instance = instance;
	}

}
