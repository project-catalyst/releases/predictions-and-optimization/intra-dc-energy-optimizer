package ro.tuc.dsrl.catalyst.optimizer.prediction;

import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.model.dto.EnergyPointValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.optimizer.planner.DayaheadPlanner;
import ro.tuc.dsrl.catalyst.optimizer.planner.IntradayPlanner;
import ro.tuc.dsrl.catalyst.optimizer.planner.Planner;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.APIURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystDBApiURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.catalyst.optimizer.utility.MathUtility;
import ro.tuc.dsrl.geyser.datamodel.other.MarketPrices;

import java.util.*;

public class PredictionNewEBB implements PredictionInterface {

    private static final List<Integer> DELAY_INTADAY_DEADLINE = new ArrayList<Integer>(
            Collections.nCopies(IntradayPlanner.INTRA_DAY_SLOTS, IntradayPlanner.INTRA_DAY_SLOTS));


    @Override
    public PredictedData getDayAheadPredictedData(DateTime startDate, double confidenceLevel, DateTime currentDate) {
        PredictedData predictedData = new PredictedData();
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("startdate", startDate.getMillis());
        urlVariables.put("currentdate", currentDate.getMillis());
        urlVariables.put("datacenter-name", Planner.DATACENTER_NAME);

        predictedData.setRealtimeWorkload(forecastDayAheadRT(urlVariables));
        predictedData.setDelayTolerableWorkload(forecastDayAheadDT(urlVariables));
        predictedData.setDelayDeadline(getDayAheadDelayDeadline());
        predictedData.setRenewableEnergy(forecastDayAheadRenewable(urlVariables));
        predictedData.setBaseline(getBaselineDayahead(urlVariables));
        predictedData.setDcsHourlyPrices(getDADCsHourlyPrices(startDate, CatalystDBApiURL.DAYAHEAD_REFERENCE_PRICE));

        return predictedData;
    }

    @Override
    public PredictedData getIntraDayPredictedData(DateTime startDate, double confidenceLevel, DateTime currentDate) {
        PredictedData predictedData = new PredictedData();
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("startdate", startDate.getMillis());
        urlVariables.put("currentdate", currentDate.getMillis());
        urlVariables.put("datacenter-name", Planner.DATACENTER_NAME);

        predictedData.setRealtimeWorkload(forecastIntradayRT(urlVariables));
        predictedData.setDelayTolerableWorkload(forecastIntradayDT(urlVariables));
        predictedData.setDelayDeadline(DELAY_INTADAY_DEADLINE);
        predictedData.setBaseline(getBaselineIntraday(urlVariables));
        predictedData.setDcsHourlyPrices(getDADCsHourlyPrices(startDate, CatalystDBApiURL.INTRADAY_REFERENCE_PRICE));
        predictedData.setRenewableEnergy(forecastIntradayRenewable(urlVariables));

        return predictedData;
    }

    private static List<Double> forecastDayAheadRenewable(Map<String, Object> urlVariables) {
        ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
                .getObject(CatalystDBApiURL.RENEWABLE_DAYAHEAD, ForecastDayAheadDTO.class, urlVariables);

        return getPredictionValues(forecastDayAheadDTO.getEnergyPointValueDTOS());
    }


    private static List<Double> forecastDayAheadRT(Map<String, Object> urlVariables) {
        ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
                .getObject(CatalystDBApiURL.FORECAST_DAY_REALTIME_WORKLOAD, ForecastDayAheadDTO.class, urlVariables);

        return getPredictionValues(forecastDayAheadDTO.getEnergyPointValueDTOS());
    }

    private static List<Double> forecastDayAheadDT(Map<String, Object> urlVariables) {
        ForecastDayAheadDTO forecastDayAheadDTO =  RestAPIClient.getObject(
                CatalystDBApiURL.FORECAST_DAY_DELAY_TOLERABLE_WORKLOAD, ForecastDayAheadDTO.class, urlVariables);
        return getPredictionValues(forecastDayAheadDTO.getEnergyPointValueDTOS());
    }


    private static List<Double> forecastIntradayRenewable(Map<String, Object> urlVariables) {
        ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
                .getObject(CatalystDBApiURL.RENEWABLE_INTRADAY, ForecastDayAheadDTO.class, urlVariables);
        return getPredictionValues(forecastDayAheadDTO.getEnergyPointValueDTOS());
    }


    private static List<Double> forecastIntradayRT(Map<String, Object> urlVariables) {
        ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
                .getObject(CatalystDBApiURL.FORECAST_INTRADAY_REALTIME_WORKLOAD_RT, ForecastDayAheadDTO.class, urlVariables);
        return getPredictionValues(forecastDayAheadDTO.getEnergyPointValueDTOS());
    }

    private static List<Double> forecastIntradayDT(Map<String, Object> urlVariables) {
        ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
                .getObject(CatalystDBApiURL.FORECAST_INTRADAY_REALTIME_WORKLOAD_DT, ForecastDayAheadDTO.class, urlVariables);
        return getPredictionValues(forecastDayAheadDTO.getEnergyPointValueDTOS());
    }

    private static List<Double> getBaselineIntraday(Map<String, Object> urlVariables) {
        ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
                .getObject(CatalystDBApiURL.BASELINE_INTRADAY, ForecastDayAheadDTO.class, urlVariables);
        return getPredictionValues(forecastDayAheadDTO.getEnergyPointValueDTOS());
    }

    private static List<Double> getBaselineDayahead(Map<String, Object> urlVariables) {
        ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
                .getObject(CatalystDBApiURL.BASELINE_DAYAHEAD, ForecastDayAheadDTO.class, urlVariables);
        return getPredictionValues(forecastDayAheadDTO.getEnergyPointValueDTOS());
    }


    private static List<Double> getPredictionValues(List<EnergyPointValueDTO> energyPointValues) {
        List<Double> values = new ArrayList<Double>();
        for (EnergyPointValueDTO energyPointValue : energyPointValues) {
            values.add(MathUtility.round4Decimals(energyPointValue.getEnergyValue()));
        }
        return values;
    }

    private static double[] getPrice(List<EnergyPointValueDTO> energyPointValues) {
        double[] values = new double[24];
        int i =0;
        for (EnergyPointValueDTO energyPointValue : energyPointValues) {
            values[i] = (MathUtility.round4Decimals(energyPointValue.getEnergyValue()));
            i++;
        }
        return values;
    }



    private static List<Integer> getDayAheadDelayDeadline() {
        List<Integer> delayDeadline = new ArrayList<Integer>();
        for (int i = 0; i < DayaheadPlanner.TIME_HORIZON; i++) {
            delayDeadline.add(DayaheadPlanner.TIME_HORIZON);
        }
        return delayDeadline;
    }


    private static MarketPrices getDayAheadMarketPrices(Map<String, Object> urlVariables) {
        return RestAPIClient.getObject(CatalystDBApiURL.DAYAHEAD_REFERENCE_PRICE,
                MarketPrices.class, urlVariables);
    }

    /**
     * @return
     */

    private static MarketPrices getIntraDayMarketPrices(Map<String, Object> urlVariables) {
        System.out.println(CatalystDBApiURL.INTRADAY_REFERENCE_PRICE.value() + " " + urlVariables.get("startdate") + " " + urlVariables.get("datacenter-name")  );
        return RestAPIClient.getObject(CatalystDBApiURL.INTRADAY_REFERENCE_PRICE,
                MarketPrices.class, urlVariables);
    }

    /**
     * @return
     */

    /**
     * @return
     */

    public double[][] getDADCsHourlyPrices(DateTime startDate, APIURL apiurl) {
//        DateTime endTime = startDate.plusHours(DayaheadPlanner.TIME_HORIZON);
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("startdate", startDate.getMillis());
        urlVariables.put("datacenter-name", Planner.DATACENTER_NAME);
        MarketPrices marketPrices = RestAPIClient.getObject(apiurl, MarketPrices.class,urlVariables);
        double [][] prices = new double[4][24];
        prices[0] = getPrice(marketPrices.getDrIncentives().getEnergyPointValueDTOS());
        prices[1] = getPrice(marketPrices.getEnergyPrices().getEnergyPointValueDTOS());
        prices[2] = getPrice(marketPrices.getThermalPrices().getEnergyPointValueDTOS());
        prices[3] = getPrice(marketPrices.getWorkloadPrices().getEnergyPointValueDTOS());
        return prices;
    }


}
