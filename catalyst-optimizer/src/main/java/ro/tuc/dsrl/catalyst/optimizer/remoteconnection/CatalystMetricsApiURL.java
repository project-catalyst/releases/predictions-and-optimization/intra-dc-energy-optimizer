package ro.tuc.dsrl.catalyst.optimizer.remoteconnection;

import ro.tuc.dsrl.catalyst.optimizer.utility.PropertiesLoader;

public enum CatalystMetricsApiURL implements APIURL {
    DAYAHEAD_METRICS("/metrics-calculator/plan-metrics/dayahead/{millis}");


    private final String catalystMetricsApiHost = PropertiesLoader.CATALYST_METRIC_API_HOST;
    private String value;

    CatalystMetricsApiURL(String value) {
        this.value = value;
    }


    @Override
    public String value() {
        return catalystMetricsApiHost + value;
    }
}