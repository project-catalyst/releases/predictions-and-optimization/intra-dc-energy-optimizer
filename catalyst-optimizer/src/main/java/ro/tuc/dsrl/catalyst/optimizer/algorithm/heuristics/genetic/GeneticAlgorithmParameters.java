package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic;

class GeneticAlgorithmParameters {
    static final int NUMBER_OF_CROMOZOMES = 5000;
    static final int NUMBER_OF_ITERATIONS = 40;

    static final double MUTATION_PROBABILITY = 0.7;

    private GeneticAlgorithmParameters() {
    }
}
