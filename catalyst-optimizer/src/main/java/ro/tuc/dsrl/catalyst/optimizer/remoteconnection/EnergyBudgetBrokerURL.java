//package ro.tuc.dsrl.catalyst.optimizer.remoteconnection;
//
//import ro.tuc.dsrl.catalyst.optimizer.utility.ExecutionPropertiesLoader;
//import ro.tuc.dsrl.catalyst.optimizer.utility.ConfigurationProperties;
//
///**
// * @Author: Technical University of Cluj-Napoca, Romania
// *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
// * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
// * @Module: intra-dc-energy-optimizer
// * @Since: Feb 2, 2015
// * @Description:
// *
// */
//public enum EnergyBudgetBrokerURL implements APIURL {
//
//	FORECAST_DAY_REALTIME_WORKLOAD(
//			"/energy-consumption-dummy-curve/forecasted/realtime/day/{startdate},{currentdate},{confidencelevel}"),
//	FORECAST_INTRADAY_REALTIME_WORKLOAD(
//			"/energy-consumption-dummy-curve/forecasted/realtime/intraday/{startdate},{currentdate},{confidencelevel}"),
//	FORECAST_DAY_DELAY_TOLERABLE_WORKLOAD(
//			"/energy-consumption-dummy-curve/forecasted/delay-tolerable/day/{startdate},{currentdate},{confidencelevel}"),
//	FORECAST_DAY_ENERGY_PRODUCTION(
//			"/energy-production-curve/forecasted/day/{startdate}"),
//	FORECAST_INTRADAY_ENERGY_PRODUCTION("/energy-production-curve/forecasted/intraday/{startdate}"),
//	FORECAST_INTRADAY_ENERGY_FROM_MARKET("/energy-production-curve/market/intraday/{currentdate}"),
//	FORECAST_DAY_ENERGY_FROM_MARKET("/energy-production-curve/market/day/{currentdate}");
//
//	private String value;
//
//	private final String ebbHost = ExecutionPropertiesLoader.getProperty(ConfigurationProperties.EBB_API_HOST);
//
//	EnergyBudgetBrokerURL(String url) {
//		this.value = url;
//	}
//
//	@Override
//	public String value() {
//		return ebbHost + value;
//	}
//}
