package ro.tuc.dsrl.catalyst.optimizer.algorithm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by doru on 12/10/2015.
 */
public class FileAlgorithm implements OptimizationAlgorithm {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileAlgorithm.class);

    private OptimizationDataCatalyst data;
    private String[][] values = new String[50][];
    private int numberLines;

    public FileAlgorithm(OptimizationDataCatalyst data) {
        this.data = data;
        numberLines = 0;
    }

    @Override
    public OptimizationDataCatalyst getOptimizationData() {
        return data;
    }

    @Override
    public void compute(ServiceType serviceType) {

        String file = "";
        double timePeriod = data.getTimePeriod()[0];

        if(timePeriod == 8.0){
            file = "/src/main/resources/file-algo-model/Scenario-PSNC-intraday.csv";
        } else if(timePeriod ==24.0){
            file = "/src/main/resources/file-algo-model/Scenario-PSNC-day-ahead.csv";
        }


        InputStream input = FileAlgorithm.class.getResourceAsStream(file);
        BufferedReader br;

        String line;
        numberLines = 0;

        try {
            br = new BufferedReader(new InputStreamReader(input));

            LOGGER.info(br.readLine());
            while ((line = br.readLine()) != null) {
                values[numberLines] = line.split(",");
                numberLines++;
            }

            // output values
            data.setEstimatedDelayExecution(getCsvColumn(0));
            data.setItCooling(getCsvColumn(1));
            data.setFinalCooling(getCsvColumn(2));
            data.setTesLevel(getCsvColumn(3));
            data.setDischargeTes(getCsvColumn(4));
            data.setChargeTes(getCsvColumn(5));
            data.setBatteryLevel(getCsvColumn(6));
            data.setDischargeBattery(getCsvColumn(7));
            data.setChargeBattery(getCsvColumn(8));
            data.setDcFinalConsumption(getCsvColumn(9));
            data.setDcThermalGeneration(getCsvColumn(10));
            data.setRelocateWorkload(getCsvColumn(11));
            data.setHostWorkload(getCsvColumn(12));

            // optimization objectives
            data.setEnergyCost(getCsvColumn(13));
            data.setThermalProfit(getCsvColumn(14));
            data.setFlexibilityPenalty(getCsvColumn(15));
            data.setLoadProfit(getCsvColumn(16));
            data.setTotalCost(getCsvColumn(17));

            // get scheduling matrix
            int scheduleMatrixStartColumn = 18;
            int matrixSize = 0;

            if(file.contains("intraday")){
                matrixSize = 8;
            } else if(file.contains("day-ahead")){
                matrixSize = 24;
            }

            data.setSchedulingMatrix(getSchedulingMatrix(scheduleMatrixStartColumn, matrixSize));

//            data.setEnableDieselGen(getCsvColumn(10));
//            data.setFinalDCPowerFactor(getCsvColumn(11));
//            data.setOptimizationCost(getCsvColumn(12));
//
//            data.setDelayDeadline(new double[]{4.0,4.0,4.0,4.0});
//            data.setMigrationSchedulingMatrix(getCsvColumn(17));
            data.setSuccess(true);
            LOGGER.info("Read data from file");
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage());
        }
    }

    private double[] getCsvColumn(int i) {
        List<Double> list = new ArrayList<>();

        int k=0;
        while (k<numberLines && values[k].length>i && !values[k][i].equals("")) {
            list.add(Double.valueOf(values[k][i]));
            k++;
        }

        return arrayFromList(list);
    }

    private double[] getSchedulingMatrix() {
        List<Double> list = new ArrayList<>();

        for (int i=0;i<=3;i++) {
            for (int j=13;j<=16;j++) {
                list.add(Double.valueOf(values[i][j]));
            }
        }
        return arrayFromList(list);
    }

    private double[] getSchedulingMatrix(int startCol, int size) {

        List<Double> list = new ArrayList<>();

        for(int i = 0; i < size; i++){
            for(int j = startCol; j < startCol + size; j++){
                list.add(Double.valueOf(values[i][j]));
            }
        }

        return arrayFromList(list);
    }

    private double[] arrayFromList(List<Double> list) {
        double[] answer = new double[list.size()];
        int k=0;

        for (Double d: list) {
            answer[k] = d;
            k++;
        }

        return answer;
    }


}
