package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;

public class OptimizationProblem {
    public OptimizationDataCatalyst computeOptimalSolution(OptimizationDataCatalyst data) {
        MetaheuristicAlgorithm algorithm = new MetaheuristicAlgorithm(data);
        return algorithm.run();
    }
}
