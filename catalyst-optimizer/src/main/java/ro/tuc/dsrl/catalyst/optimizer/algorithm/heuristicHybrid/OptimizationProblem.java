package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;

public class OptimizationProblem {
    public OptimizationDataCatalyst computeOptimalSolution(OptimizationDataCatalyst data) {
        MetaheuristicAlgorithm algorithm = new MetaheuristicAlgorithm(data);
        return algorithm.run();
    }
}
