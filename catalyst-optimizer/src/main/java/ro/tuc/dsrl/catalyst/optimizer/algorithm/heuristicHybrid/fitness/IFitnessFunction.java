package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.fitness;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.Solution;

public interface IFitnessFunction {
    double compute(Solution solution);
}
