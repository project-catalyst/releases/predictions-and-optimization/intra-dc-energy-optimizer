package ro.tuc.dsrl.catalyst.optimizer.algorithm;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.MetaheuristicAlgorithm;
import ro.tuc.dsrl.catalyst.optimizer.planner.DayaheadPlanner;
import ro.tuc.dsrl.catalyst.optimizer.planner.IntradayPlanner;
import ro.tuc.dsrl.geyser.datamodel.actions.*;
import ro.tuc.dsrl.geyser.datamodel.components.EnergyStorageComponent;
import ro.tuc.dsrl.catalyst.optimizer.planner.ActionPlan;
import ro.tuc.dsrl.catalyst.optimizer.planner.ResourceConstraints;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.catalyst.optimizer.utility.MathUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 4, 2015
 * @Description:
 *
 */
public class OptimizationActionAdapter {

	private static long actionId = 10;
	public static final double DAY_AHEAD_FACTOR = 1.0;
	public static final double INTRADAY_FACTOR = 0.5;

	public static final double BID_PERCENTAGE = -0.05;
	public static final double OFFER_PERCENTAGE = 0.1;
	public static final int REALLOC_MATRIX_SLOTS = DayaheadPlanner.TIME_HORIZON / IntradayPlanner.PREDICTION_PERIOD_LENGTH;

	private OptimizationActionAdapter() {
	}

	// on output from lingo, entry at hour i is the value at the beginning of
	// hour i
	public static ActionPlan extract(OptimizationDataCatalyst optimizationData, DCResources resources, DateTime startTime,
			Period sampling, Period duration) {
		double factor = DAY_AHEAD_FACTOR;
		ActionPlan actionPlan = new ActionPlan();
		Map<DateTime, ResourceConstraints> constraints = new HashMap<DateTime, ResourceConstraints>();
		DateTime endTime = startTime.plus(duration);
		int currrentIndex = 0;
		ResourceConstraints constraint;
		for (DateTime currentTime = startTime; currentTime
				.isBefore(endTime); currentTime = currentTime.plus(sampling)) {
			List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
			List<EnergyEfficiencyOptimizationAction> correlatedActions = new ArrayList<EnergyEfficiencyOptimizationAction>();
			DateTime end = currentTime.plus(sampling);
			Interval timeInterval = new Interval(currentTime, end);
			Interval timeIntervalCorrelatedActions = new Interval(currentTime, end);

			// modifies actions to write energy in actions, instead of power (/2
			// when intraday)
			if (duration.getHours() == IntradayPlanner.PREDICTION_PERIOD_LENGTH) {
				factor = INTRADAY_FACTOR;
			}

			// create resource constraint
			if (end.isEqual(endTime)) {
				// create constraint for the end of the interval
				constraint = new ResourceConstraints(
						optimizationData.getTesLevel()[currrentIndex]
								+ optimizationData.getChargeTes()[currrentIndex] * factor
								- optimizationData.getDischargeTes()[currrentIndex] * factor,
						optimizationData.getBatteryLevel()[currrentIndex]
								+ optimizationData.getChargeBattery()[currrentIndex] * factor
								- optimizationData.getDischargeBattery()[currrentIndex] * factor);
			} else {
				constraint = new ResourceConstraints(optimizationData.getTesLevel()[currrentIndex + 1],
						optimizationData.getBatteryLevel()[currrentIndex + 1]);
			}
			constraints.put(end, constraint);

			// create transaction energy action
//			double energyConsumption = (optimizationData.getDcFinalConsumption()[currrentIndex] -
//					optimizationData.getWREN()[0] * optimizationData.getRenewableEnergy()[currrentIndex])* factor;
			double energyConsumption = (optimizationData.getDcFinalConsumption()[currrentIndex]);
			EnergyEfficiencyOptimizationAction energy = createTransactionAction(currentTime, end,
					energyConsumption,
					optimizationData.getEnergyPrice()[currrentIndex]);
			if(energy != null) {
				actions.add(energy);
			}

			// create transaction heat action
			if(optimizationData.getWT()[0] > 0.0 ) {
				EnergyEfficiencyOptimizationAction heat = createTransactionHeatAction(currentTime, end,
				optimizationData.getDcThermalGeneration()[currrentIndex] * factor,
						optimizationData.getHeatPrice()[currrentIndex]);
				if(heat != null){
					actions.add(heat);
				}
			}

			// create transaction flexibility action
			if(optimizationData.getWF()[0] > 0.0 ) {
				EnergyEfficiencyOptimizationAction flex = createTransactionFlexibilityAction(currentTime, end,
						(optimizationData.getDcFinalConsumption()[currrentIndex] -
								optimizationData.getBaseline()[currrentIndex])* factor,
						optimizationData.getDemandPrice()[currrentIndex]);
				if(flex!=null) {
					actions.add(flex);
				}
			}


			// create actions for charge & discharge battery
			actions.addAll(
					createBatteryActions(currentTime, end, optimizationData.getChargeBattery()[currrentIndex] * factor,
							optimizationData.getDischargeBattery()[currrentIndex] * factor));

			// create actions for charge & discharge thermal-storage
			actions.addAll(createCoolingActions(currrentIndex, currentTime, end, optimizationData, factor));

			// create shift delay tolerable workload
			actions.addAll(createShiftWorkloadActions(optimizationData, startTime, sampling, duration, currrentIndex,
					currentTime));
			actions.addAll(createReallocateWorkloadActions(optimizationData, startTime, sampling, duration,
					currrentIndex, currentTime));

			//create host relocation actions
			actions.addAll(createHostWorkloadActions(optimizationData, startTime, sampling,
					duration, currrentIndex, currentTime));

//			//create thermal self heat actions
//			actions.addAll(createThermalSelfHeatActions(optimizationData, startTime, sampling,
//					duration, currrentIndex, currentTime));

			correlatedActions.addAll(createCorrelatedActions(actions));
			actionPlan.putEntry(timeInterval, actions);
			actionPlan.putEntryCorrelated(timeIntervalCorrelatedActions, correlatedActions);
			currrentIndex++;
		}
		actionPlan.setConstraints(constraints);

		return actionPlan;

	}

	private static List<EnergyEfficiencyOptimizationAction> createCorrelatedActions(List<EnergyEfficiencyOptimizationAction> actions){
		List<EnergyEfficiencyOptimizationAction> correlatedActions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		BuyEnergy buyE = null;
		SellHeat sellH = null;
		SellFlexibility sellF = null;
		for(EnergyEfficiencyOptimizationAction e : actions){
			if(e instanceof BuyEnergy) buyE = (BuyEnergy)e;
			if(e instanceof SellHeat) sellH = (SellHeat)e;
			if(e instanceof SellFlexibility) sellF = (SellFlexibility)e;
		}
		if((buyE!=null) && (sellH !=null)){
			correlatedActions.add(buyE);
			correlatedActions.add(sellH);
		}
//		if((buyE!=null) && (sellH !=null) && (sellF == null)){
//			correlatedActions.add(buyE);
//			correlatedActions.add(sellH);
//		}
//		if((buyE!=null) && (sellH ==null) && (sellF != null)){
//			correlatedActions.add(buyE);
//			correlatedActions.add(sellF);
//		}if((buyE!=null) && (sellH !=null) && (sellF != null) && (sellF.getAmountOfEnergy()>=0)){
//			correlatedActions.add(buyE);
//			correlatedActions.add(sellH);
//			correlatedActions.add(sellF);
//		}
//		if((buyE!=null) && (sellH !=null) && (sellF != null)&& (sellF.getAmountOfEnergy()<0)){
//			correlatedActions.add(buyE);
//			correlatedActions.add(sellF);
//		}
		return correlatedActions;
	}

	// on output from lingo, entry at hour i is the value at the beginning of
	// hour i
	// SAMPLING_DAY_AHEAD_ACTIONS, SAMPLIG_INTRADAY_ACTIONS,
	// SAMPLING_REALTIME_ACTIONS, DURATION_IN_HOURS
	/*
	public static ActionPlan extractActionsForAS(OptimizationDataCatalyst optimizationData, DCResources resources,
			DateTime startTime, Period samplingDayAheadActions, Period samplingIntradayActions,
			Period samplingRealtimeActions, Period duration) {
		double factor = 1;
		ActionPlan actionPlan = new ActionPlan();
		Map<DateTime, ResourceConstraints> constraints = new HashMap<DateTime, ResourceConstraints>();
		DateTime endTime = startTime.plus(duration);
		int count = 0;
		ResourceConstraints constraint;
		for (DateTime start = startTime; start.isBefore(endTime); start = start.plus(samplingDayAheadActions)) {
			List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
			DateTime end = start.plus(samplingDayAheadActions);
			Interval timeInterval = new Interval(start, end);
			// create diesel generator actions

			// create shift delay tolerable workload
			actions.addAll(createShiftWorkloadActionsForAS(optimizationData, startTime, samplingDayAheadActions,
					duration, count, start));
			//create workload relocation actions
			actions.addAll(createReallocateWorkloadActionsforAS(optimizationData, startTime, samplingDayAheadActions,
					duration, count, start));

			//create host relocation actions
			actions.addAll(createHostWorkloadActionsforAS(optimizationData, startTime, samplingDayAheadActions,
					duration, count, start));

			actionPlan.putEntry(timeInterval, actions);
			count++;
		}
		count = 0;
		factor = 0.5;
		for (DateTime start = startTime; start.isBefore(endTime); start = start.plus(samplingIntradayActions)) {
			List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
			DateTime end = start.plus(samplingIntradayActions);
			Interval timeInterval = new Interval(start, end);
			// create diesel generator actions
			// create resource contraint
			// TODO: at constraints, should I put charge/discharge loss rates?
			if (end.isEqual(endTime)) {
				// create constraint for the end of the interval
				constraint = new ResourceConstraints(
						optimizationData.getTesLevel()[count] + optimizationData.getChargeTes()[count]
								- optimizationData.getDischargeTes()[count],
						optimizationData.getBatteryLevel()[count] + optimizationData.getChargeBattery()[count]
								- optimizationData.getDischargeBattery()[count]);
			} else {
				constraint = new ResourceConstraints(optimizationData.getTesLevel()[count + 1],
						optimizationData.getBatteryLevel()[count + 1]);
			}
			constraints.put(end, constraint);

			// create actions for charge & discharge thermal-storage
			actions.addAll(createCoolingActions(count, start, end, optimizationData, factor));

			actionPlan.putEntry(timeInterval, actions);
			count++;
		}
		count = 0;
		factor = 1.0 / 12;
		for (DateTime start = startTime; start.isBefore(endTime); start = start.plus(samplingRealtimeActions)) {
			List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
			DateTime end = start.plus(samplingRealtimeActions);
			Interval timeInterval = new Interval(start, end);
			// create actions for charge & discharge battery
			actions.addAll(createBatteryActions(start, end, optimizationData.getChargeBattery()[count] * factor,
					optimizationData.getDischargeBattery()[count] * factor));

			actionPlan.putEntry(timeInterval, actions);
			count++;

		}
		actionPlan.setConstraints(constraints);

		return actionPlan;

	}
*/

	public static EnergyEfficiencyOptimizationAction createTransactionAction(DateTime start, DateTime end,
																			 double energyValue, double priceValue) {
		if(priceValue > 0) {
			actionId++;
			double amountOfEnergy = MathUtility.round2Decimals(energyValue);
			if (amountOfEnergy > 0) {
				EnergyEfficiencyOptimizationAction ret = new BuyEnergy(actionId, start.toDate(), end.minusMinutes(1).toDate(), amountOfEnergy,
						MathUtility.round2Decimals(priceValue * (1 + BID_PERCENTAGE)));
				ret.setAmountThermal(0.0);
				return ret;
			} else {
				EnergyEfficiencyOptimizationAction ret = new SellEnergy(actionId, start.toDate(), end.minusMinutes(1).toDate(), -amountOfEnergy,
						MathUtility.round2Decimals(priceValue * (1 - OFFER_PERCENTAGE)));
				ret.setAmountThermal(0.0);
				return ret;
			}
		}
		else return null;
	}

	public static EnergyEfficiencyOptimizationAction createTransactionHeatAction(DateTime start, DateTime end,
																			 double energyValue, double priceValue){
			if(priceValue > 0) {
				actionId++;
				double amountOfEnergy = MathUtility.round2Decimals(energyValue);
				if (amountOfEnergy > 0) {
					EnergyEfficiencyOptimizationAction ret = new SellHeat(actionId, start.toDate(), end.minusMinutes(1).toDate(), amountOfEnergy,
							MathUtility.round2Decimals(priceValue * (1 + BID_PERCENTAGE)));
					ret.setAmountThermal(amountOfEnergy);
					return ret;
				} else {
					EnergyEfficiencyOptimizationAction ret = new BuyHeat(actionId, start.toDate(), end.minusMinutes(1).toDate(), -amountOfEnergy,
							MathUtility.round2Decimals(priceValue * (1 - OFFER_PERCENTAGE)));
					ret.setAmountThermal(0.0);
					return ret;
				}
			}
			else return null;

	}

	public static EnergyEfficiencyOptimizationAction createTransactionFlexibilityAction(DateTime start, DateTime end,
																				 double energyValue, double priceValue) {
		if(priceValue > 0) {
			actionId++;
			double amountOfEnergy = MathUtility.round2Decimals(energyValue);
			if(amountOfEnergy < 0) amountOfEnergy = -amountOfEnergy;
			EnergyEfficiencyOptimizationAction ret = new SellFlexibility(actionId, start.toDate(), end.minusMinutes(1).toDate(), amountOfEnergy,
					MathUtility.round2Decimals(priceValue * (1 - OFFER_PERCENTAGE)));
			ret.setAmountThermal(0.0);
			return ret;
		}
		else{
			return null;
		}
	}


	private static List<EnergyEfficiencyOptimizationAction> createCoolingActions(int count, DateTime start,
			DateTime end, OptimizationDataCatalyst optimizationData, double factor) {
		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		double value;
		if (optimizationData.getChargeTes()[count] != 0) {
			value = MathUtility.round4Decimals(optimizationData.getChargeTes()[count]) * factor;
			actionId++;
			double factorHeatReuse = 1.0;
			if((ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.MetaheuristicAlgorithm.ENABLE_TES)
					&&(optimizationData.getTesMaxDischargeRate()[0]>0))
				factorHeatReuse = (value / optimizationData.getTesMaxDischargeRate()[0]);
			double heatAccumulated = optimizationData.getItLoad()[count] + optimizationData.getHostWorkload()[count] * optimizationData.getMaxHost()[0];
			double heatRemovedbyTESCharge = optimizationData.getThermalEquivalentForElectricalEnergy(heatAccumulated * factorHeatReuse);
			ChargeTes chargeTes = new ChargeTes(actionId, value, start.toDate(), end.toDate(), value);
			chargeTes.setAmountThermal(heatRemovedbyTESCharge);
			actions.add(chargeTes);

			// value =
			// MathUtility.round4Decimals(optimizationData.getFinalCooling()[count]
			// + optimizationData.getTesChargeLossRate()[0] *
			// optimizationData.getChargeTes()[count])

		}
		if (optimizationData.getDischargeTes()[count] != 0) {
			value = MathUtility.round4Decimals(optimizationData.getDischargeTes()[count]) * factor;
			actionId++;
			double factorHeatReuse = 1.0;
			//factorHeatReuse = (value / optimizationData.getTesMaxDischargeRate()[0]);
			double heatAccumulated = optimizationData.getItLoad()[count] + optimizationData.getHostWorkload()[count] * optimizationData.getMaxHost()[0];
			double heatStoredbyTESDischarge = optimizationData.getThermalEquivalentForElectricalEnergy(heatAccumulated * factorHeatReuse);
			DischargeTes dischargeTes = new DischargeTes(actionId, value, start.toDate(), end.toDate(), value);
			dischargeTes.setAmountThermal(heatStoredbyTESDischarge);
			actions.add(dischargeTes);
		}
		if (optimizationData.getFinalCooling()[count] != 0) {
			value = MathUtility.round4Decimals(optimizationData.getFinalCooling()[count]) * factor;
			double percentageElectricalCooling = optimizationData.getItCooling()[count]
					/ optimizationData.getFinalCooling()[count];

			actionId++;
			DynamicAdjustmentOfCoolingIntensity dynamicAdjustmentOfCoolingIntensity = new DynamicAdjustmentOfCoolingIntensity(
					actionId, value, start.toDate(), end.toDate(),
					MathUtility.round4Decimals(percentageElectricalCooling));
			//double rt = optimizationData.getRealtimeEnergy()[count];
			//dynamicAdjustmentOfCoolingIntensity.setAmountThermal(optimizationData.getThermalEquivalentForElectricalEnergy(rt));
			dynamicAdjustmentOfCoolingIntensity.setAmountThermal(0.0);
			actions.add(dynamicAdjustmentOfCoolingIntensity);
		}
		return actions;
	}


	public static List<EnergyEfficiencyOptimizationAction> createBatteryActions(DateTime start, DateTime end,
			double chargeValue, double dischargeValue) {
		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		double value;
		if (chargeValue != 0) {
			value = MathUtility.round4Decimals(chargeValue);
			actionId++;
			ChargeBattery chargeBattery = new ChargeBattery(actionId, value, start.toDate(), end.toDate(), value);
			chargeBattery.setAmountThermal(0.0);
			actions.add(chargeBattery);
		}
		if (dischargeValue != 0) {
			value = MathUtility.round4Decimals(dischargeValue);
			actionId++;
			DischargeBattery dischargeBattery = new DischargeBattery(actionId, value, start.toDate(), end.toDate(),
					value);
			dischargeBattery.setAmountThermal(0.0);
			actions.add(dischargeBattery);
		}
		return actions;
	}

	/**
	 * Create SDTW actions
	 * 1. compute SDTW from current hour percentages times the DT workload
	 * 2. the new DT workload is changed depending on the reallocation workload actions.
	 */
	private static List<EnergyEfficiencyOptimizationAction> createShiftWorkloadActions(
			OptimizationDataCatalyst optimizationData, DateTime startTime, Period sampling, Period duration, int count,
			DateTime start) {
		List<EnergyEfficiencyOptimizationAction> shiftWorkloadActions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		int timeHorizon = (int) (duration.toStandardDuration().getMillis() / sampling.toStandardDuration().getMillis());
		for (int index = 0; index < timeHorizon; index++) {
			// TODO: bug? why check EstimatedDelayExecution()[index] != 0 ?

			double percentageSDTW = optimizationData.getSchedulingMatrix()[timeHorizon * count + index];

			if (percentageSDTW >= MetaheuristicAlgorithm.ALLOWED_VALUE
					&& optimizationData.getEstimatedDelayExecution()[index] != 0) {
				double currentDT = optimizationData.getDelayTolerableEnergy()[count] * (1  - optimizationData.getRelocateWorkload()[count]);
				double percentageSDTWconsideringRealloc = percentageSDTW * (1  - optimizationData.getRelocateWorkload()[count]);;

				DateTime toTime = new DateTime(
						startTime.getMillis() + index * sampling.toStandardDuration().getMillis());
				if(percentageSDTWconsideringRealloc >= MetaheuristicAlgorithm.ALLOWED_VALUE) {
					actionId++;
					ShiftDelayTolerantWorkload shiftWorkload = new ShiftDelayTolerantWorkload(actionId,
							percentageSDTW * currentDT, toTime.toDate(), toTime.plusHours(1).toDate(), index - count,
							start.toDate(), toTime.toDate(), percentageSDTWconsideringRealloc);
					shiftWorkload.setAmountThermal(optimizationData.getThermalEquivalentForElectricalEnergy(percentageSDTW * currentDT));
					shiftWorkloadActions.add(shiftWorkload);
				}
			}
		}
		return shiftWorkloadActions;
	}



	/**
	 * Create WorkloadRealocation action as x% * DT i.e. the percentage matching
	 * the current hour times the input DT workload (for every DC)
	 * Obs. start from DC =1 (skip actions for current DC)
	 */
	/*
	private static List<WorkloadRelocation> createReallocateWorkloadActions(OptimizationDataCatalyst optimizationData,
			DateTime startTime, Period sampling, Period duration, int count, DateTime start) {
		List<WorkloadRelocation> migrateWorkloadActions = new ArrayList<WorkloadRelocation>();

		int currentReallocFrame = count / (DayaheadPlanner.PREDICTION_PERIOD_LENGTH/REALLOC_MATRIX_SLOTS);

		double[] migrationSchedulingMatrix = optimizationData.getMigrationSchedulingMatrix();
		for (int currentDC = 1; currentDC < optimizationData.getNoOfDCs(); currentDC++) {
			double reallocPercentage = migrationSchedulingMatrix[REALLOC_MATRIX_SLOTS * currentDC
					+ currentReallocFrame];
			if (reallocPercentage != 0) {
				double reallocWorkload = optimizationData.getDelayTolerableEnergy()[count] * reallocPercentage;
				WorkloadRelocation wRealloc = new WorkloadRelocation(1, reallocWorkload, start.toDate(), start.plusHours(1).toDate(),reallocPercentage,
						reallocWorkload, "DC" + currentDC);
				migrateWorkloadActions.add(wRealloc);
			}
		}
		return migrateWorkloadActions;
	}*/

	private static List<WorkloadRelocation> createReallocateWorkloadActions(OptimizationDataCatalyst optimizationData,
																			DateTime startTime, Period sampling, Period duration, int count, DateTime start) {
		List<WorkloadRelocation> migrateWorkloadActions = new ArrayList<WorkloadRelocation>();
		double[] migrationSchedulingMatrix = optimizationData.getRelocateWorkload();
		double reallocPercentage = migrationSchedulingMatrix[count];
		double reallocWorkload = optimizationData.getDelayTolerableEnergy()[count] * reallocPercentage;
		int currentDC = 0;
		WorkloadRelocation wRealloc = new WorkloadRelocation(1, reallocWorkload, start.toDate(), start.plusHours(1).toDate(),reallocPercentage,
				reallocWorkload, "DC" + currentDC);
		wRealloc.setAmountThermal(0.0);
		migrateWorkloadActions.add(wRealloc);
		return migrateWorkloadActions;
	}

	private static List<WorkloadHosting> createHostWorkloadActions(OptimizationDataCatalyst optimizationData,
																				 DateTime startTime, Period sampling, Period duration, int count, DateTime start) {
		List<WorkloadHosting> migrateWorkloadActions = new ArrayList<WorkloadHosting>();
		double[] migrationSchedulingMatrix = optimizationData.getHostWorkload();
		double hostPercentage = migrationSchedulingMatrix[count];
		double hostWorkload = optimizationData.getMaxHost()[0]* hostPercentage;
		int currentDC = 0;
		WorkloadHosting wRealloc = new WorkloadHosting(1, hostWorkload, start.toDate(), start.plusHours(1).toDate(),hostPercentage,
				hostWorkload, "DC" + currentDC);
		wRealloc.setAmountThermal(optimizationData.getThermalEquivalentForElectricalEnergy(hostWorkload));
		migrateWorkloadActions.add(wRealloc);
		return migrateWorkloadActions;
	}


//	private static List<ThermalSelfHeat> createThermalSelfHeatActions(OptimizationDataCatalyst optimizationData,
//																	  DateTime startTime, Period sampling, Period duration, int count, DateTime start) {
//		List<ThermalSelfHeat> migrateThermalSelfHeat = new ArrayList<ThermalSelfHeat>();
//		double thermalSelfHeat = optimizationData.getDcThermalGeneration()[count] ;
//		int currentDC = 0;
//		ThermalSelfHeat tSelfHeat = new ThermalSelfHeat(1, thermalSelfHeat, start.toDate(), start.plusHours(1).toDate(), thermalSelfHeat, "DC" + currentDC);
//		migrateThermalSelfHeat.add(tSelfHeat);
//		return migrateThermalSelfHeat;
//	}
	/*

	private static List<EnergyEfficiencyOptimizationAction> createShiftWorkloadActionsForAS(
			OptimizationDataCatalyst optimizationData, DateTime startTime, Period sampling, Period duration, int count,
			DateTime start) {
		List<EnergyEfficiencyOptimizationAction> shiftWorkloadActions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		int timeHorizon = (int) (duration.toStandardDuration().getMillis() / sampling.toStandardDuration().getMillis());
		double[] schedulingMatrix = optimizationData.getSchedulingMatrix();
		for (int index = 0; index < timeHorizon; index++) {
			if (optimizationData.getSchedulingMatrix()[timeHorizon * count + index] != 0
					&& optimizationData.getEstimatedDelayExecution()[index] != 0) {
				DateTime toTime = new DateTime(
						startTime.getMillis() + index * sampling.toStandardDuration().getMillis());
				actionId++;
				ShiftDelayTolerantWorkload shiftWorkload = new ShiftDelayTolerantWorkload(actionId,
						schedulingMatrix[timeHorizon * count + index]
								* optimizationData.getDelayTolerableEnergy()[count] / 100.0,
						toTime.toDate(), toTime.plusHours(1).toDate(), index - count, start.toDate(), toTime.toDate(),
						schedulingMatrix[timeHorizon * count + index] / 100.0);
				shiftWorkloadActions.add(shiftWorkload);
			}
		}
		return shiftWorkloadActions;
	}*/

	/**
	 * @param energyStorageOptimizationAction
	 * @param energyStorageComponents
	 * @param totalActualLoadedCapacity
	 * @param totalMaximumCapacity
	 * @return
	 */
	public static List<EnergyStorageOptimization> splitDischargeAction(
			EnergyStorageOptimization energyStorageOptimizationAction,
			List<? extends EnergyStorageComponent> energyStorageComponents, double totalActualLoadedCapacity,
			double totalMaximumCapacity) {
		List<EnergyStorageOptimization> actions = new ArrayList<EnergyStorageOptimization>();
		double totalFreeCapacity = totalMaximumCapacity - totalActualLoadedCapacity;
		double factor, value;

		for (EnergyStorageComponent esc : energyStorageComponents) {
			factor = (esc.getMaximumCapacity() - esc.getActualLoadedCapacity()) / totalFreeCapacity;
			value = MathUtility.round(energyStorageOptimizationAction.getAmountOfEnergy() * factor, 0);
			if (value > 0) {
				actionId++;
				DischargeEnergy chargeEnergy = new DischargeEnergy(actionId, value * esc.getChargeLossRate(),
						energyStorageOptimizationAction.getStartTime(), energyStorageOptimizationAction.getEndTime(),
						value, esc);
				actions.add(chargeEnergy);
			}
		}
		return actions;
	}

	/**
	 * @param energyStorageOptimizationAction
	 * @param energyStorageComponents
	 * @param totalActualLoadedCapacity
	 * @param totalMaximumCapacity
	 * @return
	 */
	public static List<EnergyStorageOptimization> splitChargeAction(
			EnergyStorageOptimization energyStorageOptimizationAction,
			List<? extends EnergyStorageComponent> energyStorageComponents, double totalActualLoadedCapacity,
			double totalMaximumCapacity) {
		List<EnergyStorageOptimization> actions = new ArrayList<EnergyStorageOptimization>();
		double totalFreeCapacity = totalMaximumCapacity - totalActualLoadedCapacity;
		double factor, value;

		for (EnergyStorageComponent esc : energyStorageComponents) {
			factor = (esc.getMaximumCapacity() - esc.getActualLoadedCapacity()) / totalFreeCapacity;
			value = MathUtility.round(energyStorageOptimizationAction.getAmountOfEnergy() * factor, 0);
			if (value > 0) {
				actionId++;
				ChargeEnergy chargeEnergy = new ChargeEnergy(actionId, value * esc.getChargeLossRate(),
						energyStorageOptimizationAction.getStartTime(), energyStorageOptimizationAction.getEndTime(),
						value, esc);
				actions.add(chargeEnergy);
			}
		}
		return actions;
	}
}