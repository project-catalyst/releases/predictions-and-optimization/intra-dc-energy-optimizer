package ro.tuc.dsrl.catalyst.optimizer.resources;


import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;

import java.util.ArrayList;
import java.util.List;

public class ServerResources {
	private static final double CONSTANT_TO_W = 1000.0;
	private double currentDCEnergyConsumption;
	private double currentDcEdPercentage;
	private List<ServerRoom> serverRooms;
	public ServerResources() {
		this.serverRooms = new ArrayList<>();
	}

	public void setServerRooms(List<ServerRoom> serverRooms){
		this.serverRooms = serverRooms;
	}

	public double getItMaximumConsumptionKW() {
		double maxEnergyConsumption =0;

		for(ServerRoom sr : serverRooms){
			maxEnergyConsumption += sr.getMaxEnergyConsumption();
		}

		return maxEnergyConsumption;
	}

	public double getMaxHostLoad() {
		double maxHost =0;

		for(ServerRoom sr : serverRooms){
			maxHost += sr.getMaxHostLoad();
		}

		return maxHost;
	}

	public double getMaxReallocLoad() {
		double maxRealloc =0;

		for(ServerRoom sr : serverRooms){
			maxRealloc += sr.getMaxReallocLoad();
		}

		return maxRealloc;
	}


	public double getItMaximumConsumptionW() {
		return getItMaximumConsumptionKW() *CONSTANT_TO_W;
	}

	public double getCurrentItEnergyConsumptionKW() {
		double currentEnergyConsumption =0;

		for(ServerRoom sr : serverRooms){
			currentEnergyConsumption += sr.getEnergyConsumption();
		}
		return currentEnergyConsumption ;
	}

	public void setCurrentEnergyConsumption(double currentEnergyConsumption) {
		this.currentDCEnergyConsumption = currentEnergyConsumption;
	}

	public double getCurrentDcEdPercentage() {


		double currentEdPercentage = 0.0;
		int cntServerRooms = 0;

		for(ServerRoom sr : serverRooms){
			currentEdPercentage += sr.getEdPercentage();
			cntServerRooms++;
		}

		if(cntServerRooms != 0) {
			return currentEdPercentage / cntServerRooms;
		} else {
			return 0;
		}
	}

	public void setCurrentDcEdPercentage(double currentEdPercentage) {
		this.currentDcEdPercentage = currentEdPercentage;
	}
}
