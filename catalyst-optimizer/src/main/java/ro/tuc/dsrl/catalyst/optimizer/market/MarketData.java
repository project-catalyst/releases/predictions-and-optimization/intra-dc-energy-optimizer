package ro.tuc.dsrl.catalyst.optimizer.market;

import java.util.List;

public class MarketData {
    private List<Double> energyPrice;
    private List<Double> workloadPrices;
    private List<Double> thermalPrices;
    private List<Double> drIncentives;

    private List<Double> drSignal;

    public MarketData() {
    }

    public MarketData(List<Double> energyPrice, List<Double> workloadPrices, List<Double> thermalPrices, List<Double> drIncentives, List<Double> drSignal) {
        this.energyPrice = energyPrice;
        this.workloadPrices = workloadPrices;
        this.thermalPrices = thermalPrices;
        this.drIncentives = drIncentives;
        this.drSignal = drSignal;
    }

    public List<Double> getEnergyPrice() {
        return energyPrice;
    }

    public void setEnergyPrice(List<Double> energyPrice) {
        this.energyPrice = energyPrice;
    }

    public List<Double> getWorkloadPrices() {
        return workloadPrices;
    }

    public void setWorkloadPrices(List<Double> workloadPrices) {
        this.workloadPrices = workloadPrices;
    }

    public List<Double> getThermalPrices() {
        return thermalPrices;
    }

    public void setThermalPrices(List<Double> thermalPrices) {
        this.thermalPrices = thermalPrices;
    }

    public List<Double> getDrIncentives() {
        return drIncentives;
    }

    public void setDrIncentives(List<Double> drIncentives) {
        this.drIncentives = drIncentives;
    }

    public List<Double> getDrSignal() {
        return drSignal;
    }

    public void setDrSignal(List<Double> drSignal) {
        this.drSignal = drSignal;
    }
}
