package ro.tuc.dsrl.catalyst.optimizer.market;

import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.model.dto.EnergyPointValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.ActiveSessionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.MarketActionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.MarketCorrelationDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketActionType;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTimeframes;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTypes;
import ro.tuc.dsrl.catalyst.optimizer.planner.Planner;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystDBApiURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.catalyst.optimizer.utility.MathUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DefaultDBMarketHandler extends MarketAPIHandlerInterface {

    @Override
    public List<Double> getClearingPrices(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate) {
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("startdate", startDate.getMillis());
        urlVariables.put("datacenter-name", Planner.DATACENTER_NAME);
        urlVariables.put("granularity", timeframe.getInternalValue());

        Double[] marketPrices;
        switch (marketType) {
            case ELECTRIC:
                marketPrices = RestAPIClient.getObject(CatalystDBApiURL.HISTORICAL_ENERGY_PRICE, Double[].class, urlVariables);
                break;
            case THERMAL:
                marketPrices = RestAPIClient.getObject(CatalystDBApiURL.HISTORICAL_THERMAL_PRICE, Double[].class, urlVariables);
                break;
            case IT_LOAD:
                marketPrices = RestAPIClient.getObject(CatalystDBApiURL.HISTORICAL_IT_LOAD_PRICE, Double[].class, urlVariables);
                break;
            default:
                return new ArrayList<>();
        }
        return getValues(marketPrices);
    }

    @Override
    public List<Double> getRefrencePrices(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate) {
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("startdate", startDate.getMillis());
        urlVariables.put("datacenter-name", Planner.DATACENTER_NAME);
        urlVariables.put("granularity", timeframe.getInternalValue());

        Double[] marketPrices;
        switch (marketType) {
            case ELECTRIC:
                marketPrices = RestAPIClient.getObject(CatalystDBApiURL.HISTORICAL_ENERGY_PRICE, Double[].class, urlVariables);
                break;
            case THERMAL:
                marketPrices = RestAPIClient.getObject(CatalystDBApiURL.HISTORICAL_THERMAL_PRICE, Double[].class, urlVariables);
                break;
            case IT_LOAD:
                marketPrices = RestAPIClient.getObject(CatalystDBApiURL.HISTORICAL_IT_LOAD_PRICE, Double[].class, urlVariables);
                break;
            default:
                return new ArrayList<>();
        }
        return getValues(marketPrices);
    }

    @Override
    public List<ActiveSessionDTO> getActiveSessions(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate) {
        List<ActiveSessionDTO> sessions = new ArrayList<>();
        sessions.add(new ActiveSessionDTO(startDate.minusDays(1), startDate.plusDays(1), startDate.plusDays(1).plusMinutes(1), startDate.plusDays(1).plusMinutes(15)));
        return sessions;
    }


    @Override
    public List<MarketActionDTO> getMarketActions(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate) {
        List<MarketActionDTO> marketActions = new ArrayList<>();
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("startdate", startDate.getMillis());
        urlVariables.put("datacenter-name", Planner.DATACENTER_NAME);
        Double[] drIncentive = RestAPIClient.getObject(CatalystDBApiURL.DAYAHEAD_HISTORICAL_DR_INCENTIVE, Double[].class, urlVariables);
        List<Double> drSignal = getDRSignal(startDate);
        int len = Math.min(drIncentive.length, drSignal.size());
        for (int time = 0; time < len; time++) {
            marketActions.add(new MarketActionDTO(1, startDate.minusDays(1).plusMinutes(10), startDate.plusHours(time), startDate.plusHours(time + 1), drSignal.get(time),
                    "Kwh", drIncentive[time], MarketActionType.BUY.getValue(), "valid"));
        }
        return marketActions;
    }


    @Override
    public List<MarketActionDTO> registerActions(List<MarketActionDTO> actions, MarketTypes marketType, MarketTimeframes timeframe) {
        int id = 0;
        for (MarketActionDTO action : actions) {
            action.setId(id);
            id++;
        }
        return actions;
    }

    @Override
    public MarketCorrelationDTO registerCorrelatedActions(MarketCorrelationDTO marketCorrelation, MarketTimeframes timeframe) {
        return marketCorrelation;
    }

    protected List<Double> getDRSignal(DateTime startDate) {
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("startdate", startDate.getMillis());
        urlVariables.put("datacenter-name", Planner.DATACENTER_NAME);

        ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
                .getObject(CatalystDBApiURL.DR_SIGNAL_DAYAHEAD, ForecastDayAheadDTO.class, urlVariables);

        List<Double> values = new ArrayList<Double>();
        for (EnergyPointValueDTO energyPointValue : forecastDayAheadDTO.getEnergyPointValueDTOS()) {
            values.add(MathUtility.round4Decimals(energyPointValue.getEnergyValue()));
        }
        return values;
    }
}
