//package ro.tuc.dsrl.catalyst.optimizer.utility;
//
///**
// *
// * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
// *          Research Laboratory, http://dsrl.coned.utcluj.ro/
// * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
// * @Module: rest-client
// * @Since: Dec 17, 2014
// * @Description:
// *
// */
//public enum ConfigurationProperties {
////	DM_API_HOST("DM_API_HOST"),
////	EBB_API_HOST("EBB_API_HOST"),
//	LINGO_API_HOST("LINGO_API_HOST"),
//	CATALYST_DB_API_HOST("CATALYST_DB_API_HOST"),
//	MARKETPLACE_MPCM_HOST("MARKETPLACE_MPCM"),
////	ESMA_API_HOST("ESMA_API_HOST"),
////	REALTIME_CM_API_HOST("REALTIME_CM_API_HOST"),
//	DAYAHEAD_VALIDATION("DAYAHEAD_VALIDATION"),
//	INTRADAY_VALIDATION("INTRADAY_VALIDATION"),
//	DAYAHEAD_SCHEDULE_TIME("DAYAHEAD_SCHEDULE_TIME"),
//	INTADAY_START_TIME("INTRADAY_START_TIME"),
////	DAYAHEAD_ENERGY_PRICE_URL("DAYAHEAD_ENERGY_PRICE_URL"),
////	OFFER_URL("OFFER_URL"),
////	BID_URL("BID_URL"),
////	INTRADAY_ENERGY_PRICE_URL("INTRADAY_ENERGY_PRICE_URL"),
////	REALTIME_CM_OPTIM_ACTIONS_URL("REALTIME_OPTIM_ACTIONS_URL"),
//	DATACENTER_NAME("DATACENTER_NAME"),
//	MARKET_ACTOR_NAME("MARKET_ACTOR_NAME");
////	MARKETPLACE_FLEXIBILITY_HOST("CATALYST_MARKETPLACE_FLEXIBILITY_HOST"),
////	MARKETPLACE_FLEXIBILITY_DAYAHEAD("FLEXIBILITY_MARKET_DAY_AHEAD_ID"),
////	MARKETPLACE_FLEXIBILITY_INTRADAY("FLEXIBILITY_MARKET_INTRA_DAY_ID"),;
//
//	private String value;
//
//	private ConfigurationProperties(String url) {
//		this.value = url;
//	}
//
//	public String value() {
//		return value;
//	}
//}
