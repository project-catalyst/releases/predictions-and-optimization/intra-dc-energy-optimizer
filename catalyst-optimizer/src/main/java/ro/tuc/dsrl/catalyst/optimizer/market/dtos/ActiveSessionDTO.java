package ro.tuc.dsrl.catalyst.optimizer.market.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.optimizer.serializers.CustomLocalDateTimeDeserializer;
import ro.tuc.dsrl.catalyst.optimizer.serializers.CustomLocalDateTimeSerializer;


public class ActiveSessionDTO {

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime sessionStartTime;

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime sessionEndTime;

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime deliveryStartTime;

    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    private DateTime deliveryEndTime;

    public ActiveSessionDTO() {
    }

    public ActiveSessionDTO(DateTime sessionStartTime, DateTime sessionEndTime, DateTime deliveryStartTime, DateTime deliveryEndTime) {
        this.sessionStartTime = sessionStartTime;
        this.sessionEndTime = sessionEndTime;
        this.deliveryStartTime = deliveryStartTime;
        this.deliveryEndTime = deliveryEndTime;
    }

    public DateTime getSessionStartTime() {
        return sessionStartTime;
    }

    public void setSessionStartTime(DateTime sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public DateTime getSessionEndTime() {
        return sessionEndTime;
    }

    public void setSessionEndTime(DateTime sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    public DateTime getDeliveryStartTime() {
        return deliveryStartTime;
    }

    public void setDeliveryStartTime(DateTime deliveryStartTime) {
        this.deliveryStartTime = deliveryStartTime;
    }

    public DateTime getDeliveryEndTime() {
        return deliveryEndTime;
    }

    public void setDeliveryEndTime(DateTime deliveryEndTime) {
        this.deliveryEndTime = deliveryEndTime;
    }
}
