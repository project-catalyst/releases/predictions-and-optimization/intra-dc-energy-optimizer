package ro.tuc.dsrl.catalyst.optimizer.algorithm;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public interface OptimizationAlgorithm {

     OptimizationDataCatalyst getOptimizationData();

     void compute(ServiceType serviceType);

     static boolean hasPredictions(OptimizationDataCatalyst data) {
        double[] emptyRT = new double[data.getSize()];
        Arrays.fill(emptyRT, 0.0);
        return !Arrays.equals(data.getRealtimeEnergy(), emptyRT);
    }
}
