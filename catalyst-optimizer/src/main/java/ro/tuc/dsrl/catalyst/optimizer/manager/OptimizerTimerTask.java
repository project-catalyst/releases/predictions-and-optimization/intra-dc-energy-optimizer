package ro.tuc.dsrl.catalyst.optimizer.manager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MetricsDTO;
import ro.tuc.dsrl.catalyst.model.enums.OptimizerConfig;
import ro.tuc.dsrl.catalyst.optimizer.energyefficiencymetricscalculator.FlexibilityMetricsService;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketAPIHandlerInterface;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketManager;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTimeframes;
import ro.tuc.dsrl.catalyst.optimizer.planner.*;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystDBApiURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystMetricsApiURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.catalyst.optimizer.utility.PropertiesLoader;
import ro.tuc.dsrl.geyser.execution.start.ActionExecution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 * Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 12, 2015
 * @Description:
 */
public class OptimizerTimerTask extends TimerTask {



    private static final Period DAYAHEAD_VALIDATION_PERIOD;
    private static final Period INTRADAY_VALIDATION_PERIOD;
    private static final double CONFIDENCE_LEVEL_SELECTED = 0.0;
    private static final int CONFIDENCE_LEVEL_PLAN_ID = 0;
    // do not change this configuration or everything will blow up
    private static final int INTRADAY_STARTHOUR;
    private static final int DAYAHEAD_STARTHOUR;
    private static final boolean remoteCall = false;

    static {
        int value = PropertiesLoader.DAYAHEAD_VALIDATION;
        DAYAHEAD_VALIDATION_PERIOD = new Period().withHours(value);
        value = PropertiesLoader.INTRADAY_VALIDATION;
        INTRADAY_VALIDATION_PERIOD = new Period().withHours(value);
        DAYAHEAD_STARTHOUR = PropertiesLoader.DAYAHEAD_SCHEDULE_TIME;
        INTRADAY_STARTHOUR = PropertiesLoader.INTRADAY_START_TIME;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(OptimizerTimerTask.class);

    private static final Period MONITORING_SAMPLE = new Period().withMinutes(5);
    private static final int UNSELECTED_PLAN = -1;
    private static OptimizerTimerTask instance = null;

    private ActionPlan activeActionPlan;
    private List<ActionPlan> dayAheadActionPlans;
    private ActionPlan intraDayActionPlan;

    private DCResources resources;
    private FlexibilityStrategyDTO strategies;

    private DayaheadPlanner dayAheadPlanner;
    private IntradayPlanner intraDayPlanner;
    private RealtimePlanner realtimePlanner;

    private DateTime currentTime;
    private DateTime startTime;
    private DateTime nextDayAheadScheduleTime;
    private DateTime nextIntraDayScheduleTime;
    private DateTime lastDayAheadRun;
    private DateTime lastIntraDayRun;

    private volatile int selectedDayAheadPlanId;
    private volatile int selectedIntraDayPlanId;

    private boolean dayAheadPlansComputed;
    private boolean intraDayPlanComputed;

    private int runDayAhead = 0;
    private int runIntraDay = 0;

    private static MarketManager marketManager;

    //TODO
    //private MarketAPIHandlerInterface.MarketAPIType marketConfiguration = MarketAPIHandlerInterface.MarketAPIType.DEFAULT_DB;

    private MarketAPIHandlerInterface.MarketAPIType marketConfiguration = MarketAPIHandlerInterface.MarketAPIType.MPCM;//DEFAULT_DB;


    private OptimizerTimerTask(DateTime currentTime) {
        FlexibilityStrategyDTO defaultStrategies = new FlexibilityStrategyDTO();
        defaultStrategies.setWe(0.0);
        defaultStrategies.setWf(0.0);
        //defaultStrategies.setWl(0.0); //scos de pe FE
        defaultStrategies.setWt(1.0);
        defaultStrategies.setREN(0.0);
        defaultStrategies.setReallocActive(false);
        this.marketManager = MarketManager.createInstance(marketConfiguration);

        startTime = currentTime;
        activeActionPlan = new ActionPlan();
        intraDayActionPlan = new ActionPlan();
        resources = new DCResources(currentTime.toDate());
        strategies = defaultStrategies;
        dayAheadPlanner = new DayaheadPlanner(activeActionPlan, resources, marketManager);
        intraDayPlanner = new IntradayPlanner(activeActionPlan, resources, marketManager);
        realtimePlanner = new RealtimePlanner(activeActionPlan, resources);
        this.currentTime = currentTime;
        this.nextDayAheadScheduleTime = new DateTime(currentTime.getYear(), currentTime.getMonthOfYear(),
                currentTime.getDayOfMonth(),
                PropertiesLoader.DAYAHEAD_SCHEDULE_TIME, 0);
        this.lastDayAheadRun = new DateTime(currentTime.getYear(), currentTime.getMonthOfYear(),
                currentTime.getDayOfMonth(), 0, 0);
        this.lastIntraDayRun = new DateTime(currentTime.getYear(), currentTime.getMonthOfYear(),
                currentTime.getDayOfMonth(), DAYAHEAD_STARTHOUR, 0);
        this.nextIntraDayScheduleTime = new DateTime(currentTime.getYear(), currentTime.getMonthOfYear(),
                currentTime.getDayOfMonth(), INTRADAY_STARTHOUR, 0);
        this.selectedDayAheadPlanId = UNSELECTED_PLAN;
        this.selectedIntraDayPlanId = UNSELECTED_PLAN;
    }

    public static OptimizerTimerTask getInstance(DateTime currentTime) {
        if (instance == null) {
            synchronized (OptimizerTimerTask.class) {
                instance = new OptimizerTimerTask(currentTime);
            }
        }
        return instance;
    }

    @Override
    public void run() {
        // refresh resource status
        //executeActions();
        LOGGER.info("Current time " + currentTime);
        resources.refresh(currentTime.toDate());
        if (currentTime.getHourOfDay() == 0 && currentTime.getMinuteOfHour() == MONITORING_SAMPLE.getMinutes()) {
            // updateTotalPowerConsumption()
        }
        if (currentTime.isAfter(nextDayAheadScheduleTime) && !dayAheadPlansComputed) {
            // / resetTime()
            // defaultStrategies = OptStrategiesGenerator.generateRandom()
            if(startTime.getDayOfYear() < currentTime.getDayOfYear()) {
                LOGGER.info("Calculating Metrics");
                try {
                    Thread.sleep(1000);
                    callMetricsCalculator(currentTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            LOGGER.info("Call Day Ahead planner");
            this.marketManager = MarketManager.createInstance(marketConfiguration);
            callDayAheadPlanner();
            selectedDayAheadPlanId = UNSELECTED_PLAN;
            dayAheadPlansComputed = true;
        }
        if (OptimizerConfig.ID_ACTIVE && currentTime.isAfter(nextIntraDayScheduleTime) && !intraDayPlanComputed) {
            LOGGER.info("Call Intra Day planner");
            callIntraDayPlanner();
            selectedIntraDayPlanId = UNSELECTED_PLAN;
            intraDayPlanComputed = true;
        }
        // check if day ahead validation period has expired and if so select a
        // default plan
        if (currentTime.isAfter(nextDayAheadScheduleTime.plus(DAYAHEAD_VALIDATION_PERIOD))
                && selectedDayAheadPlanId == UNSELECTED_PLAN && dayAheadPlansComputed) {
            LOGGER.info("Day Ahead select default plan");
            this.selectedDayAheadPlanId = CONFIDENCE_LEVEL_PLAN_ID;
        }
        // check if intra day validation period has expired and if so select a
        // default plan
        if (OptimizerConfig.ID_ACTIVE && currentTime.isAfter(nextIntraDayScheduleTime.plus(INTRADAY_VALIDATION_PERIOD))
                && selectedIntraDayPlanId == UNSELECTED_PLAN && intraDayPlanComputed) {
            LOGGER.info("Intra Day select default plan");
            this.selectedIntraDayPlanId = 0;
        }
        if (selectedDayAheadPlanId != UNSELECTED_PLAN && dayAheadPlansComputed) {
            extendActivePlanDA();
        }
        if (OptimizerConfig.ID_ACTIVE && (selectedIntraDayPlanId != UNSELECTED_PLAN && intraDayPlanComputed)) {
            extendActivePlanID();
        }
        // callRealtimePlanner()

        currentTime = currentTime.plus(MONITORING_SAMPLE);
    }

    // private void updateTotalPowerConsumption() {
    // Map<String, Object> urlVariables = new HashMap<String, Object>()
    // DateTime date2 = currentTime.minus(MONITORING_SAMPLE)
    // DateTime date1 = date2.minusDays(1)
    // urlVariables.put("date1", date1.getMillis())
    // urlVariables.put("date2", date2.getMillis())
    // RestAPIClient.getObject(DataModelAPIURL.UPDATE_TOTAL_POWER_CONSUMPTION,
    // String.class, urlVariables)
    // }

    private void executeActions() {
        ActionExecution.execute(currentTime, getUPSConstraintForDate(currentTime),
                CONFIDENCE_LEVEL_SELECTED);
    }

    private double getUPSConstraintForDate(DateTime currentTime) {
        int minutes = currentTime.getMinuteOfHour();
        DateTime date = new DateTime(currentTime);
        if (minutes < DateTimeConstants.MINUTES_PER_HOUR / 2) {
            date = date.plusMinutes(DateTimeConstants.MINUTES_PER_HOUR / 2 - minutes);
        } else {
            date = date.plusMinutes(DateTimeConstants.MINUTES_PER_HOUR - minutes);
        }

        return realtimePlanner.getEndConstraint(date).getUpsLevel();
    }

    private void setupDayAheadEstimatedProfiles() {
        Map<String, Object> urlVariables = new HashMap<>();
        DateTime startTime = lastDayAheadRun.plus(DayaheadPlanner.DURATION);
        urlVariables.put("startdate", startTime.getMillis());

        RestAPIClient.getObject(CatalystDBApiURL.SETUP_ESTIMATED_PROFILES, HashMap.class, urlVariables);
    }

    private void callDayAheadPlanner() {
        setStrategies(currentTime);

//        if (runDayAhead != 0) {
//            setupDayAheadEstimatedProfiles();
//        }
        setupDayAheadEstimatedProfiles();
        runDayAhead++;
        DateTime startTime = lastDayAheadRun.plus(DayaheadPlanner.DURATION);
        LOGGER.info("Executing day ahead for data center: " + Planner.DATACENTER_NAME);
        dayAheadActionPlans = dayAheadPlanner.execute(strategies, startTime, currentTime);
        LOGGER.info("Day-Ahead plans nr: " + dayAheadActionPlans.size() + " at date : " + startTime);

        lastDayAheadRun = startTime;
        // TODO: POST to ManagerConsole
        OptimizationPlanNotification.notifyDayAheadPlans(dayAheadActionPlans.get(0), lastDayAheadRun);

//		} else {
//			LOGGER.info("day ahead should run only once :D ");
//		}

    }

    private static void callMetricsCalculator(DateTime currentTime) {
        if(remoteCall) {
            Map<String, Object> urlVariables = new HashMap<>();
            DateTime time = currentTime.withHourOfDay(0).withMinuteOfHour(0);
            urlVariables.put("millis", time.getMillis());
            try {
                RestAPIClient.getObject(CatalystMetricsApiURL.DAYAHEAD_METRICS, List.class, urlVariables);
            } catch (Exception e) {
                LOGGER.info("Error calling Metrics Calculator");
            }
        }
        else{
            FlexibilityMetricsService flexibilityMetricsService = new FlexibilityMetricsService();
            DateTime time = currentTime.withHourOfDay(0).withMinuteOfHour(0);
            List<MetricsDTO> metrics = flexibilityMetricsService.getDA(time.getMillis());
        }
    }

    private void callIntraDayPlanner() {
        // TODO: remove on deploy
        if (runIntraDay < 6) {
            if (currentTime.getHourOfDay() == INTRADAY_STARTHOUR) {
                intraDayPlanner.setDelayTolerablePercentage(
                        dayAheadPlanner.getDelayTolerablePercentage(selectedDayAheadPlanId));
                intraDayPlanner.setRTDayWorkload(dayAheadPlanner.getPredictedRT(selectedDayAheadPlanId));
            }

            DateTime startTime = lastIntraDayRun.plus(IntradayPlanner.DURATION);
            LOGGER.info("Executing intra day for data center: " + Planner.DATACENTER_NAME);
            intraDayActionPlan = intraDayPlanner.execute(strategies, startTime, currentTime,
                    dayAheadActionPlans.get(0).getConfidenceLevel());
            lastIntraDayRun = startTime;
            // TODO: POST to ManagerConsole
            OptimizationPlanNotification.notifyIntraDayPlans(intraDayActionPlan, lastIntraDayRun);
            runIntraDay++;
        } else {
            LOGGER.info("day ahead should run only once :D ");
        }
    }

    // private void callRealtimePlanner() {
    // ActionPlan realtimeActionPlan = realtimePlanner.execute(currentTime)
    // OptimizationPlanNotification.notifyRealTimePlans(realtimeActionPlan,
    // currentTime)
    // activeActionPlan.mergeActionPlans(realtimeActionPlan)
    // }

    private void extendActivePlanDA() {
        ActionPlan actionPlan = getDayAheadPlanById(selectedDayAheadPlanId);
        if (actionPlan != null) {
            activeActionPlan.mergeActionPlans(actionPlan);
            sendBidsAndOffersToMarketplace(nextDayAheadScheduleTime, MarketTimeframes.DAYAHEAD, actionPlan);
        }
        nextDayAheadScheduleTime = nextDayAheadScheduleTime.plus(DayaheadPlanner.DURATION);
        dayAheadPlansComputed = false;
    }

    private void extendActivePlanID() {
		/*List<Bid> bids = ActionMapper.getBidsFromBuyActions(intraDayActionPlan.getBuyEnergyActions(),
				currentTime);
		List<Offer> offers = ActionMapper.getOffersFromSellActions(intraDayActionPlan.getSellEnergyActions(),
				currentTime);
		sendBidsAndOffersToMarketplace(bids, offers);
		LOGGER.info("send Intra-Day bids & offers to marketplace");*/
        //TODO call market manager for dealing with actions from energy plan
        if (intraDayActionPlan != null) {
            activeActionPlan.mergeActionPlans(intraDayActionPlan);
            sendBidsAndOffersToMarketplace(nextIntraDayScheduleTime, MarketTimeframes.INTRADAY, intraDayActionPlan);
        }
        nextIntraDayScheduleTime = nextIntraDayScheduleTime.plus(IntradayPlanner.DURATION);
        intraDayPlanComputed = false;
    }

    // this will be taken care by market manager
	/*private void sendBidsAndOffersToMarketplace(List<Bid> bids, List<Offer> offers) {
		// RestAPIClient.postObject(EsmaAPIURL.BID_URL, bids, Void.class)
		// RestAPIClient.postObject(EsmaAPIURL.OFFER_URL, bids, Void.class)
	}*/

    private void sendBidsAndOffersToMarketplace(DateTime startDate, MarketTimeframes timeframe, ActionPlan plan) {
        marketManager.postMarketActions(currentTime, startDate, timeframe, plan.getActionPool(), plan.getCorrelatedActionPool());
    }

    private ActionPlan getDayAheadPlanById(int id) {
        for (ActionPlan plan : dayAheadActionPlans) {
            if (plan.getId() == id) {
                return plan;
            }
        }
        LOGGER.error("An error occured while selecting Day-Ahead plan for id: " + id);
        return null;
    }

    /**
     * @param selectedDayAheadPlanId the selectedDayAheadPlanId to set
     */
    public boolean setSelectedDayAheadPlanId(int selectedDayAheadPlanId) {
        if (selectedDayAheadPlanId >= 0 && selectedDayAheadPlanId < dayAheadActionPlans.size()
                && this.selectedDayAheadPlanId == UNSELECTED_PLAN && dayAheadPlansComputed) {
            LOGGER.info("Day-Ahead plan selected - id: " + selectedDayAheadPlanId);
            this.selectedDayAheadPlanId = selectedDayAheadPlanId;
            return true;
        }
        return false;
    }

    /**
     * @param selectedIntraDayPlanId the selectedIntraDayPlanId to set
     */
    public boolean setSelectedIntraDayPlanId(int selectedIntraDayPlanId) {
        if (selectedIntraDayPlanId != UNSELECTED_PLAN && this.selectedIntraDayPlanId == UNSELECTED_PLAN) {
            this.selectedIntraDayPlanId = selectedIntraDayPlanId;
            LOGGER.info("Intra-Day plan selected - id: " + selectedIntraDayPlanId);
            return true;
        }
        return false;
    }


    public String setStrategies(DateTime time) {
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("time", time.getMillis());
        FlexibilityStrategyDTO strategies = RestAPIClient.getObject(CatalystDBApiURL.FLEXIBILITY_STRATEGY,
                FlexibilityStrategyDTO.class, urlVariables);
        LOGGER.info("Strategies were set!");
//		if(strategies.getWt() >1 || strategies.getWt() <0 ||
//				strategies.getWl() >1 || strategies.getWl() <0 ||
//				strategies.getWf() >1 || strategies.getWf() <0 ||
//				strategies.getWe() >1 || strategies.getWe() <0 ){
//			return "Strategies factors should have values between 0 and 1";
//		}
//		if((strategies.getWt() + strategies.getWl() + strategies.getWf() + strategies.getWe()) != 1.0){
//			return "Strategies factors should sum to 1";
//		}

        this.strategies = strategies;
        return "Strategies were set!";
    }

    /**
     * @return the dayAheadActionPlans
     */
    public List<ActionPlan> getDayAheadActionPlans() {
        return dayAheadActionPlans;
    }

    /**
     * @return the intraDayActionPlan
     */
    public ActionPlan getIntraDayActionPlan() {
        return intraDayActionPlan;
    }

    // private void resetTime() {
    // this.currentTime = new DateTime(2018, 12, 28, 20, 5)
    // this.nextDayAheadScheduleTime = new DateTime(currentTime.getYear(),
    // currentTime.getMonthOfYear(),
    // currentTime.getDayOfMonth(), Integer.parseInt(ExecutionPropertiesLoader
    // .getProperty(ConfigurationProperties.DAYAHEAD_SCHEDULE_TIME)), 0)
    // this.lastDayAheadRun = new DateTime(currentTime.getYear(),
    // currentTime.getMonthOfYear(),
    // currentTime.getDayOfMonth(), 0, 0)
    // this.lastIntraDayRun = new DateTime(currentTime.getYear(),
    // currentTime.getMonthOfYear(),
    // currentTime.getDayOfMonth(), DAYAHEAD_STARTHOUR, 0);
    // this.nextIntraDayScheduleTime = new DateTime(currentTime.getYear(),
    // currentTime.getMonthOfYear(),
    // currentTime.getDayOfMonth(), INTRADAY_STARTHOUR, 0);
    // this.selectedDayAheadPlanId = UNSELECTED_PLAN
    // this.selectedIntraDayPlanId = UNSELECTED_PLAN
    // }
}
