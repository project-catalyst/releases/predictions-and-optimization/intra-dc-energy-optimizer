package ro.tuc.dsrl.catalyst.optimizer.resources;


import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.util.ArrayList;
import java.util.List;



/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Jan 30, 2015
 * @Description:
 *
 */
public class TesResources {

	private List<ThermalEnergyStorage> thermalStorages;
	public static final double TES_DAMAGE = 0.0002;


	public TesResources() {
		this.thermalStorages = new ArrayList<>();
	}
	public double getMaximumCapacity() {
		double maximumCapacity = 0.0;
		for (ThermalEnergyStorage tes : thermalStorages) {
			maximumCapacity += tes.getMaximumCapacity();
		}
		return maximumCapacity;
	}

	public double getActualLoadedCapacity() {
		double actualLoadedCapacity = 0.0;
		for (ThermalEnergyStorage tes : thermalStorages) {
			actualLoadedCapacity += tes.getActualLoadedCapacity();
		}
		return actualLoadedCapacity;
	}

	public double getDischargeLossRate() {
		if(thermalStorages.size() ==0){
			return 0;
		}
		double dischargeLossRate = 0.0;
		for (ThermalEnergyStorage tes : thermalStorages) {
			dischargeLossRate += tes.getDischargeLossRate();
		}
		return dischargeLossRate / (double) thermalStorages.size();
	}

	public double getChargeLossRate() {
		if(thermalStorages.size() ==0){
			return 0;
		}
		double chargeLossRate = 0.0;
		for (ThermalEnergyStorage tes : thermalStorages) {
			chargeLossRate += tes.getChargeLossRate();
		}
		return chargeLossRate / (double) thermalStorages.size();
	}

	public double getMaxDischargeRate() {
		double maxDischargeRate = 0.0;
		for (ThermalEnergyStorage tes : thermalStorages) {
			maxDischargeRate += tes.getMaxDischargeRate();
		}
		return maxDischargeRate;
	}

	public double getMaxChargeRate() {
		double maxChargeRate = 0.0;
		for (ThermalEnergyStorage tes : thermalStorages) {
			maxChargeRate += tes.getMaxChargeRate();
		}
		return maxChargeRate;
	}
	public double getFactor() {
		if(thermalStorages.size() ==0){
			return 0;
		}
		double factorTes = 0.0;
		for (ThermalEnergyStorage tes : thermalStorages) {
			factorTes += tes.getTesFactor();
		}
		return factorTes / thermalStorages.size();
	}

	/**
	 * @return the thermalStorages
	 */
	public List<ThermalEnergyStorage> getThermalStorages() {
		return thermalStorages;
	}

	/**
	 * @param thermalStorages
	 *            the thermalStorages to set
	 */
	public void setThermalStorages(List<ThermalEnergyStorage> thermalStorages) {
		this.thermalStorages = thermalStorages;
	}

}
