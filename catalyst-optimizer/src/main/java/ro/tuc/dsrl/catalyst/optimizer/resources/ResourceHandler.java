package ro.tuc.dsrl.catalyst.optimizer.resources;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.util.Date;
import java.util.List;


/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class ResourceHandler {
	//private static ResourcesInterface instance = new ResourcesT();
	private static ResourcesInterface instance = new ResourcesDM();

	private ResourceHandler() {
	}

	public static List<Battery> getAllBatteries(Date date) {
		return instance.getAllBatteries(date);
	}

	public static DataCentre getAllDataCenters(Date date) {
		return instance.getDatacentre(date);
	}

	public static List<ThermalEnergyStorage> getAllThermalStorages(Date date) {
		return instance.getAllThermalStorages(date);
	}

	public static List<ServerRoom> getAllServerRooms(Date date) {
		return instance.getAllServerRooms(date);
	}

	public static List<CoolingSystem> getAllCoolingSystems(Date date) {
		return instance.getAllCoolingSystems(date);
	}

	public static DataCentre getDatacentre(Date date) {
		return instance.getDatacentre(date);
	}

//
//
	public static Double getDTWMonitoredConsumtion(Interval arrivalTime, Interval recordTime, double divide) {
		return instance.getDTWMonitoredConsumtion(arrivalTime, recordTime, divide);
	}
//
//	public static void deleteRedundantActions(DateTime startTime, double d) {
//		instance.deleteRedundantActions(startTime, d);
//	}
	/**
	 * @param instance
	 *            the instance to set
	 */
	public static void setInstance(ResourcesInterface instance) {
		ResourceHandler.instance = instance;
	}

}
