package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.constraints;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.EnergyEfficiencySolution;

public class WorkloadConstraint implements IOptimizationConstraint<EnergyEfficiencySolution> {
    private double maxIt;

    public WorkloadConstraint(double maxIt) {
        this.maxIt = maxIt;
    }

    public void verify(EnergyEfficiencySolution solution) {
        int[] violations = new int[solution.getSize()];
        for (int i = 0; i < solution.getSize(); ++i) {
            violations[i] = 0;
            if (solution.getItLoad()[i] > this.maxIt)
                ++violations[i];
        }
        solution.seteDViolations(violations);
        solution.setItLoadViolations(violations);
    }
}