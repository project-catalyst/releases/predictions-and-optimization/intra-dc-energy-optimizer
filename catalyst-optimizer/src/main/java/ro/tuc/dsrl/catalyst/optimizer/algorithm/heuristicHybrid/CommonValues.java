package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid;


import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;

import java.io.Serializable;

public class CommonValues implements Serializable {
    private static final long serialVersionUID = -6267317754573892206L;

    private int size;

    // input
    private double[] realtimeEnergy; // E_R
    private double[] delayTolerableEnergy;// E_D
    private double[] energyPrice;// E_Price
    private double[] heatPrice; // T_Price
    private double[] loadPrice;// L_Price
    private double[] ren;//REN
    private double demandPrice;// D_Price
    private double timePeriod;// MAX_T // 24
    private double timePeriodIntraday;// MAX_T_TES
    private double factor;// f // factor for battery

    private double batteryMaxCapacity;// ESD
    private double factorTes;// f_TES
    private double copC; // COP_C
    private double copH; // COP_H
    private double tesMaxCapacity;// TES_MAX

    private double tesChargeLossRate;// TES_R
    private double tesDischargeLossRate;// TES_D
    private double batteryChargeLossRate; // ESD_R/
    private double batteryDischargeLossRate;// ESD_D
    private double tesMaxDischargeRate;// MAX_DIS_TES
    private double batteryMaxDischargeRate;// MAX_DIS
    private double dod; // DOD
    private double itMaxConsumption;// MAX_IT
    private double edPercentage;// E_D_Percentage
    private double maxRealloc;

    private double relocateActive;// relocate workload enable

    private double batteryInitialValue;// ESD_INIT
    private double tesInitialValue;// TES_INIT
    private double batteryFinalConstraint; // ESD_FIN
    private double tesFinalConstraint;// TES_FIN
    private double[] drSignal;// drSignal


    private double[] renCost;

    private double we;
    private double wl;
    private double wf;
    private double wt;
    private double wRen;

    private double deltaREN;



    private int batteryDuration = 1;

    CommonValues(OptimizationDataCatalyst data) {
        this.size = data.getSize();
        this.realtimeEnergy = data.getRealtimeEnergy();
        this.delayTolerableEnergy = data.getDelayTolerableEnergy();
        this.ren = data.getRenewableEnergy();
        this.energyPrice = data.getEnergyPrice();
        this.heatPrice = data.getHeatPrice();
        this.loadPrice = data.getLoadPrice();
        this.demandPrice = data.getDemandPrice()[0];
        this.timePeriod = data.getTimePeriod()[0];
       // this.timePeriodIntraday = data.getTimePeriodIntraday()[0];
        this.factor = data.getFactor()[0];
        this.batteryMaxCapacity = data.getBatteryMaxCapacity()[0];
        this.factorTes = data.getFactorTes()[0];
        this.copC = data.getCopC()[0];
        this.copH = data.getCopH()[0];
        this.tesMaxCapacity = data.getTesMaxCapacity()[0];
        this.tesChargeLossRate = data.getTesChargeLossRate()[0];
        this.tesDischargeLossRate = data.getTesDischargeLossRate()[0];
        this.batteryChargeLossRate = data.getBatteryChargeLossRate()[0];
        this.batteryDischargeLossRate = data.getBatteryDischargeLossRate()[0];
        this.tesMaxDischargeRate = data.getTesMaxDischargeRate()[0];
        this.batteryMaxDischargeRate = data.getBatteryMaxDischargeRate()[0];
        this.dod = data.getDod()[0];
        this.itMaxConsumption = data.getItMaxConsumption()[0];
        this.edPercentage = OptimizationDataCatalyst.ED_PERCENTAGE;
        this.relocateActive = data.getRelocateActive()[0];
        this.batteryInitialValue = data.getBatteryInitialValue()[0];
        this.tesInitialValue = data.getTesInitialValue()[0];
        this.batteryFinalConstraint = data.getBatteryFinalConstraint()[0];
        this.tesFinalConstraint = data.getTesFinalConstraint()[0];
        this.drSignal = data.getDrSignal();
        this.we = data.getWE()[0];
        this.wl = data.getWL()[0];
        this.wf = data.getWF()[0];
        this.wt = data.getWT()[0];
        this.wRen = data.getWREN()[0];
        this.relocateActive = data.getRelocateActive()[0];
        this.maxRealloc = data.getMaxRealloc()[0];

        deltaREN = initDeltaRen();
        batteryDuration = Math.min((int)((batteryInitialValue - batteryMaxCapacity * dod) / batteryMaxDischargeRate), size/2-1);

        renCost = initRenCost();

    }

    private double[] initRenCost(){
        double[] renC = new double[size];
        for(int i = 0; i< size; i++){
            if(ren[i] < 0.01){
                renC[i] = 10.0;
            }
            else{
                renC[i] = 1.0 / ren[i];
            }
        }
        return renC;
    }
    private double initDeltaRen(){
        double ret = 0;
        double totalE = 0;
        double totalRen = 0;
        for(int i = 0; i< size; i++){
            totalE += (realtimeEnergy[i] + delayTolerableEnergy[i])*(1+1.0/copC);
            totalRen += ren[i];
        }
        if(totalE > totalRen){
            ret = (totalE - totalRen) / size;
        }
        return ret;
    }
    public int getSize() {
        return this.size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double[] getRealtimeEnergy() {
        return this.realtimeEnergy;
    }

    public void setRealtimeEnergy(double[] realtimeEnergy) {
        this.realtimeEnergy = realtimeEnergy;
    }

    public double[] getDelayTolerableEnergy() {
        return delayTolerableEnergy;
    }

    public void setDelayTolerableEnergy(double[] delayTolerableEnergy) {
        this.delayTolerableEnergy = delayTolerableEnergy;
    }

    public double[] getEnergyPrice() {
        return energyPrice;
    }

    public void setEnergyPrice(double[] energyPrice) {
        this.energyPrice = energyPrice;
    }

    public double[] getHeatPrice() {
        return heatPrice;
    }

    public void setHeatPrice(double[] heatPrice) {
        this.heatPrice = heatPrice;
    }

    public double[] getLoadPrice() {
        return loadPrice;
    }

    public void setLoadPrice(double[] loadPrice) {
        this.loadPrice = loadPrice;
    }

    public double getDemandPrice() {
        return demandPrice;
    }

    public void setDemandPrice(double demandPrice) {
        this.demandPrice = demandPrice;
    }

    public double getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(double timePeriod) {
        this.timePeriod = timePeriod;
    }

    public double getTimePeriodIntraday() {
        return timePeriodIntraday;
    }

    public void setTimePeriodIntraday(double timePeriodIntraday) {
        this.timePeriodIntraday = timePeriodIntraday;
    }

    public double getFactor() {
        return factor;
    }

    public void setFactor(double factor) {
        this.factor = factor;
    }

    public double getBatteryMaxCapacity() {
        return batteryMaxCapacity;
    }

    public void setBatteryMaxCapacity(double batteryMaxCapacity) {
        this.batteryMaxCapacity = batteryMaxCapacity;
    }

    public double getFactorTes() {
        return factorTes;
    }

    public void setFactorTes(double factorTes) {
        this.factorTes = factorTes;
    }

    public double getCopC() {
        return copC;
    }

    public void setCopC(double copC) {
        this.copC = copC;
    }

    public double getCopH() {
        return copH;
    }

    public void setCopH(double copH) {
        this.copH = copH;
    }

    public double getTesMaxCapacity() {
        return tesMaxCapacity;
    }

    public void setTesMaxCapacity(double tesMaxCapacity) {
        this.tesMaxCapacity = tesMaxCapacity;
    }

    public double getTesChargeLossRate() {
        return tesChargeLossRate;
    }

    public void setTesChargeLossRate(double tesChargeLossRate) {
        this.tesChargeLossRate = tesChargeLossRate;
    }

    public double getTesDischargeLossRate() {
        return tesDischargeLossRate;
    }

    public void setTesDischargeLossRate(double tesDischargeLossRate) {
        this.tesDischargeLossRate = tesDischargeLossRate;
    }

    public double getBatteryChargeLossRate() {
        return batteryChargeLossRate;
    }

    public void setBatteryChargeLossRate(double batteryChargeLossRate) {
        this.batteryChargeLossRate = batteryChargeLossRate;
    }

    public double getBatteryDischargeLossRate() {
        return batteryDischargeLossRate;
    }

    public void setBatteryDischargeLossRate(double batteryDischargeLossRate) {
        this.batteryDischargeLossRate = batteryDischargeLossRate;
    }

    public double getTesMaxDischargeRate() {
        return tesMaxDischargeRate;
    }

    public void setTesMaxDischargeRate(double tesMaxDischargeRate) {
        this.tesMaxDischargeRate = tesMaxDischargeRate;
    }

    public double getBatteryMaxDischargeRate() {
        return batteryMaxDischargeRate;
    }

    public void setBatteryMaxDischargeRate(double batteryMaxDischargeRate) {
        this.batteryMaxDischargeRate = batteryMaxDischargeRate;
    }

    public double getDod() {
        return dod;
    }

    public void setDod(double dod) {
        this.dod = dod;
    }

    public double getItMaxConsumption() {
        return itMaxConsumption;
    }

    public void setItMaxConsumption(double itMaxConsumption) {
        this.itMaxConsumption = itMaxConsumption;
    }

    public double getEdPercentage() {
        return edPercentage;
    }

    public void setEdPercentage(double edPercentage) {
        this.edPercentage = edPercentage;
    }

    public double getRelocateActive() {
        return relocateActive;
    }

    public void setRelocateActive(double relocateActive) {
        this.relocateActive = relocateActive;
    }

    public double getBatteryInitialValue() {
        return batteryInitialValue;
    }

    public void setBatteryInitialValue(double batteryInitialValue) {
        this.batteryInitialValue = batteryInitialValue;
    }

    public double getTesInitialValue() {
        return tesInitialValue;
    }

    public void setTesInitialValue(double tesInitialValue) {
        this.tesInitialValue = tesInitialValue;
    }

    public double getBatteryFinalConstraint() {
        return batteryFinalConstraint;
    }

    public void setBatteryFinalConstraint(double batteryFinalConstraint) {
        this.batteryFinalConstraint = batteryFinalConstraint;
    }

    public double getTesFinalConstraint() {
        return tesFinalConstraint;
    }

    public void setTesFinalConstraint(double tesFinalConstraint) {
        this.tesFinalConstraint = tesFinalConstraint;
    }

    public double[] getDrSignal() {
        return drSignal;
    }

    public void setDrSignal(double[] drSignal) {
        this.drSignal = drSignal;
    }

    public double getWe() {
        return we;
    }

    public void setWe(double wE) {
        we = wE;
    }

    public double getWl() {
        return wl;
    }

    public void setWl(double wL) {
        wl = wL;
    }

    public double getWf() {
        return wf;
    }

    public void setWf(double wF) {
        wf = wF;
    }

    public double getWt() {
        return wt;
    }

    public void setWt(double wT) {
        wt = wT;
    }

    public double getMaxRealloc() {
        return maxRealloc;
    }

    public void setMaxRealloc(double maxRealloc) {
        this.maxRealloc = maxRealloc;
    }


    public double[] getRen() {
        return ren;
    }

    public void setRen(double[] ren) {
        this.ren = ren;
    }


    public double getwRen() {
        return wRen;
    }

    public void setwRen(double wRen) {
        this.wRen = wRen;
    }

    public double getDeltaREN() {
        return deltaREN;
    }

    public void setDeltaREN(double deltaREN) {
        this.deltaREN = deltaREN;
    }

    public int getBatteryDuration() {
        return batteryDuration;
    }

    public void setBatteryDuration(int batteryDuration) {
        this.batteryDuration = batteryDuration;
    }

    public double[] getRenCost() {
        return renCost;
    }

    public void setRenCost(double[] renCost) {
        this.renCost = renCost;
    }

}