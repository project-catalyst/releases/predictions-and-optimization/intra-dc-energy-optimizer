package ro.tuc.dsrl.catalyst.optimizer.resources;


import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 16, 2015
 * @Description:
 *
 */
public class CoolingResources {

	List<CoolingSystem> coolingSystems;

	public CoolingResources() {
		this.coolingSystems = new ArrayList<>();
	}

	public void setCoolingSystems(List<CoolingSystem> coolingSystems) {
		this.coolingSystems = coolingSystems;
	}


	/**
	 * @return the currentEnergyConsumption
	 */
	public double getCurrentEnergyConsumption() {
		 double currentEnergyConsumption = 0 ;

		 for(CoolingSystem cs: coolingSystems){
		 	currentEnergyConsumption+= cs.getEnergyConsumption();
		 }

		return currentEnergyConsumption;
	}

	public double getCopC() {
		double copC = 0 ;

		for(CoolingSystem cs: coolingSystems){
			copC+= cs.getCopC();
		}

		return copC / coolingSystems.size();
	}

	public double getCopH() {
		double copH = 0 ;

		for(CoolingSystem cs: coolingSystems){
			copH += cs.getCopH();
		}

		return copH / coolingSystems.size();
	}


	public double getMaxEnergyConsumption() {
		double maxEnergyConsumption = 0 ;

		for(CoolingSystem cs: coolingSystems){
			maxEnergyConsumption+= cs.getMaxCoolingLoadKWh();
		}

		return maxEnergyConsumption;
	}



	public List<CoolingSystem> getCoolingSystems() {
		return coolingSystems;
	}
}
