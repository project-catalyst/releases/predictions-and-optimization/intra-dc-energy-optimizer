package ro.tuc.dsrl.catalyst.optimizer.planner;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileMultipleEnergyTypesDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySamplesForMeasurementDTO;
import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.model.enums.AggregationGranularity;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.*;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketData;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketManager;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTimeframes;
import ro.tuc.dsrl.geyser.datamodel.actions.ShiftDelayTolerantWorkload;
import ro.tuc.dsrl.catalyst.optimizer.manager.OptimizationPlanNotification;
import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictedData;
import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictionHandler;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.catalyst.optimizer.resources.ResourceHandler;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelValuesIO;

import java.util.*;

public class IntradayPlanner extends Planner {


	private static final Logger LOGGER = LoggerFactory.getLogger(IntradayPlanner.class);

	public static final int PREDICTION_PERIOD_LENGTH = 4;// hours
	private static final int SAMPLING_VALUE = 30;// minutes
	public static final int INTRA_DAY_SLOTS = PREDICTION_PERIOD_LENGTH * DateTimeConstants.MINUTES_PER_HOUR
			/ SAMPLING_VALUE;
	private static final double MONITORED_VALUES_HALFHOUR = 6.0;
	private static final int CONFIDENCE_LEVEL = 1;

	public static final Period SAMPLING = new Period().withMinutes(SAMPLING_VALUE);
	public static final Period DURATION = new Period().withHours(PREDICTION_PERIOD_LENGTH);
	//public static final String TIMEFRAME = "intra_day";

	private List<Double> delayTolerablePercentage;
	private List<Double> rtDayWorkload;
	private ActionPlan actionsToBeDeleted;
	private OptimizationDataCatalyst intradayLingoData;

	private static MarketManager marketManager;

	public IntradayPlanner(ActionPlan actionPlan, DCResources resources,MarketManager marketManager) {
		super(actionPlan, resources);
		this.marketManager = marketManager;
	}

	public ActionPlan execute(FlexibilityStrategyDTO strategies, DateTime startTime, DateTime currentTime,
							  double dayAheadConfidenceLevel) {

		ResourceConstraints startConstraints = getStartConstraint(startTime);
		ResourceConstraints endConstraints = getEndConstraint(startTime.plus(DURATION));
		this.actionsToBeDeleted = actionPlan.getDeletedActions(new Interval(startTime, startTime.plus(DURATION)));
		PredictedData predictedData = computePredictions(startTime, currentTime, dayAheadConfidenceLevel);
		List<Double> drSignal = getDRSignalIntraday(startTime);
		MarketData marketData = marketManager.getMarketData(startTime, MarketTimeframes.DAYAHEAD);

		OptimizationAlgorithm optimizationAlgorithm = OptimizationAlgorithmFactory.create(drSignal, strategies, predictedData,
                marketData, resources, startConstraints, endConstraints, INTRA_DAY_SLOTS, AlgorithmType.GREEDY);
		intradayLingoData = optimizationAlgorithm.getOptimizationData();
		ExcelValuesIO.writeDataToCSVFile(intradayLingoData, "pre-intraday-hour"+startTime.getHourOfDay() +"-"+ new Date().getTime() + ".csv");

		optimizationAlgorithm.compute(ServiceType.FLEXIBILITY);

		intradayLingoData = optimizationAlgorithm.getOptimizationData();
		ExcelValuesIO.writeDataToCSVFile(intradayLingoData, "post-intraday" + new Date().getTime() + ".csv");

		this.saveEnergyAndThermalOptimizedProfiles(intradayLingoData, startTime);
		return getIntradayPlan(intradayLingoData, startTime, dayAheadConfidenceLevel);
	}

	private void saveEnergyAndThermalOptimizedProfiles(OptimizationDataCatalyst data, DateTime startTime){

		//==== ADAPTED PROFILES ====
		Double[] optimizedDCArray = ArrayUtils.toObject(data.getDcFinalConsumption());
		List<Double> optimizedDCProfile = Arrays.asList(optimizedDCArray);

		Double[] optimizedCoolingArray = ArrayUtils.toObject(data.getFinalCooling());
		List<Double> optimizedCoolingProfile = Arrays.asList(optimizedCoolingArray);

		Double[] optimizedServerArray = ArrayUtils.toObject(data.getItLoad());
		List<Double> optimizedServerProfile = Arrays.asList(optimizedServerArray);

		Double[] optimizedThermalArray = ArrayUtils.toObject(data.getDcThermalGeneration());
		List<Double> optimizedThermalProfile = Arrays.asList(optimizedThermalArray);

		Double[] optimizedCO2Array = ArrayUtils.toObject(data.getCo2adapted());
		List<Double> optimizedCO2Profile = Arrays.asList(optimizedCO2Array);

		//==== BASELINE PROFILES ====

		Double[] baselineThermalArray = ArrayUtils.toObject(data.getHeatBaseline());
		List<Double> baselineThermalProfile = Arrays.asList(baselineThermalArray);

		Double[] baselineCO2Array = ArrayUtils.toObject(data.getCo2baseline());
		List<Double> baselineCO2Profile = Arrays.asList(baselineCO2Array);

		EnergySamplesForMeasurementDTO optimizedServerSamples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_SERVER, optimizedServerProfile);
		EnergySamplesForMeasurementDTO optimizedThermalSamples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_THERMAL, optimizedThermalProfile);
		EnergySamplesForMeasurementDTO optimizedCO2Samples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_CO2, optimizedCO2Profile);
		EnergySamplesForMeasurementDTO optimizedDCSamples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_DC, optimizedDCProfile);
		EnergySamplesForMeasurementDTO optimizedCoolingSamples = new EnergySamplesForMeasurementDTO(MeasurementType.OPTIMIZED_COOLING, optimizedCoolingProfile);
		EnergySamplesForMeasurementDTO baselineCO2Samples = new EnergySamplesForMeasurementDTO(MeasurementType.DATA_CENTER_HEAT_BASELINE, baselineThermalProfile);
		EnergySamplesForMeasurementDTO baselineDCSamples = new EnergySamplesForMeasurementDTO(MeasurementType.DATA_CENTER_CO2_BASELINE, baselineCO2Profile);

		List<EnergySamplesForMeasurementDTO> optimizedSamplesForThermalAndEnergy = new ArrayList<>();
		optimizedSamplesForThermalAndEnergy.add(optimizedServerSamples);
		optimizedSamplesForThermalAndEnergy.add(optimizedThermalSamples);
		optimizedSamplesForThermalAndEnergy.add(optimizedCO2Samples);
		optimizedSamplesForThermalAndEnergy.add(optimizedDCSamples);
		optimizedSamplesForThermalAndEnergy.add(optimizedCoolingSamples);
		optimizedSamplesForThermalAndEnergy.add(baselineCO2Samples);
		optimizedSamplesForThermalAndEnergy.add(baselineDCSamples);
		EnergyProfileMultipleEnergyTypesDTO energyProfile = new EnergyProfileMultipleEnergyTypesDTO(DATACENTER_NAME,
				AggregationGranularity.MINUTES_30, PredictionGranularity.INTRADAY, PredictionType.OPTIMIZED_PROFILE,
				ro.tuc.dsrl.catalyst.model.enums.AlgorithmType.OPTIMIZATION, startTime.getMillis(), optimizedSamplesForThermalAndEnergy);

		OptimizationPlanNotification.notifyOptimizedProfile(energyProfile);

	}

	private ActionPlan getIntradayPlan(OptimizationDataCatalyst data, DateTime startTime, double dayAheadConfidenceLevel) {

		if (! data.isSuccess()){
			LOGGER.error("Intraday Plan encountered an error. ");
		}

		ActionPlan actionPlan = OptimizationActionAdapter.extract(data, resources, startTime, SAMPLING, DURATION);
		actionPlan.setConfidenceLevel(dayAheadConfidenceLevel);

		actionPlan.setWE(data.getWE()[0]);
		actionPlan.setWL(data.getWL()[0]);
		actionPlan.setWF(data.getWF()[0]);
		actionPlan.setWT(data.getWT()[0]);
		actionPlan.setReallocActive(data.getRelocateActive()[0] != 0.0);
		return actionPlan;
		// TODO: actionstobedeleted are DA actions that were replaced in ID
		// return actionsToBeDeleted;
	}

	private PredictedData computePredictions(DateTime startTime, DateTime currentTime, double dayAheadConfidenceLevel) {
		PredictedData predictedData = PredictionHandler.getIntraDayPredictedData(startTime, CONFIDENCE_LEVEL,
				currentTime);
		List<Double> predictedDTW = predictedData.getDelayTolerableWorkload();
		predictedDTW = simulateActions(predictedDTW, startTime);
		List<Double> pastDelayedWorkload = computePastDelayedWorkload(startTime);
		List<Double> dtWorklooad = addListElements(predictedDTW, pastDelayedWorkload);
		predictedData.setRealtimeWorkload(addListElements(predictedData.getRealtimeWorkload(), dtWorklooad));
		dtWorklooad = new ArrayList<Double>(Collections.nCopies(INTRA_DAY_SLOTS, 0.0));
		predictedData.setDelayTolerableWorkload(dtWorklooad);
		return predictedData;
	}


	/**
	 * @description Step 2. ShiftDTW actions remain are simulated s.t. to
	 *              rearrange consumption
	 * @param predictedDTW
	 * @param startTime
	 * @return
	 */

	public List<Double> simulateActions(List<Double> predictedDTW, DateTime startTime) {
		List<Double> delayEnergyConsumption = new ArrayList<Double>();

		for (int i = 0; i < predictedDTW.size(); i++) {
			delayEnergyConsumption.add(0.0);
		}

		DateTime endTime = startTime.plus(DURATION);
		Interval initialization = new Interval(startTime, endTime);

		for (DateTime current = startTime; current.isBefore(endTime); current = current.plusHours(1)) {

			List<ShiftDelayTolerantWorkload> currentActions = actionPlan
					.getSDTWActionsDelayedToCurrentTime(initialization, current);
			for (ShiftDelayTolerantWorkload sdtw : currentActions) {

				int indexInitialConsumption = (new DateTime(sdtw.getFromTime()).getHourOfDay()
						- startTime.getHourOfDay()) * 2;
				double currentConsumptionFirstHalf = predictedDTW.get(indexInitialConsumption);
				indexInitialConsumption++;
				double currentConsumptionSecondHalf = predictedDTW.get(indexInitialConsumption);

				int index = (current.getHourOfDay() - startTime.getHourOfDay()) * 2;
				double oldValue = delayEnergyConsumption.get(index);
				delayEnergyConsumption.set(index,
						oldValue + currentConsumptionFirstHalf * sdtw.getPercentOfWorkloadDelayed());

				index++;
				oldValue = delayEnergyConsumption.get(index);
				delayEnergyConsumption.set(index,
						oldValue + currentConsumptionSecondHalf * sdtw.getPercentOfWorkloadDelayed());

			}
		}

		return delayEnergyConsumption;
	}

	/**
	 * @description Step 3. workload delayed from the past @3.1. get list of
	 *              SDTW actions scheduled for current hour @3.2. get list of
	 *              SDTW actions that already were executed based on the start
	 *              hour of actions in S1. @3.3. from compute percentage &
	 *              energy consumption
	 * @param startTime
	 * @return
	 */
	public List<Double> computePastDelayedWorkload(DateTime startTime) {
		List<Double> delayedDTW = new ArrayList<Double>();
		for (int i = 0; i < INTRA_DAY_SLOTS; i++) {
			delayedDTW.add(0.0);
		}

		DateTime endOfWindow = startTime.plus(DURATION);

		Interval initialization = new Interval(startTime.withHourOfDay(0), startTime);
		for (DateTime currentDate = startTime; currentDate
				.isBefore(endOfWindow); currentDate = currentDate.plusHours(1)) {
			List<ShiftDelayTolerantWorkload> delayedActions = actionPlan
					.getSDTWActionsDelayedToCurrentTime(initialization, currentDate);
			for (ShiftDelayTolerantWorkload action : delayedActions) {
				DateTime fromTime = new DateTime(action.getFromTime());
				List<ShiftDelayTolerantWorkload> alreadyExecutedSDTW = actionPlan
						.getSDTWExecutedBeforeCurrentTime(fromTime, startTime);
				double percentage = 0;
				double energyConsumptionFirstHalf = 0;
				double energyConsumptionSecondHalf = 0;
				for (ShiftDelayTolerantWorkload executedAction : alreadyExecutedSDTW) {
					DateTime toTime = new DateTime(executedAction.getToTime());
					percentage += executedAction.getPercentOfWorkloadDelayed();

					Interval arrivalTime = new Interval(fromTime, fromTime.plusHours(1));
					Interval recordTime = new Interval(toTime, toTime.plus(SAMPLING));
					energyConsumptionFirstHalf += ResourceHandler.getDTWMonitoredConsumtion(arrivalTime, recordTime,
							MONITORED_VALUES_HALFHOUR);

					arrivalTime = new Interval(fromTime, fromTime.plusHours(1));
					recordTime = new Interval(toTime.plus(SAMPLING), toTime.plusHours(1));
					energyConsumptionSecondHalf += ResourceHandler.getDTWMonitoredConsumtion(arrivalTime, recordTime,
							MONITORED_VALUES_HALFHOUR);
				}
				int index1 = (currentDate.getHourOfDay() - startTime.getHourOfDay()) * 2;
				int index2 = index1 + 1;
				double valueFirstHalf = 0;
				double valueSecondHalf = 0;
				//TODO: remove
				if(percentage!=0 && energyConsumptionFirstHalf==0 && energyConsumptionSecondHalf==0){
					percentage=0;
				}//remove
				if (percentage != 0) {
					valueFirstHalf = action.getPercentOfWorkloadDelayed() * energyConsumptionFirstHalf / percentage;
					valueSecondHalf = action.getPercentOfWorkloadDelayed() * energyConsumptionSecondHalf / percentage;
				} else {
					double estimatedConsumption = rtDayWorkload.get(fromTime.getHourOfDay())
							* delayTolerablePercentage.get(fromTime.getHourOfDay());
					valueFirstHalf = estimatedConsumption * action.getPercentOfWorkloadDelayed();
					valueSecondHalf = valueFirstHalf;
				}
				double oldValue1 = delayedDTW.get(index1);
				double oldValue2 = delayedDTW.get(index2);
				delayedDTW.set(index1, valueFirstHalf + oldValue1);
				delayedDTW.set(index2, valueSecondHalf + oldValue2);
			}

		}
		return delayedDTW;
	}



	private List<Double> addListElements(List<Double> first, List<Double> second) {
		List<Double> result = new ArrayList<Double>();
		for (int i = 0; i < INTRA_DAY_SLOTS; i++) {
			result.add(first.get(i) + second.get(i));
		}
		return result;
	}

	/**
	 * @param delayedPercentage
	 *            the delayedPercentage to set
	 */
	public void setDelayTolerablePercentage(List<Double> delayedPercentage) {
		this.delayTolerablePercentage = delayedPercentage;
	}

	public void setRTDayWorkload(List<Double> rtDayWorkload) {
		this.rtDayWorkload = rtDayWorkload;
	}
}
