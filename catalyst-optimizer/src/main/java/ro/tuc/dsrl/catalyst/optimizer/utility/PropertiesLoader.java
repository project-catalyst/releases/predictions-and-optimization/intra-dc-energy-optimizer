package ro.tuc.dsrl.catalyst.optimizer.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.optimizer.web.controller.IndexController;
import ro.tuc.dsrl.geyser.execution.utility.ExecutionPropertiesLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 * Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: rest-client
 * @Since: Dec 17, 2014
 * @Description:
 */

@Component
public  class PropertiesLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesLoader.class);

    @Value("${CATALYST_METRICS_API_HOST}")
    private String metricsHost;

    @Value("${CATALYST_DB_API_HOST}")
    private String dbApiHost;


    @Value("${LINGO_API_HOST}")
    private String lingoApiHost;

    @Value("${MARKETPLACE_MPCM}")
    private String marketApiHost;

    @Value("${DAYAHEAD_VALIDATION}")
    private int dayaheadValidation;

    @Value("${INTRADAY_VALIDATION}")
    private int intradayValidation;

    @Value("${DAYAHEAD_SCHEDULE_TIME}")
    private int getDayaheadScheduleTime;

    @Value("${INTRADAY_START_TIME}")
    private int intradayStartTime;

    @Value("${DATACENTER_NAME}")
    private String dcName;

    @Value("${MARKET_ACTOR_NAME}")
    private String marketActorName;

    public static String CATALYST_METRIC_API_HOST;
    public static String CATALYST_DB_API_HOST;
    public static String LINGO_API_HOST;
    public static String MARKETPLACE_MPCM;

    public static int DAYAHEAD_VALIDATION;
    public static int INTRADAY_VALIDATION;
    public static int DAYAHEAD_SCHEDULE_TIME;
    public static int INTRADAY_START_TIME;

    public static String DATACENTER_NAME;
    public static String MARKET_ACTOR_NAME;

    @Value("${CATALYST_METRICS_API_HOST}")
    public void setMetricAPIStatic(String host) {
        PropertiesLoader.CATALYST_METRIC_API_HOST = host;
    }

    @Value("${CATALYST_DB_API_HOST}")
    public void setDBAPIStatic(String host) {
        PropertiesLoader.CATALYST_DB_API_HOST = host;
        ExecutionPropertiesLoader.CATALYST_DB_API_HOST = host;
    }

    @Value("${MARKETPLACE_MPCM}")
    public void setMarketAPIStatic(String host) {
        PropertiesLoader.MARKETPLACE_MPCM = host;
    }

    @Value("${LINGO_API_HOST}")
    public void setLingoAPIStatic(String host) {
        PropertiesLoader.LINGO_API_HOST = host;
    }

    @Value("${DAYAHEAD_VALIDATION}")
    public void setDAValidStatic(String value) {
        PropertiesLoader.DAYAHEAD_VALIDATION = Integer.parseInt(value);
    }

    @Value("${INTRADAY_VALIDATION}")
    public void setIDValidStatic(String value) {
        PropertiesLoader.INTRADAY_VALIDATION = Integer.parseInt(value);
    }

    @Value("${DAYAHEAD_SCHEDULE_TIME}")
    public void setDAHourStatic(String value) {
        PropertiesLoader.DAYAHEAD_SCHEDULE_TIME = Integer.parseInt(value);
    }

    @Value("${INTRADAY_START_TIME}")
    public void setIDHourStatic(String value) {
        PropertiesLoader.INTRADAY_START_TIME = Integer.parseInt(value);
    }

    @Value("${DATACENTER_NAME}")
    public void setDCNameStatic(String value) {
        PropertiesLoader.DATACENTER_NAME = value;
        ExecutionPropertiesLoader.DATACENTER_NAME = value;
    }

    @Value("${MARKET_ACTOR_NAME}")
    public void setActorNameStatic(String value) {
        PropertiesLoader.MARKET_ACTOR_NAME = value;
    }
}
