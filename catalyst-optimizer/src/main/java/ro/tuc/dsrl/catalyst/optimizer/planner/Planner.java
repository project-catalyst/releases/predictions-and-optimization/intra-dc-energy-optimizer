package ro.tuc.dsrl.catalyst.optimizer.planner;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.model.dto.EnergyPointValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.catalyst.optimizer.utility.MathUtility;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystDBApiURL;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.catalyst.optimizer.utility.PropertiesLoader;
import ro.tuc.dsrl.geyser.execution.start.ActionExecution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Mar 2, 2015
 * @Description:
 *
 */
public abstract class Planner {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActionExecution.class);
	public static final String DATACENTER_NAME = PropertiesLoader.DATACENTER_NAME;

	protected ActionPlan actionPlan;
	protected DCResources resources;

	public Planner(ActionPlan actionPlan, DCResources resources) {
		this.actionPlan = actionPlan;
		this.resources = resources;
	}

	public ResourceConstraints getEndConstraint(DateTime date) {
		ResourceConstraints endConstraint = actionPlan.getResourceConstraintsByDate(date);

		if (endConstraint == null) {
			// TODO: relevant constraint
			double batteryMax = resources.getBatteryResource().getMaximumCapacity();
			double tesMax = resources.getTesResource().getMaximumCapacity();
			endConstraint = new ResourceConstraints(tesMax,	batteryMax);
		}
		return new ResourceConstraints(MathUtility.round2Decimals(endConstraint.getTesLevel()),
				MathUtility.round2Decimals(endConstraint.getUpsLevel()));
	}

	public ResourceConstraints getStartConstraint(DateTime date) {
		ResourceConstraints startConstraint = actionPlan.getResourceConstraintsByDate(date);
		if (startConstraint == null) {
			LOGGER.info("getting constraints from resource constraints");
			startConstraint = new ResourceConstraints(resources.getTesResource().getMaximumCapacity(),
					resources.getBatteryResource().getMaximumCapacity());
		}

		return new ResourceConstraints(MathUtility.round2Decimals(startConstraint.getTesLevel()),
				MathUtility.round2Decimals(startConstraint.getUpsLevel()));
	}

	protected List<Double> getDRSignalDayahead(DateTime startDate){
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("startdate", startDate.getMillis());
		urlVariables.put("datacenter-name", DATACENTER_NAME);

		ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
				.getObject(CatalystDBApiURL.DR_SIGNAL_DAYAHEAD, ForecastDayAheadDTO.class, urlVariables);

		List<Double> values = new ArrayList<Double>();
		for (EnergyPointValueDTO energyPointValue : forecastDayAheadDTO.getEnergyPointValueDTOS()) {
			values.add(MathUtility.round4Decimals(energyPointValue.getEnergyValue()));
		}

		// TODO: get bids from DSO in case of flex market an add values to DR signal
		return values;
	}

	protected List<Double> getDRSignalIntraday(DateTime startDate){
		Map<String, Object> urlVariables = new HashMap<String, Object>();
		urlVariables.put("startdate", startDate.getMillis());
		urlVariables.put("datacenter-name", DATACENTER_NAME);

		ForecastDayAheadDTO forecastDayAheadDTO = RestAPIClient
				.getObject(CatalystDBApiURL.DR_SIGNAL_INTRADAY, ForecastDayAheadDTO.class, urlVariables);

		List<Double> values = new ArrayList<Double>();
		for (EnergyPointValueDTO energyPointValue : forecastDayAheadDTO.getEnergyPointValueDTOS()) {
			values.add(MathUtility.round4Decimals(energyPointValue.getEnergyValue()));
		}
		return values;
	}

}
