package ro.tuc.dsrl.catalyst.optimizer.prediction;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class PredictedData {

	private List<Double> realtimeWorkload;
	private List<Double> delayTolerableWorkload;
	private List<Double> totalEnergy;

	private List<Double> renewableEnergy;
	private List<Double> baseline;
	private List<Double> energyFromMarket;

//	private List<Double> energyPrice;
//	private List<Double> workloadPrices;
//	private List<Double> thermalPrices;
//	private List<Double> drIncentives;

	private List<Integer> delayDeadline;
	
	private double[][] dcsHourlyPrices;
	private double[] incomingReallocatedWorkload;
	

	public PredictedData() {
		totalEnergy = new ArrayList<Double>();
		realtimeWorkload = new ArrayList<Double>();
		delayTolerableWorkload = new ArrayList<Double>();
		renewableEnergy = new ArrayList<Double>();
		baseline = new ArrayList<Double>();
		energyFromMarket = new ArrayList<Double>();
//		energyPrice = new ArrayList<Double>();
//		workloadPrices = new ArrayList<Double>();
//		thermalPrices = new ArrayList<Double>();
//		drIncentives = new ArrayList<Double>();
		delayDeadline = new ArrayList<Integer>();
		dcsHourlyPrices = new double[0][0];
		incomingReallocatedWorkload = new double[0];
	}

	public List<Double> getBaseline() {
		return baseline;
	}

	public void setBaseline(List<Double> baseline) {
		this.baseline = baseline;
	}

	public List<Double> getTotalEnergy() {
		return totalEnergy;
	}

	public void setTotalEnergy(List<Double> totalEnergy) {
		this.totalEnergy = totalEnergy;
	}

//	public List<Double> getWorkloadPrices() {
//		return workloadPrices;
//	}
//
//	public void setWorkloadPrices(List<Double> workloadPrices) {
//		this.workloadPrices = workloadPrices;
//	}
//
//	public List<Double> getThermalPrices() {
//		return thermalPrices;
//	}
//
//	public void setThermalPrices(List<Double> thermalPrices) {
//		this.thermalPrices = thermalPrices;
//	}
//
//	public List<Double> getDrIncentives() {
//		return drIncentives;
//	}
//
//	public void setDrIncentives(List<Double> drIncentives) {
//		this.drIncentives = drIncentives;
//	}

	/**
	 * @return the realtimeWorkload
	 */
	public List<Double> getRealtimeWorkload() {
		return realtimeWorkload;
	}

	/**
	 * @param realtimeWorkload the realtimeWorkload to set
	 */
	public void setRealtimeWorkload(List<Double> realtimeWorkload) {
		this.realtimeWorkload = realtimeWorkload;
	}

	/**
	 * @return the delayTolerableWorkload
	 */
	public List<Double> getDelayTolerableWorkload() {
		return delayTolerableWorkload;
	}

	/**
	 * @param delayTolerableWorkload the delayTolerableWorkload to set
	 */
	public void setDelayTolerableWorkload(List<Double> delayTolerableWorkload) {
		this.delayTolerableWorkload = delayTolerableWorkload;
	}


	/**
	 * @return the energyFromMarket
	 */
	public List<Double> getEnergyFromMarket() {
		return energyFromMarket;
	}

	/**
	 * @param energyFromMarket the energyFromMarket to set
	 */
	public void setEnergyFromMarket(List<Double> energyFromMarket) {
		this.energyFromMarket = energyFromMarket;
	}

	/**
	 * @return the renewableEnergy
	 */
	public List<Double> getRenewableEnergy() {
		return renewableEnergy;
	}

	/**
	 * @param renewableEnergy the renewableEnergy to set
	 */
	public void setRenewableEnergy(List<Double> renewableEnergy) {
		this.renewableEnergy = renewableEnergy;
	}

//	/**
//	 * @return the energyPrice
//	 */
//	public List<Double> getEnergyPrice() {
//		return energyPrice;
//	}
//
//	/**
//	 * @param energyPrice the energyPrice to set
//	 */
//	public void setEnergyPrice(List<Double> energyPrice) {
//		this.energyPrice = energyPrice;
//	}

	/**
	 * @return the delayDeadline
	 */
	public List<Integer> getDelayDeadline() {
		return delayDeadline;
	}

	/**
	 * @param delayDeadline the delayDeadline to set
	 */
	public void setDelayDeadline(List<Integer> delayDeadline) {
		this.delayDeadline = delayDeadline;
	}

	public double[][] getDcsHourlyPrices() {
		return dcsHourlyPrices;
	}

	public void setDcsHourlyPrices(double[][] dcsHourlyPrices) {
		this.dcsHourlyPrices = dcsHourlyPrices;
	}

	public double[] getIncomingReallocatedWorkload() {
		return incomingReallocatedWorkload;
	}

	public void setIncomingReallocatedWorkload(double[] incomingReallocatedWorkload) {
		this.incomingReallocatedWorkload = incomingReallocatedWorkload;
	}
	
	
}
