package ro.tuc.dsrl.catalyst.optimizer.market;

import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.ActiveSessionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.MarketActionDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.MarketCorrelationDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTimeframes;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTypes;

import java.util.ArrayList;
import java.util.List;

public abstract class MarketAPIHandlerInterface {

    public static MarketAPIHandlerInterface getInstance(MarketAPIType type) {
        if (MarketAPIType.MPCM.equals(type)) {
            return new MarketAPIHandler();
        } else {
            return new DefaultDBMarketHandler();
        }
    }


    public abstract List<Double> getClearingPrices(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate);

    public abstract List<Double> getRefrencePrices(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate);

    public abstract List<ActiveSessionDTO> getActiveSessions(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate);

    public abstract List<MarketActionDTO> getMarketActions(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate);

    public abstract List<MarketActionDTO> registerActions(List<MarketActionDTO> actions, MarketTypes marketType, MarketTimeframes timeframe);

    public abstract MarketCorrelationDTO registerCorrelatedActions(MarketCorrelationDTO marketCorrelation, MarketTimeframes timeframe);

    public static enum MarketAPIType {
        MPCM, DEFAULT_DB
    }

    public List<Double> getValues(Double[] values) {
        List<Double> toReturn = new ArrayList<>();
        for (Double d : values) {
            toReturn.add(d);
        }
        return toReturn;
    }
}
