package ro.tuc.dsrl.catalyst.optimizer.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;
import ro.tuc.dsrl.catalyst.optimizer.planner.ActionPlan;
import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.joda.time.Interval;

public class ExcelValuesIO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelValuesIO.class);
    private static boolean correctData = true;

    private static ExcelIO.LingoConfigData lingoConfigData;

    private ExcelValuesIO() {
    }

    public static void writeDataToCSVFile(OptimizationDataCatalyst data, String fileName) {
        PrintWriter writer;
        try {
            writer = new PrintWriter(fileName, "UTF-8");
            writer.println("E_Total, Real_Time_Workload, Delay_Tolerant_Workload, REN, Energy_Price, " +
                    "L_Price, T_Price, D_Price, DR_Signal, Baseline, Delay_Tolerant_Executed, Total Cooling, IT Cooling, " +
                    "TES, D_TES, R_TES, ESD, D_ESD, R_ESD, DC Demand, DC Thermal Generation(Adapted Heat)," +
                    " Adapted CO2, Adapted Server, Adapted Cooling, Adapted DC, Baseline Heat, Baseline CO2, " +
                    " Relocated, Hosted, Y");

            for (int i = 0; i < data.getSize(); i++) {
                String toPrint = "";
                toPrint += data.getRealtimeEnergy()[i] + data.getDelayTolerableEnergy()[i] + ",";
                toPrint += data.getRealtimeEnergy()[i] + ",";
                toPrint += data.getDelayTolerableEnergy()[i] + ",";
                toPrint += data.getRenewableEnergy()[i] + ",";
                toPrint += data.getEnergyPrice()[i] + ",";
                toPrint += data.getLoadPrice()[i] + ",";
                toPrint += data.getHeatPrice()[i] + ",";
                toPrint += data.getDemandPrice()[i] + ",";
                toPrint += data.getDrSignal()[i] + ",";
                toPrint += data.getBaseline()[i] + ",";
                toPrint += data.getEstimatedDelayExecution()[i] + ",";
                toPrint += data.getFinalCooling()[i] + ",";
                toPrint += data.getItCooling()[i] + ",";
                toPrint += data.getTesLevel()[i] + ",";
                toPrint += data.getDischargeTes()[i] + ",";
                toPrint += data.getChargeTes()[i] + ",";
                toPrint += data.getBatteryLevel()[i] + ",";
                toPrint += data.getDischargeBattery()[i] + ",";
                toPrint += data.getChargeBattery()[i] + ",";
                toPrint += data.getDcFinalConsumption()[i] + ",";
                toPrint += data.getDcThermalGeneration()[i] + ",";
                toPrint += data.getCo2adapted()[i] + ",";
                toPrint += data.getItLoad()[i] + ",";
                toPrint += data.getFinalCooling()[i] + ",";
                toPrint += data.getDcFinalConsumption()[i] + ",";
                toPrint += data.getHeatBaseline()[i] + ",";
                toPrint += data.getCo2baseline()[i] + ",";
                toPrint += data.getRelocateWorkload()[i] + ",";
                toPrint += data.getHostWorkload()[i] + ",";
                for (int j = 0; j < data.getSize(); j++) {
                    toPrint += data.getSchedulingMatrix()[i * data.getSize() + j] + ",";
                }
                writer.println(toPrint);
            }

            String toPrint = "";
            writer.println("MAX_T, " + data.getTimePeriod()[0] + ",");
            writer.println("f_ESD, " + data.getFactor()[0] + ",");
//            writer.println("Time_Half_Hour, " + data.getTimeHalfHour()[0] + ",");
//            writer.println("Time_Hour, " + data.getTimeHour()[0] + ",");
            writer.println("ESD_MAX, " + data.getBatteryMaxCapacity()[0] + ",");
            writer.println("f_TES, " + data.getFactorTes()[0] + ",");
            writer.println("TES_MAX, " + data.getTesMaxCapacity()[0] + ",");
            writer.println("TES_R, " + data.getTesChargeLossRate()[0] + ",");
            writer.println("TES_D, " + data.getTesDischargeLossRate()[0] + ",");
            writer.println("ESD_R, " + data.getBatteryChargeLossRate()[0] + ",");
            writer.println("ESD_D, " + data.getBatteryDischargeLossRate()[0] + ",");
            writer.println("MAX_DIS_TES, " + data.getTesMaxDischargeRate()[0] + ",");
            writer.println("MAX_DIS_ESD, " + data.getBatteryMaxDischargeRate()[0] + ",");
            writer.println("DoD, " + data.getDod()[0] + ",");
            writer.println("MAX_IT, " + data.getItMaxConsumption()[0] + ",");
       //     writer.println("MAX_T_TES, " + data.getTimePeriodIntraday()[0] + ",");
//            writer.println("MAX_DC, " + data.getMaxDatacenter()[0] + ",");
            writer.println("COP_C, " + data.getCopC()[0] + ",");
            writer.println("COP_H, " + data.getCopH()[0] + ",");
            writer.println("MAX_HOST, " + data.getMaxHost()[0] + ",");
            writer.println("MAX_Relocate, " + data.getMaxRealloc()[0] + ",");
            writer.println("relocateActive, " + data.getRelocateActive()[0] + ",");
            writer.println("TES_INIT, " + data.getTesInitialValue()[0] + ",");
            writer.println("ESD_INIT, " + data.getBatteryInitialValue()[0] + ",");
            writer.println("ESD_FIN, " + data.getBatteryFinalConstraint()[0] + ",");
            writer.println("TES_FIN, " + data.getTesFinalConstraint()[0] + ",");
            writer.println("WE, " + data.getWE()[0] + ",");
            writer.println("WF, " + data.getWF()[0] + ",");
            writer.println("WT, " + data.getWT()[0] + ",");
            writer.println("WL, " + data.getWL()[0] + ",");
            writer.println("WREN, " + data.getWREN()[0] + ",");
            writer.println("E_D_Percentage, " + data.getEdPercentage()[0]);
            writer.println(toPrint);


            writer.println();
            writer.close();

        } catch (FileNotFoundException e) {
            LOGGER.error("", e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("", e);
        }
    }

    public static void writePlan(ActionPlan actionPlan, String fileName) {
        PrintWriter writer;
        try {
            writer = new PrintWriter(fileName, "UTF-8");
            Map<Interval, List<EnergyEfficiencyOptimizationAction>> actions = actionPlan.getActionPool();
            Map<Interval, List<EnergyEfficiencyOptimizationAction>> actionsC = actionPlan.getCorrelatedActionPool();
            Iterator<Interval> it = actions.keySet().iterator();
            Iterator<Interval> it2 = actionsC.keySet().iterator();
            while(it.hasNext()){
                Interval time = it.next();
                List<EnergyEfficiencyOptimizationAction> act = actions.get(time);
                String line = time.getStart() + ",";
                writer.println(line);
                line = "";
                for(EnergyEfficiencyOptimizationAction e : act){
                    line += e.getClass().getSimpleName() + ",";
                }
                writer.println(line);
            }
            while(it2.hasNext()){
                Interval time = it2.next();
                List<EnergyEfficiencyOptimizationAction> actC = actionsC.get(time);
                String line = time.getStart() + ",";
                writer.println(line);
                line = "";
                for(EnergyEfficiencyOptimizationAction e : actC){
                    line += e.getClass().getSimpleName() + ",";
                }
                writer.println(line);
            }
            writer.println();
            writer.close();
        }catch (FileNotFoundException e) {
            LOGGER.error("", e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("", e);
        }
    }
}
