package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.OptimizationDataCatalyst;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.constraints.*;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.fitness.FitnessFunctionGenetic;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.fitness.IFitnessFunction;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.utility.RandomGenerator;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelValuesIO;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MetaheuristicAlgorithm {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetaheuristicAlgorithm.class);

    private List<Solution> population = new ArrayList<>();
    private List<IOptimizationConstraint> constraints = new ArrayList<>();

    private IFitnessFunction fitnessFunction;
    private OptimizationDataCatalyst data;
    private CommonValues commonValues;

    private static boolean enableWorkloadShift = true;
    public static final boolean ENABLE_TES = false;
    public static final boolean ENABLE_ESD = true;
    private static boolean enableWorkloadRelocate = false;
    private static boolean enableREN = true;

    public static final double ALLOWED_VALUE = 0.01;

    public static boolean isPVProduction(){return enableREN;}

    public static boolean isWorkloadShiftEnabled() {
        return enableWorkloadShift;
    }

    public static boolean isWorkloadRelocateEnabled() {
        return enableWorkloadRelocate;
    }

    public static void intradaySetup() {
        enableWorkloadShift = false;
        enableWorkloadRelocate = false;
    }

    public MetaheuristicAlgorithm(OptimizationDataCatalyst data) {

//        data.setMaxHost(new double[]{300.0});
//        data.setTesLevel(new double[]{100.0});
//        data.setTesMaxCapacity(new double[]{100.0});
//        data.setTesMaxDischargeRate(new double[]{100.0});
//        data.setTesChargeLossRate(new double[]{1.0});
//        data.setTesDischargeLossRate(new double[]{1.0});
//        data.setTesInitialValue(new double[]{100.0});
//        data.setTesFinalConstraint(new double[]{100.0});
//        data.setBatteryInitialValue(new double[]{300.0});

        if (data.getSize() == 8) MetaheuristicAlgorithm.intradaySetup();
        if (data.getRelocateActive()[0] > 0.0) {
            enableWorkloadRelocate = true;
        } else {
            enableWorkloadRelocate = false;
        }

        commonValues = new CommonValues(data);
        this.data = data;
        fitnessFunction = new FitnessFunctionGenetic(data);

        // BatteryConstraint bc = new BatteryConstraint(data.getDod()[0], data.getBatteryMaxCapacity()[0],
        //       data.getBatteryMaxDischargeRate()[0], data.getBatteryMaxDischargeRate()[0], data.getBatteryFinalConstraint()[0]);
        WorkloadConstraint wc = new WorkloadConstraint(data.getItMaxConsumption()[0]);
        // TesConstraint tc = new TesConstraint(data.getTesMaxCapacity()[0], data.getTesMaxDischargeRate()[0],
        ///          data.getTesMaxDischargeRate()[0], data.getTesFinalConstraint()[0]);
        //TesBatteryConstraint tbc = new TesBatteryConstraint();
        TransactionConstraint trc = new TransactionConstraint();
        if (isWorkloadShiftEnabled()) {
            this.constraints.add(wc);
        }
//        if (ENABLE_ESD) {
//            this.constraints.add(bc);
//        }
//        if (ENABLE_TES) {
//            this.constraints.add(tc);
//        }
//        if (ENABLE_ESD && ENABLE_TES) {
//          //  this.constraints.add(tbc);
//            this.constraints.add(trc);
//        }
    }

    private void initializePopulation() {
        LOGGER.info("Initializing population");
        for (int i = 0; i < GeneticAlgorithmParameters.NUMBER_OF_CROMOZOMES; i++) {
            population.add(generateIndividual(data.getSize()));
        }
    }

    private void rankPopulation() {
        for (Solution s : population) {
            s.rank();
        }

        Collections.sort(population);
    }

    private void checkConstraints() {
        for (Solution solution : population) {
            for (IOptimizationConstraint constraint : constraints) {
                constraint.verify(solution);
            }
        }
    }

    private void evaluatePopulation() {
        for (Solution solution : population) {
            fitnessFunction.compute(solution);
        }
    }

    private void computeNewGeneration() {
        List<Solution> newPopulation = new ArrayList<>();
        for (int i = 0; i < population.size() / 4; i++) {
            Solution a = population.get(i);
            newPopulation.add(a);
        }
        for (int i = 0; i < population.size() / 2; i = i + 2) {
            Solution a = population.get(i).newInstance();
            Solution b = population.get(i + 1).newInstance();
            a.crossover(b);
            if (probabilityMutation())
                a.mutate();
            if (probabilityMutation())
                b.mutate();
            newPopulation.add(a);
            newPopulation.add(b);
        }
        for (int i = newPopulation.size(); i < population.size(); i++) {
            newPopulation.add(generateIndividual(data.getSize()));
        }
        population.clear();
        population.addAll(newPopulation);
    }

    private boolean probabilityMutation() {
        return RandomGenerator.random(0.0, 1.0) < GeneticAlgorithmParameters.MUTATION_PROBABILITY;
    }


    private Solution generateIndividual(int size) {
        Solution s = new EnergyEfficiencySolution(size, commonValues);
        s.initialize();
        return s;
    }

    OptimizationDataCatalyst run() {
        initializePopulation();
        EnergyEfficiencySolution best = (EnergyEfficiencySolution) population.get(0);

        try (PrintWriter writer = new PrintWriter("Chart-P=" + GeneticAlgorithmParameters.NUMBER_OF_CROMOZOMES + "-It="
                + GeneticAlgorithmParameters.NUMBER_OF_ITERATIONS + ".csv", "UTF-8")) {
            int repetitions = 0;

            while (repetitions < GeneticAlgorithmParameters.NUMBER_OF_ITERATIONS) {
                LOGGER.info(String.format("Generation number: %s", repetitions));

                checkConstraints();
                evaluatePopulation();
                rankPopulation();

                best = (EnergyEfficiencySolution) population.get(0);

                LOGGER.info("Best individual fitness: " + best.getFitnessValue() + "; violated constraints: " + best.getViolations());

                writer.println(repetitions + "," + best.getViolations() + "," + best.getFitnessValue());
                computeNewGeneration();
                repetitions++;
            }
        } catch (FileNotFoundException e) {
            LOGGER.info(String.format("Could not open file %s", e.getMessage()));
            return null;
        } catch (UnsupportedEncodingException e) {
            LOGGER.info(String.format("Unsupported encoding %s", e.getMessage()));
            return null;
        }

        //TO DO - fill in the outputs from the solution
        data.setBatteryLevel(best.getEsd());
        data.setTesLevel(best.getTes());

        double[][] auy = new double[data.getSize()][data.getSize()];
        double[] matrix = new double[data.getSize() * data.getSize()];
        for (int i = 0; i < data.getSize(); i++) {
            for (int j = 0; j < data.getSize(); j++) {
                matrix[i * data.getSize() + j] = 0;
            }
        }

        if (MetaheuristicAlgorithm.isWorkloadShiftEnabled()) {
            for (int i = 0; i < data.getSize(); i++) {
                System.arraycopy(best.getY()[i], 0, auy[i], 0, data.getSize());
                for (int j = 0; j < data.getSize(); ++j) {
                    matrix[i * data.getSize() + j] = auy[i][j];
                }
            }
        } else {
            for (int i = 0; i < data.getSize(); i++) {
                //auy[i] = i;
                matrix[i * data.getSize() + i] = 1;
            }
        }
      /*  for (int i = 0; i < data.getSize(); i++) {
            System.arraycopy(best.getyEDE()[i], 0, auy[i], 0, data.getSize());
            for (int j = 0; j < data.getSize(); ++j) {
                matrix[i * data.getSize() + j] = auy[i][j];
            }
        }*/
        data.setSchedulingMatrix(matrix);
        data.setEstimatedDelayExecution(best.geteDExecuted());
        data.setFinalCooling(best.getTotalCooling());

        fillBattery(best);
        fillTes(best);

        double[] host = new double[data.getSize()];
        double[] relocate = new double[data.getSize()];

        for (int i = 0; i < data.getSize(); i++) {
            double val = best.getItRelocateActions()[i];
            if (val < 0) {
                relocate[i] = (-1) * val;
                host[i] = 0;
            } else {
                relocate[i] = 0;
                host[i] = val;
            }

        }


        double[] heatBaseline = new double[data.getSize()];
        double[] co2baseline = new double[data.getSize()];
        double[] co2adapted = new double[data.getSize()];
        double[] itLoad = new double[data.getSize()];

        for (int i = 0; i < data.getSize(); i++) {
            itLoad[i] = data.getRealtimeEnergy()[i] + data.getEstimatedDelayExecution()[i];
            heatBaseline[i] = (data.getCopH()[0] - 1) / data.getCopC()[0] * (itLoad[i]);
            co2baseline[i] = (itLoad[i] + itLoad[i] / data.getCopC()[0]) * 0.5;
            co2adapted[i] = best.getEDC()[i] * 0.5;
        }

        data.setHeatBaseline(heatBaseline);
        data.setCo2baseline(co2baseline);
        data.setCo2adapted(co2adapted);
        data.setItLoad(itLoad);

        data.setItCooling(best.getItCooling());
        data.setHostWorkload(host);
        data.setRelocateWorkload(relocate);
        //data.setHostWorkload(best.getItHostActions());
        //data.setRelocateWorkload(best.getItRelocateActions());
        data.setDcFinalConsumption(best.getEDC());
        data.setDcThermalGeneration(best.gettDc());
        data.setEnergyCost(new double[]{best.getEnergyCost()});
        data.setThermalProfit(new double[]{best.getThermalProfit()});
        data.setFlexibilityPenalty(new double[]{best.getFlexibilityPenalty()});
        data.setLoadProfit(new double[]{best.getLoadProfit()});
        data.setTotalCost(new double[]{best.getFitnessValue()});


        ExcelValuesIO.writeDataToCSVFile(data, "data_after_HYBRID_" + new Date().getTime() + ".csv");

        return data;
    }

    private void fillBattery(EnergyEfficiencySolution best) {
        double[] dESD = new double[data.getSize()];
        double[] rESD = new double[data.getSize()];
        for (int i = 0; i < data.getSize(); i++) {
            if (best.getEsdActions()[i] < 0) {
                dESD[i] = -best.getEsdActions()[i];
            } else {
                rESD[i] = best.getEsdActions()[i];
            }
        }
        data.setChargeBattery(rESD);
        data.setDischargeBattery(dESD);
    }

    private void fillTes(EnergyEfficiencySolution best) {
        double[] dTES = new double[data.getSize()];
        double[] rTES = new double[data.getSize()];
        for (int i = 0; i < data.getSize(); i++) {
            if (best.getTesActions()[i] < 0) {
                dTES[i] = -best.getTesActions()[i];
            } else {
                rTES[i] = best.getTesActions()[i];
            }
        }
        data.setChargeTes(rTES);
        data.setDischargeTes(dTES);
    }
}

