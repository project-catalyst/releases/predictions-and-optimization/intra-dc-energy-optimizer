package ro.tuc.dsrl.catalyst.optimizer.market;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.optimizer.market.dtos.*;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTimeframes;
import ro.tuc.dsrl.catalyst.optimizer.market.enums.MarketTypes;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.MarketplaceMpcmURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;

import java.util.*;



public class MarketAPIHandler extends MarketAPIHandlerInterface {

    private static final Logger LOGGER = LoggerFactory.getLogger(MarketAPIHandler.class);


    @Override
    public List<Double> getClearingPrices(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate) {
        Map<String, Object> urlVariables = getParametersMap(marketType, timeframe);
        ClearingPriceDTO[] clearingPriceDTOS = RestAPIClient.getObject(MarketplaceMpcmURL.CLEARING_PRICE_PREVIOUS_DAY, ClearingPriceDTO[].class, urlVariables);

        return getValues(clearingPriceDTOS,timeframe);
    }

    @Override
    public List<Double> getRefrencePrices(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate) {
        Map<String, Object> urlVariables = getParametersMap(marketType, timeframe);
        ReferencePriceDTO[] referencePrices = RestAPIClient.getObject(MarketplaceMpcmURL.REFERENCE_PRICE_PREVIOUS_DAY, ReferencePriceDTO[].class, urlVariables);

        return  getValues(referencePrices, timeframe);
    }

    @Override
    public List<ActiveSessionDTO> getActiveSessions(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate) {
        Map<String, Object> urlVariables = getParametersMap(marketType, timeframe);
        ActiveSessionDTO[] activeSessions = RestAPIClient.getObject(MarketplaceMpcmURL.ACTIVE_SESSIONS, ActiveSessionDTO[].class, urlVariables);

        if(activeSessions == null){
            LOGGER.error("The MarketConnector > getActiveSessions responded with null.");
            return new ArrayList<>();
        }
        return new ArrayList<>(Arrays.asList(activeSessions));
    }

    @Override
    public List<MarketActionDTO> getMarketActions(MarketTypes marketType, MarketTimeframes timeframe, DateTime startDate) {
        Map<String, Object> urlVariables = getParametersMap(marketType, timeframe);
        MarketActionDTO[] marketActions = RestAPIClient.getObject(MarketplaceMpcmURL.MARKET_ACTIONS, MarketActionDTO[].class, urlVariables);

        if(marketActions == null){
            LOGGER.error("The MarketConnector > getMarketActions responded with null.");
            return new ArrayList<>();
        }
        return new ArrayList<>(Arrays.asList(marketActions));
    }

    @Override
    public List<MarketActionDTO> registerActions(List<MarketActionDTO> actions, MarketTypes marketType, MarketTimeframes timeframe) {
        Map<String, Object> urlVariables = getParametersMap(marketType, timeframe);
        ObjectMapper objectMapper = new ObjectMapper();
        String s = null;
        try {
            s = objectMapper.writeValueAsString(actions);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        LOGGER.debug(s);

//        String marketActionsResultArrayString = RestAPIClient.postObject(MarketplaceMpcmURL.MARKET_ACTIONS, actions, String.class, urlVariables);
//        LOGGER.debug(marketActionsResultArrayString);
//        return new ArrayList<>();
        MarketActionDTO[] marketActionsResultArray = RestAPIClient.postObject(MarketplaceMpcmURL.MARKET_ACTIONS, actions, MarketActionDTO[].class, urlVariables);
        if(marketActionsResultArray == null){
            LOGGER.error("The MarketConnector > registerActions responded with null.");
            //return actions;
            return new ArrayList<>();
        }
        return new ArrayList<>(Arrays.asList(marketActionsResultArray));

    }

    @Override
    public MarketCorrelationDTO registerCorrelatedActions(MarketCorrelationDTO marketCorrelation, MarketTimeframes timeframe) {
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("marketActorName", MarketManager.MARKET_ACTOR_NAME);
        urlVariables.put("timeframe", timeframe.getValue());
        ObjectMapper objectMapper = new ObjectMapper();
        String s = null;
        try {
            s = objectMapper.writeValueAsString(marketCorrelation);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        LOGGER.debug(s);
        return RestAPIClient.postObject(MarketplaceMpcmURL.CORRELATED_MARKET_ACTIONS, marketCorrelation, MarketCorrelationDTO.class, urlVariables);
    }


    private Map<String, Object> getParametersMap(MarketTypes marketType, MarketTimeframes timeframe) {
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("marketActorName", MarketManager.MARKET_ACTOR_NAME);
        urlVariables.put("marketplaceForm", marketType.getValue());
        urlVariables.put("timeframe", timeframe.getValue());
        return urlVariables;
    }

    private List<Double> getValues(ReferencePriceDTO[] refPrices,  MarketTimeframes timeframe) {

        HashMap<Integer, Double> refPricesAtEachHour = new HashMap<>();
        for (int i = 0; i < timeframe.getTimeslots(); i++) {
            refPricesAtEachHour.put(i, 0.0);
        }

        for (ReferencePriceDTO rPrice : refPrices) {
            int rPriceHourOfDay = rPrice.getValidityStartTime().getHourOfDay();
            refPricesAtEachHour.put(rPriceHourOfDay, rPrice.getReferencePrice());
        }

        return new ArrayList<>(refPricesAtEachHour.values());
    }

    private List<Double> getValues(ClearingPriceDTO[] clPrices,  MarketTimeframes timeframe) {

        HashMap<Integer, Double> clearingPricesAtEachHour = new HashMap<>();
        for (int i = 0; i < timeframe.getTimeslots(); i++) {
            clearingPricesAtEachHour.put(i, 0.0);
        }

        for (ClearingPriceDTO cPrice : clPrices) {
            int cPriceHourOfDay = cPrice.getValidityStartTime().getHourOfDay();
            clearingPricesAtEachHour.put(cPriceHourOfDay, cPrice.getClearingPrice());
        }

        return new ArrayList<>(clearingPricesAtEachHour.values());
    }

}
