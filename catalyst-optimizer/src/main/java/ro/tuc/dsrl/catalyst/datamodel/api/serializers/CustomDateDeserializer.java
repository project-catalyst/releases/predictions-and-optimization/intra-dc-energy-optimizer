//package ro.tuc.dsrl.catalyst.datamodel.api.serializers;
//
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.JsonDeserializer;
//
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.TimeZone;
//
///**
// *
// * @author Terpsi Velivassaki <tvelivassaki@ep.singularlogic.eu>
// */
//public class CustomDateDeserializer extends JsonDeserializer<Date>{
//
//	@Override
//	public Date deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
//		TimeZone tz = TimeZone.getTimeZone("UTC");
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//		df.setTimeZone(tz);
//		String date = jp.getText();
//		date = date.substring(0, 22) + date.substring(23);
//		try {
//			return df.parse(date);
//		} catch (ParseException e) {
//            throw new RuntimeException(e);
//		}
//	}
//}
