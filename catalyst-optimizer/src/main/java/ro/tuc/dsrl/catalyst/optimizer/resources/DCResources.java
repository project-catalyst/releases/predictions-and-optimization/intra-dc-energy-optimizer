package ro.tuc.dsrl.catalyst.optimizer.resources;

import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Jan 30, 2015
 * @Description:
 *
 */
public class DCResources {

	private BatteryResources batteryResource;
	private TesResources tesResource;
	private ServerResources serverResources;
	private CoolingResources coolingResources;
	private DataCentre dataCentre;
	private GeneratorResources generatorResources;
	private WindResources windResources;


	public DCResources(Date date) {
		this.batteryResource = new BatteryResources();
		this.tesResource = new TesResources();
		this.coolingResources = new CoolingResources();
		this.serverResources = new ServerResources();
		this.dataCentre = new DataCentre();
//

		refresh(date);
	}


	public void refresh(Date date) {
		this.batteryResource.setBatteries(ResourceHandler.getAllBatteries(date));
		this.tesResource.setThermalStorages(ResourceHandler.getAllThermalStorages(date));
		this.serverResources.setServerRooms(ResourceHandler.getAllServerRooms(date));
		this.coolingResources.setCoolingSystems(ResourceHandler.getAllCoolingSystems(date));
		this.serverResources.setCurrentEnergyConsumption(coolingResources.getCurrentEnergyConsumption() + serverResources.getCurrentItEnergyConsumptionKW());
		this.dataCentre = ResourceHandler.getAllDataCenters(date);
	}

	/**
	 * @return the batteryResource
	 */
	public BatteryResources getBatteryResource() {
		return batteryResource;
	}

	/**
	 * @return the tesResource
	 */
	public TesResources getTesResource() {
		return tesResource;
	}

	/**
	 * @return
	 */
	public ServerResources getServerResources() {
		return serverResources;
	}

	/**
	 * @return
	 */
	public GeneratorResources getGeneratorResources() {
		return generatorResources;
	}

	/**
	 * @return the coolingResources
	 */
	public CoolingResources getCoolingResources() {
		return coolingResources;
	}

	/**
	 * @return the windResources
	 */
	public WindResources getWindResources() {
		return windResources;
	}


	public DataCentre getDataCentre() {
		return dataCentre;
	}
}
