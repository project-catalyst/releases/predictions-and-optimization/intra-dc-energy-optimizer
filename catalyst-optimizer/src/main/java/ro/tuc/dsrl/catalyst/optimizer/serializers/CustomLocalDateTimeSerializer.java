package ro.tuc.dsrl.catalyst.optimizer.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;

public class CustomLocalDateTimeSerializer extends JsonSerializer<DateTime> {

    @Override
    public void serialize(DateTime time,
                          JsonGenerator jg,
                          SerializerProvider sp) throws IOException {

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String value = formatter.print(time);
        jg.writeString(value);
    }

}

