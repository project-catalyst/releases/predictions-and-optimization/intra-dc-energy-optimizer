package ro.tuc.dsrl.catalyst.optimizer.remoteconnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelIO;

import java.util.Map;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 * Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 */
public final class RestAPIClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestAPIClient.class);

    private RestAPIClient() {
    }

    public static <T> T postObject(APIURL url, Object o, Class<T> clazz) {
     //   prettyPrint("POST", url, null);
        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());
        return rt.postForObject(url.value(), o, clazz);
    }

    public static <T> T postObject(APIURL url, Object o, Class<T> clazz, Map<String, ?> urlVariables) {
     //   prettyPrint("POST", url, urlVariables);
        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());
        return rt.postForObject(url.value(), o, clazz, urlVariables);
    }

    public static <T> T getObject(APIURL url, Class<T> responseType, Map<String, ?> urlVariables) {
     //   prettyPrint("GET", url, urlVariables);
        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());
        return rt.getForObject(url.value(), responseType, urlVariables);
    }

    public static <T> T getObject(APIURL url, Class<T> responseType) {
      //  prettyPrint("GET", url, null);
        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());
        return rt.getForObject(url.value(), responseType);
    }


    private static void prettyPrint(String method, APIURL url, Map<String, ?> urlVariables) {
        LOGGER.info(method + " " + url.value());
        if (urlVariables != null) {
            for (Map.Entry<String, ?> entry : urlVariables.entrySet()) {
                LOGGER.info(entry.getKey() + " " + entry.getValue());
            }
        }

    }

}
