package ro.tuc.dsrl.catalyst.optimizer.market.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;



public class MarketCorrelationDTO {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private int id;
    private String constraintType;
    private List<CorrelatedActionDTO> marketActions;

    public MarketCorrelationDTO() {
    }

    public MarketCorrelationDTO(int id, String constraintType) {
        this.id = id;
        this.constraintType = constraintType;
        this.marketActions = new ArrayList<>();
    }

    public void addCorrelatedAction(CorrelatedActionDTO correlatedAction) {
        this.marketActions.add(correlatedAction);
    }

   // @JsonIgnore
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConstraintType() {
        return constraintType;
    }

    public void setConstraintType(String constraintType) {
        this.constraintType = constraintType;
    }

    public List<CorrelatedActionDTO> getMarketActions() {
        return marketActions;
    }

    public void setMarketActions(List<CorrelatedActionDTO> marketActions) {
        this.marketActions = marketActions;
    }
}
