package ro.tuc.dsrl.catalyst.optimizer.resources;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ResourcesT implements ResourcesInterface {

    private static Map<Interval, Map<Interval, Double>> dtwMonitored;

    private static List<Battery> batteries;
    private static List<ThermalEnergyStorage> thermalStorages;
    private static double generatorsMaxCapacity;
    private static double serverMaxCapacity;
    private static double itCurrentEnergyConsumption;
    private static double coolingCurrentEnergyConsumption;
    private static double windCurrentEnergyProduction;

    public static void setDTWMonitored(Map<Interval, Map<Interval, Double>> dtwMonitored) {
        ResourcesT.dtwMonitored = dtwMonitored;
    }

    public static void setBatteries(List<Battery> batteries) {
        ResourcesT.batteries = batteries;
    }

    public static void setThermalStorages(List<ThermalEnergyStorage> thermalStorages) {
        ResourcesT.thermalStorages = thermalStorages;
    }

    public static void setGeneratorsMaxCapacity(double generatorsMaxCapacity) {
        ResourcesT.generatorsMaxCapacity = generatorsMaxCapacity;
    }

    public static void setServersMaxCapacity(double serverMaxCapacity) {
        ResourcesT.serverMaxCapacity = serverMaxCapacity;
    }

    public static void setItCurrentEnergyConsumption(double itCurrentEnergyConsumption) {
        ResourcesT.itCurrentEnergyConsumption = itCurrentEnergyConsumption;
    }

    public static void setCoolingCurrentEnergyConsumption(double coolingCurrentEnergyConsumption) {
        ResourcesT.coolingCurrentEnergyConsumption = coolingCurrentEnergyConsumption;
    }

    public static void setWindCurrentEnergyProduction(double windCurrentEnergyProduction) {
        ResourcesT.windCurrentEnergyProduction = windCurrentEnergyProduction;
    }

    @Override
    public Double getDTWMonitoredConsumtion(Interval arrivalTime, Interval recordTime, double divide) {
        Map<Interval, Double> arrivalMap = dtwMonitored.get(arrivalTime);
        if (arrivalMap == null) {
            return 0.0;
        }
        return arrivalMap.get(recordTime);
    }

    @Override
    public List<Battery> getAllBatteries(Date date) {
        return batteries;
    }

    @Override
    public List<ThermalEnergyStorage> getAllThermalStorages(Date date) {
        return thermalStorages;
    }

    @Override
    public List<ServerRoom> getAllServerRooms(Date date) {
        return null;
    }

    @Override
    public List<CoolingSystem> getAllCoolingSystems(Date date) {
        return null;
    }

    @Override
    public DataCentre getDatacentre(Date date) {
        return null;
    }

//    @Override
//    public double getItMaxEnergyConsumption() {
//        return serverMaxCapacity;
//    }
//
//    @Override
//    public double getItCurrentEnergyConsumption() {
//        return itCurrentEnergyConsumption;
//    }
//
//    @Override
//    public double getCoolingCurrentEnergyConsumption() {
//        return coolingCurrentEnergyConsumption;
//    }
//
//    @Override
//    public double getGeneratorsCurrentEnergyProduction() {
//        return 0;
//    }
//
//    @Override
//    public double getWindCurrentEnergyProduction() {
//        return windCurrentEnergyProduction;
//    }
//
//    @Override
//    public double getGeneratorsMaxCapacity() {
//        return generatorsMaxCapacity;
//    }

    public static void initializeBatteries() {
        batteries = new ArrayList<Battery>();

        Battery battery = new Battery();
        battery.setActualLoadedCapacity(100);
        battery.setMaximumCapacity(500);
        battery.setDischargeLossRate(1);
        battery.setChargeLossRate(1);
        battery.setMaxChargeRate(500);
        battery.setMaxDischargeRate(500);
        batteries.add(battery);

        battery = new Battery();
        battery.setActualLoadedCapacity(0);
        battery.setMaximumCapacity(500);
        battery.setDischargeLossRate(1);
        battery.setChargeLossRate(1);
        battery.setMaxChargeRate(500);
        battery.setMaxDischargeRate(500);
        batteries.add(battery);

        battery = new Battery();
        battery.setActualLoadedCapacity(200);
        battery.setMaximumCapacity(500);
        battery.setDischargeLossRate(1);
        battery.setChargeLossRate(1);
        battery.setMaxChargeRate(500);
        battery.setMaxDischargeRate(500);
        batteries.add(battery);

        battery = new Battery();
        battery.setActualLoadedCapacity(500);
        battery.setMaximumCapacity(500);
        battery.setDischargeLossRate(1);
        battery.setChargeLossRate(1);
        battery.setMaxChargeRate(500);
        battery.setMaxDischargeRate(500);
        batteries.add(battery);
    }

    public static void initializeThermalStorages() {
        thermalStorages = new ArrayList<ThermalEnergyStorage>();
        ThermalEnergyStorage tes = new ThermalEnergyStorage();
        tes.setActualLoadedCapacity(0);
        tes.setMaximumCapacity(750);
        tes.setDischargeLossRate(1);
        tes.setChargeLossRate(1);
        tes.setMaxChargeRate(250);
        tes.setMaxDischargeRate(250);
        thermalStorages.add(tes);

        tes = new ThermalEnergyStorage();
        tes.setActualLoadedCapacity(0);
        tes.setMaximumCapacity(750);
        tes.setDischargeLossRate(1);
        tes.setChargeLossRate(1);
        tes.setMaxChargeRate(250);
        tes.setMaxDischargeRate(250);
        thermalStorages.add(tes);

        tes = new ThermalEnergyStorage();
        tes.setActualLoadedCapacity(0);
        tes.setMaximumCapacity(750);
        tes.setDischargeLossRate(1);
        tes.setChargeLossRate(1);
        tes.setMaxChargeRate(250);
        tes.setMaxDischargeRate(250);
        thermalStorages.add(tes);

        tes = new ThermalEnergyStorage();
        tes.setActualLoadedCapacity(0);
        tes.setMaximumCapacity(750);
        tes.setDischargeLossRate(1);
        tes.setChargeLossRate(1);
        tes.setMaxChargeRate(250);
        tes.setMaxDischargeRate(250);
        thermalStorages.add(tes);
    }

//    @Override
//    public TotalPowerREST getCurrentState(Date date) {
//        initializeBatteries();
//        initializeThermalStorages();
//        TotalPowerREST tp =  new TotalPowerREST();
//        tp.setBrownGeneration(0.0);
//        tp.setGreenGeneration(windCurrentEnergyProduction);
//        tp.setFacilityPowerConsumption(coolingCurrentEnergyConsumption);
//        tp.setItPowerConsumptionDt(0.0);
//        tp.setItPowerConsumptionRt(itCurrentEnergyConsumption);
//        return tp;
//    }
//
//    @Override
//    public void deleteRedundantActions(DateTime startTime, double d) {
//
//    }

}
