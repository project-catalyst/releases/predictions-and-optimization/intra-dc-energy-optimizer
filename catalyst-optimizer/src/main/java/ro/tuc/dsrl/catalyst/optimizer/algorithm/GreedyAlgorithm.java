package ro.tuc.dsrl.catalyst.optimizer.algorithm;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.MetaheuristicAlgorithm;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelValuesIO;

import java.util.Date;

public class GreedyAlgorithm implements OptimizationAlgorithm {

    private OptimizationDataCatalyst data;

    public GreedyAlgorithm(OptimizationDataCatalyst data){
        this.data = data;
    }

    @Override
    public OptimizationDataCatalyst getOptimizationData() {
        return data;
    }

    private int getMaxIndex(double[] arr, int startIndex, int size){
        int ret = startIndex;
        for(int i = startIndex; i< size; i++){
            if(arr[i]>arr[ret]){
                ret = i;
            }
        }
        return ret;
    }

    private int getMinIndex(double[] arr, int startIndex, int size){
        int ret = startIndex;
        for(int i = startIndex; i< size; i++){
            if(arr[i]<arr[ret]){
                ret = i;
            }
        }
        return ret;
    }

    private double minimum(double a, double b){
        if(a>b) return b;
        else return a;
    }

    double[] computeEDE(double[] E_D, double[][]Y, int size){
        double[] E_DE = new double[size];
        for (int j = 0; j < size; ++j) {
            if (MetaheuristicAlgorithm.isWorkloadShiftEnabled()) {
                E_DE[j] = 0;
                for(int i = 0; i < size; ++i)
                    E_DE[j] += Y[i][j] * data.getDelayTolerableEnergy()[j];
            } else {
                E_DE[j] = data.getDelayTolerableEnergy()[j];
            }
        }
        /*
        for(int i = 0; i< size; i++){
            E_DE[i] = 0;
            for(int j = 0; j< size; j++){
                E_DE[i] += Y[i][j] * E_D[j];
            }
        }*/
        return E_DE;
    }

    double[][] initY(int size){
        double[][]Y = new double[size][size];
        for(int i = 0; i< size; i++)
            for(int j = 0; j< size; j++)
                Y[i][j] = 0;
        for(int i = 0; i< size; i++)
            Y[i][i] = 1.0;
        return Y;
    }

    double[] computeDifference(double[] DR, double[] E_R, double[] E_DE, double COP, int size){
        double[] ret = new double[size];
        for(int i = 0; i< size; i++) {
            ret[i] =  DR[i] - ((E_R[i] + E_DE[i])*(1+1.0/COP)) ;
        }
        return ret;
    }

    double[] computeDCFinal(double[] E_R, double[] E_DE, double COP, int size){
        double[] ret = new double[size];
        for(int i = 0; i< size; i++) {
            ret[i] =  ((E_R[i] + E_DE[i])*(1+1.0/COP)) ;
        }
        return ret;
    }

    double[] computeCooling(double[] E_R, double[] E_DE, double COP, int size){
        double[] ret = new double[size];
        for(int i = 0; i< size; i++) {
            ret[i] =  ((E_R[i] + E_DE[i])*(1.0/COP)) ;
        }
        return ret;
    }

    private void printArray(double[] a, int size){
        for(int i = 0; i< size; i++) {
            System.out.print(a[i]+",");
        }
        System.out.println("");
    }

    double[] initArray(double value, int size){
        double[] ret = new double[size];
        for(int i = 0; i< size; i++) {
            ret[i] =  value;
        }
        return ret;
    }

    @Override
    public void compute(ServiceType serviceType) {
        System.out.println("Greedy algorithm");
        int size = data.getSize();
        double[] E_R = data.getRealtimeEnergy();
        printArray(E_R, size);
        double[] E_D = data.getDelayTolerableEnergy();
        printArray(E_D, size);
        double[] baseline = data.getBaseline();
        double[] DR =  data.getDrSignal();
        double COP = data.getCopC()[0];
        double[][] Y = initY(size);
        double[] E_DE = new double[size];
        double[] diff = computeDifference(DR, E_R, E_D, COP, size);
        System.out.println("COP = "+COP);
        System.out.println("Size = "+size);
        printArray(DR, size);
        printArray(diff, size);
        int loops = 0;
        if(size>8) loops = 1;
        for(int loop = 0; loop < loops; loop++) {
            E_DE = computeEDE(E_D, Y, size);
            for (int i = 0; i < size - 1; i++) {
                if (diff[i] < 0) { //DR < DC - move workload
                    int moveTo = getMaxIndex(diff, i, size);
                    if (diff[moveTo] > 0) { //DR > DC - need workload
                        System.out.println("Move workload from "+i+" to "+moveTo);
                        //we can move here workload
                        double quantity = minimum(-diff[i], diff[moveTo]);
                        quantity = minimum(E_DE[i], quantity);
                        quantity = minimum(E_DE[moveTo], quantity);
                        double percentage = quantity / E_DE[i];
                        Y[i][i] -= percentage;
                        Y[i][moveTo] += percentage;
                    }
                }
                E_DE = computeEDE(E_D, Y, size);
                diff = computeDifference(DR, E_R, E_DE, COP, size);
                printArray(diff, size);
                printArray(E_D, size);
                printArray(E_DE, size);
            }
        }


        double[] Yarray = new double[size*size];
        for(int i = 0; i< size; i++){
            for(int j = 0; j< size; j++){
                Yarray[i*size+j] = Y[i][j];
            }
        }

        double[] dcFinal = computeDCFinal(E_R,E_DE,COP,size);
        printArray(dcFinal, size);
        data.setDcFinalConsumption(dcFinal);
        data.setSchedulingMatrix(Yarray);
        data.setEstimatedDelayExecution(E_DE);
        data.setFinalCooling(computeCooling(E_R,E_DE,COP,size));
        data.setItCooling(computeCooling(E_R,E_D,COP,size));
        data.setBatteryLevel(initArray(data.getBatteryInitialValue()[0],size));
        data.setTesLevel(initArray(data.getTesInitialValue()[0],size));
        data.setChargeBattery(initArray(0.0, size));
        data.setDischargeBattery(initArray(0.0, size));
        data.setChargeTes(initArray(0.0, size));
        data.setDischargeTes(initArray(0.0, size));
        data.setRelocateWorkload(initArray(0.0, size));
        data.setHostWorkload(initArray(0.0, size));


        double[] heatBaseline= new double[data.getSize()];
        double[] co2baseline= new double[data.getSize()];
        double[] co2adapted= new double[data.getSize()];
        double[] itLoad= new double[data.getSize()];
        double[] thermalGeneration = new double[size];

        for (int i = 0; i < data.getSize(); i++) {
            itLoad[i] = data.getRealtimeEnergy()[i]+data.getDelayTolerableEnergy()[i];
            heatBaseline[i] = (data.getCopH()[0] - 1) / data.getCopC()[0] * (itLoad[i]);
            co2baseline[i] = (itLoad[i] + itLoad[i]/data.getCopC()[0]) * 0.5;
            co2adapted[i] = dcFinal[i] * 0.5;
            thermalGeneration[i] = dcFinal[i] / data.getCopC()[0]*(data.getCopH()[0]-1);
        }
        data.setHeatBaseline(heatBaseline);
        data.setCo2baseline(co2baseline);
        data.setCo2adapted(co2adapted);
        data.setItLoad(itLoad);
        data.setDcThermalGeneration(thermalGeneration);
        data.setEnergyCost(new double[]{0.0});
        data.setThermalProfit(new double[]{0.0});
        data.setFlexibilityPenalty(new double[]{0.0});
        data.setLoadProfit(new double[]{0.0});
        data.setTotalCost(new double[]{0.0});

        ExcelValuesIO.writeDataToCSVFile(data, "greedyResult" + new Date().getTime() +".csv");
    }
}
