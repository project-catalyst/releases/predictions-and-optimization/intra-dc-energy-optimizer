package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.constraints;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.EnergyEfficiencySolution;

public class TransactionConstraint implements IOptimizationConstraint<EnergyEfficiencySolution> {
    @Override
    public void verify(EnergyEfficiencySolution solution) {
        // eliminate cases when we have
        // sell - discharge <=> EDC < 0 and TES_actions < 0 and ESD_actions < 0
        // buy - charge <=> EDC > 0 and TES_actions > 0 and ESD_actions > 0

        int[] tesViolations = solution.getTesViolations();
        int[] esdViolations = solution.getEsdViolations();
        int size = solution.getSize();

        for (int i = 0; i < size; ++i) {
            if (solution.getEDC()[i] >= 0) {
                continue;
            }

            if ((solution.getEsdActions()[i] < 0) && (solution.getTesActions()[i] < 0)) {
                ++tesViolations[i];
                ++esdViolations[i];
            }
            if ((solution.getTesActions()[i] < 0)) {
                ++tesViolations[i];
            }
            if ((solution.getEsdActions()[i] < 0)) {
                ++esdViolations[i];
            }
        }

        solution.setTesViolations(tesViolations);
        solution.setEsdViolations(esdViolations);
    }
}
