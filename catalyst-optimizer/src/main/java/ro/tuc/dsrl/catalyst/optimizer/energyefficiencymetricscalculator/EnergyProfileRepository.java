package ro.tuc.dsrl.catalyst.optimizer.energyefficiencymetricscalculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MetricsDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.CatalystDBApiURL;
import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
import ro.tuc.dsrl.geyser.datamodel.other.MarketPrices;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;


public class EnergyProfileRepository {

    private static final String DC_FIELD = "datacenter-name";
    private static final String STARTDATE_FIELD = "startdate";
    private static final String GRANULARITY = "granularity";


    public EnergyProfileRepository() {

    }

    public ForecastDayAheadDTO getDCBaselineProfile(LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.BASELINE, ForecastDayAheadDTO.class, urlVariables);
    }

    public ForecastDayAheadDTO getServerAdaptedProfile(LocalDateTime time, String dcName, String granularity) {
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.SERVER_ADAPTED, ForecastDayAheadDTO.class, urlVariables);
    }

    public ForecastDayAheadDTO getDCAdaptedProfile( LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.DC_ADAPTED, ForecastDayAheadDTO.class, urlVariables);
    }

    public ForecastDayAheadDTO getHeatAdaptedProfile( LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.THERMAL_ADAPTED, ForecastDayAheadDTO.class, urlVariables);
    }

    public ForecastDayAheadDTO getHeatBaselineProfile( LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.THERMAL_BASELINE, ForecastDayAheadDTO.class, urlVariables);
    }

    public ForecastDayAheadDTO getCoolingAdaptedProfile( LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.COOLING_ADAPTED, ForecastDayAheadDTO.class, urlVariables);
    }

    public ForecastDayAheadDTO getCO2AdaptedProfile( LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.CO2_ADAPTED, ForecastDayAheadDTO.class, urlVariables);
    }

    public ForecastDayAheadDTO getCO2BaselineProfile( LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.CO2_BASELINE, ForecastDayAheadDTO.class, urlVariables);
    }

    public MarketPrices getMarketPrices(LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        urlVariables.put(GRANULARITY, granularity);
        return  RestAPIClient.getObject(CatalystDBApiURL.MARKET, MarketPrices.class, urlVariables);
    }

    public ForecastDayAheadDTO getDRSignalProfileDA( LocalDateTime time, String dcName){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put(DC_FIELD, dcName);
        return  RestAPIClient.getObject(CatalystDBApiURL.DR_SIGNAL_DAYAHEAD, ForecastDayAheadDTO.class, urlVariables);
    }

    public EnergyProfileDTO getMonitoredConsumptionProfile(LocalDateTime time, String dcName, String granularity){
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("device-name", dcName);

        String id = RestAPIClient.getObject(CatalystDBApiURL.DEVICE_ID, String.class, urlVariables);

        urlVariables = new HashMap<>();
        urlVariables.put(STARTDATE_FIELD, DateUtils.localDateTimeToLongInMillis(time));
        urlVariables.put("dataCenterID", id);
        urlVariables.put(GRANULARITY, granularity.equals("dayahead") ? "24hoursbefore" : "4hoursbefore");
        return  RestAPIClient.getObject(CatalystDBApiURL.MONITORED, EnergyProfileDTO.class, urlVariables);
    }



    public MetricsDTO insertMetric(MetricsDTO metric){
        return RestAPIClient.postObject(CatalystDBApiURL.METRIC, metric, MetricsDTO.class);

    }
}
