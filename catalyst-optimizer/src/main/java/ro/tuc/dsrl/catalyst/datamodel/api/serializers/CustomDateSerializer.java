//package ro.tuc.dsrl.catalyst.datamodel.api.serializers;
//
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JsonSerializer;
//import com.fasterxml.jackson.databind.SerializerProvider;
//
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.TimeZone;
//
///**
// *
// * @author Terpsi Velivassaki <tvelivassaki@ep.singularlogic.eu>
// */
//public class CustomDateSerializer extends JsonSerializer<Date>{
//
//	@Override
//	public void serialize(Date t, JsonGenerator jg, SerializerProvider sp) throws IOException, JsonProcessingException {
//		TimeZone tz = TimeZone.getTimeZone("UTC");
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//		df.setTimeZone(tz);
//		String formatted = df.format(t);
//		formatted = formatted.substring(0, 22) + ":" + formatted.substring(22);
//		jg.writeString(formatted);
//	}
//
//}
