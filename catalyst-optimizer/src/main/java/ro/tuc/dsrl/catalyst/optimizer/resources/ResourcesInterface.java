package ro.tuc.dsrl.catalyst.optimizer.resources;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.util.Date;
import java.util.List;

;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 9, 2015
 * @Description:
 *
 */
public interface ResourcesInterface {

	 List<Battery> getAllBatteries(Date date);
	 List<ThermalEnergyStorage> getAllThermalStorages(Date date);
	 List<ServerRoom> getAllServerRooms(Date date);
	 List<CoolingSystem> getAllCoolingSystems(Date date);
	 DataCentre getDatacentre(Date date);


	@Deprecated
	public Double getDTWMonitoredConsumtion(Interval arrivalTime, Interval recordTime, double divide);

//	@Deprecated
//	public TotalPowerREST getCurrentState(Date date);
//	@Deprecated
//	public void deleteRedundantActions(DateTime startTime, double d);



}
