package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.utility.RandomGenerator;

import java.io.Serializable;
import java.util.Arrays;

public class EnergyEfficiencySolution implements Solution, Serializable {

    private static final long serialVersionUID = 6246429997120798877L;

    private double[] eDc;
    private double[][] y;
    private double[][] yEDE;
    private double[] esd;
    private double[] tes;
    private double[] itLoad;

    /**
     * Auxiliary values
     */

    private double[] heatToBeRemoved;
    private double[] tDc;
    private double[] eDExecuted;
    private double[] itCooling;
    private double[] totalCooling;
    private double[] esdActions;
    private double[] tesActions;
    private double[] itRelocateActions;
    private double[] itHostActions;


    /**
     * Violations for constraints
     */
    private int[] esdViolations;
    private int[] eDViolations;
    private int[] tesViolations;
    private int[] itLoadViolations;

    /**
     * Common Values
     */
    private CommonValues data;

    private int size;

    /**
     * fitness values
     */
    private double fitnessValue = 0;

    private int violations = 0;

    private double energyCost = 0;
    private double thermalProfit = 0;
    private double loadProfit = 0;
    private double flexibilityPenalty = 0;

    /**
     * Constructor
     */
    EnergyEfficiencySolution(int size, CommonValues data) {
        super();
        this.size = size;
        this.data = data;
        this.eDc = new double[size];
        this.tDc = new double[size];
        this.y = new double[size][size];
        this.yEDE = new double[size][size];
        this.esd = new double[size];
        this.tes = new double[size];
        this.itLoad = new double[size];
        this.eDExecuted = new double[size];
        this.esdActions = new double[size];
        this.tesActions = new double[size];
        this.itCooling = new double[size];
        this.totalCooling = new double[size];
        this.itRelocateActions = new double[size];
        this.itHostActions = new double[size];
        this.heatToBeRemoved = new double[size];
        this.esdViolations = new int[size];
        this.eDViolations = new int[size];
        this.tesViolations = new int[size];
        this.itLoadViolations = new int[size];
    }

    private static double[][] fill(double[][] matrix, double n)
    {
        for(int i = 0; i < matrix.length; i++)
            Arrays.fill(matrix[i], n);

        return matrix;
    }

    @Override
    public void initialize() {
        Arrays.fill(esdViolations, 0);
        Arrays.fill(eDViolations, 0);
        Arrays.fill(tesViolations, 0);
        Arrays.fill(itLoadViolations, 0);
        //Arrays.fill(y, 0);
        fill(y, 0.0);
        fill(yEDE, 0.0);
        Arrays.fill(eDc, 0);
        Arrays.fill(itRelocateActions, 0);
        Arrays.fill(itHostActions, 0);


        if (MetaheuristicAlgorithm.isWorkloadShiftEnabled()) {
            initializeRandomShiftY();
            //initializaDoubleY(); //variant Geyser
            for (int j = 0; j < size; ++j) {
                eDExecuted[j] = 0;
                for(int i = 0 ; i < size; ++i)
                    eDExecuted[j] += data.getDelayTolerableEnergy()[j] * y[i][j];
            }
        } else {
            initializeNoShiftY();
            eDExecuted = copyArray(data.getDelayTolerableEnergy());
        }

//        for (int i = 0; i < size; ++i) {
//            itLoad[i] = (data.getRealtimeEnergy()[i] + eDExecuted[i]
//                    + itLoadActions[i] * data.getDelayTolerableEnergy()[i]);
//            itCooling[i] = itLoad[i] / data.getCopC();
//            heatToBeRemoved[i] = itLoad[i];
//            tDc[i] = (data.getCopH() - 1) / data.getCopC() * heatToBeRemoved[i];
//        }

        initializeEsd();

        initializeTes();

        initializeRelocateActions();

       // initializeHostActions();

        recomputeActions();
    }

    private void initializeRelocateActions(){
        for (int i = 1; i < size; ++i) {
            if (MetaheuristicAlgorithm.isWorkloadRelocateEnabled()) {
                itRelocateActions[i] = 2 * (RandomGenerator.randomRound1Decimal(0,1)  - 0.5);
            } else {
                itRelocateActions[i] = 0.0;
            }
        }
    }

    private void initializeHostActions(){
        for (int i = 1; i < size; ++i) {
            if (MetaheuristicAlgorithm.isWorkloadRelocateEnabled()) {
                itHostActions[i] = RandomGenerator.randomRound1Decimal(0,1);
            } else {
                itHostActions[i] = 0.0;
            }
        }
    }
    private void initializeEsd() {
        if (MetaheuristicAlgorithm.ENABLE_ESD) {
            esd[0] = data.getBatteryInitialValue();
            for (int i = 1; i < size - 1; ++i) {
                esd[i] = RandomGenerator.randomRound1Decimal(data.getBatteryMaxCapacity() * data.getDod(),
                        data.getBatteryMaxCapacity());
            }
            esd[size - 1] = data.getBatteryFinalConstraint();
        } else {
            for (int i = 0; i < size; ++i) {
                esd[i] = data.getBatteryInitialValue();
            }
        }
    }

    private void initializeTes() {
        if (MetaheuristicAlgorithm.ENABLE_TES) {
            tes[0] = data.getTesInitialValue();
            for (int i = 1; i < size - 1; ++i) {
                double limitDown = 0.0;
                double limitUp = data.getTesMaxCapacity();
                if (esd[i] > esd[i - 1]) {
                    // charge TES
                    limitDown = tes[i - 1];
                } else {
                    // discharge TES
                    limitUp = Math.min(data.getTesMaxCapacity(), itCooling[i] - 1);
                }

                tes[i] = RandomGenerator.randomRound1Decimal(limitDown, limitUp);
            }
            tes[size - 1] = data.getTesFinalConstraint();
        } else {
            for (int i = 0; i < size; ++i) {
                tes[i] = data.getTesInitialValue();
            }
        }
    }

    private void initializaDoubleY() {
        for (int i = 0; i < size; i++) {
            // move workload to maximum 3 positions
            int[] j = new int[3];
            double[] nrs = new double[3];
            double sumRow = 0.0;
            for (int k = 0; k < 3; k++) {
                j[k] = RandomGenerator.random(i, size);
                nrs[k] = RandomGenerator.randomRound1Decimal(0.0, 1.0);
                sumRow += nrs[k];
            }
            for (int k = 0; k < 3; k++) {
                nrs[k] = nrs[k] / sumRow;
                y[i][j[k]] += nrs[k];
            }
        }
    }

    private void initializaZeroY() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++)
                y[i][j] = 0;
        }
    }

    private void mutateDoubleY() {
        for (int j = 0; j < size; j++) {
            if (itLoadViolations[j] != 0) {
                int line_to_change = 0;
                for (int i = size - 1; i >= 0; i--) {
                    if (y[i][j] >= 0)
                        line_to_change = i;
                }
                int new_j = RandomGenerator.random(line_to_change, size);
                double aux = y[line_to_change][j];
                y[line_to_change][j] = 0;
                y[line_to_change][new_j] += aux;
            }
        }
    }


    private void initializeRandomShiftY() {
        double max;
        for (int i = 0; i < size; ++i) {
            max = 1.0;
            for(int j = i; j < size; ++j) {
                y[i][j] = RandomGenerator.randomRound1Decimal(0, max);
                max -= y[i][j];
            }
            y[i][size-1]+=max;
        }
    }

    private void initializeNoShiftY() {
        for (int i = 0; i < size; ++i) {
            Arrays.fill(y[i], 0);
            y[i][i] = 1.0;
        }
    }

    @Override
    public Solution crossover(Solution part) {
        EnergyEfficiencySolution solution = (EnergyEfficiencySolution) part;
        // crossover the Y array
        int k = RandomGenerator.random(0, size);
        double auxInt[];
        if (MetaheuristicAlgorithm.isWorkloadShiftEnabled()) {
            for (int i = 0; i < k; ++i) {
                int j = RandomGenerator.random(0, size);
                auxInt = y[j];
                y[j] = solution.getY()[j];
                solution.getY()[j] = auxInt;
            }
        }

        double aux;
        // crossover the host workload array
        if (MetaheuristicAlgorithm.isWorkloadRelocateEnabled()) {
            for (int i = 0; i < k; ++i) {
                aux = itRelocateActions[i];
                itRelocateActions[i] = solution.getItRelocateActions()[i];
                solution.getItRelocateActions()[i] = aux;
            }
//            for (int i = 0; i < k; ++i) {
//                aux = itHostActions[i];
//                itHostActions[i] = solution.getItHostActions()[i];
//                solution.getItHostActions()[i] = aux;
//            }
        }
        // crossover the ESD array
        k = RandomGenerator.random(0, size);
        if (MetaheuristicAlgorithm.ENABLE_ESD) {
            for (int i = 0; i < k; ++i) {
                aux = esd[i];
                esd[i] = solution.getEsd()[i];
                solution.getEsd()[i] = aux;
            }
        }

        // crossover the TES array
        if (MetaheuristicAlgorithm.ENABLE_TES) {
            for (int i = 0; i < k; ++i) {
                aux = tes[i];
                tes[i] = solution.getTes()[i];
                solution.getTes()[i] = aux;
            }
        }
        recomputeActions();
        return this;
    }

    @Override
    public Solution mutate() {
        // choose some battery actions to change
        if (MetaheuristicAlgorithm.ENABLE_ESD) {
            mutateEsd();
        }
        // choose some TES actions to change
        if (MetaheuristicAlgorithm.ENABLE_TES) {
            mutateTes();
        }

        if (MetaheuristicAlgorithm.isWorkloadShiftEnabled()) {
            //mutateDoubleY(); //variant Geyser
            mutateWorkload();
        }

        if (MetaheuristicAlgorithm.isWorkloadRelocateEnabled()) {
            mutateRelocate();
        }
        recomputeActions();
        return this;
    }

    private void mutateRelocate(){
        for (int j = 0; j < size; ++j) {
            if (itLoadViolations[j] != 0 && RandomGenerator.random(0, 2) == 1) {
                itRelocateActions[j] = 2* ( RandomGenerator.randomRound1Decimal(0, 1)  - 0.5);
               // itHostActions[j] = RandomGenerator.randomRound1Decimal(0, 1) ;

            }
        }
    }


    private void mutateEsd() {
        for (int i = 1; i < size - 1; ++i) {
            if (RandomGenerator.random(0, 2) == 0) {
                esd[i] += (RandomGenerator.random(0, 2) == 0 ? 1 : -1)
                        * RandomGenerator.randomRound1Decimal(0, data.getBatteryMaxCapacity() / 2);
            }
            if (esdViolations[i] != 0) {
                esd[i] = RandomGenerator.randomRound1Decimal(data.getBatteryMaxCapacity() * data.getDod(),
                        data.getBatteryMaxCapacity());
            }
        }
    }

    private void mutateTes() {
        for (int i = 1; i < size - 1; ++i) {
            if (RandomGenerator.random(0, 2) == 0) {
                tes[i] += (RandomGenerator.random(0, 2) == 0 ? 1 : -1)
                        * RandomGenerator.randomRound1Decimal(0, data.getTesMaxCapacity() / 2);
            }
            if (tesViolations[i] != 0) {
                tes[i] = RandomGenerator.randomRound1Decimal(0.0, data.getTesMaxCapacity());
            }
        }
    }

    private void mutateWorkload() {
        for (int j = 0; j < size; ++j) {
            if (eDViolations[j] != 0 && RandomGenerator.random(0, 2) == 2) {
                double max = 1.0;
                if (RandomGenerator.random(0, 1) == 0) {
                    for (int i = j; i < size; ++i) {
                        y[j][i] = RandomGenerator.randomRound1Decimal(0,1);
                        max -= y[j][i];
                    }
                    y[j][size - 1] += max;
                }
                else
                {
                    for (int i = size-1; i >= j; --i) {
                        y[j][i] = RandomGenerator.randomRound1Decimal(0,1);
                        max -= y[j][i];
                    }

                    y[j][j] += max;
                }
            }
        }
    }

    @Override
    public void rank() {
        recomputeViolations();
    }

    private void recomputeActions() {
        /*
         * ESD_actions[i] < 0 => discharge ESD_actions[i] > 0 => charge
         */
        recomputeEsdAndTes();

        Arrays.fill(eDExecuted, 0);

        fill(yEDE,0);

        double[] eDaux = new double[size];
        for(int i = 0; i< size; i++){
            eDaux[i] = data.getDelayTolerableEnergy()[i];
        }
        if(MetaheuristicAlgorithm.isWorkloadRelocateEnabled()){
            for(int i = 0; i< size; i++){
                if(itRelocateActions[i] < 0) {
                    eDaux[i] = (1 + itRelocateActions[i]) * data.getDelayTolerableEnergy()[i];
                }
            }
        }
        for (int j = 0; j < size; ++j) {
            if (MetaheuristicAlgorithm.isWorkloadShiftEnabled()) {
                eDExecuted[j] = 0;
                for(int i = 0; i < size; ++i){
                    eDExecuted[j] += y[i][j]  * eDaux[j];
                }
            } else {
                    eDExecuted[j] = eDaux[j];
                    }
            itLoad[j] = (data.getRealtimeEnergy()[j] + eDExecuted[j]) + itRelocateActions[j]*data.getMaxRealloc();
        }

/*
        for (int j = 0; j < size; ++j) {
            if (MetaheuristicAlgorithm.isWorkloadRelocateEnabled()) {
                itLoad[j] = (data.getRealtimeEnergy()[j] + eDExecuted[j]
                        + itLoadActions[j] * data.getDelayTolerableEnergy()[j]);
            } else {
                itLoadActions[j] = 0;
                itLoad[j] = (data.getRealtimeEnergy()[j] + eDExecuted[j]);
            }
        }*/

        for (int i = 0; i < size; ++i) {
            itCooling[i] = itLoad[i] / data.getCopC();
            heatToBeRemoved[i] = itLoad[i];
            tDc[i] = (data.getCopH() - 1) / data.getCopC() * heatToBeRemoved[i];
            totalCooling[i] = itCooling[i] + tesActions[i];
            eDc[i] = itLoad[i] + totalCooling[i] + esdActions[i];
        }
    }

    private void recomputeEsdAndTes() {
        for (int i = 1; i < size; ++i) {
            if (MetaheuristicAlgorithm.ENABLE_ESD) {
                esdActions[i - 1] = esd[i] - esd[i - 1];
            } else {
                esdActions[i - 1] = 0;
            }

            if (MetaheuristicAlgorithm.ENABLE_TES) {
                tesActions[i - 1] = tes[i] - tes[i - 1];
            } else {
                tesActions[i - 1] = 0;
            }
        }
    }

    public EnergyEfficiencySolution newInstance() {
        EnergyEfficiencySolution ret = new EnergyEfficiencySolution(this.size, this.data);
        ret.setFitnessValue(fitnessValue);

        ret.setEDC(copyArray(eDc));
        ret.setY(copyMatrix(y));
        ret.setyEDE(copyMatrix(yEDE));
        ret.setEsd(copyArray(esd));
        ret.setTes(copyArray(tes));
        ret.setItLoad(copyArray(itLoad));

        ret.seteDExecuted(copyArray(eDExecuted));
        ret.setItCooling(copyArray(itCooling));
        ret.setTotalCooling(copyArray(totalCooling));
        ret.setEsdActions(copyArray(esdActions));
        ret.setTesActions(copyArray(tesActions));
        ret.setItRelocateActions(copyArray(itRelocateActions));
        ret.setItHostActions(copyArray(itHostActions));

        ret.setItLoad(copyArray(itLoad));

        ret.setEsdViolations(copyArray(esdViolations));
        ret.setTesViolations(copyArray(tesViolations));
        ret.seteDViolations(copyArray(eDViolations));
        ret.setItLoadViolations(copyArray(itLoadViolations));

        return ret;
    }

    public int compareTo(Solution arg0) {
        EnergyEfficiencySolution solution = (EnergyEfficiencySolution) arg0;
        if (this.violations < solution.getViolations())
            return -1;
        if (this.violations > solution.getViolations())
            return 1;
        return Double.compare(this.fitnessValue, solution.getFitnessValue());
    }

    private int[] copyArray(int[] a) {
        int[] b = new int[a.length];
        System.arraycopy(a, 0, b, 0, a.length);
        return b;
    }

    private double[] copyArray(double[] a) {
        double[] b = new double[a.length];
        System.arraycopy(a, 0, b, 0, a.length);
        return b;
    }

    private double[][] copyMatrix(double[][] a) {
        double[][] b = new double[a.length][a[0].length];
        for(int i = 0; i < a.length; ++i)
            System.arraycopy(a[i], 0, b[i], 0, a[i].length);
        return b;
    }

    private int sumArray(int[] a) {
        return Arrays.stream(a).sum();
    }

    private void recomputeViolations() {
        violations = sumArray(this.getEsdViolations()) + sumArray(this.geteDViolations())
                + sumArray(this.getTesViolations()) + sumArray(this.getItLoadViolations());
    }

    public int getViolations() {
        return violations;
    }

    public void setViolations(int violations) {
        this.violations = violations;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double[] getTes() {
        return tes;
    }

    public void setTes(double[] tES) {
        tes = tES;
    }

    public double[] getEsd() {
        return esd;
    }

    public void setEsd(double[] eSD) {
        esd = eSD;
    }

    public double[] getEDC() {
        return eDc;
    }

    public void setEDC(double[] etr) {
        eDc = etr;
    }

    public double[][] getY() {
        return y;
    }

    public void setY(double[][] y) {
        this.y = y;
    }

    public double[] getEsdActions() {
        return esdActions;
    }

    public void setEsdActions(double[] esdActions) {
        this.esdActions = esdActions;
    }

    public double[] geteDExecuted() {
        return eDExecuted;
    }

    public void seteDExecuted(double[] eDExecuted) {
        this.eDExecuted = eDExecuted;
    }

    public int[] getEsdViolations() {
        return esdViolations;
    }

    public void setEsdViolations(int[] esdViolations) {
        this.esdViolations = esdViolations;
    }

    public int[] geteDViolations() {
        return eDViolations;
    }

    public void seteDViolations(int[] eDViolations) {
        this.eDViolations = eDViolations;
    }

    public double getFitnessValue() {
        return fitnessValue;
    }

    public void setFitnessValue(double fitnessValue) {
        this.fitnessValue = fitnessValue;
    }

    public int[] getTesViolations() {
        return tesViolations;
    }

    public void setTesViolations(int[] tesViolations) {
        this.tesViolations = tesViolations;
    }

    public double[] getTesActions() {
        return tesActions;
    }

    public void setTesActions(double[] tesActions) {
        this.tesActions = tesActions;
    }

    public double[] getItCooling() {
        return itCooling;
    }

    public void setItCooling(double[] itCooling) {
        this.itCooling = itCooling;
    }

    public double[] getTotalCooling() {
        return totalCooling;
    }

    public void setTotalCooling(double[] totalCooling) {
        this.totalCooling = totalCooling;
    }

    public double[] getItLoad() {
        return itLoad;
    }

    public void setItLoad(double[] itLoad) {
        this.itLoad = itLoad;
    }

    public double[] getItRelocateActions() {
        return itRelocateActions;
    }

    public void setItRelocateActions(double[] itRelocateActions) {
        this.itRelocateActions = itRelocateActions;
    }

    public int[] getItLoadViolations() {
        return itLoadViolations;
    }

    public void setItLoadViolations(int[] itLoadViolations) {
        this.itLoadViolations = itLoadViolations;
    }

    public double[] gettDc() {
        return tDc;
    }

    public void settDc(double[] tDc) {
        this.tDc = tDc;
    }

    public double[] getHeatToBeRemoved() {
        return heatToBeRemoved;
    }

    public void setHeatToBeRemoved(double[] heatToBeRemoved) {
        this.heatToBeRemoved = heatToBeRemoved;
    }

    public double getEnergyCost() {
        return energyCost;
    }

    public void setEnergyCost(double e) {
        this.energyCost = e;
    }

    public double getThermalProfit() {
        return thermalProfit;
    }

    public void setThermalProfit(double thermalProfit) {
        this.thermalProfit = thermalProfit;
    }

    public double getLoadProfit() {
        return loadProfit;
    }

    public void setLoadProfit(double loadProfit) {
        this.loadProfit = loadProfit;
    }

    public double getFlexibilityPenalty() {
        return flexibilityPenalty;
    }

    public void setFlexibilityPenalty(double flexibilityPenalty) {
        this.flexibilityPenalty = flexibilityPenalty;
    }
    public double[][] getyEDE() {
        return yEDE;
    }

    public void setyEDE(double[][] yEDE) {
        this.yEDE = yEDE;
    }

   public double[] getItHostActions() {
        return itHostActions;
    }

    public void setItHostActions(double[] itHostActions) {
        this.itHostActions = itHostActions;
    }
}
