package ro.tuc.dsrl.catalyst.optimizer.algorithm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristicHybrid.OptimizationProblem;
import ro.tuc.dsrl.catalyst.optimizer.web.controller.IndexController;

public class GeneticHybridAlgorithm implements OptimizationAlgorithm {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneticHybridAlgorithm.class);

    private OptimizationDataCatalyst data;
    private OptimizationProblem optimizationProblem;

    public GeneticHybridAlgorithm(OptimizationDataCatalyst data) {
        this.data = data;
        optimizationProblem = new OptimizationProblem();
    }

    @Override
    public OptimizationDataCatalyst getOptimizationData() {
        return data;
    }

    @Override
    public void compute(ServiceType serviceType) {
        if(OptimizationAlgorithm.hasPredictions(data)) {
            this.data = this.optimizationProblem.computeOptimalSolution(this.data);
        }else{
            LOGGER.debug("No predictions found!");
        }
    }
}
