package ro.tuc.dsrl.catalyst.optimizer.prediction;

import org.joda.time.DateTime;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 9, 2015
 * @Description:
 *
 */
public interface PredictionInterface {
	PredictedData getDayAheadPredictedData(DateTime startDate, double confidenceLevel, DateTime currentDate);

	PredictedData getIntraDayPredictedData(DateTime startDate, double confidenceLevel, DateTime currentDate);

}
