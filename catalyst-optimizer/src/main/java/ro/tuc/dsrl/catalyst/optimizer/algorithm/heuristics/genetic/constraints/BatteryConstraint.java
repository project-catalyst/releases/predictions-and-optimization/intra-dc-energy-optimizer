package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.constraints;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.EnergyEfficiencySolution;

public class BatteryConstraint implements IOptimizationConstraint<EnergyEfficiencySolution> {
    private double dod;
    private double maxEsd;
    private double maxChargeEsd;
    private double maxDischargeEsd;
    private double esdFin;

    public BatteryConstraint(double dod, double maxEsd, double maxChargeEsd, double maxDischargeEsd, double esdFin) {
        this.dod = dod;
        this.maxEsd = maxEsd;
        this.maxChargeEsd = maxChargeEsd;
        this.maxDischargeEsd = maxDischargeEsd;
        this.esdFin = esdFin;
    }

    @Override
    public void verify(EnergyEfficiencySolution solution) {
        int[] violations = new int[solution.getSize()];
        double sum = 0;

        for (int i = 0; i < solution.getSize(); ++i) {
            violations[i] = 0;

            if (solution.getEsd()[i] < this.dod * this.maxEsd) {
                ++violations[i];
            }

            if (solution.getEsd()[i] > this.maxEsd) {
                ++violations[i];
            }

            if ((solution.getEsdActions()[i] < 0 && solution.getEsdActions()[i] < this.maxDischargeEsd) ||
                    (solution.getEsdActions()[i] > 0 && solution.getEsdActions()[i] > this.maxChargeEsd)) {
                ++violations[i];
            }

            sum += solution.getEsd()[i];
        }

        for (int i = 0; i < solution.getSize() && sum < this.esdFin; ++i) {
            if (solution.getEsdActions()[i] < 0) {
                ++violations[i];
            }
        }

        solution.setEsdViolations(violations);
    }
}
