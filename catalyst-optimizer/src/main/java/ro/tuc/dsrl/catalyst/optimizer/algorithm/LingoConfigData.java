package ro.tuc.dsrl.catalyst.optimizer.algorithm;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.tuc.dsrl.catalyst.optimizer.manager.OptimizerTimerTask;

import java.io.*;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: lingo-service-connection
 * @Since: Dec 17, 2014
 * @Description:
 *
 */
public final class LingoConfigData {

	private static final String TEMPLATE = " ~~~xyzt~~~ ";
	private static final int SUFFIX_LENGTH = 8;

	private static final Logger LOGGER = LoggerFactory.getLogger(LingoConfigData.class);
	private static String value;

	private LingoConfigData() {
	}

	public static String initLingoConfigData(int size) {
		String randomSuffix = RandomStringUtils.randomAlphanumeric(SUFFIX_LENGTH);
		String file = "src/main/resources/lingo/lingo-model-" + randomSuffix + ".LNG";
		value = readFile("src/main/resources/lingo/lingo-model-backup.LNG");
		value = value.replace(TEMPLATE, size + "");
		writeFile(file, value);
		return file;
	}

	public static void deleteLingoFile(String filePath) {
		File file = new File(filePath);
		if (file.delete()) {
			LOGGER.info(file.getName() + " is deleted!");
		} else {
			LOGGER.error("Delete operation has failed.");
		}
	}

	private static String readFile(String fileName) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			LOGGER.error(e.getLocalizedMessage());
			return "";
		}

		StringBuilder bd = new StringBuilder();
		String line;
		try {
			while ((line = br.readLine()) != null) {
				bd.append(line);
				bd.append("\n");
			}

		} catch (IOException e) {
			LOGGER.error(e.getLocalizedMessage());
		}
		try {
			br.close();
		} catch (IOException e) {
			LOGGER.error(e.getLocalizedMessage());
		}
		return bd.toString();
	}

	private static void writeFile(String fileName, String data) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName, "UTF-8");
			writer.println(data);
			writer.close();
			LOGGER.info("Lingo Config fileAlgorithm written.");
		} catch (FileNotFoundException e) {
			LOGGER.error(e.getLocalizedMessage());
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e.getLocalizedMessage());
		}
	}

}
