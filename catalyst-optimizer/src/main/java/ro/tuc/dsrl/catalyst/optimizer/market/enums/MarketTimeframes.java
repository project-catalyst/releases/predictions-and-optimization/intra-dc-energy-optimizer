package ro.tuc.dsrl.catalyst.optimizer.market.enums;

public enum MarketTimeframes {
    DAYAHEAD("day_ahead", "dayahead", 24, 24, 60),
    INTRADAY("intra_day", "intraday", 8, 4, 30);

    private String value;
    private String internalValue;
    private int timeslots;
    private int hours;
    private int granularityInMinutes;

    MarketTimeframes(String value, String internalValue, int timeslots, int hours, int granularityInMinutes)
    {
        this.value = value;
        this.internalValue = internalValue;
        this.timeslots = timeslots;
        this.hours = hours;
        this.granularityInMinutes = granularityInMinutes;
    }

    public String getInternalValue(){return internalValue;}

    public String getValue() {
        return value;
    }

    public int getTimeslots(){
        return  timeslots;
    }

    public int getHours(){
        return  hours;
    }

    public int getGranularityInMinutes() {return granularityInMinutes;}
}
