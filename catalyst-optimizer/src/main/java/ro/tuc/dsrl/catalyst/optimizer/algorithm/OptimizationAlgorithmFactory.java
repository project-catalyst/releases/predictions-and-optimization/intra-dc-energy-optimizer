package ro.tuc.dsrl.catalyst.optimizer.algorithm;


import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.optimizer.market.MarketData;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelValuesIO;
import ro.tuc.dsrl.catalyst.optimizer.planner.ResourceConstraints;
import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictedData;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;

import java.util.Date;
import java.util.List;


public class OptimizationAlgorithmFactory {

	private OptimizationAlgorithmFactory() {
	}

	public static OptimizationAlgorithm create(List<Double> drSignal,
											   FlexibilityStrategyDTO strategies, PredictedData predictedData,
											   MarketData marketData, DCResources resources, ResourceConstraints startConstraint,
											   ResourceConstraints endConstraint, int timeHorizon,
											   AlgorithmType algorithm) {
		OptimizationDataCatalyst data =  OptimizationDataCatalyst.getInstance(drSignal, strategies, predictedData, marketData,
					resources, startConstraint, endConstraint, timeHorizon);
		ExcelValuesIO.writeDataToCSVFile(data, "Before-Optimization"+new Date().getTime() +".csv");

		if (AlgorithmType.GENETIC.equals(algorithm)) {
			return new GeneticAlgorithm(data);
		}
		if (AlgorithmType.GREEDY.equals(algorithm)) {
			return new GreedyAlgorithm(data);
		}

		if (AlgorithmType.FILE.equals(algorithm)) {
			return new FileAlgorithm(data);
		}
		if (AlgorithmType.HYBRID.equals(algorithm)) {
			return new GeneticHybridAlgorithm(data);
		}

		//DEFAULT
		return new GeneticAlgorithm(data);
	}
}
