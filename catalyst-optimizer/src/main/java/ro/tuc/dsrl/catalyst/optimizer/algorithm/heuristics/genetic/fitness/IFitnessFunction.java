package ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.fitness;

import ro.tuc.dsrl.catalyst.optimizer.algorithm.heuristics.genetic.Solution;

public interface IFitnessFunction {
    double compute(Solution solution);
}
