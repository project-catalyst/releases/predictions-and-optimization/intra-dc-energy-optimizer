package ro.tuc.dsrl.geyser.optimizer.config;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: lingo-service-connection
 * @Since: Dec 17, 2014
 * @Description:
 *
 */
public final class DataWriter {

	private DataWriter() {
	}

	public static void writeDataToCSVFile(double[][] values, String fileName) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName, "UTF-8");

			for (int i = 0; i < values.length; i++) {
				for (int j = 0; j < values[i].length; j++) {
					writer.print(values[i][j] + ",");
				}
				writer.println();
			}

			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}