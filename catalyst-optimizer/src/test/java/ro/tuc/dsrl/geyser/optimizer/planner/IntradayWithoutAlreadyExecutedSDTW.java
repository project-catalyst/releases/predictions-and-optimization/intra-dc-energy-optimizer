//package ro.tuc.dsrl.geyser.optimizer.planner;
//
//import static org.junit.Assert.assertTrue;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.joda.time.DateTime;
//import org.joda.time.Interval;
//import org.junit.Before;
//import org.junit.Test;
//
//import ro.tuc.dsrl.geyser.datamodel.actions.BuyEnergy;
//import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
//import ro.tuc.dsrl.geyser.datamodel.actions.SellEnergy;
//import ro.tuc.dsrl.geyser.datamodel.actions.ShiftDelayTolerantWorkload;
//import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
//import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;
//import ro.tuc.dsrl.geyser.datamodel.other.FlexibilityStrategies;
//import ro.tuc.dsrl.geyser.optimizer.algorithm.OptimizationData;
//import ro.tuc.dsrl.geyser.optimizer.config.Environment;
//import ro.tuc.dsrl.geyser.optimizer.config.ResourcesT;
//import ro.tuc.dsrl.geyser.optimizer.resources.DCResources;
//import ro.tuc.dsrl.geyser.optimizer.utility.ExcelIO;
//import ro.tuc.dsrl.geyser.optimizer.utility.ExcelIO.ExcelData;
//import ro.tuc.dsrl.geyser.optimizer.utility.ExcelIO.LingoConfigData;
//
//public class IntradayWithoutAlreadyExecutedSDTW {
//	private IntradayPlanner planner;
//	private FlexibilityStrategies strategies;
//	private static final int IT_MAX_CONSUMPTION = 3503000;
//	private static final int DIESEL_MAC_CAPACITY = 3000;
//	private static final double BATTERY_CHARGE_LOSSRATE = 1.2;
//	private static final double BATTERY_DISCHARGE_LOSSRATE = 0.8;
//	private static final double BATTERY_MAX_DISCHARGE = 1000;
//	private static final double BATTERY_MAX_CAPACITY = 1000;
//
//	private static final double TES_CHARGE_LOSSRATE = 1.1;
//	private static final double TES_DISCHARGE_LOSSRATE = 1;
//	private static final double TES_MAX_DISCHARGE = 1000;
//	private static final double TES_MAX_CAPACITY = 3000;
//	private static final List<Double> DELAYED_PERCENTAGE = new ArrayList<Double>(Collections.nCopies(24, 0.3));
//
//	public static final String TEST_OUTPUT_INTDAY_FILE = "/src/test/resources/ling-output-intraday-t4.xls";
//
//	private static final List<Double> RT_WORKLOAD = Arrays.asList(new Double[] { 378.4305, 838.005, 1062.138, 923.419,
//			836.003, 1262.331, 1215.466, 1808.877, 2015.307, 888.538, 959.098, 1109.766, 1012.144, 1212.295, 863.429,
//			818.328, 969.094, 888.041, 780.22, 806.946, 610.6121, 439.4789, 678.0753, 291.2182 });
//
//	@Before
//	public void setup() {
//		Environment.setTest();
//
//		strategies = new FlexibilityStrategies();
//		strategies.setGreenEnergy(true);
//		strategies.setContractEnergy(true);
//		strategies.setTransactionEnergy(true);
//		strategies.setMarketplaceGreenEnergy(true);
//		strategies.setDieselGenerators(false);
//		strategies.setOperationalOptimization(false);
//
//		ResourcesT.setBatteries(getBatteryResources());
//		ResourcesT.setThermalStorages(getTesResources());
//		ResourcesT.setGeneratorsMaxCapacity(DIESEL_MAC_CAPACITY);
//		ResourcesT.setServersMaxCapacity(IT_MAX_CONSUMPTION);
//
//		DCResources resources = new DCResources(new Date());
//
//		Map<DateTime, ResourceConstraints> constraints = new HashMap<DateTime, ResourceConstraints>();
//		constraints.put(new DateTime(getDateTime(4)), new ResourceConstraints(0, 500));
//		constraints.put(new DateTime(getDateTime(8)), new ResourceConstraints(0, 0));
//
//		ActionPlan actionPlan = new ActionPlan();
//		actionPlan.setConstraints(constraints);
//		// HOUR 0-1
//		Date startTime = getDateTime(0);
//		Date endTime = getDateTime(1);
//		List<EnergyEfficiencyOptimizationAction> actions0 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		Date toTime = getDateTime(1);
//		Date toTimePlusOne = getDateTime(2);
//		actions0.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.2));
//		toTime = getDateTime(2);
//		toTimePlusOne = getDateTime(3);
//		actions0.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.3));
//		toTime = getDateTime(4);
//		toTimePlusOne = getDateTime(5);
//		actions0.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.5));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions0);
//
//		// HOUR 1-2
//		startTime = getDateTime(1);
//		endTime = getDateTime(2);
//		List<EnergyEfficiencyOptimizationAction> actions1 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		toTime = getDateTime(1);
//		toTimePlusOne = getDateTime(2);
//		actions1.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 1));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions1);
//
//		// HOUR 2-3
//		startTime = getDateTime(2);
//		endTime = getDateTime(3);
//		List<EnergyEfficiencyOptimizationAction> actions2 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		toTime = getDateTime(5);
//		toTimePlusOne = getDateTime(6);
//		actions2.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.7));
//		toTime = getDateTime(4);
//		toTimePlusOne = getDateTime(5);
//		actions2.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.3));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions2);
//
//		// HOUR 3-4
//		startTime = getDateTime(3);
//		endTime = getDateTime(4);
//		List<EnergyEfficiencyOptimizationAction> actions3 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		toTime = getDateTime(3);
//		toTimePlusOne = getDateTime(4);
//		actions3.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 1));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions3);
//
//		// HOUR 4-5
//		startTime = getDateTime(4);
//		endTime = getDateTime(5);
//		List<EnergyEfficiencyOptimizationAction> actions4 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		toTime = getDateTime(5);
//		toTimePlusOne = getDateTime(6);
//		actions4.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.2));
//		toTime = getDateTime(6);
//		toTimePlusOne = getDateTime(7);
//		actions4.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.3));
//		toTime = getDateTime(9);
//		toTimePlusOne = getDateTime(10);
//		actions4.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.5));
//		actions4.add(new SellEnergy(0, startTime, endTime, 923.4124421, 200));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions4);
//
//		// HOUR 5-6
//		startTime = getDateTime(5);
//		endTime = getDateTime(6);
//		List<EnergyEfficiencyOptimizationAction> actions5 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		toTime = getDateTime(5);
//		toTimePlusOne = getDateTime(6);
//		actions5.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 1));
//		actions5.add(new SellEnergy(0, startTime, endTime, 676.5129553, 200));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions5);
//
//		// HOUR 6-7
//		startTime = getDateTime(6);
//		endTime = getDateTime(7);
//		List<EnergyEfficiencyOptimizationAction> actions6 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		toTime = getDateTime(6);
//		toTimePlusOne = getDateTime(7);
//		actions6.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.1));
//		toTime = getDateTime(7);
//		toTimePlusOne = getDateTime(8);
//		actions6.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.6));
//		toTime = getDateTime(9);
//		toTimePlusOne = getDateTime(10);
//		actions6.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 0.3));
//		actions6.add(new BuyEnergy(0, startTime, endTime, 216.0643853, 200));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions6);
//
//		// HOUR 7-8
//		startTime = getDateTime(7);
//		endTime = getDateTime(8);
//		List<EnergyEfficiencyOptimizationAction> actions7 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		toTime = getDateTime(7);
//		toTimePlusOne = getDateTime(8);
//		actions7.add(new ShiftDelayTolerantWorkload(0, 20, toTime, toTimePlusOne, 2, startTime, toTime, 1));
//		actions7.add(new SellEnergy(0, startTime, endTime, 1084.160578, 200));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions7);
//
//		planner = new IntradayPlanner(actionPlan, resources);
//		planner.setDelayTolerablePercentage(DELAYED_PERCENTAGE);
//		planner.setRTDayWorkload(RT_WORKLOAD);
//
//		// ===================RESOURCES DATA ================
//
//		Map<Interval, Map<Interval, Double>> dtwMonitored = new HashMap<Interval, Map<Interval, Double>>();
//
//		Date startInt = getDateTime(0);
//		Date endInt = getDateTime(1);
//		Interval arrvalTime1 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(1);
//		endInt = getDateTime(1, 30);
//		Interval recordTime12 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(1, 30);
//		endInt = getDateTime(2);
//		Interval recordTime13 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(2);
//		endInt = getDateTime(2, 30);
//		Interval recordTime14 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(2, 30);
//		endInt = getDateTime(3);
//		Interval recordTime15 = new Interval(new DateTime(startInt), new DateTime(endInt));
//
//		startInt = getDateTime(2);
//		endInt = getDateTime(3);
//		Interval arrvalTime2 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(2);
//		endInt = getDateTime(2, 30);
//		Interval recordTime24 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(2, 30);
//		endInt = getDateTime(3);
//		Interval recordTime25 = new Interval(new DateTime(startInt), new DateTime(endInt));
//
//		Map<Interval, Double> recordedConsumption1 = new HashMap<Interval, Double>();
//		recordedConsumption1.put(recordTime12, 145.8);
//		recordedConsumption1.put(recordTime13, 166.4);
//		recordedConsumption1.put(recordTime14, 218.7);
//		recordedConsumption1.put(recordTime15, 249.6);
//
//		Map<Interval, Double> recordedConsumption2 = new HashMap<Interval, Double>();
//		// recordedConsumption2.put(recordTime24, 366.1);
//		// recordedConsumption2.put(recordTime25, 330.4);
//		recordedConsumption2.put(recordTime24, 0.0);
//		recordedConsumption2.put(recordTime25, 0.0);
//
//		dtwMonitored.put(arrvalTime1, recordedConsumption1);
//		dtwMonitored.put(arrvalTime2, recordedConsumption2);
//		ResourcesT.setDTWMonitored(dtwMonitored);
//	}
//
//	private List<ThermalEnergyStorage> getTesResources() {
//		List<ThermalEnergyStorage> tesList = new ArrayList<ThermalEnergyStorage>();
//		ThermalEnergyStorage tes1 = new ThermalEnergyStorage();
//		tes1.setChargeLossRate(TES_CHARGE_LOSSRATE);
//		tes1.setDischargeLossRate(TES_DISCHARGE_LOSSRATE);
//		tes1.setMaximumCapacity(TES_MAX_CAPACITY / 2);
//		tes1.setMaxDischargeRate(TES_MAX_DISCHARGE / 2);
//		ThermalEnergyStorage tes2 = new ThermalEnergyStorage();
//		tes2.setChargeLossRate(TES_CHARGE_LOSSRATE);
//		tes2.setDischargeLossRate(TES_DISCHARGE_LOSSRATE);
//		tes2.setMaximumCapacity(TES_MAX_CAPACITY / 2);
//		tes2.setMaxDischargeRate(TES_MAX_DISCHARGE / 2);
//		tesList.add(tes1);
//		tesList.add(tes2);
//
//		return tesList;
//	}
//
//	private List<Battery> getBatteryResources() {
//		List<Battery> batteriesList = new ArrayList<Battery>();
//		Battery battery1 = new Battery();
//		battery1.setChargeLossRate(BATTERY_CHARGE_LOSSRATE);
//		battery1.setDischargeLossRate(BATTERY_DISCHARGE_LOSSRATE);
//		battery1.setMaximumCapacity(BATTERY_MAX_CAPACITY / 2);
//		battery1.setMaxDischargeRate(BATTERY_MAX_DISCHARGE / 2);
//		Battery battery2 = new Battery();
//		battery2.setChargeLossRate(BATTERY_CHARGE_LOSSRATE);
//		battery2.setDischargeLossRate(BATTERY_DISCHARGE_LOSSRATE);
//		battery2.setMaximumCapacity(BATTERY_MAX_CAPACITY / 2);
//		battery2.setMaxDischargeRate(BATTERY_MAX_DISCHARGE / 2);
//		batteriesList.add(battery1);
//		batteriesList.add(battery2);
//
//		return batteriesList;
//
//	}
//
//	private Date getDateTime(int hour) {
//		return new DateTime(2018, 12, 4, hour, 0).toDate();
//	}
//
//	@Test
//	public void testExecutionMethod() {
//		DateTime date = new DateTime(getDateTime(4));
//		ActionPlan plan = planner.execute(strategies, date, date, 0.0);
//		OptimizationData intradayData = planner.getOptimizationData();
//		assertTrue("", plan.getActionPool() != null);
//		Map<Integer, ExcelData> excelData = ExcelIO.getExcelData(TEST_OUTPUT_INTDAY_FILE, 8);
//		//ExcelIO.writeDataToCSVFile(intradayData, "output-test4.csv");
//
//		LingoConfigData configData = ExcelIO.getConfigData();
//		EvealuateLingoData.evaluateConfigurationData(intradayData, configData);
//		EvealuateLingoData.evaluateLingoData(intradayData, excelData);
//	}
//
//	private Date getDateTime(int hour, int min) {
//		return new DateTime(2018, 12, 4, hour, min).toDate();
//	}
//
//}
