package ro.tuc.dsrl.geyser.optimizer.algorithm;

import org.junit.Test;
import ro.tuc.dsrl.catalyst.optimizer.algorithm.*;
import ro.tuc.dsrl.catalyst.optimizer.utility.DataMapper;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelIO;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelValuesIO;
import ro.tuc.dsrl.catalyst.optimizer.utility.Parser;
import ro.tuc.dsrl.geyser.optimizer.SpringBootTestConfig;
import ro.tuc.dsrl.geyser.optimizer.config.Environment;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class TestGenetic extends SpringBootTestConfig {

    private final String scenario1 = "src/main/resources/scenario-catalyst/Scenario 1/Scenario-PSNC-energy.csv";
    private final String scenario2 = "src/main/resources/scenario-catalyst/Scenario 2/Scenario-PSNC-flex.csv";
    private final String scenario3 = "src/main/resources/scenario-catalyst/Scenario 3/Scenario-PSNC-thermal.csv";
    private final String scenario4 = "src/main/resources/scenario-catalyst/Scenario 4/Scenario-PSNC-all-7.csv";
    private final String scenario5 = "src/main/resources/scenario-catalyst/Scenario 5/Scenario-PSNC-all-5.csv";
    private final String scenario6 = "src/main/resources/scenario-catalyst/Scenario 6/Scenario-PSNC-thermal.csv";

    private final String selfTest1 = "src/main/resources/scenario-catalyst/self-made/Scenario-PSNC-energy1.csv";
    private final String selfTest2 = "src/main/resources/scenario-catalyst/self-made/Scenario-PSNC-energy2.csv";
    private final String selfTest3 = "src/main/resources/scenario-catalyst/self-made/Scenario-PSNC-energy3.csv";

    private final String combined = "src/main/resources/scenario-catalyst/self-made/combined.csv";

//    @Test
//    public void testGenetic() {
//        Environment.setTest();
//        String file = scenario4;
//        Map<String, List<Double>> params = null;
//
//        try {
//            params = Parser.specialParse(file, 0.0);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        assert params != null;
//        for(String key: params.keySet())
//            System.out.println(key + " : " + params.get(key));
//
//        OptimizationDataCatalyst optimizationDataCatalyst = DataMapper.map(params);
//
//        System.out.println(params);
//        optimizationDataCatalyst.setEdPercentage(new double[]{0.3});
//        GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm(optimizationDataCatalyst);
//
//
//        geneticAlgorithm.compute(ServiceType.ANCILLARY_SERVICES);
//        ExcelValuesIO.writeDataToCSVFile(geneticAlgorithm.getOptimizationData(), "Test-Genetic"+new Date().getTime()+".csv");
//        try {
//            System.out.println(Arrays.toString(geneticAlgorithm.getOptimizationData().getItLoad()));
//            System.out.println(Arrays.toString(geneticAlgorithm.getOptimizationData().getRelocateWorkload()));
//
//
//            FileWriter writer = new FileWriter("genetic.txt");
//
//            writer.write("E_Total : " + params.get("E_Total") + "\n");
//            writer.write("Real Time Workload : " + params.get("Real Time Workload") + "\n");
//            writer.write("Delay Tolerant Workload : " + params.get("Delay Tolerant Workload") + "\n");
//            writer.write("Electrical Energy Price : " + (params.get("Electrical Energy Price") != null ? params.get("Electrical Energy Price") : params.get("E_Price") != null ? params.get("E_Price") : params.get("Energy Price")) + "\n");
//            writer.write("L_Price : " + params.get("L_Price") + "\n");
//            writer.write("Cooling System : " + params.get("Cooling System") + "\n");
//            writer.write("Hosted Workload from partner DC : " + params.get("Hosted Workload from partner DC") + "\n");
//            writer.write("Heat Demand : " + params.get("Heat Demand") + "\n");
//            writer.write("T_Price : " + params.get("T_Price") + "\n");
//            writer.write("D_Price : " + params.get("D_Price") + "\n");
//            writer.write("DR_signal : " + params.get("DR_signal") + "\n");
//
//            writer.write("Delay Tolerant Workload : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getDelayTolerableEnergy()) + "\n");
//            //writer.write("True Delay Tolerant Workload : " + params.get("Delay Tolerant Workload") + "\n\n");
//
//            writer.write("Total Cooling : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getFinalCooling()) + "\n");
//            //writer.write("True Total Cooling : " + params.get("totalCooling") + "\n\n");
//
//            writer.write("Cooling System : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getItCooling()) + "\n");
//            //writer.write("True Cooling System : " + params.get("Cooling System") + "\n\n");
//
//            writer.write("TES : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getTesLevel()) + "\n");
//            //writer.write("True TES : " + params.get("TES") + "\n\n");
//
//            writer.write("D_TES : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getDischargeTes()) + "\n");
//            //writer.write("True D_TES : " + params.get("D_TES") + "\n\n");
//
//            writer.write("R_TES : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getChargeTes()) + "\n");
//            //writer.write("True R_TES : " + params.get("R_TES") + "\n\n");
//
//            writer.write("Es : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getBatteryLevel()) + "\n");
//            //writer.write("True Es : " + params.get("Es") + "\n\n");
//
//            writer.write("D_ESD : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getDischargeBattery()) + "\n");
//            //writer.write("True D_ESD : " + params.get("D_ESD") + "\n\n");
//
//            writer.write("R_ESD : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getChargeBattery()) + "\n");
//            //writer.write("True R_ESD : " + params.get("R_ESD") + "\n\n");
//
//            writer.write("DC Demand : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getDcFinalConsumption()) + "\n");
//            //writer.write("True DC Demand : " + params.get("DC Demand") + "\n\n");
//
//            writer.write("DC Thermal Generation : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getDcThermalGeneration()) + "\n");
//            //writer.write("True DC Thermal Generation : " + params.get("DC Thermal Generation") + "\n\n");
//            writer.write("DC Thermal generation without optimization : " + params.get("DC Thermal generation without optimization") + "\n");
//
//            writer.write("Relocated Workload to partner DC : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getRelocateWorkload()) + "\n");
//            //writer.write("True Relocated Workload to partner DC : " + params.get("Relocated Workload to partner DC") + "\n\n");
//
//            writer.write("hostedWorkload : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getHostWorkload()) + "\n");
//            //writer.write("True hostedWorkload : " + params.get("hostedWorkload") + "\n\n");
//
//            writer.write("\n\n");
//
//            writer.write("Energy Profit : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getEnergyCost()) + "\n");
//            //writer.write("True Energy Profit : " + params.get("Energy Profit") + "\n\n");
//
//            writer.write("Thermal Profit : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getThermalProfit()) + "\n");
//            //writer.write("True Thermal Profit : " + params.get("Thermal Profit") + "\n\n");
//
//            writer.write("Flexibility Penalty : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getFlexibilityPenalty()) + "\n");
//            //writer.write("True Flexibility Penalty : " + params.get("Flexibility Penalty") + "\n\n");
//
//            writer.write("Load Profit : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getLoadProfit()) + "\n");
//            //writer.write("True Load Profit : " + params.get("Load Profit") + "\n\n");
//
//            writer.write("Total Profit : " + Arrays.toString(geneticAlgorithm.getOptimizationData().getTotalCost()) + "\n");
//            //writer.write("True Total Profit : " + params.get("Total Profit") + "\n\n");
//
//            writer.write("\n");
//            writer.write("Y :\n");
//
//            for(int i = 0; i < geneticAlgorithm.getOptimizationData().getSize(); i++) {
//                for (int j = 0; j < geneticAlgorithm.getOptimizationData().getSize(); j++)
//                    writer.write(geneticAlgorithm.getOptimizationData().getSchedulingMatrix()[i * 24 + j] + ", ");
//                writer.write("\n");
//            }
//
//            writer.write("\n");
//            /*writer.write("True Y :\n");
//
//            for(int i = 0; i < optimizationDataCatalyst.getSize(); i++) {
//                for (int j = 0; j < optimizationDataCatalyst.getSize(); j++)
//                    writer.write(params.get("Y").get(i * 24 + j) + " ");
//                writer.write("\n");
//            }*/
//
//
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
}
