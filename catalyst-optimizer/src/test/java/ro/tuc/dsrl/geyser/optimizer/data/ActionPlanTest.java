package ro.tuc.dsrl.geyser.optimizer.data;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Before;
import org.junit.Test;
import ro.tuc.dsrl.geyser.datamodel.actions.BuyEnergy;
import ro.tuc.dsrl.geyser.datamodel.actions.ChargeEnergy;
import ro.tuc.dsrl.geyser.datamodel.actions.DischargeEnergy;
import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;
import ro.tuc.dsrl.catalyst.optimizer.planner.ActionPlan;
import ro.tuc.dsrl.catalyst.optimizer.planner.ResourceConstraints;
import ro.tuc.dsrl.geyser.optimizer.SpringBootTestConfig;
import ro.tuc.dsrl.geyser.optimizer.config.Environment;

import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class ActionPlanTest extends SpringBootTestConfig {

	private ActionPlan actionPlan;

	@Before
	public void init() {
		Environment.setTest();
		Map<Interval, List<EnergyEfficiencyOptimizationAction>> actionPool = new HashMap<Interval, List<EnergyEfficiencyOptimizationAction>>();
		Map<DateTime, ResourceConstraints> constraints = new HashMap<DateTime, ResourceConstraints>();
		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();

		BuyEnergy buyAction = new BuyEnergy(1, new Date(), new Date(), 805, 200);
		actions.add(buyAction);
		ChargeEnergy ceAction = new ChargeEnergy(2, 220, new Date(), new Date(), 180, new Battery());
		actions.add(ceAction);
		actionPool.put(new Interval(new DateTime(2015, 1, 2, 0, 0), new DateTime(2015, 1, 2, 1, 0)), actions);

		actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		buyAction = new BuyEnergy(4, new Date(), new Date(), 750, 200);
		actions.add(buyAction);
		DischargeEnergy deAction = new DischargeEnergy(7, 220, new Date(), new Date(), 180, new Battery());
		actions.add(deAction);
		actionPool.put(new Interval(new DateTime(2015, 1, 2, 0, 0), new DateTime(2015, 1, 2, 0, 30)), actions);

		actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		buyAction = new BuyEnergy(4, new Date(), new Date(), 750, 200);
		actions.add(buyAction);
		ceAction = new ChargeEnergy(2, 220, new Date(), new Date(), 180, new Battery());
		actions.add(ceAction);
		deAction = new DischargeEnergy(7, 220, new Date(), new Date(), 180, new ThermalEnergyStorage());
		actions.add(deAction);
		actionPool.put(new Interval(new DateTime(2015, 1, 2, 1, 0), new DateTime(2015, 1, 2, 2, 0)), actions);

		ResourceConstraints resourceConstraint = new ResourceConstraints(1500, 1000);
		constraints.put(new DateTime(2015, 1, 2, 1, 0), resourceConstraint);

		resourceConstraint = new ResourceConstraints(2000, 900);
		constraints.put(new DateTime(2015, 1, 2, 2, 0), resourceConstraint);

		actionPlan = new ActionPlan(158, actionPool, constraints);
	}

	@Test
	public void testGetCurrentActions() {
		List<EnergyEfficiencyOptimizationAction> currentActions = actionPlan.getCurrentActions(new DateTime(2015, 1, 2,
				0, 0));
		assertTrue(currentActions.size() == 4);
		currentActions = actionPlan.getCurrentActions(new DateTime(2015, 1, 2, 0, 25));
		assertTrue(currentActions.size() == 4);
		currentActions = actionPlan.getCurrentActions(new DateTime(2015, 1, 2, 0, 35));
		assertTrue(currentActions.size() == 2);
		currentActions = actionPlan.getCurrentActions(new DateTime(2015, 1, 2, 1, 0));
		assertTrue(currentActions.size() == 3);
		currentActions = actionPlan.getCurrentActions(new DateTime(2015, 1, 2, 1, 59));
		assertTrue(currentActions.size() == 3);
	}

	@Test
	public void testDeleteActions() {
		List<EnergyEfficiencyOptimizationAction> initialActions = actionPlan.getCurrentActions(new DateTime(2015, 1, 2,
				1, 0));
		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		actions.add(initialActions.get(1));
		actions.add(initialActions.get(2));

		actionPlan.deleteActions(new DateTime(2015, 1, 2, 1, 0), actions);

		List<EnergyEfficiencyOptimizationAction> remainingActions = actionPlan.getCurrentActions(new DateTime(2015, 1,
				2, 1, 0));

		assertTrue(remainingActions.size() == initialActions.size() - actions.size());

		for (EnergyEfficiencyOptimizationAction action : remainingActions) {
			assertTrue(action.getId() != initialActions.get(1).getId());
			assertTrue(action.getId() != initialActions.get(2).getId());
		}
	}

	@Test
	public void testMergeActionPlans() {
		Map<Interval, List<EnergyEfficiencyOptimizationAction>> actionPool = new HashMap<Interval, List<EnergyEfficiencyOptimizationAction>>();
		Map<DateTime, ResourceConstraints> constraints = new HashMap<DateTime, ResourceConstraints>();
		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();

		actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		BuyEnergy bseAction = new BuyEnergy(4, new Date(), new Date(), 750, 200);
		actions.add(bseAction);
		ChargeEnergy ceAction = new ChargeEnergy(2, 220, new Date(), new Date(), 180, new Battery());
		actions.add(ceAction);
		DischargeEnergy deAction = new DischargeEnergy(7, 220, new Date(), new Date(), 180, new ThermalEnergyStorage());
		actions.add(deAction);
		actionPool.put(new Interval(new DateTime(2015, 1, 2, 1, 0), new DateTime(2015, 1, 2, 1, 30)), actions);
		ActionPlan secondActionPlan = new ActionPlan(158, actionPool, constraints);

		int actionPoolSizeBeforeMerge = getActionPoolSize(actionPlan);
		actionPlan.mergeActionPlans(secondActionPlan);

		assertTrue(actionPoolSizeBeforeMerge + getActionPoolSize(secondActionPlan) == getActionPoolSize(actionPlan));
	}

	/**
	 * @param plan
	 * @return
	 */
	private int getActionPoolSize(ActionPlan plan) {
		int size = 0;
		Set<Interval> keySet = plan.getActionPool().keySet();
		Iterator<Interval> timeIterator = keySet.iterator();
		while (timeIterator.hasNext()) {
			Interval time = timeIterator.next();
			size += actionPlan.getActionPool().get(time).size();
		}
		return size;
	}

	@Test
	public void testGetResourceConstraintsByDate() {
		ResourceConstraints rc = actionPlan.getResourceConstraintsByDate(new DateTime(2015, 1, 2, 2, 0));
		assertTrue(rc.getUpsLevel() == 900);
		assertTrue(rc.getTesLevel() == 2000);
	}
}
