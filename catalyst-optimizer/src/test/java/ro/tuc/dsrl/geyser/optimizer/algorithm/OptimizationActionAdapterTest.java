package ro.tuc.dsrl.geyser.optimizer.algorithm;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.junit.Before;
import org.junit.Test;
import ro.tuc.dsrl.geyser.optimizer.SpringBootTestConfig;
import ro.tuc.dsrl.geyser.optimizer.config.Environment;
import ro.tuc.dsrl.geyser.optimizer.config.ResourcesT;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 4, 2015
 * @Description:
 *
 */
public class OptimizationActionAdapterTest extends SpringBootTestConfig {
	// shift delay action test
	private double[] schedulingTestMatrix = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.217827624, 0, 0, 0, 0, 0, 0.088519044, 0.693653333, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0.122139208, 0.877860792, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.981339857, 0, 0, 0.018660143,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0.401105064, 0, 0.598894936, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 1 };
	private double[] delayedWorkload = { 162.1845, 359.145, 455.202, 395.751, 358.287, 540.999, 520.914, 775.233,
			863.703, 380.802, 411.042, 475.614, 433.776, 519.555, 370.041, 350.712, 415.326, 380.589, 334.38, 345.834,
			261.6909, 188.3481, 290.6037, 124.8078 };

	// charge / discharge tes actions test
	private double[] D_TES = { 0, 0, 0, 0, 0, 0, 480.9959384, 1000, 0, 0, 0, 210.5295473, 467.6995549, 667.8505549,
			318.9845549, 156.3787026, 424.6495549, 343.5965549, 0, 262.5015549, 0, 0, 133.6308549, 0 };
	private double[] R_TES = { 500, 500, 500, 500, 500, 500, 0, 0, 500, 500, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	private double[] expected_TES = { 0, 499.5, 998.5005, 1497.002, 1995.004, 2492.509, 2989.517, 2506.013, 1504.507,
			2002.502, 2500, 2997, 2783.683982, 2313.668, 1644.172, 1323.862, 1166.316, 740.924, 396.931, 396.534,
			133.898, 133.764, 133.630, 0 };
	private double[] itCooling = { 108.123, 239.43, 322.222222, 263.834, 238.858, 360.666, 355.555555, 547.882268,
			662.167983, 532.79732, 455.019542, 595.019542, 289.184, 455.019542, 246.694, 561.686209, 543.908431,
			582.246505, 222.92, 399.205666, 312.79732, 286.130653, 321.686209, 348.888889 };
	private double[] finalCooling = { 658.123, 700, 872.2222, 793.6309, 483.997, 360.666, 355.5556, 500, 300, 532.7973,
			1005.02, 595.0195, 1000, 1005.02, 1000, 1111.686, 1093.908, 700, 500, 520.3681, 862.7973, 836.1307,
			871.6862, 898.8889 };

	// charge / discharge battery actions test
	private double[] D_ESD = { 0, 0, 0, 0, 0, 0, 0, 424.9808929, 0, 0, 0, 570.2014419, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			400 };
	private double[] R_ESD = { 0, 0, 0, 333.3333333, 17.77948347, 0, 0, 0, 333.3333333, 270.3333333, 5, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0 };
	private double[] expected_UPS = { 500, 497.500001, 495.0125, 492.5374365, 821.741416, 835.323295, 831.1466775,
			826.9909431, 400, 729.6666667, 995, 995, 422.6745653, 420.5611915, 418.4583846, 416.3660916, 414.2842602,
			412.2128379, 410.1517727, 408.1010129, 406.0605068, 404.0302033, 402.0100513, 400 };

	private DateTime startTime;
	private DateTime endTime;
	private Period sampling;
	private Period duration;
	private DCResources resources;

	@Before
	public void initializeTest() {
		Environment.setTest();
		ResourcesT.initializeBatteries();
		ResourcesT.initializeThermalStorages();
		ResourcesT.setGeneratorsMaxCapacity(2000);
		ResourcesT.setServersMaxCapacity(3000000);
		startTime = new DateTime(2015, 2, 3, 0, 0);
		sampling = new Period().withHours(1);
		duration = new Period().withHours(24);
		endTime = startTime.plus(duration);
		resources = new DCResources(new Date());
	}

	@Test
	public void testSplitChargeAction() {
//		OptimizationData optimizationData = OptimizationData.getInstance(new FlexibilityStrategies(),
//				new PredictedData(), resources, new ResourceConstraints(), new ResourceConstraints(), 24);
//		optimizationData.setBatteryLevel(expected_UPS);
//		optimizationData.setChargeBattery(R_ESD);
//		optimizationData.setDischargeBattery(D_ESD);
//		optimizationData.setTesChargeLossRate(new double[] { 1 });
//		optimizationData.setTesDischargeLossRate(new double[] { 1 });
//		optimizationData.setTesLevel(expected_TES);
//		optimizationData.setItColing(itCooling);
//		optimizationData.setFinalCooling(finalCooling);
//		optimizationData.setChargeTes(R_TES);
//		optimizationData.setDischargeTes(D_TES);
//
//		ActionPlan actionPlan = OptimizationActionAdapter.extract(optimizationData, resources, startTime, sampling,
//				duration);
//
//		for (DateTime currentTime = startTime; currentTime.isBefore(endTime); currentTime = currentTime.plus(sampling)) {
//			List<EnergyEfficiencyOptimizationAction> actionList = actionPlan.getActionsByStartDate(currentTime);
//			for (EnergyEfficiencyOptimizationAction action : actionList) {
//				if (action instanceof ChargeBattery) {
//					List<EnergyStorageOptimization> chargeEnergyStorageOptimizations = OptimizationActionAdapter
//							.splitChargeAction((EnergyStorageOptimization) action, resources.getBatteryResource()
//									.getBatteries(), resources.getBatteryResource().getActualLoadedCapacity(),
//									resources.getBatteryResource().getMaximumCapacity());
//					double totalEnergy = 0;
//					for (EnergyStorageOptimization energyStorageOptimization : chargeEnergyStorageOptimizations) {
//						totalEnergy += energyStorageOptimization.getAmountOfEnergy();
//					}
//					System.out.println("Charge battery: " + totalEnergy + " "
//							+ ((EnergyStorageOptimization) action).getAmountOfEnergy());
//					assertTrue(Math.abs(totalEnergy - ((EnergyStorageOptimization) action).getAmountOfEnergy()) <= 1);
//				}
//				if (action instanceof ChargeTes) {
//					List<EnergyStorageOptimization> chargeEnergyStorageOptimizations = OptimizationActionAdapter
//							.splitChargeAction((EnergyStorageOptimization) action, resources.getTesResource()
//									.getThermalStorages(), resources.getTesResource().getActualLoadedCapacity(),
//									resources.getTesResource().getMaximumCapacity());
//					double totalEnergy = 0;
//					for (EnergyStorageOptimization energyStorageOptimization : chargeEnergyStorageOptimizations) {
//						totalEnergy += energyStorageOptimization.getAmountOfEnergy();
//					}
//					System.out.println("Charge tes: " + totalEnergy + " "
//							+ ((EnergyStorageOptimization) action).getAmountOfEnergy());
//					assertTrue(Math.abs(totalEnergy - ((EnergyStorageOptimization) action).getAmountOfEnergy()) <= 1);
//				}
//			}
//		}
	}

	@Test
	public void testSplitDischargeAction() {
//		OptimizationData optimizationData = OptimizationData.getInstance(new FlexibilityStrategies(),
//				new PredictedData(), resources, new ResourceConstraints(), new ResourceConstraints(), 24);
//		optimizationData.setBatteryLevel(expected_UPS);
//		optimizationData.setChargeBattery(R_ESD);
//		optimizationData.setDischargeBattery(D_ESD);
//		optimizationData.setTesChargeLossRate(new double[] { 1 });
//		optimizationData.setTesDischargeLossRate(new double[] { 1 });
//		optimizationData.setTesLevel(expected_TES);
//		optimizationData.setItColing(itCooling);
//		optimizationData.setFinalCooling(finalCooling);
//		optimizationData.setChargeTes(R_TES);
//		optimizationData.setDischargeTes(D_TES);
//
//		ActionPlan actionPlan = OptimizationActionAdapter.extract(optimizationData, resources, startTime, sampling,
//				duration);
//
//		for (DateTime currentTime = startTime; currentTime.isBefore(endTime); currentTime = currentTime.plus(sampling)) {
//			List<EnergyEfficiencyOptimizationAction> actionList = actionPlan.getActionsByStartDate(currentTime);
//			for (EnergyEfficiencyOptimizationAction action : actionList) {
//				if (action instanceof DischargeBattery) {
//					List<EnergyStorageOptimization> chargeEnergyStorageOptimizations = OptimizationActionAdapter
//							.splitChargeAction((EnergyStorageOptimization) action, resources.getBatteryResource()
//									.getBatteries(), resources.getBatteryResource().getActualLoadedCapacity(),
//									resources.getBatteryResource().getMaximumCapacity());
//					double totalEnergy = 0;
//					for (EnergyStorageOptimization energyStorageOptimization : chargeEnergyStorageOptimizations) {
//						totalEnergy += energyStorageOptimization.getAmountOfEnergy();
//					}
//					System.out.println("Discharge battery: " + totalEnergy + " "
//							+ ((EnergyStorageOptimization) action).getAmountOfEnergy());
//					assertTrue(Math.abs(totalEnergy - ((EnergyStorageOptimization) action).getAmountOfEnergy()) <= 1);
//				}
//				if (action instanceof DischargeTes) {
//					List<EnergyStorageOptimization> chargeEnergyStorageOptimizations = OptimizationActionAdapter
//							.splitChargeAction((EnergyStorageOptimization) action, resources.getTesResource()
//									.getThermalStorages(), resources.getTesResource().getActualLoadedCapacity(),
//									resources.getTesResource().getMaximumCapacity());
//					double totalEnergy = 0;
//					for (EnergyStorageOptimization energyStorageOptimization : chargeEnergyStorageOptimizations) {
//						totalEnergy += energyStorageOptimization.getAmountOfEnergy();
//					}
//					System.out.println("Discharge tes: " + totalEnergy + " "
//							+ ((EnergyStorageOptimization) action).getAmountOfEnergy());
//					assertTrue(Math.abs(totalEnergy - ((EnergyStorageOptimization) action).getAmountOfEnergy()) <= 2);
//				}
//			}
//		}
	}

	@Test
	public void testCreateBatteryActions() {
//		int count = 0;
//		OptimizationData optimizationData = OptimizationData.getInstance(new FlexibilityStrategies(),
//				new PredictedData(), resources, new ResourceConstraints(), new ResourceConstraints(), 24);
//		optimizationData.setBatteryLevel(expected_UPS);
//		optimizationData.setChargeBattery(R_ESD);
//		optimizationData.setDischargeBattery(D_ESD);
//		ActionPlan actionPlan = OptimizationActionAdapter.extract(optimizationData, resources, startTime, sampling,
//				duration);
//		for (DateTime currentTime = startTime; currentTime.isBefore(endTime); currentTime = currentTime.plus(sampling)) {
//			List<EnergyEfficiencyOptimizationAction> actionList = actionPlan.getCurrentActions(currentTime);
//			for (EnergyEfficiencyOptimizationAction action : actionList) {
//				assertTrue((action instanceof ChargeBattery && Math.abs(((ChargeBattery) action).getAmountOfEnergy()
//						- R_ESD[count]) <= ConfigT.EPSILON)
//						|| (action instanceof DischargeBattery && Math.abs(((DischargeBattery) action)
//								.getAmountOfEnergy() - D_ESD[count]) <= ConfigT.EPSILON));
//			}
//			count++;
//		}
	}

	@Test
	public void testCreateTesActions() {
//		int count = 0;
//		OptimizationData optimizationData = OptimizationData.getInstance(new FlexibilityStrategies(),
//				new PredictedData(), resources, new ResourceConstraints(), new ResourceConstraints(), 24);
//		optimizationData.setTesChargeLossRate(new double[] { 1 });
//		optimizationData.setTesDischargeLossRate(new double[] { 1 });
//		optimizationData.setTesLevel(expected_TES);
//		optimizationData.setItColing(itCooling);
//		optimizationData.setFinalCooling(finalCooling);
//		optimizationData.setChargeTes(R_TES);
//		optimizationData.setDischargeTes(D_TES);
//		ActionPlan actionPlan = OptimizationActionAdapter.extract(optimizationData, resources, startTime, sampling,
//				duration);
//		for (DateTime currentTime = startTime; currentTime.isBefore(endTime); currentTime = currentTime.plus(sampling)) {
//			List<EnergyEfficiencyOptimizationAction> actionList = actionPlan.getCurrentActions(currentTime);
//			for (EnergyEfficiencyOptimizationAction action : actionList) {
//				if (action instanceof EnergyStorageOptimization) {
//					assertTrue(action.getId() != 0);
//					assertTrue((action instanceof ChargeTes && Math.abs(((ChargeTes) action).getAmountOfEnergy()
//							- R_TES[count]) <= ConfigT.EPSILON)
//							|| (action instanceof DischargeTes && Math.abs(((DischargeTes) action).getAmountOfEnergy()
//									- D_TES[count]) <= ConfigT.EPSILON));
//				} else if (action instanceof DynamicAdjustmentOfCoolingIntensity) {
//					double percentageElectricalCooling = itCooling[count] / finalCooling[count];
//
//					assertTrue(Math.abs(((DynamicAdjustmentOfCoolingIntensity) action).getCoolingIntensityLevelAdjust()
//							- percentageElectricalCooling) <= ConfigT.EPSILON);
//				} else if (action instanceof DynamicallyUsingNonElectricalCooling) {
//					double percentageElectricalCooling = 1 - itCooling[count] / finalCooling[count];
//
//					assertTrue(Math.abs(((DynamicallyUsingNonElectricalCooling) action)
//							.getNonElectricalCoolingIntensity() - percentageElectricalCooling) <= ConfigT.EPSILON);
//				}
//			}
//			count++;
//		}
	}

	@Test
	public void testCreateShiftWorkloadActions() {
//		PredictedData data = new PredictedData();
//		data.setDelayTolerableWorkload(new ArrayList<Double>(Collections.nCopies(24, 0.0)));
//		int count = 0;
//		OptimizationData optimizationData = OptimizationData.getInstance(new FlexibilityStrategies(),
//				data, resources, new ResourceConstraints(), new ResourceConstraints(), 24);
//		optimizationData.setSchedulingMatrix(schedulingTestMatrix);
//		optimizationData.setEstimatedDelayExecution(delayedWorkload);
//
//		ActionPlan actionPlan = OptimizationActionAdapter.extract(optimizationData, resources, startTime, sampling,
//				duration);
//		for (DateTime currentTime = startTime; currentTime.isBefore(endTime); currentTime = currentTime.plus(sampling)) {
//			List<EnergyEfficiencyOptimizationAction> actionList = actionPlan.getCurrentActions(currentTime);
//			double totalPercent = 0;
//			for (EnergyEfficiencyOptimizationAction action : actionList) {
//				assertTrue(action instanceof ShiftDelayTolerantWorkload);
//				ShiftDelayTolerantWorkload shiftAction = (ShiftDelayTolerantWorkload) action;
//				int toHour = new DateTime(shiftAction.getToTime()).getHourOfDay();
//				assertTrue(schedulingTestMatrix[count * duration.getHours() + toHour] == shiftAction
//						.getPercentOfWorkloadDelayed());
//				totalPercent += shiftAction.getPercentOfWorkloadDelayed();
//			}
//			assertTrue(1 - totalPercent <= ConfigT.EPSILON);
//			count++;
//		}
	}
}
