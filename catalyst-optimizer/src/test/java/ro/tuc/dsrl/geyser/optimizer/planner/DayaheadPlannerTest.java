//package ro.tuc.dsrl.geyser.optimizer.planner;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//import java.util.Date;
//import java.util.List;
//
//import org.joda.time.DateTime;
//import org.junit.Before;
//import org.junit.Test;
//
//import ro.tuc.dsrl.geyser.datamodel.actions.BuyEnergy;
//import ro.tuc.dsrl.geyser.datamodel.actions.ChargeBattery;
//import ro.tuc.dsrl.geyser.datamodel.actions.ChargeTes;
//import ro.tuc.dsrl.geyser.datamodel.actions.DischargeBattery;
//import ro.tuc.dsrl.geyser.datamodel.actions.DischargeTes;
//import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
//import ro.tuc.dsrl.geyser.datamodel.actions.EnergyStorageOptimization;
//import ro.tuc.dsrl.geyser.datamodel.actions.MarketplaceAction;
//import ro.tuc.dsrl.geyser.datamodel.actions.RunDieselGeneratorsForPeriodicMaintenance;
//import ro.tuc.dsrl.geyser.datamodel.actions.SellEnergy;
//import ro.tuc.dsrl.geyser.datamodel.actions.ShiftDelayTolerantWorkload;
//import ro.tuc.dsrl.geyser.datamodel.other.OptimizationStrategies;
//import ro.tuc.dsrl.geyser.optimizer.algorithm.OptimizationData;
//import ro.tuc.dsrl.geyser.optimizer.config.ConfigT;
//import ro.tuc.dsrl.geyser.optimizer.config.Environment;
//import ro.tuc.dsrl.geyser.optimizer.config.PredictionT;
//import ro.tuc.dsrl.geyser.optimizer.config.ResourcesT;
//import ro.tuc.dsrl.geyser.optimizer.prediction.PredictedData;
//import ro.tuc.dsrl.geyser.optimizer.resources.DCResources;
//import ro.tuc.dsrl.geyser.optimizer.utility.ExcelIO.ExcelData;
//
///**
// * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
// *          Research Laboratory, http://dsrl.coned.utcluj.ro/
// * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
// * @Module: intra-dc-energy-optimizer
// * @Since: Feb 9, 2015
// * @Description:
// *
// */
//public class DayaheadPlannerTest {
//
//	private DayaheadPlanner daPlanner;
//	private DCResources resources;
//	private OptimizationStrategies strategies;
//	private DateTime startTime;
//	private ResourceConstraints startConstraint;
//	private ResourceConstraints endConstraint;
//
//	@Before
//	public void init() {
//		ResourcesT.initializeBatteries();
//		ResourcesT.initializeThermalStorages();
//		ResourcesT.setGeneratorsMaxCapacity(2000);
//		ResourcesT.setServersMaxCapacity(3000000);
//		Environment.setTest();
//		resources = new DCResources(new Date());
//		startTime = new DateTime(2015, 2, 5, 0, 0);
//		startConstraint = new ResourceConstraints(resources.getTesResource().getActualLoadedCapacity(), resources
//				.getBatteryResource().getActualLoadedCapacity());
//		endConstraint = new ResourceConstraints(0, 0);
//		ActionPlan actionPlan = new ActionPlan();
//		actionPlan.addResourceConstraint(startTime, startConstraint);
//		actionPlan.addResourceConstraint(new DateTime(2015, 2, 6, 0, 0), endConstraint);
//
//		daPlanner = new DayaheadPlanner(actionPlan, resources);
//		strategies = new OptimizationStrategies();
//		strategies.setGreenEnergy(true);
//		strategies.setContractEnergy(true);
//		strategies.setMarketplaceGreenEnergy(true);
//		strategies.setTransactionEnergy(true);
//		strategies.setOperationalOptimization(false);
//		strategies.setDieselGenerators(false);
//
//	}
//
//	@Test
//	public void testExecute() {
//		double[] delayedWorkload = new double[24];
//		double nonitEnergyConsumption, itEnergyConsumption, coolingEnergyConsumption;
//		double energyProduction;
//		double tesLevel = resources.getTesResource().getActualLoadedCapacity();
//		double upsLevel = resources.getBatteryResource().getActualLoadedCapacity();
//		int index;
//		PredictionT predictionTest = new PredictionT();
//		PredictedData predictedData = predictionTest.getDayAheadPredictedData(startTime, 1, null);
//		List<ActionPlan> actionPlans = daPlanner.execute(strategies, startTime, startTime);
//		ActionPlan selectedPlan = actionPlans.get(0);
//		DateTime endTime = startTime.plus(DayaheadPlanner.DURATION);
//		ExcelData testData;
//		for (DateTime currentTime = startTime; currentTime.isBefore(endTime); currentTime = currentTime
//				.plus(DayaheadPlanner.SAMPLING)) {
//			List<EnergyEfficiencyOptimizationAction> actions = selectedPlan.getCurrentActions(currentTime);
//			ResourceConstraints constraint = selectedPlan.getResourceConstraintsByDate(currentTime
//					.plus(DayaheadPlanner.SAMPLING));
//			index = currentTime.getHourOfDay();
//			testData = ConfigT.DAY_AHEAD_TEST_DATA.get(index);
//			itEnergyConsumption = predictedData.getRealtimeWorkload().get(index);
//			nonitEnergyConsumption = 0;
//			energyProduction = predictedData.getRenewableEnergy().get(index)
//					+ predictedData.getContractedEnergy().get(index);
//			for (EnergyEfficiencyOptimizationAction action : actions) {
//				if (action instanceof BuyEnergy) {
//					MarketplaceAction marketplaceAction = (BuyEnergy) action;
//					System.out.println(marketplaceAction.getAmountOfEnergy() + "-" + testData.getEtr());
//					assertTrue(Math.abs(marketplaceAction.getAmountOfEnergy() - testData.getEtr()) <= ConfigT.EPSILON);
//					energyProduction += marketplaceAction.getAmountOfEnergy();
//					assertTrue(Math.abs(marketplaceAction.getPrice() - testData.getPgr()
//							* (1 + OptimizationData.BID_PERCENTAGE)) <= ConfigT.EPSILON);
//				}
//				if (action instanceof SellEnergy) {
//					MarketplaceAction marketplaceAction = (SellEnergy) action;
//					System.out.println(marketplaceAction.getAmountOfEnergy() + "-" + testData.getEtr());
//					assertTrue(Math.abs(marketplaceAction.getAmountOfEnergy() + testData.getEtr()) <= ConfigT.EPSILON);
//					energyProduction -= marketplaceAction.getAmountOfEnergy();
//					assertTrue(Math.abs(marketplaceAction.getPrice() - testData.getPgr()
//							* (1 - OptimizationData.OFFER_PERCENTAGE)) <= ConfigT.EPSILON);
//				}
//				if (action instanceof EnergyStorageOptimization) {
//					{
//						EnergyStorageOptimization esoAction = (EnergyStorageOptimization) action;
//						if (action instanceof ChargeBattery) {
//							nonitEnergyConsumption += esoAction.getAmountOfEnergy();
//							// System.out.println(esoAction.getAmountOfEnergy()
//							// + " ---  " + testData.getR_ESD());
//							assertTrue(Math.abs(esoAction.getAmountOfEnergy() - testData.getChargeBattery()) <= ConfigT.EPSILON);
//							upsLevel += esoAction.getAmountOfEnergy();
//						} else if (action instanceof ChargeTes) {
//							nonitEnergyConsumption += esoAction.getAmountOfEnergy();
//							assertTrue(Math.abs(esoAction.getAmountOfEnergy() - testData.getChargeTES()) <= ConfigT.EPSILON);
//							tesLevel += esoAction.getAmountOfEnergy();
//						} else if (action instanceof DischargeBattery) {
//							energyProduction += esoAction.getAmountOfEnergy();
//							// System.out.println("Battery discharge " +
//							// esoAction.getAmountOfEnergy() + " ---  "
//							// + testData.getD_ESD());
//							assertTrue(Math.abs(esoAction.getAmountOfEnergy() - testData.getDischargeBattery()) <= ConfigT.EPSILON);
//							upsLevel -= esoAction.getAmountOfEnergy();
//						} else if (action instanceof DischargeTes) {
//							energyProduction += esoAction.getAmountOfEnergy();
//							// System.out.println("TES discharge " +
//							// esoAction.getAmountOfEnergy() + " ---  "
//							// + testData.getD_TES());
//							assertTrue(Math.abs(esoAction.getAmountOfEnergy() - testData.getDischargeTES()) <= ConfigT.EPSILON);
//							tesLevel -= esoAction.getAmountOfEnergy();
//						}
//					}
//				}
//				if (action instanceof RunDieselGeneratorsForPeriodicMaintenance) {
//					energyProduction += ((RunDieselGeneratorsForPeriodicMaintenance) action).getAmountOfEnergy();
//				}
//				if (action instanceof ShiftDelayTolerantWorkload) {
//					ShiftDelayTolerantWorkload sdtwAction = (ShiftDelayTolerantWorkload) action;
//					delayedWorkload[new DateTime(sdtwAction.getToTime()).getHourOfDay()] += sdtwAction
//							.getPercentOfWorkloadDelayed() * predictedData.getDelayTolerableWorkload().get(index);
//
//				}
//			}
//			itEnergyConsumption += delayedWorkload[index];
//			coolingEnergyConsumption = itEnergyConsumption / OptimizationData.COP;
//			// System.out.println(coolingEnergyConsumption + " - " +
//			// testData.getTotalCool());
//			assertTrue(Math.abs(coolingEnergyConsumption - testData.getTotalCool()) <= ConfigT.EPSILON);
//			// System.out.println(nonitEnergyConsumption + itEnergyConsumption +
//			// coolingEnergyConsumption
//			// - energyProduction);
//			assertTrue(Math.abs(nonitEnergyConsumption + itEnergyConsumption + coolingEnergyConsumption
//					- energyProduction) <= ConfigT.EPSILON);
//
//			// System.out.println(constraint.getTesLevel() + " -- " + tesLevel);
//			assertEquals(constraint.getTesLevel(), tesLevel, ConfigT.EPSILON);
//			assertEquals(constraint.getUpsLevel(), upsLevel, ConfigT.EPSILON);
//			System.out.println("Hour: " + index + " OK!");
//		}
//	}
//}
