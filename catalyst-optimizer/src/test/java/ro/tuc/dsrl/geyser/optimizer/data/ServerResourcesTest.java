//package ro.tuc.dsrl.geyser.optimizer.data;
//
//import org.junit.Before;
//import org.junit.Test;
//import ro.tuc.dsrl.geyser.optimizer.resources.ServerResources;
//
//import static org.junit.Assert.assertTrue;
//
///**
// * @Author: Technical University of Cluj-Napoca, Romania
// *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
// * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
// * @Module: intra-dc-energy-optimizer-v2
// * @Since: Feb 11, 2015
// * @Description:
// *
// */
//public class ServerResourcesTest {
//
//	private final double itPower = 3510256.0;
//	private static final double CONSTANT = 1000.0;
//	private ServerResources serverResources;
//
//	@Before
//	public void init() {
//		serverResources = new ServerResources();
//		serverResources.setMaxEnergyConsumption(itPower);
//	}
//
//	@Test
//	public void testGetItMaximumConsumptionKW() {
//		assertTrue(itPower / CONSTANT == serverResources.getItMaximumConsumptionKW());
//	}
//
//	@Test
//	public void testGetItMaximumConsumptionW() {
//		assertTrue(itPower == serverResources.getItMaximumConsumptionW());
//	}
//}
