package ro.tuc.dsrl.geyser.optimizer.manager;

import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.optimizer.manager.OptimizerTimerTask;
import ro.tuc.dsrl.geyser.optimizer.config.Environment;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 12, 2015
 * @Description:
 *
 */
public class OptimizerTimerTaskMain {

	private static OptimizerTimerTask optimizerTimerTask;
	private static DateTime currentTime;

	public static void init() {
		Environment.setTest();
		DateTime now = DateTime.now();
		currentTime = new DateTime(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), 19, 50);
		optimizerTimerTask = OptimizerTimerTask.getInstance(currentTime);
	}

	public static void main(String[] args) {
		init();
		ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(1);
		stpe.scheduleAtFixedRate(optimizerTimerTask, 0, 1000, TimeUnit.MILLISECONDS);
		// try {
		// stpe.awaitTermination(15, TimeUnit.SECONDS);
		//
		// // optimizerTimerTask.setSelectedDayAheadPlanId(1);
		// stpe.awaitTermination(20, TimeUnit.SECONDS);
		// // optimizerTimerTask.setSelectedIntraDayPlanId(0);
		// stpe.awaitTermination(7, TimeUnit.MINUTES);
		//
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}
}
