//package ro.tuc.dsrl.geyser.optimizer.manager;
//
//
//import ro.tuc.dsrl.geyser.datamodel.actions.ChargeEnergy;
//import ro.tuc.dsrl.geyser.datamodel.actions.DischargeEnergy;
//import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
//import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
//import ro.tuc.dsrl.geyser.datamodel.other.OptimizerPlan;
//import ro.tuc.dsrl.catalyst.optimizer.remoteconnection.RestAPIClient;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
///**
// * @Author: Technical University of Cluj-Napoca, Romania
// *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
// * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
// * @Module: intra-dc-energy-optimizer
// * @Since: Feb 27, 2015
// * @Description:
// *
// */
//public class PostListMain {
//
//	public static void main(String[] args) {
//		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		ChargeEnergy ce = new ChargeEnergy(1, 200, new Date(), new Date(), 200, new Battery());
//		actions.add(ce);
//		DischargeEnergy de = new DischargeEnergy(1, 200, new Date(), new Date(), 200, new Battery());
//		actions.add(de);
//		OptimizerPlan dw = new OptimizerPlan();
//		dw.setActions(actions);
//		RestAPIClient.postObject(DataModelAPIURL.SAVE_ACTIONS, dw, String.class);
//	}
//}
