package ro.tuc.dsrl.geyser.optimizer.data;

import org.junit.Before;
import org.junit.Test;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;
import ro.tuc.dsrl.geyser.optimizer.SpringBootTestConfig;
import ro.tuc.dsrl.geyser.optimizer.config.Environment;
import ro.tuc.dsrl.geyser.optimizer.config.ResourcesT;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.catalyst.optimizer.resources.ResourceHandler;

import java.util.Date;

import static org.junit.Assert.assertTrue;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class TesResourcesTest extends SpringBootTestConfig {

	private DCResources dcResources;

	@Before
	public void init() {
		Environment.setTest();
		ResourcesT.initializeThermalStorages();
		dcResources = new DCResources(new Date());
	}

	@Test
	public void testGetMaximumCapacity() {
		double maxCapacity = 0;
		for (ThermalEnergyStorage tes : ResourceHandler.getAllThermalStorages(new Date())) {
			maxCapacity += tes.getMaximumCapacity();
		}
		assertTrue(maxCapacity == dcResources.getTesResource().getMaximumCapacity());
	}

	@Test
	public void testGetActualLoadedCapacity() {
		double actualCapacity = 0;
		for (ThermalEnergyStorage tes : ResourceHandler.getAllThermalStorages(new Date())) {
			actualCapacity += tes.getActualLoadedCapacity();
		}
		assertTrue(actualCapacity == dcResources.getTesResource().getActualLoadedCapacity());
	}

	@Test
	public void testGetDischargeLossRate() {
		double dischargeLossRate = 0;
		for (ThermalEnergyStorage tes : ResourceHandler.getAllThermalStorages(new Date())) {
			dischargeLossRate += tes.getDischargeLossRate();
		}
		assertTrue(dischargeLossRate / ResourceHandler.getAllThermalStorages(new Date()).size() == dcResources.getTesResource()
				.getDischargeLossRate());
	}

	@Test
	public void testGetChargeLossRate() {
		double chargeLossRate = 0;
		for (ThermalEnergyStorage tes : ResourceHandler.getAllThermalStorages(new Date())) {
			chargeLossRate += tes.getChargeLossRate();
		}
		assertTrue(chargeLossRate / ResourceHandler.getAllThermalStorages(new Date()).size() == dcResources.getTesResource()
				.getChargeLossRate());
	}

	@Test
	public void testGetMaxDischargeRate() {
		double maxDischargeRate = 0;
		for (ThermalEnergyStorage tes : ResourceHandler.getAllThermalStorages(new Date())) {
			maxDischargeRate += tes.getMaxDischargeRate();
		}
		assertTrue(maxDischargeRate == dcResources.getTesResource().getMaxDischargeRate());
	}

	@Test
	public void testGetMaxChargeRate() {
		double maxChargeRate = 0;
		for (ThermalEnergyStorage tes : ResourceHandler.getAllThermalStorages(new Date())) {
			maxChargeRate += tes.getMaxChargeRate();
		}
		assertTrue(maxChargeRate == dcResources.getTesResource().getMaxChargeRate());
	}
}
