package ro.tuc.dsrl.geyser.optimizer.config;

import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelIO.ExcelData;

import java.util.Map;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 10, 2015
 * @Description:
 *
 */
public class ConfigT {

	public static final double EPSILON = 0.01;
	public static final String TEST_DAYAHEAD_FILE = "/src/test/resources/7d_fb_Optimizer-final.xls";

	public static Map<Integer, ExcelData> DAY_AHEAD_TEST_DATA;
	public static final String TEST_INTDAY_FILE = "/src/test/resources/data-intraday.xls";
	public static Map<Integer, ExcelData> INTRADAY_TEST_DATA;

	static {
	//	DAY_AHEAD_TEST_DATA = ExcelIO.getExcelData(TEST_DAYAHEAD_FILE,24);
	//	INTRADAY_TEST_DATA = ExcelIO.getExcelData(TEST_INTDAY_FILE,8);
	}
}
