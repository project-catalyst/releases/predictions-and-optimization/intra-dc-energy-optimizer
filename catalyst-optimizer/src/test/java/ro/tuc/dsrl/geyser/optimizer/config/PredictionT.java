package ro.tuc.dsrl.geyser.optimizer.config;

import org.joda.time.DateTime;
import ro.tuc.dsrl.catalyst.optimizer.planner.IntradayPlanner;
import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictedData;
import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictionInterface;
import ro.tuc.dsrl.catalyst.optimizer.utility.ExcelIO.ExcelData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 9, 2015
 * @Description:
 *
 */
public class PredictionT implements PredictionInterface {

	private static final List<Integer> DELAY_DAYEAHEAD_DEADLINE = Arrays.asList(new Integer[] { 24, 24, 24, 24, 24, 24,
			24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24 });
	private static final List<Integer> DELAY_INTADAY_DEADLINE = new ArrayList<Integer>(
			Collections.nCopies(IntradayPlanner.INTRA_DAY_SLOTS, IntradayPlanner.INTRA_DAY_SLOTS));

	@Override
	public PredictedData getDayAheadPredictedData(DateTime startDate, double confidenceLevel, DateTime currentDate) {
		PredictedData predictedData = new PredictedData();
		Double[] contractedEnergy = new Double[24];
		Double[] rtWorkload = new Double[24];
		Double[] dtWorkload = new Double[24];
		Double[] energyPrice = new Double[24];
		Double[] marketplaceEnergy = new Double[24];
		Double[] renewableEnergy = new Double[24];
		for (Integer index : ConfigT.DAY_AHEAD_TEST_DATA.keySet()) {
			ExcelData data = ConfigT.DAY_AHEAD_TEST_DATA.get(index);
			contractedEnergy[index] = data.getEContract();
			rtWorkload[index] = data.getRealTimeEnergy();
			dtWorkload[index] = data.getDelayEnergy();
			energyPrice[index] = data.getPgr();
			marketplaceEnergy[index] = data.getMarketEnergy();
			renewableEnergy[index] = data.getGreenEnergy();
		}

		predictedData.setDelayDeadline(DELAY_DAYEAHEAD_DEADLINE);
		predictedData.setDelayTolerableWorkload(Arrays.asList(dtWorkload));
		predictedData.setEnergyFromMarket(Arrays.asList(marketplaceEnergy));
		//predictedData.setEnergyPrice(Arrays.asList(energyPrice));
		predictedData.setRealtimeWorkload(Arrays.asList(rtWorkload));
		predictedData.setRenewableEnergy(Arrays.asList(renewableEnergy));
		predictedData.setDcsHourlyPrices(dummyHostingPrices(24));
		predictedData.setIncomingReallocatedWorkload(new double[24]);
		return predictedData;
	}

	@Override
	public PredictedData getIntraDayPredictedData(DateTime startDate, double confidenceLevel, DateTime currentDate) {
		PredictedData predictedData = new PredictedData();
		Double[] rtWorkload = new Double[8];
		Double[] energyPrice = new Double[8];
		Double[] marketplaceEnergy = new Double[8];
		Double[] renewableEnergy = new Double[8];
		for (Integer index : ConfigT.INTRADAY_TEST_DATA.keySet()) {
			ExcelData data = ConfigT.INTRADAY_TEST_DATA.get(index);
			rtWorkload[index] = data.getRealTimeEnergy();
			energyPrice[index] = data.getPgr();
			marketplaceEnergy[index] = data.getMarketEnergy();
			renewableEnergy[index] = data.getGreenEnergy();
		}
		predictedData.setDelayDeadline(DELAY_INTADAY_DEADLINE);
		predictedData.setEnergyFromMarket(Arrays.asList(marketplaceEnergy));
		//predictedData.setEnergyPrice(Arrays.asList(energyPrice));
		predictedData.setRealtimeWorkload(Arrays.asList(rtWorkload));
		predictedData.setRenewableEnergy(Arrays.asList(renewableEnergy));
		predictedData.setDcsHourlyPrices(dummyHostingPrices(8));
		predictedData.setIncomingReallocatedWorkload(new double[8]);
		return predictedData;
	}


	private static double[][] dummyHostingPrices(int size) {
		// TODO Auto-generated method stub
		double[][] hostingPrices = new double[4][];
		for (int i = 0; i < 4; i++) {
			hostingPrices[i] = new double[size];
			for (int j = 0; j < size; j++) {
				hostingPrices[i][j] = 1.0;
			}
		}
		if (size == 24) {
			hostingPrices[0] = new double[] { 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0,
					2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0 };
			hostingPrices[1] = new double[] { 2.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 0.8, 0.8, 0.8,
					0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 2.0, 2.0, 2.0 };
			hostingPrices[2] = new double[] { 1.0, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0,
					4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0 };
			hostingPrices[3] = new double[] { 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78,
					1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78, 1.78 };
		}
		return hostingPrices;
	}
}
