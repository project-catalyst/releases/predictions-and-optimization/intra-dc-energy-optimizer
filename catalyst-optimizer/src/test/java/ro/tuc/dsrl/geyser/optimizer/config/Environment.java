package ro.tuc.dsrl.geyser.optimizer.config;

import ro.tuc.dsrl.catalyst.optimizer.prediction.PredictionHandler;
import ro.tuc.dsrl.catalyst.optimizer.resources.ResourceHandler;

public class Environment {

	private static final String TEST_CONFIG_FILE = "/src/test/resources/test-configuration-file.config";

	private Environment() {
	}

	public static void setTest() {
		PredictionHandler.setInstance(new PredictionT());
		ResourceHandler.setInstance(new ResourcesT());
	}

}
