//package ro.tuc.dsrl.geyser.optimizer.planner;
//
//import static org.junit.Assert.assertTrue;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.joda.time.DateTime;
//import org.joda.time.Interval;
//import org.junit.Before;
//import org.junit.Test;
//
//import ro.tuc.dsrl.geyser.datamodel.actions.BuyEnergy;
//import ro.tuc.dsrl.geyser.datamodel.actions.ChargeBattery;
//import ro.tuc.dsrl.geyser.datamodel.actions.ChargeTes;
//import ro.tuc.dsrl.geyser.datamodel.actions.DischargeBattery;
//import ro.tuc.dsrl.geyser.datamodel.actions.DischargeEnergy;
//import ro.tuc.dsrl.geyser.datamodel.actions.DischargeTes;
//import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
//import ro.tuc.dsrl.geyser.datamodel.actions.ShiftDelayTolerantWorkload;
//import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
//import ro.tuc.dsrl.geyser.optimizer.config.Environment;
//import ro.tuc.dsrl.geyser.optimizer.config.ResourcesT;
//
//public class IntradayPlannerTest {
//
//	private ActionPlan actionPlan;
//	private IntradayPlanner intradayPlanner;
//	private List<Double> delayedPercentage;
//	private List<Double> predictedRTW;
//	private DateTime startTime;
//
//	@Before
//	public void setup() {
//		Environment.setTest();
//		startTime = new DateTime(getDateTime(4));
//		// =======DELAY & predicted RT Workload=======
//		delayedPercentage = Arrays.asList(0.25, 0.4, 0.3, 0.12, 0.35, 0.34, 0.56, 0.12, 0.23, 0.32, 0.19, 0.28, 0.25,
//				0.26, 0.31, 0.25, 0.4, 0.3, 0.12, 0.35, 0.12, 0.23, 0.32, 0.19);
//		predictedRTW = Arrays.asList(100.0, 110.0, 120.0, 130.0, 140.0, 150.0, 160.0, 170.0);
//
//		Map<DateTime, ResourceConstraints> constraints = new HashMap<DateTime, ResourceConstraints>();
//		constraints.put(new DateTime(getDateTime(4)), new ResourceConstraints(4, 40));
//		constraints.put(new DateTime(getDateTime(5)), new ResourceConstraints(5, 50));
//		constraints.put(new DateTime(getDateTime(6)), new ResourceConstraints(6, 60));
//		constraints.put(new DateTime(getDateTime(7)), new ResourceConstraints(7, 70));
//
//		constraints.put(new DateTime(getDateTime(16)), new ResourceConstraints(16, 160));
//		constraints.put(new DateTime(getDateTime(17)), new ResourceConstraints(17, 170));
//		constraints.put(new DateTime(getDateTime(18)), new ResourceConstraints(18, 180));
//		constraints.put(new DateTime(getDateTime(19)), new ResourceConstraints(19, 190));
//
//		constraints.put(new DateTime(getDateTime(20)), new ResourceConstraints(20, 200));
//		constraints.put(new DateTime(getDateTime(21)), new ResourceConstraints(21, 210));
//		constraints.put(new DateTime(getDateTime(22)), new ResourceConstraints(22, 220));
//		constraints.put(new DateTime(getDateTime(23)), new ResourceConstraints(23, 230));
//
//		actionPlan = new ActionPlan();
//		actionPlan.setConstraints(constraints);
//
//		// HOUR 0-1
//		Date startTime = getDateTime(0);
//		Date endTime = getDateTime(1);
//		List<EnergyEfficiencyOptimizationAction> actions0 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions0.add(new DischargeEnergy(0, 39, startTime, endTime, 100, new Battery()));
//		Date toTime = getDateTime(0);
//		actions0.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.5));
//		toTime = getDateTime(4);
//		actions0.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.2));
//		toTime = getDateTime(5);
//		actions0.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.3));
//
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions0);
//
//		// HOUR 1-2
//		startTime = getDateTime(1);
//		endTime = getDateTime(2);
//		List<EnergyEfficiencyOptimizationAction> actions1 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions1.add(new DischargeEnergy(0, 39, startTime, endTime, 100, new Battery()));
//		toTime = getDateTime(1);
//		actions1.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.2));
//		toTime = getDateTime(3);
//		actions1.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.1));
//		toTime = getDateTime(5);
//		actions1.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.6));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions1);
//
//		// HOUR 2-3
//		startTime = getDateTime(2);
//		endTime = getDateTime(3);
//		List<EnergyEfficiencyOptimizationAction> actions2 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions2.add(new DischargeBattery(0, 39, startTime, endTime, 100));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions2);
//
//		// HOUR 3-4
//		startTime = getDateTime(3);
//		endTime = getDateTime(4);
//		List<EnergyEfficiencyOptimizationAction> actions3 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions3.add(new DischargeEnergy(0, 39, startTime, endTime, 100, new Battery()));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions3);
//
//		// HOUR 4-5
//		startTime = getDateTime(4);
//		endTime = getDateTime(5);
//		List<EnergyEfficiencyOptimizationAction> actions4 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions4.add(new DischargeBattery(1, 39, startTime, endTime, 100));
//		toTime = getDateTime(4);
//		actions4.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.4));
//		toTime = getDateTime(5);
//		actions4.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.4));
//		toTime = getDateTime(9);
//		actions4.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 0.2));
//		actions4.add(new BuyEnergy(0, startTime, endTime, 40, 200));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions4);
//
//		// HOUR 5-6
//		startTime = getDateTime(5);
//		endTime = getDateTime(6);
//		List<EnergyEfficiencyOptimizationAction> actions5 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions5.add(new DischargeTes(2, 39, startTime, endTime, 100));
//		toTime = getDateTime(5);
//		actions5.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 1));
//		actions5.add(new BuyEnergy(0, startTime, endTime, 50, 200));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions5);
//
//		// HOUR 6-7
//		startTime = getDateTime(6);
//		endTime = getDateTime(7);
//		List<EnergyEfficiencyOptimizationAction> actions6 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions6.add(new ChargeBattery(3, 39, startTime, endTime, 100));
//		toTime = getDateTime(6);
//		actions6.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 1));
//		actions6.add(new BuyEnergy(0, startTime, endTime, 60, 200));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions6);
//
//		// HOUR 7-8
//		startTime = getDateTime(7);
//		endTime = getDateTime(8);
//		List<EnergyEfficiencyOptimizationAction> actions7 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions7.add(new ChargeTes(4, 39, startTime, endTime, 100));
//		toTime = getDateTime(7);
//		actions7.add(new ShiftDelayTolerantWorkload(0, 20, startTime, endTime, 2, startTime, toTime, 1));
//		actions7.add(new BuyEnergy(0, startTime, endTime, 70, 200));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions7);
//
//		// HOUR 23-24
//		startTime = getDateTime(23);
//		endTime = getNextDay();
//		List<EnergyEfficiencyOptimizationAction> actions23 = new ArrayList<EnergyEfficiencyOptimizationAction>();
//		actions23.add(new ChargeTes(4, 39, startTime, endTime, 100));
//		actions23.add(new DischargeBattery(4, 39, startTime, endTime, 100));
//		actionPlan.addEntry(new Interval(new DateTime(startTime), new DateTime(endTime)), actions23);
//
//		intradayPlanner = new IntradayPlanner(actionPlan, null);
//		intradayPlanner.setDelayedPercentage(delayedPercentage);
//
//		// ===================RESOURCES DATA ================
//
//		Map<Interval, Map<Interval, Double>> dtwMonitored = new HashMap<Interval, Map<Interval, Double>>();
//
//		Date startInt = getDateTime(0);
//		Date endInt = getDateTime(1);
//		Interval arrvalTime0 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(0);
//		endInt = getDateTime(0, 30);
//		Interval recordTime01 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(0, 30);
//		endInt = getDateTime(1);
//		Interval recordTime01Half = new Interval(new DateTime(startInt), new DateTime(endInt));
//
//		startInt = getDateTime(1);
//		endInt = getDateTime(2);
//		Interval arrvalTime1 = new Interval(new DateTime(startInt), new DateTime(endInt));
//
//		startInt = getDateTime(1);
//		endInt = getDateTime(1, 30);
//		Interval recordTime1 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(1, 30);
//		endInt = getDateTime(2);
//		Interval recordTime1Half = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(3);
//		endInt = getDateTime(3, 30);
//		Interval recordTime2 = new Interval(new DateTime(startInt), new DateTime(endInt));
//		startInt = getDateTime(3, 30);
//		endInt = getDateTime(4);
//		Interval recordTime2Half = new Interval(new DateTime(startInt), new DateTime(endInt));
//
//		Map<Interval, Double> recordedConsumption0 = new HashMap<Interval, Double>();
//		recordedConsumption0.put(recordTime01, 200.0);
//		recordedConsumption0.put(recordTime01Half, 250.0);
//
//		Map<Interval, Double> recordedConsumption1 = new HashMap<Interval, Double>();
//		recordedConsumption1.put(recordTime1, 600.0);
//		recordedConsumption1.put(recordTime1Half, 500.0);
//		recordedConsumption1.put(recordTime2, 400.0);
//		recordedConsumption1.put(recordTime2Half, 450.0);
//
//		dtwMonitored.put(arrvalTime0, recordedConsumption0);
//		dtwMonitored.put(arrvalTime1, recordedConsumption1);
//		ResourcesT.setDTWMonitored(dtwMonitored);
//
//	}
//
//	@Test
//	public void testDelayedFromThePast() {
//		List<Double> values = intradayPlanner.computePastDelayedWorkload(new DateTime(getDateTime(4)));
//		// 80 from T0
//		assertTrue("first half hour 4 ", values.get(0) == 80);
//		// 100 from T0
//		assertTrue("second half hour 4", values.get(1) == 100);
//		// 2000 from T1 + 120 from T0
//		assertTrue("first half hour 5 ", values.get(2) == 2120);
//		// 1900 from T1 + 150 from T0
//		assertTrue("second half hour 5", values.get(3) == 2050);
//	}
//
//	@Test
//	public void testDelayTolerableWrokloadFromPrecentages() {
//		List<Double> values = intradayPlanner.computeDelayTolerableWrokloadFromPrecentages(predictedRTW, new DateTime(
//				getDateTime(4)));
//		assertTrue("0", values.get(0) == delayedPercentage.get(4) * predictedRTW.get(0));
//		assertTrue("1", values.get(1) == delayedPercentage.get(4) * predictedRTW.get(1));
//		assertTrue("2", values.get(2) == delayedPercentage.get(5) * predictedRTW.get(2));
//		assertTrue("3", values.get(3) == delayedPercentage.get(5) * predictedRTW.get(3));
//		assertTrue("4", values.get(4) == delayedPercentage.get(6) * predictedRTW.get(4));
//		assertTrue("5", values.get(5) == delayedPercentage.get(6) * predictedRTW.get(5));
//		assertTrue("6", values.get(6) == delayedPercentage.get(7) * predictedRTW.get(6));
//		assertTrue("7", values.get(7) == delayedPercentage.get(7) * predictedRTW.get(7));
//	}
//
//	@Test
//	public void testSimulateActions() {
//		DateTime startTime = new DateTime(getDateTime(4));
//		List<Double> values = intradayPlanner.computeDelayTolerableWrokloadFromPrecentages(predictedRTW, startTime);
//		List<Double> predictedDTW = intradayPlanner.simulateActions(values, startTime);
//
//		assertTrue("0", predictedDTW.get(0) == 0.4 * values.get(0));
//		assertTrue("1", predictedDTW.get(1) == 0.4 * values.get(1));
//		assertTrue("2", predictedDTW.get(2) == 0.4 * values.get(0) + values.get(2));
//		assertTrue("3", predictedDTW.get(3) == 0.4 * values.get(1) + values.get(3));
//
//		assertTrue("4", predictedDTW.get(4) - values.get(4) == 0);
//		assertTrue("5", predictedDTW.get(5) - values.get(5) == 0);
//		assertTrue("6", predictedDTW.get(6) - values.get(6) == 0);
//		assertTrue("7", predictedDTW.get(7) - values.get(7) == 0);
//	}
//
//	@Test
//	public void getToBeDeletedActions() {
//
//		ActionPlan deletedPool = actionPlan.getDeletedActions(new Interval(startTime, new DateTime(getDateTime(8))));
//		// assertTrue("size: ", actions.size() == 4);
//		// assertTrue("deleted 1", actions.get(0).getId() == 1);
//		// assertTrue("deleted 2", actions.get(1).getId() == 2);
//		// assertTrue("deleted 3", actions.get(2).getId() == 3);
//		// assertTrue("deleted 4", actions.get(3).getId() == 4);
//
//		Interval key = new Interval(new DateTime(startTime), new DateTime(startTime.plusHours(1)));
//		List<EnergyEfficiencyOptimizationAction> poolActions = actionPlan.getActionPool().get(key);
//		List<EnergyEfficiencyOptimizationAction> deletedActions = deletedPool.getActionPool().get(key);
//		assertTrue("deleted 1", deletedActions.get(0).getId() == 1);
//		assertTrue("not contains 1", !poolActions.contains(deletedActions.get(0)));
//
//		key = new Interval(new DateTime(startTime.plusHours(1)), new DateTime(startTime.plusHours(2)));
//		poolActions = actionPlan.getActionPool().get(key);
//		deletedActions = deletedPool.getActionPool().get(key);
//		assertTrue("deleted 2", deletedActions.get(0).getId() == 2);
//		assertTrue("not contains 2", !poolActions.contains(deletedActions.get(0)));
//
//		key = new Interval(new DateTime(startTime.plusHours(2)), new DateTime(startTime.plusHours(3)));
//		poolActions = actionPlan.getActionPool().get(key);
//		deletedActions = deletedPool.getActionPool().get(key);
//		assertTrue("deleted 3", deletedActions.get(0).getId() == 3);
//		assertTrue("not contains 3", !poolActions.contains(deletedActions.get(0)));
//
//		key = new Interval(new DateTime(startTime.plusHours(3)), new DateTime(startTime.plusHours(4)));
//		poolActions = actionPlan.getActionPool().get(key);
//		deletedActions = deletedPool.getActionPool().get(key);
//		assertTrue("deleted 4", deletedActions.get(0).getId() == 4);
//		assertTrue("not contains 4", !poolActions.contains(deletedActions.get(0)));
//
//	}
//
//	@Test
//	public void testContractedEnergy() {
//		List<Double> contractedEnergy = intradayPlanner.computeContractedEnergy(startTime);
//		assertTrue("size: ", contractedEnergy.size() == 8);
//		assertTrue("1", contractedEnergy.get(0) - 40 == 0);
//		assertTrue("2", contractedEnergy.get(1) - 40 == 0);
//		assertTrue("3", contractedEnergy.get(2) - 50 == 0);
//		assertTrue("4", contractedEnergy.get(3) - 50 == 0);
//		assertTrue("5", contractedEnergy.get(4) - 60 == 0);
//		assertTrue("6", contractedEnergy.get(5) - 60 == 0);
//		assertTrue("7", contractedEnergy.get(6) - 70 == 0);
//		assertTrue("8", contractedEnergy.get(7) - 70 == 0);
//
//	}
//
//	@Test
//	public void testREsourceConstraints() {
//		DateTime time = new DateTime(getDateTime(16));
//		ResourceConstraints constraints = actionPlan.getResourceConstraintsByDate(time);
//		assertTrue("16", constraints.getTesLevel() - 16 == 0);
//		assertTrue("16", constraints.getUpsLevel() - 160 == 0);
//
//		time = new DateTime(getDateTime(17));
//		constraints = actionPlan.getResourceConstraintsByDate(time);
//		assertTrue("17", constraints.getTesLevel() - 17 == 0);
//		assertTrue("17", constraints.getUpsLevel() - 170 == 0);
//
//		time = new DateTime(getDateTime(18));
//		constraints = actionPlan.getResourceConstraintsByDate(time);
//		assertTrue("18", constraints.getTesLevel() - 18 == 0);
//		assertTrue("18", constraints.getUpsLevel() - 180 == 0);
//
//		time = new DateTime(getDateTime(19));
//		constraints = actionPlan.getResourceConstraintsByDate(time);
//		assertTrue("19", constraints.getTesLevel() - 19 == 0);
//		assertTrue("19", constraints.getUpsLevel() - 190 == 0);
//
//		time = new DateTime(getDateTime(20));
//		constraints = actionPlan.getResourceConstraintsByDate(time);
//		assertTrue("20", constraints.getTesLevel() - 20 == 0);
//		assertTrue("20", constraints.getUpsLevel() - 200 == 0);
//
//		time = new DateTime(getDateTime(21));
//		constraints = actionPlan.getResourceConstraintsByDate(time);
//		assertTrue("21", constraints.getTesLevel() - 21 == 0);
//		assertTrue("21", constraints.getUpsLevel() - 210 == 0);
//
//		time = new DateTime(getDateTime(22));
//		constraints = actionPlan.getResourceConstraintsByDate(time);
//		assertTrue("22", constraints.getTesLevel() - 22 == 0);
//		assertTrue("22", constraints.getUpsLevel() - 220 == 0);
//
//		time = new DateTime(getDateTime(23));
//		constraints = actionPlan.getResourceConstraintsByDate(time);
//		assertTrue("23", constraints.getTesLevel() - 23 == 0);
//		assertTrue("23", constraints.getUpsLevel() - 230 == 0);
//	}
//
//	private Date getDateTime(int hour) {
//		return new DateTime(2018, 12, 4, hour, 0).toDate();
//	}
//
//	private Date getDateTime(int hour, int min) {
//		return new DateTime(2018, 12, 4, hour, min).toDate();
//	}
//
//	private Date getNextDay() {
//		return new DateTime(2018, 12, 5, 0, 0).toDate();
//	}
//}
