//package ro.tuc.dsrl.geyser.optimizer.planner;
//
//import static org.junit.Assert.assertTrue;
//
//import java.util.Map;
//
//import ro.tuc.dsrl.geyser.optimizer.algorithm.OptimizationData;
//import ro.tuc.dsrl.geyser.optimizer.config.ConfigT;
//import ro.tuc.dsrl.geyser.optimizer.utility.ExcelIO.ExcelData;
//import ro.tuc.dsrl.geyser.optimizer.utility.ExcelIO.LingoConfigData;
//
//public class EvealuateLingoData {
//
//
//	public static void evaluateLingoData(OptimizationData intradayData, Map<Integer, ExcelData> excelData) {
//		for (int i = 0; i < 8; i++) {
//			assertTrue(
//					"diff realtime " + i,
//					(Math.abs(excelData.get(i).getRealTimeEnergy() - intradayData.getRealtimeEnergy()[i]) < ConfigT.EPSILON));
//			assertTrue(
//					"diff delay tolerable " + i,
//					(Math.abs(excelData.get(i).getDelayEnergy() - intradayData.getDelayTolerableEnergy()[i]) < ConfigT.EPSILON));
//			assertTrue(
//					"diff contracted energy" + i,
//					(Math.abs(excelData.get(i).getEContract() - intradayData.getContractedEnergy()[i]) < ConfigT.EPSILON));
//			assertTrue(
//					"diff renewable energy " + i,
//					(Math.abs(excelData.get(i).getGreenEnergy() - intradayData.getRenewableEnergy()[i]) < ConfigT.EPSILON));
//			assertTrue("diff price " + i,
//					(Math.abs(excelData.get(i).getPgr() - intradayData.getEnergyPrice()[i]) < ConfigT.EPSILON));
//			assertTrue(
//					"diff market energy " + i,
//					(Math.abs(excelData.get(i).getMarketEnergy() - intradayData.getMarketEnergy()[i]) < ConfigT.EPSILON));
//			assertTrue(
//					"diff delay deadline " + i,
//					(Math.abs(excelData.get(i).getDelayDeadline() - intradayData.getDelayDeadline()[i]) < ConfigT.EPSILON));
////			assertTrue("diff transactioned energy " + i,
////					(Math.abs(excelData.get(i).getEtr() - intradayData.getTransactionEnergy()[i]) < ConfigT.EPSILON));
//
////			assertTrue("diff tes",
////					(Math.abs(excelData.get(i).getTES() - intradayData.getTesLevel()[i]) < ConfigT.EPSILON));
////			assertTrue(
////					"diff d_tes" + i,
////					(Math.abs(excelData.get(i).getDischargeTES() - intradayData.getDischargeTes()[i]) < ConfigT.EPSILON));
////			assertTrue("diff r_tes" + i,
////					(Math.abs(excelData.get(i).getChargeTES() - intradayData.getChargeTes()[i]) < ConfigT.EPSILON));
////			assertTrue("diff es" + i,
////					(Math.abs(excelData.get(i).getES() - intradayData.getBatteryLevel()[i]) < ConfigT.EPSILON));
////			assertTrue(
////					"diff d_esd" + i,
////					(Math.abs(excelData.get(i).getDischargeBattery() - intradayData.getDischargeBattery()[i]) < ConfigT.EPSILON));
////			assertTrue(
////					"diff r_esd" + i,
////					(Math.abs(excelData.get(i).getChargeBattery() - intradayData.getChargeBattery()[i]) < ConfigT.EPSILON));
////			assertTrue(
////					"diff e_de" + i,
////					(Math.abs(excelData.get(i).getEstimatedDelay() - intradayData.getEstimatedDelayExecution()[i]) < ConfigT.EPSILON));
////			assertTrue("diff totalCool" + i,
////					(Math.abs(excelData.get(i).getTotalCool() - intradayData.getItCooling()[i]) < ConfigT.EPSILON));
////			assertTrue("diff cool" + i,
////					(Math.abs(excelData.get(i).getCOOL() - intradayData.getFinalCooling()[i]) < ConfigT.EPSILON));
//
//			// (E_R+E_D+1.2*R_ESD+finalCooling) - (Econtract +
//			// Egr+Etr+0.8*D_ESD) = 0
//
//			double consumption = intradayData.getRealtimeEnergy()[i] + intradayData.getDelayTolerableEnergy()[i]
//					+ intradayData.getFinalCooling()[i] + intradayData.getBatteryChargeLossRate()[0]
//					* intradayData.getChargeBattery()[i];
//			double production = intradayData.getContractedEnergy()[i] + intradayData.getRenewableEnergy()[i]
//					+ intradayData.getTransactionEnergy()[i] + intradayData.getBatteryDischargeLossRate()[0]
//					* intradayData.getDischargeBattery()[i];
//
//			assertTrue("budget" + i, (Math.abs(consumption - production) < ConfigT.EPSILON));
//
//		}
//	}
//
//	public static void evaluateConfigurationData(OptimizationData intradayData, LingoConfigData configData) {
//		assertTrue(" timePeriod",
//				Math.abs(configData.getTimePeriod() - intradayData.getTimePeriod()[0]) < ConfigT.EPSILON);
////		assertTrue(
////				" transactionEnergyLimit",
////				Math.abs(configData.getTransactionEnergyLimit() - intradayData.getTransactionEnergyLimit()[0]) < ConfigT.EPSILON);
//		assertTrue(" factor", Math.abs(configData.getFactor() - intradayData.getFactor()[0]) < ConfigT.EPSILON);
//		assertTrue(" batteryDamage",
//				Math.abs(configData.getBatteryDamage() - intradayData.getBatteryDamage()[0]) < ConfigT.EPSILON);
//		assertTrue(" transactionFee",
//				Math.abs(configData.getTransactionFee() - intradayData.getTransactionFee()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				" batteryMaxCapacity",
//				Math.abs(configData.getBatteryMaxCapacity() - intradayData.getBatteryMaxCapacity()[0]) < ConfigT.EPSILON);
//		assertTrue(" factorTes", Math.abs(configData.getFactorTes() - intradayData.getFactorTES()[0]) < ConfigT.EPSILON);
//		assertTrue(" cop", Math.abs(configData.getCop() - intradayData.getCop()[0]) < ConfigT.EPSILON);
//		assertTrue(" tesMaxCapacity",
//				Math.abs(configData.getTesMaxCapacity() - intradayData.getTesMaxCapacity()[0]) < ConfigT.EPSILON);
//		assertTrue("tesDamage", Math.abs(configData.getTesDamage() - intradayData.getTesDamage()[0]) < ConfigT.EPSILON);
//		assertTrue(" tesChargeLossRate",
//				Math.abs(configData.getTesChargeLossRate() - intradayData.getTesChargeLossRate()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				" tesDischargeLossRate",
//				Math.abs(configData.getTesDischargeLossRate() - intradayData.getTesDischargeLossRate()[0]) < ConfigT.EPSILON);
//
//		assertTrue(
//				" batteryChargeLossRate",
//				Math.abs(configData.getBatteryChargeLossRate() - intradayData.getBatteryChargeLossRate()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				" batteryDischargeLossRate",
//				Math.abs(configData.getBatteryDischargeLossRate() - intradayData.getBatteryDischargeLossRate()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				" tesMaxDischargeRate",
//				Math.abs(configData.getTesMaxDischargeRate() - intradayData.getTesMaxDischargeRate()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				" batteryMaxDischargeRate",
//				Math.abs(configData.getBatteryMaxDischargeRate() - intradayData.getBatteryMaxDischargeRate()[0]) < ConfigT.EPSILON);
//		assertTrue(" dod", Math.abs(configData.getDod() - intradayData.getDod()[0]) < ConfigT.EPSILON);
//		System.out.println("it max : "+ configData.getItMaxConsumption() +" "+ intradayData.getItMaxConsumption()[0]);
////		assertTrue(" itMaxConsumption",
////				Math.abs(configData.getItMaxConsumption() - intradayData.getItMaxConsumption()[0]) < ConfigT.EPSILON);
//		assertTrue(" edPercentage",
//				Math.abs(configData.getEdPercentage() - intradayData.getEdPercentage()[0]) < ConfigT.EPSILON);
//		assertTrue(" peakCharge",
//				Math.abs(configData.getPeakCharge() - intradayData.getPeakCharge()[0]) < ConfigT.EPSILON);
//		assertTrue(" dieselMaxCapacity",
//				Math.abs(configData.getDieselMaxCapacity() - intradayData.getDieselMaxCapacity()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				"contractEnergyActive",
//				Math.abs(configData.getContractEnergyActive() - intradayData.getContractEnergyActive()[0]) < ConfigT.EPSILON);
//		assertTrue("greenEnergyActive",
//				Math.abs(configData.getGreenEnergyActive() - intradayData.getGreenEnergyActive()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				"transactionEnergyActive",
//				Math.abs(configData.getTransactionEnergyActive() - intradayData.getTransactionEnergyActive()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				"marketplaceGreenEnergyActive",
//				Math.abs(configData.getMarketplaceGreenEnergyActive()
//						- intradayData.getMarketplaceGreenEnergyActive()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				"dieselGeneratorsActive",
//				Math.abs(configData.getDieselGeneratorsActive() - intradayData.getDieselGeneratorsActive()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				"batteryInitialValue",
//				Math.abs(configData.getBatteryInitialValue() - intradayData.getBatteryInitialValue()[0]) < ConfigT.EPSILON);
//		assertTrue(" tesInitialValue",
//				Math.abs(configData.getTesInitialValue() - intradayData.getTesInitialValue()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				" operationalOptimizationActiv",
//				Math.abs(configData.getOperationalOptimizationActiv()
//						- intradayData.getOperationalOptimizationActiv()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				" batteryFinalConstraint",
//				Math.abs(configData.getBatteryFinalConstraint() - intradayData.getBatteryFinalConstraint()[0]) < ConfigT.EPSILON);
//		assertTrue(
//				" tesFinalConstraint",
//				Math.abs(configData.getTesFinalConstraint() - intradayData.getTesFinalConstraint()[0]) < ConfigT.EPSILON);
//	}
//}
