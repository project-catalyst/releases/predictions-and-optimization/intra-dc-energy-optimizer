/*package ro.tuc.dsrl.geyser.optimizer.planner;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Before;
import org.junit.Test;

import ro.tuc.dsrl.geyser.datamodel.actions.BuyEnergy;
import ro.tuc.dsrl.geyser.datamodel.actions.ChargeBattery;
import ro.tuc.dsrl.geyser.datamodel.actions.DischargeBattery;
import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.optimizer.config.Environment;
import ro.tuc.dsrl.geyser.optimizer.config.ResourcesT;
import ro.tuc.dsrl.geyser.optimizer.resources.DCResources;


public class RealtimePlannerTest {

	private ActionPlan activeActionPlan;
	private DCResources resources;
	private DateTime currentTime;

	private RealtimePlanner realtimePlanner;

	@Before
	public void init() {
		ResourcesT.initializeThermalStorages();
		ResourcesT.setGeneratorsMaxCapacity(2000);
		ResourcesT.setServersMaxCapacity(3000000);
		Environment.setTest();
		activeActionPlan = new ActionPlan();
		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		ChargeBattery chargeBattery = new ChargeBattery(0, 400, new DateTime(2015, 2, 5, 0, 30).toDate(), new DateTime(
				2015, 2, 5, 1, 0).toDate(), 400);
		actions.add(chargeBattery);
		Map<DateTime, ResourceConstraints> constraints = new HashMap<DateTime, ResourceConstraints>();
		constraints.put(new DateTime(2015, 2, 5, 0, 30), new ResourceConstraints(0, 800));
		constraints.put(new DateTime(2015, 2, 5, 1, 0), new ResourceConstraints(0, 1000));
		activeActionPlan.putEntry(new Interval(new DateTime(2015, 2, 5, 0, 30), new DateTime(2015, 2, 5, 1, 0)),
				actions);
		activeActionPlan.setConstraints(constraints);

	}

	@Test
	public void testExecuteChargeAction() {
		ResourcesT.initializeBatteries();
		ResourcesT.setItCurrentEnergyConsumption(1000000);
		ResourcesT.setCoolingCurrentEnergyConsumption(500);
		ResourcesT.setWindCurrentEnergyProduction(1520);
		resources = new DCResources(new Date());
		currentTime = new DateTime(2015, 2, 5, 0, 15);
		realtimePlanner = new RealtimePlanner(activeActionPlan, resources);
		ActionPlan realtimeActionPlan = realtimePlanner.execute(currentTime);
		List<EnergyEfficiencyOptimizationAction> actions = realtimeActionPlan.getCurrentActions(currentTime);
		assertTrue(actions.size() == 1);
		EnergyEfficiencyOptimizationAction action = actions.get(0);
		assertTrue(action instanceof ChargeBattery);
		ChargeBattery charge = (ChargeBattery) action;
		assertTrue(charge.getAmountOfEnergy() == 20);
		assertTrue(charge.getEstimatedEnergySaving() == 20);
		assertTrue(charge.getStartTime().equals(currentTime.toDate()));
		assertTrue(charge.getEndTime().equals(currentTime.plusMinutes(5).toDate()));
	}

	@Test
	public void testExecuteDischargeAndBuySellActions() {
		ResourcesT.setBatteries(getTestExecuteDischargeActionBatteries());
		ResourcesT.setItCurrentEnergyConsumption(1000000);
		ResourcesT.setCoolingCurrentEnergyConsumption(500);
		ResourcesT.setWindCurrentEnergyProduction(1000);

		resources = new DCResources(new Date());
		currentTime = new DateTime(2015, 2, 5, 0, 45);
		realtimePlanner = new RealtimePlanner(activeActionPlan, resources);

		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		actions.add(new BuyEnergy(0, new DateTime(2015, 2, 5, 0, 0).toDate(), new DateTime(2015, 2, 5, 1, 0).toDate(),
				500, 200));
		activeActionPlan
				.addEntry(new Interval(new DateTime(2015, 2, 5, 0, 0), new DateTime(2015, 2, 5, 1, 0)), actions);
		ActionPlan realtimeActionPlan = realtimePlanner.execute(currentTime);
		List<EnergyEfficiencyOptimizationAction> currentActions = realtimeActionPlan.getCurrentActions(currentTime);
		assertTrue(currentActions.size() == 1);
		EnergyEfficiencyOptimizationAction action = currentActions.get(0);
		assertTrue(action instanceof DischargeBattery);
		DischargeBattery discharge = (DischargeBattery) action;
		assertTrue(discharge.getAmountOfEnergy() == 240);
		assertTrue(discharge.getEstimatedEnergySaving() == 240);
		assertTrue(discharge.getStartTime().equals(currentTime.toDate()));
		assertTrue(discharge.getEndTime().equals(currentTime.plusMinutes(5).toDate()));
	}

	@Test
	public void testExecuteDischargeAction() {
		ResourcesT.setBatteries(getTestExecuteDischargeActionBatteries());
		ResourcesT.setItCurrentEnergyConsumption(1000000);
		ResourcesT.setCoolingCurrentEnergyConsumption(340);
		ResourcesT.setWindCurrentEnergyProduction(1000);
		resources = new DCResources(new Date());
		currentTime = new DateTime(2015, 2, 5, 0, 25);
		realtimePlanner = new RealtimePlanner(activeActionPlan, resources);

		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		actions.add(new BuyEnergy(0, new DateTime(2015, 2, 5, 0, 0).toDate(), new DateTime(2015, 2, 5, 1, 0).toDate(),
				100, 200));
		activeActionPlan
				.addEntry(new Interval(new DateTime(2015, 2, 5, 0, 0), new DateTime(2015, 2, 5, 1, 0)), actions);
		ActionPlan realtimeActionPlan = realtimePlanner.execute(currentTime);
		List<EnergyEfficiencyOptimizationAction> currentActions = realtimeActionPlan.getCurrentActions(currentTime);
		assertTrue(currentActions.size() == 1);
		EnergyEfficiencyOptimizationAction action = currentActions.get(0);
		assertTrue(action instanceof DischargeBattery);
		DischargeBattery discharge = (DischargeBattery) action;
		assertTrue(discharge.getAmountOfEnergy() == 240);
		assertTrue(discharge.getEstimatedEnergySaving() == 240);
		assertTrue(discharge.getStartTime().equals(currentTime.toDate()));
		assertTrue(discharge.getEndTime().equals(currentTime.plusMinutes(5).toDate()));
	}


	private List<Battery> getTestExecuteDischargeActionBatteries() {
		List<Battery> batteries = new ArrayList<Battery>();
		Battery battery = new Battery();
		battery.setActualLoadedCapacity(120);
		battery.setMaximumCapacity(500);
		battery.setDischargeLossRate(1);
		battery.setChargeLossRate(1);
		battery.setMaxChargeRate(500);
		battery.setMaxDischargeRate(500);
		batteries.add(battery);

		battery = new Battery();
		battery.setActualLoadedCapacity(0);
		battery.setMaximumCapacity(500);
		battery.setDischargeLossRate(1);
		battery.setChargeLossRate(1);
		battery.setMaxChargeRate(500);
		battery.setMaxDischargeRate(500);
		batteries.add(battery);

		battery = new Battery();
		battery.setActualLoadedCapacity(200);
		battery.setMaximumCapacity(500);
		battery.setDischargeLossRate(1);
		battery.setChargeLossRate(1);
		battery.setMaxChargeRate(500);
		battery.setMaxDischargeRate(500);
		batteries.add(battery);

		battery = new Battery();
		battery.setActualLoadedCapacity(500);
		battery.setMaximumCapacity(500);
		battery.setDischargeLossRate(1);
		battery.setChargeLossRate(1);
		battery.setMaxChargeRate(500);
		battery.setMaxDischargeRate(500);
		batteries.add(battery);
		return batteries;
	}

}*/
