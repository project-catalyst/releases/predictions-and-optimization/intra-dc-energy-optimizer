package ro.tuc.dsrl.geyser.optimizer.data;

import org.junit.Before;
import org.junit.Test;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.optimizer.SpringBootTestConfig;
import ro.tuc.dsrl.geyser.optimizer.config.Environment;
import ro.tuc.dsrl.geyser.optimizer.config.ResourcesT;
import ro.tuc.dsrl.catalyst.optimizer.resources.DCResources;
import ro.tuc.dsrl.catalyst.optimizer.resources.ResourceHandler;

import java.util.Date;

import static org.junit.Assert.assertTrue;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class BatteryResourcesTest extends SpringBootTestConfig {

	private DCResources dcResources;

	@Before
	public void init() {
		Environment.setTest();
		ResourcesT.initializeBatteries();
		dcResources = new DCResources(new Date());
	}

	@Test
	public void testGetMaximumCapacity() {
		double maxCapacity = 0;
		for (Battery b : ResourceHandler.getAllBatteries(new Date())) {
			maxCapacity += b.getMaximumCapacity();
		}
		assertTrue(maxCapacity == dcResources.getBatteryResource().getMaximumCapacity());
	}

	@Test
	public void testGetActualLoadedCapacity() {
		double actualCapacity = 0;
		for (Battery b : ResourceHandler.getAllBatteries(new Date())) {
			actualCapacity += b.getActualLoadedCapacity();
		}
		assertTrue(actualCapacity == dcResources.getBatteryResource().getActualLoadedCapacity());
	}

	@Test
	public void testGetDischargeLossRate() {
		double dischargeLossRate = 0;
		for (Battery b : ResourceHandler.getAllBatteries(new Date())) {
			dischargeLossRate += b.getDischargeLossRate();
		}
		assertTrue(dischargeLossRate / ResourceHandler.getAllBatteries(new Date()).size() == dcResources
				.getBatteryResource().getDischargeLossRate());
	}

	@Test
	public void testGetChargeLossRate() {
		double chargeLossRate = 0;
		for (Battery b : ResourceHandler.getAllBatteries(new Date())) {
			chargeLossRate += b.getChargeLossRate();
		}
		assertTrue(chargeLossRate / ResourceHandler.getAllBatteries(new Date()).size() == dcResources
				.getBatteryResource().getChargeLossRate());
	}

	@Test
	public void testGetMaxDischargeRate() {
		double maxDischargeRate = 0;
		for (Battery b : ResourceHandler.getAllBatteries(new Date())) {
			maxDischargeRate += b.getMaxDischargeRate();
		}
		assertTrue(maxDischargeRate == dcResources.getBatteryResource().getMaxDischargeRate());
	}

	@Test
	public void testGetMaxChargeRate() {
		double maxChargeRate = 0;
		for (Battery b : ResourceHandler.getAllBatteries(new Date())) {
			maxChargeRate += b.getMaxChargeRate();
		}
		assertTrue(maxChargeRate == dcResources.getBatteryResource().getMaxChargeRate());
	}
}
