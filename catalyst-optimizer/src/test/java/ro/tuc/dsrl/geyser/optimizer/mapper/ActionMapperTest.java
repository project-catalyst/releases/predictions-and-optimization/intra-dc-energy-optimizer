package ro.tuc.dsrl.geyser.optimizer.mapper;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;
import ro.tuc.dsrl.catalyst.optimizer.mapper.ActionMapper;
import ro.tuc.dsrl.geyser.datamodel.actions.BuyEnergy;
import ro.tuc.dsrl.geyser.datamodel.actions.ChargeEnergy;
import ro.tuc.dsrl.geyser.datamodel.actions.DischargeEnergy;
import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizerPlan;
import ro.tuc.dsrl.catalyst.optimizer.planner.ActionPlan;
import ro.tuc.dsrl.geyser.optimizer.SpringBootTestConfig;
import ro.tuc.dsrl.geyser.optimizer.config.Environment;

import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: intra-dc-energy-optimizer
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class ActionMapperTest extends SpringBootTestConfig {

	@Test
	public void testGetOptimizationDecision() {
		Environment.setTest();
		Map<Interval, List<EnergyEfficiencyOptimizationAction>> actionPool = new HashMap<Interval, List<EnergyEfficiencyOptimizationAction>>();
		List<EnergyEfficiencyOptimizationAction> actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		long totalSize = 0;
		BuyEnergy bseAction = new BuyEnergy(1, new Date(), new Date(), 805, 200);
		actions.add(bseAction);
		ChargeEnergy ceAction = new ChargeEnergy(2, 220, new Date(), new Date(), 180, new Battery());
		actions.add(ceAction);
		totalSize += actions.size();
		actionPool.put(new Interval(new DateTime(2000, 1, 1, 1, 1, 0), new DateTime(2000, 1, 1, 1, 2, 0)), actions);

		actions = new ArrayList<EnergyEfficiencyOptimizationAction>();
		DischargeEnergy deAction = new DischargeEnergy(7, 220, new Date(), new Date(), 180, new Battery());
		actions.add(deAction);
		totalSize += actions.size();
		actionPool.put(new Interval(new DateTime(2000, 1, 1, 1, 1, 30), new DateTime(2000, 1, 1, 1, 2, 0)), actions);

		ActionPlan actionPlan = new ActionPlan(158, actionPool, null);

		OptimizerPlan optimDecisions = ActionMapper.getOptimizationDecisions(actionPlan);

		assertTrue(actionPlan.getId() == optimDecisions.getId());
		assertTrue(optimDecisions.getActions().size() == totalSize);
	}
}
